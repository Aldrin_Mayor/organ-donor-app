import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class InfoWidget extends StatelessWidget {
  final Map<String, dynamic> map;

  const InfoWidget({
    Key? key,
    required this.map,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) => map.isEmpty
      ? Center(child: CircularProgressIndicator())
      : ListView(
          physics: BouncingScrollPhysics(),
          padding: EdgeInsets.all(16),
          children: map.keys.map((key) {
            final value = map[key];

            return Container(
                margin: EdgeInsets.only(top: 260),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      key + '\t$value',
                      style: GoogleFonts.roboto(
                          color: Colors.black87, fontSize: 16),
                    ),
                  ],
                ));
          }).toList(),
        );
}

class InfoWidgetOs extends StatelessWidget {
  final Map<String, dynamic> mapos;

  const InfoWidgetOs({
    Key? key,
    required this.mapos,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) => mapos.isEmpty
      ? Center(child: CircularProgressIndicator())
      : ListView(
          physics: BouncingScrollPhysics(),
          padding: EdgeInsets.all(16),
          children: mapos.keys.map((key) {
            final value = mapos[key];

            return Container(
                margin: EdgeInsets.only(top: 335, right: 30),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    Text(
                      key + '\t$value',
                      style: GoogleFonts.roboto(
                          color: Colors.black45, fontSize: 16),
                    ),
                  ],
                ));
          }).toList(),
        );
}
