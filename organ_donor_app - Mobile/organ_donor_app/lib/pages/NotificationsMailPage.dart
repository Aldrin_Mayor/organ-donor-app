import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:organ_donor_app/pages/DashboardNotificationPage.dart';
import 'package:organ_donor_app/pages/ViewMailPage.dart';

class NotificationsMailPage extends StatefulWidget {
  NotificationsMailPage({Key? key}) : super(key: key);

  @override
  _NotificationsMailPageState createState() => _NotificationsMailPageState();
}

class _NotificationsMailPageState extends State<NotificationsMailPage> {
  final toast = FToast();
  @override
  void initState() {
    super.initState();

    toast.init(context);
  }

  String? currentUserID;
  String? currentUserName;
  String? currentMail;
  String? currentUserType;
  String? currentNotifications;
  String? getSubject;
  String? getNotifications;
  String? getMail;

  //get specific email data
  String? getEmailFrom;
  String? getEmailTo;
  String? getEmailSubject;
  String? getEmailBody;
  String? getEmailDate;
  String? getEmailTime;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Color(0xffe5eef8),
        body: Stack(
          children: [
            //Header
            Container(
              decoration: BoxDecoration(
                color: Color(0xffe5eef8),
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey.withOpacity(0.5),
                    blurRadius: 8,
                  ),
                ],
              ),
              height: 80,
              margin: EdgeInsets.only(top: 0, left: 0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  InkWell(
                    onTap: () {
                      Navigator.pushAndRemoveUntil(
                          context,
                          MaterialPageRoute(
                              builder: (builder) =>
                                  DashboardNotificationPage()),
                          (route) => false);
                    },
                    child: Padding(
                      padding: const EdgeInsets.only(top: 26, left: 20),
                      child: Row(
                        children: [
                          Icon(Icons.close_outlined, size: 25.0),
                          Container(
                            margin: EdgeInsets.only(left: 20),
                            child: Text(
                              "Mails",
                              style: GoogleFonts.roboto(
                                color: Colors.black87,
                                fontWeight: FontWeight.w500,
                                fontSize: 18,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
            //check user ID of donor or patient
            FutureBuilder(
              future: getUserData(),
              builder: (context, snapshot) {
                if (snapshot.connectionState != ConnectionState.done)
                  return Text("");
                if (currentUserID != null) {
                  return Container(
                      height: 20,
                      width: 240,
                      child: Text(
                        '',
                        style: GoogleFonts.roboto(
                            color: Colors.black87,
                            fontSize: 15,
                            fontWeight: FontWeight.w500),
                      ));
                } else {
                  return Text(
                    "",
                    style:
                        GoogleFonts.roboto(color: Colors.black54, fontSize: 14),
                  );
                }
              },
            ),
            //Contents
            Container(
              margin: EdgeInsets.only(top: 60),
              child: FutureBuilder(
                future: getUsersMailData(),
                builder: (context, AsyncSnapshot snapshot) {
                  if (snapshot.hasData) {
                    return ListView.builder(
                      itemCount: snapshot.data!.length,
                      itemBuilder: (context, index) {
                        DocumentSnapshot userData = snapshot.data[index];
                        return Stack(
                          children: [
                            InkWell(
                              onTap: () {
                                getEmailFrom = userData["From"];
                                getEmailTo = userData["To"];
                                getEmailSubject = userData["Subject"];
                                getEmailBody = userData["Body"];
                                getEmailDate = userData["Date"];
                                getEmailTime = userData["Time"];
                                Navigator.of(context).push(MaterialPageRoute(
                                    builder: (context) => ViewMailPage(
                                        getEmailFrom: getEmailFrom,
                                        getEmailTo: getEmailTo,
                                        getEmailSubject: getEmailSubject,
                                        getEmailBody: getEmailBody,
                                        getEmailDate: getEmailDate,
                                        getEmailTime: getEmailTime)));
                              },
                              child: Container(
                                margin: EdgeInsets.only(top: 10),
                                decoration: BoxDecoration(
                                  color: Colors.red[50],
                                ),
                                child: ListTile(
                                  title: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          //Profile Image
                                          Container(
                                            margin: EdgeInsets.only(top: 5),
                                            child: Image.asset(
                                              'assets/logo.png',
                                              height: 60,
                                            ),
                                          ),
                                          //Body
                                          Container(
                                            margin: EdgeInsets.only(
                                                top: 10, left: 10),
                                            child: Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                Container(
                                                  child: Text(
                                                    userData["From"],
                                                    style: GoogleFonts.roboto(
                                                        color: Colors.black87,
                                                        fontWeight:
                                                            FontWeight.w600,
                                                        fontSize: 16,
                                                        letterSpacing: 2),
                                                  ),
                                                ),
                                                Container(
                                                  width: 150,
                                                  margin:
                                                      EdgeInsets.only(top: 5),
                                                  child: Text(
                                                    userData["Subject"],
                                                    style: GoogleFonts.roboto(
                                                        color: Colors.black87,
                                                        fontWeight:
                                                            FontWeight.w600,
                                                        fontSize: 16,
                                                        letterSpacing: 2),
                                                  ),
                                                ),
                                                Container(
                                                  width: 150,
                                                  height: 18,
                                                  margin: EdgeInsets.only(
                                                      top: 5, bottom: 10),
                                                  child: Text(
                                                    userData["Body"],
                                                    style: GoogleFonts.roboto(
                                                        color: Colors.black87,
                                                        fontWeight:
                                                            FontWeight.w300,
                                                        fontSize: 14,
                                                        letterSpacing: 2),
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                          Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.end,
                                            children: [
                                              Container(
                                                margin:
                                                    EdgeInsets.only(top: 10),
                                                child: Text(
                                                  userData["Date"],
                                                  style: GoogleFonts.roboto(
                                                      color: Colors.grey,
                                                      fontWeight:
                                                          FontWeight.w400,
                                                      fontSize: 14,
                                                      letterSpacing: 2),
                                                ),
                                              ),
                                              //delete
                                              Container(
                                                margin:
                                                    EdgeInsets.only(top: 20),
                                                child: InkWell(
                                                  onTap: () {
                                                    getSubject =
                                                        userData["Subject"];
                                                    showCustomDialog(context);
                                                  },
                                                  child: Icon(
                                                    Icons
                                                        .delete_forever_outlined,
                                                    color: Colors.grey,
                                                    size: 25,
                                                  ),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ],
                        );
                      },
                    );
                  } else {
                    return Container(
                      margin: EdgeInsets.only(top: 300),
                      child: Text('No Data'),
                    );
                  }
                },
              ),
            ),
          ],
        ));
  }

//Alert Dialog For Delete
  void showCustomDialog(BuildContext context) => showDialog(
      context: context,
      builder: (BuildContext context) {
        return Dialog(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20.0)), //this right here
          child: Container(
            height: 200,
            width: 100,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  "Delete Mail",
                  style: GoogleFonts.roboto(
                      color: Colors.black87,
                      fontWeight: FontWeight.bold,
                      fontSize: 18),
                ),
                Text(
                  "\nAre you sure you want to delete?",
                  style:
                      GoogleFonts.roboto(color: Colors.grey[500], fontSize: 14),
                ),
                Container(
                  margin: EdgeInsets.only(top: 40),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Container(
                        child: InkWell(
                          onTap: () {
                            Navigator.of(context).pop();
                          },
                          child: Container(
                            height: 35,
                            width: 100,
                            decoration: BoxDecoration(
                                color: Colors.red[50],
                                borderRadius: BorderRadius.circular(18)),
                            child: Center(
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Text(
                                  "Cancel",
                                  style: TextStyle(
                                      color: Colors.grey[600], fontSize: 17),
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                      Container(
                        child: InkWell(
                          onTap: () {
                            setState(() {
                              if (currentUserType == 'I am a Donor') {
                                //Notification Count
                                int x =
                                    int.parse(currentNotifications.toString()) -
                                        1;
                                getNotifications = x.toString();
                                updateDonorNotification();
                                updateAllNotification();
                                //Mail count
                                int y = int.parse(currentMail.toString()) - 1;
                                getMail = y.toString();
                                updateDonorMail();
                                updateAllMail();
                                //Delete
                                deleteEmailDonor();
                                deleteEmailAll();
                                showMessageDeleteToast();
                                Navigator.of(context).pop();
                              } else if (currentUserType == 'I am a Patient') {
                                //Notification Count
                                int x =
                                    int.parse(currentNotifications.toString()) -
                                        1;
                                getNotifications = x.toString();
                                updatePatientNotification();
                                updateAllNotification();
                                //Mail count
                                int y = int.parse(currentMail.toString()) - 1;
                                getMail = y.toString();
                                updatePatientMail();
                                updateAllMail();
                                //Delete
                                deleteEmailPatient();
                                deleteEmailAll();
                                showMessageDeleteToast();
                                Navigator.of(context).pop();
                              }
                            });
                          },
                          child: Container(
                            height: 35,
                            width: 100,
                            decoration: BoxDecoration(
                                color: Colors.red[200],
                                borderRadius: BorderRadius.circular(18)),
                            child: Center(
                              child: Text(
                                "Confirm",
                                style: TextStyle(
                                    color: Colors.red[400], fontSize: 17),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        );
      });

  //Show Delete Success
  void showMessageDeleteToast() => toast.showToast(
      child: buildMessageDeleteToast(), gravity: ToastGravity.CENTER);

  Widget buildMessageDeleteToast() => Container(
        padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
        decoration: BoxDecoration(
          color: Colors.green[300],
          borderRadius: BorderRadius.circular(5),
        ),
        child: Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            Text(
              "Mail Deleted!",
              style: TextStyle(color: Colors.black87, fontSize: 16),
            ),
          ],
        ),
      );

  getUserData() async {
    final currentUser = FirebaseAuth.instance.currentUser;
    if (currentUser != null) {
      await FirebaseFirestore.instance
          .collection('Users')
          .doc("All")
          .collection("Lists")
          .doc(currentUser.uid)
          .get()
          .then((value) {
        currentUserName = value.data()!['UserName'];
        currentUserID = value.data()!['UserID'];
        currentUserType = value.data()!['User Type'];
        currentMail = value.data()!['Mail'];
        currentNotifications = value.data()!['Notifications'];
      }).catchError((e) {
        print(e);
      });
    }
  }

  getUsersMailData() async {
    final currentUser = FirebaseAuth.instance.currentUser;
    final firestore = FirebaseFirestore.instance;
    QuerySnapshot user = await firestore
        .collection('Users')
        .doc('All')
        .collection('Lists')
        .doc(currentUser!.uid)
        .collection('Messages')
        .get();
    return user.docs;
  }

  deleteEmailDonor() async {
    final currentUser = FirebaseAuth.instance.currentUser;
    String? did = getSubject;
    await FirebaseFirestore.instance
        .collection("Users")
        .doc("Donor")
        .collection("Lists")
        .doc(currentUser!.uid)
        .collection('Messages')
        .doc(did)
        .delete();
  }

  deleteEmailPatient() async {
    final currentUser = FirebaseAuth.instance.currentUser;
    String? did = getSubject;
    await FirebaseFirestore.instance
        .collection("Users")
        .doc("Patient")
        .collection("Lists")
        .doc(currentUser!.uid)
        .collection('Messages')
        .doc(did)
        .delete();
  }

  deleteEmailAll() async {
    final currentUser = FirebaseAuth.instance.currentUser;
    String? did = getSubject;
    await FirebaseFirestore.instance
        .collection("Users")
        .doc("All")
        .collection("Lists")
        .doc(currentUser!.uid)
        .collection('Messages')
        .doc(did)
        .delete();
  }

  updateDonorNotification() async {
    String? uid = currentUserID;
    await FirebaseFirestore.instance
        .collection('Users')
        .doc('Donor')
        .collection('Lists')
        .doc(uid)
        .update({
      'Notifications': getNotifications,
    });
  }

  updatePatientNotification() async {
    String? uid = currentUserID;
    await FirebaseFirestore.instance
        .collection('Users')
        .doc('Patient')
        .collection('Lists')
        .doc(uid)
        .update({
      'Notifications': getNotifications,
    });
  }

  updateAllNotification() async {
    String? uid = currentUserID;
    await FirebaseFirestore.instance
        .collection('Users')
        .doc('All')
        .collection('Lists')
        .doc(uid)
        .update({
      'Notifications': getNotifications,
    });
  }

  updateDonorMail() async {
    String? uid = currentUserID;
    await FirebaseFirestore.instance
        .collection('Users')
        .doc('Donor')
        .collection('Lists')
        .doc(uid)
        .update({
      'Mail': getMail,
    });
  }

  updatePatientMail() async {
    String? uid = currentUserID;
    await FirebaseFirestore.instance
        .collection('Users')
        .doc('Patient')
        .collection('Lists')
        .doc(uid)
        .update({
      'Mail': getMail,
    });
  }

  updateAllMail() async {
    String? uid = currentUserID;
    await FirebaseFirestore.instance
        .collection('Users')
        .doc('All')
        .collection('Lists')
        .doc(uid)
        .update({
      'Mail': getMail,
    });
  }
}
