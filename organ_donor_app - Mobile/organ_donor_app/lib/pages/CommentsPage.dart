import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:intl/intl.dart';
import 'package:organ_donor_app/pages/DasboardHomePage.dart';
import 'package:organ_donor_app/pages/api/notification_api.dart';

class CommentsPage extends StatefulWidget {
  final String? getUserName;
  final String? getFullName;
  final String? getUserID;
  final String? getUserType;
  final String? getDate;
  final String? getTime;
  final String? getPostTitle;
  final String? getPostContent;
  final String? getPostViews;
  late final String? getPostComments;
  final String? getPostLikes;

  CommentsPage({
    required this.getUserName,
    required this.getFullName,
    required this.getUserID,
    required this.getUserType,
    required this.getDate,
    required this.getTime,
    required this.getPostTitle,
    required this.getPostContent,
    required this.getPostViews,
    required this.getPostComments,
    required this.getPostLikes,
  });

  @override
  _CommentsPageState createState() => _CommentsPageState();
}

class _CommentsPageState extends State<CommentsPage> {
  String? getUserName;
  String? getFullName;
  String? getUserID;
  String? getUserType;
  String? getDate;
  String? getTime;
  String? getPostTitle;
  String? getPostContent;
  String? getPostViews;
  String? getPostComments;
  String? getPostLikes;

  _CommentsPageState({
    this.getUserName,
    this.getFullName,
    this.getUserID,
    this.getUserType,
    this.getDate,
    this.getTime,
    this.getPostTitle,
    this.getPostContent,
    this.getPostViews,
    this.getPostComments,
    this.getPostLikes,
  });

//For Checking whether donor or patient is current
  String? donorUserType;
  String? donorUserName;
  String? donorUserID;

  String? patientUserType;
  String? patientUserName;
  String? patientUserID;

  String? userName;
  String? usersValue;
  String? userID;

  //For Reply
  TextEditingController _reply = TextEditingController();
  String? reply = "";
  bool isButtonEnabled = false;

  //Time and Date
  var now = new DateTime.now();
  var formatter = new DateFormat('dd-MM-yyyy');

  //Toast Message
  final toast = FToast();
  @override
  void initState() {
    super.initState();
    toast.init(context);
    //NotificationApi.init();
    //listenNotifications();
  }

  //temporary Trash!
  void listenNotifications() =>
      NotificationApi.onNotifications.stream.listen(onClickedNotification);

  void onClickedNotification(String? payload) => Navigator.pushAndRemoveUntil(
      context,
      MaterialPageRoute(builder: (builder) => DashboardHomePage()),
      (route) => false);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: Color(0xffe5eef8),
      body: Stack(
        children: [
          //Content
          SingleChildScrollView(
            child: Column(
              children: [
                //Entire Posts Details
                Container(
                  margin: EdgeInsets.only(top: 80),
                  decoration: BoxDecoration(color: Colors.white),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      //Post Title
                      Container(
                        margin: EdgeInsets.only(top: 15, left: 15, right: 15),
                        child: Text(
                          widget.getPostTitle!,
                          style: GoogleFonts.roboto(
                            color: Colors.black87,
                            fontWeight: FontWeight.w600,
                            fontSize: 22,
                            letterSpacing: 1,
                            height: 1.5,
                          ),
                        ),
                      ),
                      //User Profile
                      Row(
                        children: [
                          //Default Profile Image
                          Container(
                            margin:
                                EdgeInsets.only(top: 20, left: 20, right: 15),
                            child: Image.asset(
                              'assets/user.png',
                              height: 45,
                            ),
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              //User Name
                              Container(
                                margin: EdgeInsets.only(top: 20),
                                child: Text(
                                  widget.getUserName!,
                                  style: GoogleFonts.roboto(
                                    color: Colors.black87,
                                    fontWeight: FontWeight.w600,
                                    fontSize: 14,
                                  ),
                                ),
                              ),
                              //Date and Time
                              Container(
                                margin: EdgeInsets.only(top: 10),
                                child: Row(
                                  children: [
                                    Text(
                                      widget.getDate!,
                                      style: GoogleFonts.roboto(
                                        color: Colors.grey,
                                        fontWeight: FontWeight.w400,
                                        fontSize: 12,
                                      ),
                                    ),
                                    Text(
                                      "\t" + widget.getTime!,
                                      style: GoogleFonts.roboto(
                                        color: Colors.grey,
                                        fontWeight: FontWeight.w400,
                                        fontSize: 12,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          )
                        ],
                      ),
                      //Post Content
                      Container(
                        margin: EdgeInsets.only(top: 40, left: 15, right: 15),
                        child: Text(
                          widget.getPostContent!,
                          style: GoogleFonts.roboto(
                            color: Colors.black87,
                            fontWeight: FontWeight.w400,
                            fontSize: 16,
                            letterSpacing: 1,
                            height: 1.5,
                          ),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(top: 20, left: 15, right: 15),
                        child: Text(
                          'Organ Donor App',
                          style: GoogleFonts.roboto(
                            color: Colors.grey[400],
                            fontWeight: FontWeight.w400,
                            fontSize: 12,
                            letterSpacing: 1,
                            height: 1.5,
                          ),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(top: 10),
                        child: Divider(
                          color: Colors.grey,
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(top: 10, left: 10),
                        child: Text(
                          'Post Comment',
                          style: GoogleFonts.roboto(
                            color: Colors.grey[700],
                            fontWeight: FontWeight.w400,
                            fontSize: 14,
                          ),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(top: 20),
                        child: TextFormField(
                          decoration: InputDecoration(
                            filled: true,
                            fillColor: Colors.red[50],
                            hintStyle: GoogleFonts.roboto(
                              color: Colors.black54,
                              fontWeight: FontWeight.w400,
                              fontSize: 14,
                            ),
                            hintText: 'Please select and enter reply',
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(0),
                              borderSide: BorderSide.none,
                            ),
                          ),
                          controller: _reply..text = reply!,
                          key: Key(reply.toString()), // <- Magic!
                          maxLines: 2,
                          readOnly: true,
                          enabled: false,
                        ),
                      ),

                      //Post Button
                      Container(
                        margin: EdgeInsets.only(top: 5, bottom: 10, right: 20),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            ElevatedButton(
                              style: ElevatedButton.styleFrom(
                                primary: Colors.red[100],
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(30),
                                ),
                              ),
                              onPressed: isButtonEnabled
                                  ? () {
                                      setState(() {
                                        if (widget.getUserType ==
                                            'I am a Donor') {
                                          //Count Comments
                                          getPostComments =
                                              widget.getPostComments;
                                          int x =
                                              int.parse(getPostComments!) + 1;
                                          getPostComments = x.toString();
                                          commentAddDonor();
                                          commentAddAll();
                                          commentAddSpecificDonor();
                                          commentAddSpecificAll();
                                          //Post Reply
                                          replyDonor();
                                          replyAll();
                                          replySpecificDonor();
                                          replySpecificAll();
                                          showReplyToast();
                                          reply = "";
                                          isButtonEnabled = false;
                                        } else if (widget.getUserType ==
                                            'I am a Patient') {
                                          //Count Comments
                                          getPostComments =
                                              widget.getPostComments;
                                          int x =
                                              int.parse(getPostComments!) + 1;
                                          getPostComments = x.toString();
                                          commentAddPatient();
                                          commentAddAll();
                                          commentAddSpecificPatient();
                                          commentAddSpecificAll();

                                          //Post Reply
                                          replyPatient();
                                          replyAll();
                                          replySpecificPatient();
                                          replySpecificAll();
                                          showReplyToast();
                                          reply = "";
                                          isButtonEnabled = false;
                                        } else {
                                          //count Comments
                                          getPostComments =
                                              widget.getPostComments;
                                          int x =
                                              int.parse(getPostComments!) + 1;
                                          getPostComments = x.toString();
                                          commentAddAdmin();
                                          commentAddAll();

                                          //Posts Reply
                                          replyAdmin();
                                          replyAll();
                                          showReplyToast();
                                          reply = "";
                                          isButtonEnabled = false;
                                        }
                                      });
                                    }
                                  : null,
                              child: Text(
                                'Post',
                                style: GoogleFonts.roboto(
                                  color: Colors.red[400],
                                  fontWeight: FontWeight.w400,
                                  fontSize: 14,
                                ),
                              ),
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                ),
                // Checks if User is Donor or Patient
                FutureBuilder(
                  future: getUserData(),
                  builder: (context, snapshot) {
                    if (snapshot.connectionState != ConnectionState.done)
                      return Text("");
                    if (donorUserType != null) {
                      usersValue = donorUserType;
                      userName = donorUserName;
                      userID = donorUserID;
                      return Container(
                          height: 20,
                          width: 240,
                          child: Text(
                            '',
                            style: GoogleFonts.roboto(
                                color: Colors.black87,
                                fontSize: 15,
                                fontWeight: FontWeight.w500),
                          ));
                    }
                    if (patientUserType != null) {
                      usersValue = patientUserType;
                      userName = patientUserName;
                      userID = patientUserID;
                      return Container(
                          height: 20,
                          width: 240,
                          child: Text(
                            '',
                            style: GoogleFonts.roboto(
                                color: Colors.black87,
                                fontSize: 15,
                                fontWeight: FontWeight.w500),
                          ));
                    } else {
                      return Text(
                        "",
                        style: GoogleFonts.roboto(
                            color: Colors.black54, fontSize: 14),
                      );
                    }
                  },
                ),
                //All Comments
                Container(
                  height: 550,
                  width: double.infinity,
                  margin: EdgeInsets.only(top: 0, bottom: 50),
                  color: Colors.white,
                  child: Stack(
                    children: [
                      Container(
                        margin: EdgeInsets.only(top: 15, left: 15),
                        child: Text(
                          'All Comments',
                          style: GoogleFonts.roboto(
                              color: Colors.black87,
                              fontSize: 14,
                              fontWeight: FontWeight.w800),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(top: 40),
                        child: Divider(
                          color: Colors.grey,
                        ),
                      ),
                      Container(
                        color: Colors.white, // change color here!!!!!!
                        margin: EdgeInsets.only(top: 50),
                        child: FutureBuilder(
                          future: getAllCommentsData(),
                          builder: (context, AsyncSnapshot snapshot) {
                            if (snapshot.hasData) {
                              return ListView.builder(
                                itemCount: snapshot.data!.length,
                                itemBuilder: (context, index) {
                                  DocumentSnapshot userData =
                                      snapshot.data[index];

                                  return Stack(
                                    children: [
                                      Container(
                                        color: Colors.white,
                                        margin: EdgeInsets.only(top: 10),
                                        child: ListTile(
                                          title: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: [
                                              Row(
                                                children: [
                                                  //Profile Image
                                                  Container(
                                                    margin: EdgeInsets.only(
                                                        top: 10),
                                                    child: Image.asset(
                                                      'assets/user.png',
                                                      height: 35,
                                                    ),
                                                  ),
                                                  Container(
                                                    margin: EdgeInsets.only(
                                                        left: 10),
                                                    child: Column(
                                                      crossAxisAlignment:
                                                          CrossAxisAlignment
                                                              .start,
                                                      children: [
                                                        //UserName
                                                        Container(
                                                          margin:
                                                              EdgeInsets.only(
                                                                  top: 10),
                                                          child: Row(
                                                            children: [
                                                              Text(
                                                                userData[
                                                                    "UserName"],
                                                                style: GoogleFonts.roboto(
                                                                    color: Colors
                                                                        .black87,
                                                                    fontSize:
                                                                        14,
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .w600),
                                                              ),
                                                            ],
                                                          ),
                                                        ),

                                                        //Replied Time
                                                        Container(
                                                          margin:
                                                              EdgeInsets.only(
                                                                  top: 3),
                                                          child: Text(
                                                            userData[
                                                                "Replied Time"],
                                                            style: GoogleFonts.roboto(
                                                                color:
                                                                    Colors.grey,
                                                                fontSize: 12,
                                                                fontWeight:
                                                                    FontWeight
                                                                        .w600),
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                                  ),
                                                ],
                                              ),
                                              //Reply
                                              Container(
                                                margin: EdgeInsets.only(
                                                    top: 20, left: 45),
                                                child: Text(
                                                  userData["Reply"],
                                                  style: GoogleFonts.roboto(
                                                    color: Colors.black87,
                                                    fontSize: 18,
                                                    letterSpacing: 1,
                                                    fontWeight: FontWeight.w400,
                                                  ),
                                                ),
                                              ),
                                              //Replied Date
                                              Container(
                                                margin: EdgeInsets.only(
                                                    top: 20, left: 45),
                                                child: Row(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment
                                                          .spaceBetween,
                                                  children: [
                                                    Text(
                                                      userData["Replied Date"],
                                                      style: GoogleFonts.roboto(
                                                          color: Colors.grey,
                                                          fontSize: 12,
                                                          fontWeight:
                                                              FontWeight.w400),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                              //Divider Line
                                              Container(
                                                margin: EdgeInsets.only(
                                                    top: 10, left: 42),
                                                child: Divider(
                                                  color: Colors.grey,
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                    ],
                                  );
                                },
                              );
                            } else {
                              return Text('Users Data Not Available');
                            }
                          },
                        ),
                      ),
                    ],
                  ),
                )
              ],
            ),
          ),
          //Header
          Container(
            decoration: BoxDecoration(
              color: Color(0xffe5eef8),
              boxShadow: [
                BoxShadow(
                  color: Colors.grey.withOpacity(0.5),
                  blurRadius: 8,
                ),
              ],
            ),
            height: 80,
            margin: EdgeInsets.only(top: 0, left: 0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(top: 26, left: 20),
                  child: Row(
                    children: [
                      InkWell(
                          onTap: () {
                            Navigator.pushAndRemoveUntil(
                                context,
                                MaterialPageRoute(
                                    builder: (builder) => DashboardHomePage()),
                                (route) => false);
                          },
                          child: Icon(Icons.arrow_back_ios_new_outlined,
                              size: 25.0)),
                      //Verified Icon
                      Container(
                        margin: EdgeInsets.only(left: 10),
                        child:
                            Icon(Icons.verified_outlined, color: Colors.green),
                      ),
                      //UserName
                      Container(
                        margin: EdgeInsets.only(left: 10),
                        child: Text(
                          widget.getUserType!,
                          style: GoogleFonts.roboto(
                            color: Colors.black87,
                            fontWeight: FontWeight.w500,
                            fontSize: 18,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          //Put Comments
          Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              Container(
                height: 50,
                width: double.infinity,
                decoration: BoxDecoration(
                  boxShadow: [BoxShadow(color: Colors.grey.withOpacity(0.5))],
                  color: Colors.white,
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          margin: EdgeInsets.only(top: 5, left: 20, right: 20),
                          child: Row(
                            children: [
                              //Insert Reply
                              InkWell(
                                onTap: () {
                                  setState(() {
                                    if (usersValue == 'I am a Donor') {
                                      showModalBottomSheet(
                                        context: context,
                                        builder: (context) =>
                                            buildDonorReplySheet(),
                                      );
                                    } else {
                                      showModalBottomSheet(
                                        context: context,
                                        builder: (context) =>
                                            buildPatientReplySheet(),
                                      );
                                    }
                                  });
                                },
                                child: Container(
                                  width: 200,
                                  decoration: BoxDecoration(
                                      color: Color(0xffe5eef8),
                                      borderRadius: BorderRadius.circular(20)),
                                  padding: EdgeInsets.all(10),
                                  child: Text(
                                    'I have something to say...',
                                    style: GoogleFonts.roboto(
                                      color: Colors.grey,
                                      fontWeight: FontWeight.w400,
                                      fontSize: 12,
                                    ),
                                  ),
                                ),
                              ),
                              //Number of Comments
                              Container(
                                margin: EdgeInsets.only(left: 20),
                                child: Column(
                                  children: [
                                    Icon(Icons.comment_outlined),
                                    Container(
                                      child: Text(
                                        widget.getPostComments!,
                                        style: GoogleFonts.roboto(
                                          color: Colors.grey,
                                          fontWeight: FontWeight.w400,
                                          fontSize: 12,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),

                              //Number of Likes
                              Container(
                                margin: EdgeInsets.only(left: 20),
                                child: Column(
                                  children: [
                                    Icon(Icons.thumb_up_outlined),
                                    Text(
                                      widget.getPostLikes!,
                                      style: GoogleFonts.roboto(
                                        color: Colors.grey,
                                        fontWeight: FontWeight.w400,
                                        fontSize: 12,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

  // Bottom Sheet For Donor Reply
  Widget buildDonorReplySheet() => Container(
        height: 300,
        child: ListView(
          children: [
            //Header Patient
            ListTile(
              title: Row(
                children: [
                  Text(
                    'Reply',
                    style: GoogleFonts.roboto(
                        color: Colors.black87,
                        fontSize: 15,
                        fontWeight: FontWeight.w500),
                  ),
                  Container(
                    margin: EdgeInsets.only(left: 10),
                    child: Icon(
                      Icons.reply_outlined,
                      color: Colors.red,
                    ),
                  ),
                ],
              ),
            ),
            //Selection
            ListTile(
              title: Container(
                padding: EdgeInsets.all(15),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5),
                    color: Colors.red[100]),
                child: Text(
                  'Thank you Doctor, I will be waiting for your updates.',
                  style:
                      GoogleFonts.roboto(color: Colors.black87, fontSize: 15),
                ),
              ),
              onTap: () {
                setState(() {
                  reply =
                      'Thank you Doctor, I will be waiting for your updates.';
                  isButtonEnabled = true;
                  Navigator.of(context).pop();
                });
              },
            ),
            ListTile(
              title: Container(
                padding: EdgeInsets.all(15),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5),
                    color: Colors.red[100]),
                child: Text(
                  "I highly Appreciate it Doctor. Thank you",
                  style:
                      GoogleFonts.roboto(color: Colors.black87, fontSize: 15),
                ),
              ),
              onTap: () {
                setState(() {
                  reply = "I highly Appreciate it Doctor. Thank you";
                  isButtonEnabled = true;
                  Navigator.of(context).pop();
                });
              },
            ),
            ListTile(
              title: Container(
                padding: EdgeInsets.all(15),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5),
                    color: Colors.red[100]),
                child: Text(
                  'I am looking for a patient like you. I hope the doctor will find us best match.',
                  style:
                      GoogleFonts.roboto(color: Colors.black87, fontSize: 15),
                ),
              ),
              onTap: () {
                setState(() {
                  reply =
                      'I am looking for a patient like you. I hope the doctor will find us best match.';
                  isButtonEnabled = true;
                  Navigator.of(context).pop();
                });
              },
            ),
            ListTile(
              title: Container(
                padding: EdgeInsets.all(15),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5),
                    color: Colors.red[100]),
                child: Text(
                  "Hello, I guess we might be having a best match? Isn't?",
                  style:
                      GoogleFonts.roboto(color: Colors.black87, fontSize: 15),
                ),
              ),
              onTap: () {
                setState(() {
                  reply =
                      "Hello, I guess we might be having a best match? Isn't?";
                  isButtonEnabled = true;
                  Navigator.of(context).pop();
                });
              },
            ),
            ListTile(
              title: Container(
                padding: EdgeInsets.all(15),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5),
                    color: Colors.red[100]),
                child: Text(
                  "I hope so. Let's see what the doctor will give us",
                  style:
                      GoogleFonts.roboto(color: Colors.black87, fontSize: 15),
                ),
              ),
              onTap: () {
                setState(() {
                  reply = "I hope so. Let's see what the doctor will give us";
                  isButtonEnabled = true;
                  Navigator.of(context).pop();
                });
              },
            ),
            ListTile(
              title: Container(
                padding: EdgeInsets.all(15),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5),
                    color: Colors.red[100]),
                child: Text(
                  "Agreed! Thank you.",
                  style:
                      GoogleFonts.roboto(color: Colors.black87, fontSize: 15),
                ),
              ),
              onTap: () {
                setState(() {
                  reply = "Agreed! Thank you.";
                  isButtonEnabled = true;
                  Navigator.of(context).pop();
                });
              },
            ),
            ListTile(
              title: Container(
                padding: EdgeInsets.all(15),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5),
                    color: Colors.red[100]),
                child: Text(
                  "Hello Doctor, Update will be made in a moment. Thank you.",
                  style:
                      GoogleFonts.roboto(color: Colors.black87, fontSize: 15),
                ),
              ),
              onTap: () {
                setState(() {
                  reply =
                      "Hello Doctor, Update will be made in a moment. Thank you.";
                  isButtonEnabled = true;
                  Navigator.of(context).pop();
                });
              },
            ),
            ListTile(
              title: Container(
                padding: EdgeInsets.all(15),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5),
                    color: Colors.red[100]),
                child: Text(
                  "Hello Doctor, Please check my information and see if i have match with the kidney patients. Thank you.",
                  style:
                      GoogleFonts.roboto(color: Colors.black87, fontSize: 15),
                ),
              ),
              onTap: () {
                setState(() {
                  reply =
                      "Hello Doctor, Please check my information and see if i have match with the kidney patients. Thank you.";
                  isButtonEnabled = true;
                  Navigator.of(context).pop();
                });
              },
            ),
          ],
        ),
      );

  // Bottom Sheet For Patient Reply
  Widget buildPatientReplySheet() => Container(
        height: 300,
        child: ListView(
          children: [
            //Header Patient
            ListTile(
              title: Row(
                children: [
                  Text(
                    'Reply',
                    style: GoogleFonts.roboto(
                        color: Colors.black87,
                        fontSize: 15,
                        fontWeight: FontWeight.w500),
                  ),
                  Container(
                    margin: EdgeInsets.only(left: 10),
                    child: Icon(
                      Icons.reply_outlined,
                      color: Colors.red,
                    ),
                  ),
                ],
              ),
            ),
            //Selection
            ListTile(
              title: Container(
                padding: EdgeInsets.all(15),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5),
                    color: Colors.red[100]),
                child: Text(
                  'Thank you Doctor, I will be waiting for your updates.',
                  style:
                      GoogleFonts.roboto(color: Colors.black87, fontSize: 15),
                ),
              ),
              onTap: () {
                setState(() {
                  reply =
                      'Thank you Doctor, I will be waiting for your updates.';
                  isButtonEnabled = true;
                  Navigator.of(context).pop();
                });
              },
            ),
            ListTile(
              title: Container(
                padding: EdgeInsets.all(15),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5),
                    color: Colors.red[100]),
                child: Text(
                  "I highly Appreciate it Doctor. Thank you",
                  style:
                      GoogleFonts.roboto(color: Colors.black87, fontSize: 15),
                ),
              ),
              onTap: () {
                setState(() {
                  reply = "I highly Appreciate it Doctor. Thank you";
                  isButtonEnabled = true;
                  Navigator.of(context).pop();
                });
              },
            ),
            ListTile(
              title: Container(
                padding: EdgeInsets.all(15),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5),
                    color: Colors.red[100]),
                child: Text(
                  'I am looking for a donor like you. I hope the doctor will find us best match.',
                  style:
                      GoogleFonts.roboto(color: Colors.black87, fontSize: 15),
                ),
              ),
              onTap: () {
                setState(() {
                  reply =
                      'I am looking for a donor like you. I hope the doctor will find us best match.';
                  isButtonEnabled = true;
                  Navigator.of(context).pop();
                });
              },
            ),
            ListTile(
              title: Container(
                padding: EdgeInsets.all(15),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5),
                    color: Colors.red[100]),
                child: Text(
                  "Hello, I guess we might be having a best match? Isn't?",
                  style:
                      GoogleFonts.roboto(color: Colors.black87, fontSize: 15),
                ),
              ),
              onTap: () {
                setState(() {
                  reply =
                      "Hello, I guess we might be having a best match? Isn't?";
                  isButtonEnabled = true;
                  Navigator.of(context).pop();
                });
              },
            ),
            ListTile(
              title: Container(
                padding: EdgeInsets.all(15),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5),
                    color: Colors.red[100]),
                child: Text(
                  "I hope so. Let's see what the doctor will give us",
                  style:
                      GoogleFonts.roboto(color: Colors.black87, fontSize: 15),
                ),
              ),
              onTap: () {
                setState(() {
                  reply = "I hope so. Let's see what the doctor will give us";
                  isButtonEnabled = true;
                  Navigator.of(context).pop();
                });
              },
            ),
            ListTile(
              title: Container(
                padding: EdgeInsets.all(15),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5),
                    color: Colors.red[100]),
                child: Text(
                  "Agreed! Thank you.",
                  style:
                      GoogleFonts.roboto(color: Colors.black87, fontSize: 15),
                ),
              ),
              onTap: () {
                setState(() {
                  reply = "Agreed! Thank you.";
                  isButtonEnabled = true;
                  Navigator.of(context).pop();
                });
              },
            ),
            ListTile(
              title: Container(
                padding: EdgeInsets.all(15),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5),
                    color: Colors.red[100]),
                child: Text(
                  "Hello Doctor, Update will be made in a moment. Thank you.",
                  style:
                      GoogleFonts.roboto(color: Colors.black87, fontSize: 15),
                ),
              ),
              onTap: () {
                setState(() {
                  reply =
                      "Hello Doctor, Update will be made in a moment. Thank you.";
                  isButtonEnabled = true;
                  Navigator.of(context).pop();
                });
              },
            ),
            ListTile(
              title: Container(
                padding: EdgeInsets.all(15),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5),
                    color: Colors.red[100]),
                child: Text(
                  "Hello Doctor, Please check my information and see if i have match with the kidney donors. Thank you.",
                  style:
                      GoogleFonts.roboto(color: Colors.black87, fontSize: 15),
                ),
              ),
              onTap: () {
                setState(() {
                  reply =
                      "Hello Doctor, Please check my information and see if i have match with the kidney donors. Thank you.";
                  isButtonEnabled = true;
                  Navigator.of(context).pop();
                });
              },
            ),
          ],
        ),
      );

  //Get User Information
  getUserData() async {
    final donor = FirebaseAuth.instance.currentUser;
    if (donor != null) {
      await FirebaseFirestore.instance
          .collection('Users')
          .doc("Donor")
          .collection("Lists")
          .doc(donor.uid)
          .get()
          .then((value) {
        donorUserName = value.data()!['UserName'];
        donorUserType = value.data()!['User Type'];
        donorUserID = value.data()!['UserID'];
      }).catchError((e) {
        print(e);
      });
    }

    final patient = FirebaseAuth.instance.currentUser;
    if (patient != null) {
      await FirebaseFirestore.instance
          .collection('Users')
          .doc("Patient")
          .collection("Lists")
          .doc(patient.uid)
          .get()
          .then((value) {
        patientUserName = value.data()!['UserName'];
        patientUserType = value.data()!['User Type'];
        patientUserID = value.data()!['UserID'];
      }).catchError((e) {
        print(e);
      });
    }
  }

  //Posts Document
  replyDonor() async {
    String? pid = widget.getTime;

    String formattedTime = DateFormat('kk:mm:s:a').format(now);
    String formattedDate = formatter.format(now);
    await FirebaseFirestore.instance
        .collection("Posts")
        .doc('Donor')
        .collection('All Posts')
        .doc(pid)
        .collection('Comments')
        .doc(formattedTime)
        .set({
      'UserID': userID,
      'UserName': userName,
      'User Type': usersValue,
      'Reply': _reply.text,
      'Replied Date': formattedDate,
      'Replied Time': formattedTime,
      'Likes': '0',
    });
  }

  replyPatient() async {
    String? pid = widget.getTime;
    String formattedTime = DateFormat('kk:mm:s:a').format(now);
    String formattedDate = formatter.format(now);
    await FirebaseFirestore.instance
        .collection("Posts")
        .doc('Patient')
        .collection('All Posts')
        .doc(pid)
        .collection('Comments')
        .doc(formattedTime)
        .set({
      'UserID': userID,
      'UserName': userName,
      'User Type': usersValue,
      'Reply': _reply.text,
      'Replied Date': formattedDate,
      'Replied Time': formattedTime,
      'Likes': '0',
    });
  }

  replyAdmin() async {
    String? pid = widget.getTime;
    String formattedTime = DateFormat('kk:mm:s:a').format(now);
    String formattedDate = formatter.format(now);
    await FirebaseFirestore.instance
        .collection("Posts")
        .doc('Admin')
        .collection('All Posts')
        .doc(pid)
        .collection('Comments')
        .doc(formattedTime)
        .set({
      'UserID': userID,
      'UserName': userName,
      'User Type': usersValue,
      'Reply': _reply.text,
      'Replied Date': formattedDate,
      'Replied Time': formattedTime,
      'Likes': '0',
    });
  }

  replyAll() async {
    String? pid = widget.getTime;
    String formattedTime = DateFormat('kk:mm:s:a').format(now);
    String formattedDate = formatter.format(now);
    await FirebaseFirestore.instance
        .collection("Posts")
        .doc('All')
        .collection('All Posts')
        .doc(pid)
        .collection('Comments')
        .doc(formattedTime)
        .set({
      'UserID': userID,
      'UserName': userName,
      'User Type': usersValue,
      'Reply': _reply.text,
      'Replied Date': formattedDate,
      'Replied Time': formattedTime,
      'Likes': '0',
    });
  }

  //Specific Donor and Patient Document
  replySpecificDonor() async {
    String? pid = widget.getTime;
    String? did = widget.getUserID;
    String formattedTime = DateFormat('kk:mm:s:a').format(now);
    String formattedDate = formatter.format(now);
    await FirebaseFirestore.instance
        .collection("Users")
        .doc('Donor')
        .collection('Lists')
        .doc(did)
        .collection('Posts')
        .doc(pid)
        .collection('Comments')
        .doc(formattedTime)
        .set({
      'UserID': userID,
      'UserName': userName,
      'User Type': usersValue,
      'Reply': _reply.text,
      'Replied Date': formattedDate,
      'Replied Time': formattedTime,
      'Likes': '0',
    });
  }

  replySpecificPatient() async {
    String? pid = widget.getTime;
    String? did = widget.getUserID;
    String formattedTime = DateFormat('kk:mm:s:a').format(now);
    String formattedDate = formatter.format(now);
    await FirebaseFirestore.instance
        .collection("Users")
        .doc('Patient')
        .collection('Lists')
        .doc(did)
        .collection('Posts')
        .doc(pid)
        .collection('Comments')
        .doc(formattedTime)
        .set({
      'UserID': userID,
      'UserName': userName,
      'User Type': usersValue,
      'Reply': _reply.text,
      'Replied Date': formattedDate,
      'Replied Time': formattedTime,
      'Likes': '0',
    });
  }

  replySpecificAll() async {
    String? pid = widget.getTime;
    String? did = widget.getUserID;
    String formattedTime = DateFormat('kk:mm:s:a').format(now);
    String formattedDate = formatter.format(now);
    await FirebaseFirestore.instance
        .collection("Users")
        .doc('All')
        .collection('Lists')
        .doc(did)
        .collection('Posts')
        .doc(pid)
        .collection('Comments')
        .doc(formattedTime)
        .set({
      'UserID': userID,
      'UserName': userName,
      'User Type': usersValue,
      'Reply': _reply.text,
      'Replied Date': formattedDate,
      'Replied Time': formattedTime,
      'Likes': '0',
    });
  }

  //Display From All Posts
  getAllCommentsData() async {
    String? pid = widget.getTime;
    final firestore = FirebaseFirestore.instance;
    QuerySnapshot user = await firestore
        .collection('Posts')
        .doc('All')
        .collection('All Posts')
        .doc(pid)
        .collection('Comments')
        .get();
    return user.docs;
  }

  //Show Reply Succes
  void showReplyToast() =>
      toast.showToast(child: buildReplyToast(), gravity: ToastGravity.CENTER);

  Widget buildReplyToast() => Container(
        padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
        decoration: BoxDecoration(
          color: Colors.green[300],
          borderRadius: BorderRadius.circular(5),
        ),
        child: Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            Text(
              "You have replied on this post",
              style: TextStyle(color: Colors.black87, fontSize: 16),
            ),
          ],
        ),
      );

  //CommentCount
  commentAddDonor() async {
    String? did = widget.getTime;
    await FirebaseFirestore.instance
        .collection("Posts")
        .doc('Donor')
        .collection('All Posts')
        .doc(did)
        .update({
      'Comments': getPostComments,
    });
  }

  commentAddPatient() async {
    String? did = widget.getTime;
    await FirebaseFirestore.instance
        .collection("Posts")
        .doc('Patient')
        .collection('All Posts')
        .doc(did)
        .update({
      'Comments': getPostComments,
    });
  }

  commentAddAdmin() async {
    String? did = widget.getTime;
    await FirebaseFirestore.instance
        .collection("Posts")
        .doc('Admin')
        .collection('All Posts')
        .doc(did)
        .update({
      'Comments': getPostComments,
    });
  }

  commentAddAll() async {
    String? did = widget.getTime;
    await FirebaseFirestore.instance
        .collection("Posts")
        .doc('All')
        .collection('All Posts')
        .doc(did)
        .update({
      'Comments': getPostComments,
    });
  }

  commentAddSpecificDonor() async {
    String? uid = widget.getUserID;
    String? did = widget.getTime;
    await FirebaseFirestore.instance
        .collection("Users")
        .doc('Donor')
        .collection('Lists')
        .doc(uid)
        .collection('Posts')
        .doc(did)
        .update({
      'Comments': getPostComments,
    });
  }

  commentAddSpecificPatient() async {
    String? uid = widget.getUserID;
    String? did = widget.getTime;
    await FirebaseFirestore.instance
        .collection("Users")
        .doc('Patient')
        .collection('Lists')
        .doc(uid)
        .collection('Posts')
        .doc(did)
        .update({
      'Comments': getPostComments,
    });
  }

  commentAddSpecificAll() async {
    String? uid = widget.getUserID;
    String? did = widget.getTime;
    await FirebaseFirestore.instance
        .collection("Users")
        .doc('All')
        .collection('Lists')
        .doc(uid)
        .collection('Posts')
        .doc(did)
        .update({
      'Comments': getPostComments,
    });
  }
}
