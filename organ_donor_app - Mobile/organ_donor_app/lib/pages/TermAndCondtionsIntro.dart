import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:organ_donor_app/pages/SignInPage.dart';
import 'package:organ_donor_app/pages/TermsOfUse.dart';

class TermAndConditionsIntro extends StatefulWidget {
  TermAndConditionsIntro({Key? key}) : super(key: key);

  @override
  _TermAndConditionsIntroState createState() => _TermAndConditionsIntroState();
}

class _TermAndConditionsIntroState extends State<TermAndConditionsIntro> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: Stack(
        children: [
          SingleChildScrollView(
            child: Column(
              children: [
                Container(
                  margin: EdgeInsets.only(top: 30, bottom: 100),
                  child: Center(
                    child: Column(
                      children: [
                        Container(
                          width: 200,
                          height: 200,
                          child: Transform.rotate(
                            angle: 0,
                            child: Image(image: AssetImage('assets/logo.png')),
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(left: 20, right: 20),
                          child: Text(
                            'Be The Reason Someone Starts Living Again.',
                            textAlign: TextAlign.center,
                            style: GoogleFonts.roboto(
                                color: Colors.black,
                                fontSize: 20,
                                fontWeight: FontWeight.w400,
                                height: 1.5,
                                letterSpacing: 2),
                          ),
                        ),
                        SizedBox(height: 20),
                        Text(
                          "Privacy Statement",
                          style: GoogleFonts.roboto(
                              color: Colors.black, fontSize: 15),
                        ),
                        SizedBox(height: 25),
                        Container(
                          margin: EdgeInsets.only(left: 20, right: 20),
                          child: RichText(
                            text: TextSpan(
                              children: [
                                TextSpan(
                                  text: "Privacy Notice\n\n".toUpperCase(),
                                  style: GoogleFonts.roboto(
                                      color: Colors.red[400],
                                      fontSize: 25,
                                      fontWeight: FontWeight.bold),
                                ),
                                TextSpan(
                                  children: [
                                    TextSpan(
                                      text:
                                          "Organ Donor App is a kidney donor application for those who have a hard time finding the best match of kidney donors and waiting for a kidney organ transplant.",
                                      style: GoogleFonts.roboto(
                                          color: Colors.black87,
                                          fontSize: 14,
                                          height: 1.7),
                                    ),
                                    TextSpan(
                                      text:
                                          "\nThis Privacy Notice explains the privacy practices for a Kidney donation and transplantation application and secures what data we gather and process; why we do so; and your privileges concerning patient and donor data. We followed the Republic Act No. 7170 (the “Organ Donation Act of 1991”).",
                                      style: GoogleFonts.roboto(
                                          color: Colors.black87,
                                          fontSize: 14,
                                          height: 1.7),
                                    ),
                                    TextSpan(
                                      text:
                                          "\nWe might alter this Privacy Notice occasionally. At the point when certain changes require your assent, we will communicate the changes to you and request your assent.\n\n\n",
                                      style: GoogleFonts.roboto(
                                          color: Colors.black87,
                                          fontSize: 14,
                                          height: 1.7),
                                    ),
                                    TextSpan(
                                      text: "What Information We Do Collect\n\n"
                                          .toUpperCase(),
                                      style: GoogleFonts.roboto(
                                          color: Colors.red[400],
                                          fontSize: 25,
                                          fontWeight: FontWeight.bold),
                                    ),
                                    TextSpan(
                                      text:
                                          "While using our application, we may gather the following information when you register or fill out a form like User name, Gender, Mobile Number, and if you are a donor or a patient. Full name and Email address will be optional",
                                      style: GoogleFonts.roboto(
                                          color: Colors.black87,
                                          fontSize: 14,
                                          height: 1.7),
                                    ),
                                    TextSpan(
                                      text:
                                          "\nThis information will not be sold or otherwise transferred to untrusted third parties and share the credentials or details of the donor to the patient to make sure the safety of both users. The donor and patient must be complete the registration of the app to proceed to the homepage. Organ Donor App Admin(Doctor) will inform registered kidney donor and patient if there is an available and best match for them.\n\n\n",
                                      style: GoogleFonts.roboto(
                                          color: Colors.black87,
                                          fontSize: 14,
                                          height: 1.7),
                                    ),
                                    TextSpan(
                                      text: "Collected Information Uses\n\n"
                                          .toUpperCase(),
                                      style: GoogleFonts.roboto(
                                          color: Colors.red[400],
                                          fontSize: 25,
                                          fontWeight: FontWeight.bold),
                                    ),
                                    TextSpan(
                                      text:
                                          "We collect your information to understand your needs and give a better service to our application. Organ Donor App will use the data collected for the safety of the patient and donor before matching them. With your consent, we use your data to provide you with information about kidney donation.\n\n\n",
                                      style: GoogleFonts.roboto(
                                          color: Colors.black87,
                                          fontSize: 14,
                                          height: 1.7),
                                    ),
                                    TextSpan(
                                      text:
                                          "How We Secured the Data Collected\n\n"
                                              .toUpperCase(),
                                      style: GoogleFonts.roboto(
                                          color: Colors.red[400],
                                          fontSize: 25,
                                          fontWeight: FontWeight.bold),
                                    ),
                                    TextSpan(
                                      text:
                                          "To prevent any unauthorized access or exposure of the personal information that we collected, we keep and protect your information using a database, one-time password authentication, email verification, and encryption.",
                                      style: GoogleFonts.roboto(
                                          color: Colors.black87,
                                          fontSize: 14,
                                          height: 1.7),
                                    ),
                                    TextSpan(
                                      text: "\n\n\n",
                                      style: TextStyle(
                                          fontSize: 14,
                                          color: Colors.black,
                                          height: 1.5),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              Container(
                decoration: BoxDecoration(
                  boxShadow: [BoxShadow(color: Colors.grey.withOpacity(0.5))],
                  color: Colors.white,
                ),
                padding: EdgeInsets.only(top: 10, bottom: 10),
                child: Column(
                  children: [
                    Container(
                      height: 50,
                      width: double.infinity,
                      padding: EdgeInsets.only(left: 30, right: 30),
                      child: ElevatedButton(
                        style: ElevatedButton.styleFrom(
                          primary: Colors.red[400],
                        ),
                        onPressed: () {
                          Navigator.of(context).push(MaterialPageRoute(
                              builder: (context) => SignInPage()));
                        },
                        child: Text(
                          'Accept Privacy Notice',
                          style: GoogleFonts.roboto(
                              color: Colors.white, fontSize: 14),
                        ),
                      ),
                    ),
                    Container(
                      height: 50,
                      width: double.infinity,
                      padding: EdgeInsets.only(left: 30, right: 30, top: 10),
                      child: ElevatedButton(
                        style: ElevatedButton.styleFrom(
                          primary: Colors.red[100],
                        ),
                        onPressed: () {
                          Navigator.of(context).push(MaterialPageRoute(
                              builder: (context) => TermsOfUse()));
                        },
                        child: Text('Not Now',
                            style: GoogleFonts.roboto(
                                color: Colors.red[400], fontSize: 14)),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
