import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:intl/intl.dart';
import 'package:fluttertoast/fluttertoast.dart';

class FeedBackPage extends StatefulWidget {
  FeedBackPage({Key? key}) : super(key: key);

  @override
  _FeedBackPageState createState() => _FeedBackPageState();
}

class _FeedBackPageState extends State<FeedBackPage> {
  String? donorUserID;
  String? donorUserName;
  String? donorUserType;
  String? donorDonated;
  String? patientUserID;
  String? patientUserName;
  String? patientUserType;
  String? patientReceived;

  //global variable
  String? currentUserID;
  String? currentFullName;
  String? currentUserName;
  String? currentUserType;

  String? donateAndReceived;

  double? rating = 0;

  String? feedBack;
  TextEditingController _feedBack = TextEditingController();

  //Time and Date
  var now = new DateTime.now();
  var formatter = new DateFormat('dd-MM-yyyy');

  //Toast Message
  final toast = FToast();
  @override
  void initState() {
    super.initState();
    toast.init(context);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xffe5eef8),
      body: Stack(
        children: [
          SingleChildScrollView(
            child: Column(
              children: [
                Container(
                  margin: EdgeInsets.only(top: 40, right: 15),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Container(width: 32.0, height: 0.0),
                      Text(
                        "My Match List",
                        style: GoogleFonts.roboto(
                            color: Colors.black87,
                            fontWeight: FontWeight.bold,
                            fontSize: 18),
                      ),
                    ],
                  ),
                ),
                //Check Current UserID for mapping of lists
                FutureBuilder(
                  future: getUserData(),
                  builder: (context, snapshot) {
                    if (snapshot.connectionState != ConnectionState.done)
                      return Text("");
                    if (donorUserID != null) {
                      currentUserID = donorUserID;
                      currentUserName = donorUserName;
                      currentUserType = donorUserType;
                      donateAndReceived = donorDonated;
                      return Text('');
                    }
                    if (patientUserID != null) {
                      currentUserID = patientUserID;
                      currentUserName = patientUserName;
                      currentUserType = patientUserType;
                      donateAndReceived = patientReceived;
                      return Text('');
                    } else {
                      return Text(
                        "null",
                        style: GoogleFonts.roboto(
                            color: Colors.black54,
                            fontSize: 18,
                            fontWeight: FontWeight.bold),
                      );
                    }
                  },
                ),

                Container(
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(10)),
                  width: 380,
                  height: MediaQuery.of(context).size.height * 0.61,
                  child: FutureBuilder(
                    future: getAllMatchData(),
                    builder: (context, AsyncSnapshot snapshot) {
                      if (snapshot.hasData) {
                        return ListView.builder(
                          itemCount: snapshot.data!.length,
                          itemBuilder: (context, index) {
                            DocumentSnapshot userData = snapshot.data[index];
                            int x = index + 1;
                            index = x;

                            return Stack(
                              children: [
                                Container(
                                  color: Colors.red[50],
                                  margin: EdgeInsets.only(
                                      top: 10, left: 10, right: 10),
                                  child: ListTile(
                                      title: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: [
                                      Row(
                                        children: [
                                          Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: [
                                              Text(
                                                userData["Match Date Time"],
                                                style: GoogleFonts.roboto(
                                                    color: Colors.grey,
                                                    fontSize: 12),
                                              ),
                                              Text(
                                                "\nTop " +
                                                    index.toString() +
                                                    ".",
                                                style: GoogleFonts.roboto(
                                                    color: Colors.black87,
                                                    fontSize: 14),
                                              ),
                                            ],
                                          ),
                                        ],
                                      ),
                                      Container(
                                        margin: EdgeInsets.only(top: 10),
                                        child: Image.asset(
                                          'assets/user.png',
                                          height: 30,
                                        ),
                                      ),
                                      Text(
                                        userData["UserName"],
                                        style: GoogleFonts.roboto(
                                            color: Colors.black87,
                                            fontSize: 16),
                                      ),
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: [
                                          Text(
                                            "Match Rate: ",
                                            style: GoogleFonts.roboto(
                                                color: Colors.grey[600],
                                                fontSize: 16),
                                          ),
                                          Text(
                                            userData["Match Rate"],
                                            style: GoogleFonts.roboto(
                                                color: Colors.black87,
                                                fontSize: 16,
                                                fontWeight: FontWeight.w700),
                                          ),
                                        ],
                                      ),
                                    ],
                                  )),
                                ),
                              ],
                            );
                          },
                        );
                      } else {
                        return Text('Users Data Not Available');
                      }
                    },
                  ),
                ),

                Container(
                  margin: EdgeInsets.only(top: 30, left: 40, right: 30),
                  child: SelectableText.rich(
                    TextSpan(
                      children: [
                        TextSpan(
                          text: 'Warning: ',
                          style: GoogleFonts.roboto(
                              color: Colors.red[400],
                              fontSize: 16,
                              fontWeight: FontWeight.w700),
                        ),
                        TextSpan(
                            text:
                                ' Feedbacks should only be given after you successfully process the organ donation. Thank you.'),
                      ],
                    ),
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(top: 30),
                  child: Text(
                    '$rating',
                    style: GoogleFonts.roboto(
                        color: Colors.black87,
                        fontSize: 36,
                        fontWeight: FontWeight.w700),
                  ),
                ),

                Container(
                  margin: EdgeInsets.only(top: 10),
                  child: RatingBar.builder(
                    minRating: 1,
                    itemSize: 30,
                    itemPadding: EdgeInsets.symmetric(horizontal: 4),
                    itemBuilder: (context, _) => Icon(
                      Icons.stars_outlined,
                      color: Colors.green,
                    ),
                    updateOnDrag: true,
                    onRatingUpdate: (rating) => setState(() {
                      this.rating = rating;
                    }),
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(
                    top: 20,
                  ),
                  child: TextFormField(
                    decoration: InputDecoration(
                      filled: true,
                      fillColor: Colors.red[50],
                      hintStyle: GoogleFonts.roboto(
                        color: Colors.black54,
                        fontWeight: FontWeight.w400,
                        fontSize: 14,
                      ),
                      hintText: 'Give Feedback',
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(0),
                        borderSide: BorderSide.none,
                      ),
                    ),
                    controller: _feedBack..text = "",
                    maxLines: 3,
                  ),
                ),
                //Send Feedback Button
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    Container(
                      margin: EdgeInsets.only(top: 20, bottom: 80, right: 20),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(20),
                        color: Colors.red,
                      ),
                      padding: EdgeInsets.all(5),
                      child: InkWell(
                        onTap: () {
                          if (currentUserType == "I am a Donor") {
                            sendFeedback();
                            addDonated();
                            showFeedbackToast();
                            _feedBack.text = "";
                            rating = 0;
                          } else if (currentUserType == "I am a Patient") {
                            sendFeedback();
                            showFeedbackToast();
                            addReceived();
                            _feedBack.text = "";
                            rating = 0;
                          }
                        },
                        child: Row(
                          children: [
                            Container(
                              margin: EdgeInsets.only(left: 10),
                              child: Text(
                                'Send feedback',
                                style: GoogleFonts.roboto(
                                    color: Colors.white,
                                    fontSize: 14,
                                    fontWeight: FontWeight.w700),
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(left: 10, right: 10),
                              child: Icon(Icons.send_outlined),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  //Show Reply Succes
  void showFeedbackToast() => toast.showToast(
      child: buildFeedbackToast(), gravity: ToastGravity.CENTER);

  Widget buildFeedbackToast() => Container(
        padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
        decoration: BoxDecoration(
          color: Colors.green[300],
          borderRadius: BorderRadius.circular(5),
        ),
        child: Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            Text(
              " FeedBack Submitted! ",
              style: TextStyle(color: Colors.black87, fontSize: 16),
            ),
          ],
        ),
      );

  //Getting User ID
  getUserData() async {
    final donor = FirebaseAuth.instance.currentUser;
    if (donor != null) {
      await FirebaseFirestore.instance
          .collection('Users')
          .doc("Donor")
          .collection("Lists")
          .doc(donor.uid)
          .get()
          .then((value) {
        donorUserID = value.data()!['UserID'];
        donorUserName = value.data()!['UserName'];
        donorUserType = value.data()!['User Type'];
        donorDonated = value.data()!['Donated'];
      }).catchError((e) {
        print(e);
      });
    }

    final patient = FirebaseAuth.instance.currentUser;
    if (patient != null) {
      await FirebaseFirestore.instance
          .collection('Users')
          .doc("Patient")
          .collection("Lists")
          .doc(patient.uid)
          .get()
          .then((value) {
        patientUserID = value.data()!['UserID'];
        patientUserName = value.data()!['UserName'];
        patientUserType = value.data()!['User Type'];
        patientReceived = value.data()!['Received'];
      }).catchError((e) {
        print(e);
      });
    }
  }

  //Display All Matches Made
  getAllMatchData() async {
    final uid = FirebaseAuth.instance.currentUser;
    final firestore = FirebaseFirestore.instance;
    QuerySnapshot user = await firestore
        .collection('Kidney Match')
        .doc(uid!.uid)
        .collection('Matches')
        .orderBy("Match Rate", descending: true)
        .get();
    return user.docs;
  }

  sendFeedback() async {
    User? user = FirebaseAuth.instance.currentUser;
    String formattedTime = DateFormat('kk:mm:s:a').format(now);
    String formattedDate = formatter.format(now);
    await FirebaseFirestore.instance
        .collection('Feedbacks')
        .doc(user!.uid)
        .set({
      'UserID': user.uid,
      'UserName': currentUserName,
      'User Type': currentUserType,
      'Rating': this.rating.toString(),
      'Feedback': _feedBack.text,
      'Feedback Date': formattedDate,
      'Feedback Time': formattedTime,
    });
  }

  addDonated() async {
    User? user = FirebaseAuth.instance.currentUser;
    await FirebaseFirestore.instance
        .collection('Users')
        .doc('Donor')
        .collection('Lists')
        .doc(user!.uid)
        .update({'Donated': "1"});
  }

  addReceived() async {
    User? user = FirebaseAuth.instance.currentUser;
    await FirebaseFirestore.instance
        .collection('Users')
        .doc('Patient')
        .collection('Lists')
        .doc(user!.uid)
        .update({'Received': "1"});
  }
}
