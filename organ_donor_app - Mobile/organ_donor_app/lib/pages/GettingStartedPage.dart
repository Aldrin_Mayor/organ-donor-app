import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intro_slider/dot_animation_enum.dart';
import 'package:intro_slider/intro_slider.dart';
import 'package:intro_slider/slide_object.dart';
import 'package:organ_donor_app/pages/TermsOfUse.dart';

class GettingStartedPage extends StatefulWidget {
  GettingStartedPage({Key? key}) : super(key: key);

  @override
  _GettingStartedPageState createState() => _GettingStartedPageState();
}

class _GettingStartedPageState extends State<GettingStartedPage> {
  List<Slide> slides = [];

  @override
  void initState() {
    super.initState();
    slides.add(
      new Slide(
        title: "Welcome to Organ Donor App",
        description:
            "Organ Donor App is a kidney donor application for those who have a hard time finding the best match of kidney donors and waiting for a kidney organ transplant.",
        pathImage: "assets/logo.png",
      ),
    );
    slides.add(
      new Slide(
        title: "We Respect Your Data Privacy",
        description:
            "Your Data Privacy is our priority. While using our application, we may gather the following information when you register or fill out a form like mobile number, gender, provided username and if you are a donor or a patient. Full Name and Email will be optional. Upon Logging in to the app, it is neccessary to provide blood, type, kidney size(end to end in cm) and age. This information will be used by the Admin (Doctor) to find a kidney donor or patient on the waiting list",
        pathImage: "assets/dataprivacy.png",
      ),
    );
    slides.add(
      new Slide(
        title:
            "Help us Save Lives of Kidney Patients waiting for Kidney Organ Transplant",
        description:
            "Let's help each other find people who are willing to donate their kidney to Patients on the Waiting List.",
        pathImage: "assets/helpus1.png",
      ),
    );
  }

  List<Widget>? renderListCustomTabs() {
    List<Widget> tabs = [];
    for (int i = 0; i < slides.length; i++) {
      Slide currentSlide = slides[i];
      tabs.add(
        Container(
          width: double.infinity,
          height: double.infinity,
          child: Container(
            margin: EdgeInsets.only(bottom: 80, top: 60),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                //Image
                Container(
                  padding: EdgeInsets.all(20),
                  child: Image.asset(
                    currentSlide.pathImage!,
                    matchTextDirection: true,
                    height: 260,
                  ),
                ),
                //Text
                Container(
                  margin: EdgeInsets.symmetric(horizontal: 20),
                  child: Text(
                    currentSlide.title!,
                    style: GoogleFonts.roboto(
                        color: Colors.black87,
                        fontWeight: FontWeight.bold,
                        fontSize: 18),
                    textAlign: TextAlign.center,
                  ),
                ),
                //Description
                Container(
                  child: Text(
                    currentSlide.description!,
                    style: GoogleFonts.roboto(
                        color: Colors.black87, fontSize: 14, height: 1.2),
                    textAlign: TextAlign.center,
                  ),
                  margin: EdgeInsets.only(top: 15, left: 30, right: 30),
                )
              ],
            ),
          ),
        ),
      );
    }
    return tabs;
  }

  @override
  Widget build(BuildContext context) {
    return IntroSlider(
      backgroundColorAllSlides: Colors.white,
      renderSkipBtn: Text(
        "Skip",
        style: TextStyle(color: Colors.red),
      ),
      renderNextBtn: Container(
        width: double.infinity,
        height: double.infinity,
        alignment: Alignment.center,
        decoration: BoxDecoration(
            color: Colors.red[100], borderRadius: BorderRadius.circular(5)),
        child: Text(
          "Next",
          style: GoogleFonts.roboto(
              color: Colors.red, fontSize: 14, fontWeight: FontWeight.bold),
        ),
      ),
      renderDoneBtn: Container(
        width: double.infinity,
        height: double.infinity,
        alignment: Alignment.center,
        decoration: BoxDecoration(
            color: Colors.red, borderRadius: BorderRadius.circular(5)),
        child: Text(
          "Get Started",
          style: GoogleFonts.roboto(
              color: Colors.white, fontSize: 12, fontWeight: FontWeight.bold),
        ),
      ),
      colorDot: Colors.red,
      sizeDot: 8.0,
      typeDotAnimation: dotSliderAnimation.SIZE_TRANSITION,
      listCustomTabs: this.renderListCustomTabs(),
      scrollPhysics: BouncingScrollPhysics(),
      hideStatusBar: false,
      onDonePress: () => Navigator.pushReplacement(
        context,
        MaterialPageRoute(
          builder: (_) => TermsOfUse(),
        ),
      ),
    );
  }
}
