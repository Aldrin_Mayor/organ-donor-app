import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:organ_donor_app/pages/Dashboard.dart';
import 'package:organ_donor_app/Service/Auth_Service.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';

class MakePolls extends StatefulWidget {
  MakePolls({Key? key}) : super(key: key);

  @override
  _MakePollsState createState() => _MakePollsState();
}

class _MakePollsState extends State<MakePolls> {
  String? title;
  String? donorFullName;
  String? patientFullName;
  String? donorUserType;
  String? patientUserType;

  AuthClass authClass = AuthClass();

  String? usersValue;
  String? textTitle;
  final formKey = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: Color(0xffe5eef8),
      body: Stack(
        children: [
          //Header
          Container(
            decoration: BoxDecoration(
              color: Color(0xffe5eef8),
              boxShadow: [
                BoxShadow(
                  color: Colors.grey.withOpacity(0.5),
                  blurRadius: 8,
                ),
              ],
            ),
            height: 80,
            margin: EdgeInsets.only(top: 0, left: 0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                InkWell(
                  onTap: () {
                    Navigator.of(context).pop();
                  },
                  child: Padding(
                    padding: const EdgeInsets.only(top: 26, left: 20),
                    child: Row(
                      children: [
                        Icon(Icons.close_outlined, size: 25.0),
                        Container(
                          margin: EdgeInsets.only(left: 20),
                          child: Text(
                            "Poll post",
                            style: GoogleFonts.roboto(
                              color: Colors.black87,
                              fontWeight: FontWeight.w500,
                              fontSize: 18,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                InkWell(
                  onTap: () {
                    final isValid = formKey.currentState!.validate();

                    if (isValid) {
                      formKey.currentState!.save();
                      Navigator.of(context).push(
                          MaterialPageRoute(builder: (context) => Dashboard()));
                    }
                  },
                  child: Padding(
                    padding: const EdgeInsets.only(top: 26, right: 20),
                    child: Text(
                      'Post',
                      style: GoogleFonts.roboto(
                        color: Colors.black54,
                        fontWeight: FontWeight.w500,
                        fontSize: 18,
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
          //Content
          Form(
            key: formKey,
            child: Column(
              children: [
                //Post Title
                Container(
                  margin: EdgeInsets.only(top: 150, left: 15, right: 15),
                  child: InkWell(
                    onTap: () {
                      if (usersValue == 'I am a Donor') {
                        showModalBottomSheet(
                          context: context,
                          builder: (context) => buildDonorTitleSheet(),
                        );
                      } else {
                        showModalBottomSheet(
                          context: context,
                          builder: (context) => buildPatientTitleSheet(),
                        );
                      }
                    },
                    child: TextFormField(
                      decoration: InputDecoration(
                          filled: true,
                          fillColor: Colors.white,
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10.0),
                            borderSide: BorderSide.none,
                          ),
                          errorStyle:
                              TextStyle(fontSize: 14, color: Colors.red[400]),
                          hintText: 'An Interesting Title'),
                      initialValue: title,
                      key: Key(title.toString()), // <- Magic!
                      maxLines: 2,
                      readOnly: true,
                      enabled: false,
                      validator: (value) {
                        if (value!.isEmpty) {
                          return 'This Section is Required';
                        } else {
                          return null;
                        }
                      },
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 15, right: 15),
                  child: Divider(color: Colors.grey),
                ),
                //Text Post
                Container(
                  margin: EdgeInsets.only(top: 10, left: 15, right: 15),
                  child: InkWell(
                    onTap: () {
                      showModalBottomSheet(
                        context: context,
                        builder: (context) => buildTextTitleSheet(),
                      );
                    },
                    child: TextFormField(
                      decoration: InputDecoration(
                          filled: true,
                          fillColor: Colors.white,
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10.0),
                            borderSide: BorderSide.none,
                          ),
                          hintText: 'Your text post (Optional)'),
                      initialValue: textTitle,
                      key: Key(textTitle.toString()), // <- Magic!
                      maxLines: 1,
                      enabled: false,
                    ),
                  ),
                ),
                // Checks if User is Donor or Patient
                FutureBuilder(
                  future: getUserData(),
                  builder: (context, snapshot) {
                    if (snapshot.connectionState != ConnectionState.done)
                      return Text("");
                    if (donorUserType != null) {
                      usersValue = donorUserType;
                      return Container(
                          height: 20,
                          width: 240,
                          child: Text(
                            '',
                            style: GoogleFonts.roboto(
                                color: Colors.black87,
                                fontSize: 15,
                                fontWeight: FontWeight.w500),
                          ));
                    }
                    if (patientUserType != null) {
                      usersValue = patientUserType;
                      return Container(
                          height: 20,
                          width: 240,
                          child: Text(
                            '',
                            style: GoogleFonts.roboto(
                                color: Colors.black87,
                                fontSize: 15,
                                fontWeight: FontWeight.w500),
                          ));
                    } else {
                      return Text(
                        "",
                        style: GoogleFonts.roboto(
                            color: Colors.black54, fontSize: 14),
                      );
                    }
                  },
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

// Bottom Sheet For Donor Title
  Widget buildDonorTitleSheet() => Container(
        height: 400,
        child: ListView(
          children: [
            //Header Patient Title
            ListTile(
              title: Row(
                children: [
                  Text(
                    'Choose a Title',
                    style: GoogleFonts.roboto(
                        color: Colors.black87,
                        fontSize: 15,
                        fontWeight: FontWeight.w500),
                  ),
                  Container(
                    margin: EdgeInsets.only(left: 10),
                    child: Icon(
                      Icons.select_all_outlined,
                      color: Colors.red,
                    ),
                  ),
                ],
              ),
            ),
            //Donor Selection
            ListTile(
              title: Container(
                padding: EdgeInsets.all(15),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5),
                    color: Colors.red[100]),
                child: Text(
                  'Donor: Are you a Kidney patient looking for a Kidney donor?',
                  style:
                      GoogleFonts.roboto(color: Colors.black87, fontSize: 15),
                ),
              ),
              onTap: () {
                setState(() {
                  title =
                      "[Donor]: Are you a Kidney patient looking for a Kidney donor? ";
                  Navigator.of(context).pop();
                });
              },
            ),
            ListTile(
              title: Container(
                padding: EdgeInsets.all(15),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5),
                    color: Colors.red[100]),
                child: Text(
                  'Donor: Are you a Kidney Patient on the waiting list?',
                  style:
                      GoogleFonts.roboto(color: Colors.black87, fontSize: 15),
                ),
              ),
              onTap: () {
                setState(() {
                  title =
                      "[Donor]: Are you a Kidney Patient on the waiting list?";
                  Navigator.of(context).pop();
                });
              },
            ),
            ListTile(
              title: Container(
                padding: EdgeInsets.all(15),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5),
                    color: Colors.red[100]),
                child: Text(
                  'Donor: What is your Blood Type?',
                  style:
                      GoogleFonts.roboto(color: Colors.black87, fontSize: 15),
                ),
              ),
              onTap: () {
                setState(() {
                  title = "[Donor]: Select your  Blood Type";
                  Navigator.of(context).pop();
                });
              },
            ),
            ListTile(
              title: Container(
                padding: EdgeInsets.all(15),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5),
                    color: Colors.red[100]),
                child: Text(
                  'Donor: How Large is your Kidney',
                  style:
                      GoogleFonts.roboto(color: Colors.black87, fontSize: 15),
                ),
              ),
              onTap: () {
                setState(() {
                  title = "[Donor]: How Large is your Kidney";
                  Navigator.of(context).pop();
                });
              },
            ),
            ListTile(
              title: Container(
                padding: EdgeInsets.all(15),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5),
                    color: Colors.red[100]),
                child: Text(
                  'Donor: Select your Age',
                  style:
                      GoogleFonts.roboto(color: Colors.black87, fontSize: 15),
                ),
              ),
              onTap: () {
                setState(() {
                  title = "[Donor]: Select your Age";
                  Navigator.of(context).pop();
                });
              },
            ),
          ],
        ),
      );

  // Bottom Sheet For Patient Title
  Widget buildPatientTitleSheet() => Container(
        height: 400,
        child: ListView(
          children: [
            //Header Patient Title
            ListTile(
              title: Row(
                children: [
                  Row(
                    children: [
                      Text(
                        'Choose a Title',
                        style: GoogleFonts.roboto(
                            color: Colors.black87,
                            fontSize: 15,
                            fontWeight: FontWeight.w500),
                      ),
                      Container(
                        margin: EdgeInsets.only(left: 10),
                        child: Icon(
                          Icons.select_all_outlined,
                          color: Colors.red,
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
            //Patient Selection
            ListTile(
              title: Container(
                padding: EdgeInsets.all(15),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5),
                    color: Colors.red[100]),
                child: Text(
                  'Patient: Are you a Kidney donor looking for a Kidney patient?',
                  style:
                      GoogleFonts.roboto(color: Colors.black87, fontSize: 15),
                ),
              ),
              onTap: () {
                setState(() {
                  title =
                      "[Patient]: Are you a Kidney donor looking for a Kidney patient?";
                  Navigator.of(context).pop();
                });
              },
            ),
            ListTile(
              title: Container(
                padding: EdgeInsets.all(15),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5),
                    color: Colors.red[100]),
                child: Text(
                  'Patient: What is your Blood Type?',
                  style:
                      GoogleFonts.roboto(color: Colors.black87, fontSize: 15),
                ),
              ),
              onTap: () {
                setState(() {
                  title = "[Patient]:  Select your  Blood Type";
                  Navigator.of(context).pop();
                });
              },
            ),
            ListTile(
              title: Container(
                padding: EdgeInsets.all(15),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5),
                    color: Colors.red[100]),
                child: Text(
                  'Patient:  How Large (cm) is your Kidney',
                  style:
                      GoogleFonts.roboto(color: Colors.black87, fontSize: 15),
                ),
              ),
              onTap: () {
                setState(() {
                  title = "[Patient]:  How Large (cm) is your Kidney";
                  Navigator.of(context).pop();
                });
              },
            ),
            ListTile(
              title: Container(
                padding: EdgeInsets.all(15),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5),
                    color: Colors.red[100]),
                child: Text(
                  'Patient:  Select your Age',
                  style:
                      GoogleFonts.roboto(color: Colors.black87, fontSize: 15),
                ),
              ),
              onTap: () {
                setState(() {
                  title = "[Patient]:  Select your Age";
                  Navigator.of(context).pop();
                });
              },
            ),
          ],
        ),
      );

  // Bottom Sheet For Text Title
  Widget buildTextTitleSheet() => Container(
        height: 250,
        child: ListView(
          children: [
            //Header Text Title
            ListTile(
              title: Row(
                children: [
                  Text(
                    'Choose a Message',
                    style: GoogleFonts.roboto(
                        color: Colors.black87,
                        fontSize: 15,
                        fontWeight: FontWeight.w500),
                  ),
                  Container(
                    margin: EdgeInsets.only(left: 10),
                    child: Icon(
                      Icons.message_outlined,
                      color: Colors.red,
                    ),
                  ),
                ],
              ),
            ),
            //Selection
            ListTile(
              title: Container(
                padding: EdgeInsets.all(15),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5),
                    color: Colors.red[100]),
                child: Text(
                  'For Kidney Donors Only',
                  style:
                      GoogleFonts.roboto(color: Colors.black87, fontSize: 15),
                ),
              ),
              onTap: () {
                setState(() {
                  textTitle = "Attention: For Kidney Donors Only";
                  Navigator.of(context).pop();
                });
              },
            ),
            ListTile(
              title: Container(
                padding: EdgeInsets.all(15),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5),
                    color: Colors.red[100]),
                child: Text(
                  'For Kidney Patients Only',
                  style:
                      GoogleFonts.roboto(color: Colors.black87, fontSize: 15),
                ),
              ),
              onTap: () {
                setState(() {
                  textTitle = "Attention: For Kidney Patients Only";
                  Navigator.of(context).pop();
                });
              },
            ),
            ListTile(
              title: Container(
                padding: EdgeInsets.all(15),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5),
                    color: Colors.red[100]),
                child: Text(
                  'For Kidney Donor and Patients',
                  style:
                      GoogleFonts.roboto(color: Colors.black87, fontSize: 15),
                ),
              ),
              onTap: () {
                setState(() {
                  textTitle = "Attention: For Kidney Donor and Patients ";
                  Navigator.of(context).pop();
                });
              },
            ),
          ],
        ),
      );

  //Get User Information
  getUserData() async {
    final donor = FirebaseAuth.instance.currentUser;
    if (donor != null) {
      await FirebaseFirestore.instance
          .collection('Users')
          .doc("Donor")
          .collection("Lists")
          .doc(donor.uid)
          .get()
          .then((value) {
        donorFullName = value.data()!['FullName'];
        donorUserType = value.data()!['User Type'];
        print(donorFullName);
        print(donorUserType);
      }).catchError((e) {
        print(e);
      });
    }

    final patient = FirebaseAuth.instance.currentUser;
    if (patient != null) {
      await FirebaseFirestore.instance
          .collection('Users')
          .doc("Patient")
          .collection("Lists")
          .doc(patient.uid)
          .get()
          .then((value) {
        patientFullName = value.data()!['FullName'];
        patientUserType = value.data()!['User Type'];
        print(patientFullName);
        print(patientUserType);
      }).catchError((e) {
        print(e);
      });
    }
  }
}
