import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class ViewMailPage extends StatefulWidget {
  final String? getEmailFrom;
  final String? getEmailTo;
  final String? getEmailSubject;
  final String? getEmailBody;
  final String? getEmailDate;
  final String? getEmailTime;

  ViewMailPage({
    required this.getEmailFrom,
    required this.getEmailTo,
    required this.getEmailSubject,
    required this.getEmailBody,
    required this.getEmailDate,
    required this.getEmailTime,
  });

  @override
  _ViewMailPageState createState() => _ViewMailPageState();
}

class _ViewMailPageState extends State<ViewMailPage> {
  String? getEmailFrom;
  String? getEmailTo;
  String? getEmailSubject;
  String? getEmailBody;
  String? getEmailDate;
  String? getEmailTime;

  _ViewMailPageState({
    this.getEmailFrom,
    this.getEmailTo,
    this.getEmailSubject,
    this.getEmailBody,
    this.getEmailDate,
    this.getEmailTime,
  });

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xffe5eef8),
      body: Stack(
        children: [
          //Header
          Container(
            decoration: BoxDecoration(
              color: Color(0xffe5eef8),
              boxShadow: [
                BoxShadow(
                  color: Colors.grey.withOpacity(0.5),
                  blurRadius: 8,
                ),
              ],
            ),
            height: 80,
            margin: EdgeInsets.only(top: 0, left: 0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                InkWell(
                  onTap: () {
                    Navigator.of(context).pop();
                  },
                  child: Padding(
                    padding: const EdgeInsets.only(top: 26, left: 20),
                    child: Row(
                      children: [
                        Icon(Icons.arrow_back_outlined, size: 25.0),
                        Container(
                          margin: EdgeInsets.only(left: 20),
                          child: Text(
                            "Hello! " + widget.getEmailTo!,
                            style: GoogleFonts.roboto(
                              color: Colors.black87,
                              fontWeight: FontWeight.w500,
                              fontSize: 18,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                margin: EdgeInsets.only(left: 30, top: 100, right: 30),
                child: Text(
                  widget.getEmailSubject!,
                  style: GoogleFonts.roboto(
                      color: Colors.black87,
                      fontWeight: FontWeight.w400,
                      fontSize: 20,
                      letterSpacing: 2),
                ),
              ),
              Row(
                children: [
                  Container(
                    margin: EdgeInsets.only(top: 30, left: 20),
                    child: Image.asset(
                      'assets/logo.png',
                      height: 80,
                    ),
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        children: [
                          Container(
                            margin: EdgeInsets.only(left: 10, top: 30),
                            child: Text(
                              widget.getEmailFrom!,
                              style: GoogleFonts.roboto(
                                  color: Colors.black87,
                                  fontWeight: FontWeight.w500,
                                  fontSize: 16,
                                  letterSpacing: 1),
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.only(left: 5, top: 30),
                            child: Text(
                              widget.getEmailDate!,
                              style: GoogleFonts.roboto(
                                  color: Colors.black54,
                                  fontWeight: FontWeight.w400,
                                  fontSize: 12,
                                  letterSpacing: 1),
                            ),
                          )
                        ],
                      ),
                      Row(
                        children: [
                          Container(
                            margin: EdgeInsets.only(left: 10, top: 5),
                            child: Text('to me',
                                style: GoogleFonts.roboto(
                                    color: Colors.black54,
                                    fontWeight: FontWeight.w400,
                                    fontSize: 12,
                                    letterSpacing: 1)),
                          ),
                          Container(
                            margin: EdgeInsets.only(top: 5),
                            child: Icon(Icons.arrow_drop_down),
                          )
                        ],
                      ),
                    ],
                  )
                ],
              ),
              Container(
                margin: EdgeInsets.only(left: 20, right: 20, top: 30),
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(10)),
                child: Padding(
                  padding: const EdgeInsets.all(20),
                  child: Text(widget.getEmailBody!,
                      textAlign: TextAlign.justify,
                      style: GoogleFonts.roboto(
                          color: Colors.black87,
                          fontWeight: FontWeight.w400,
                          fontSize: 14,
                          letterSpacing: 1)),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
