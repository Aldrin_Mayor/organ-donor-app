import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:organ_donor_app/pages/AboutPage.dart';

class ForumsPrivacyPolicy extends StatefulWidget {
  ForumsPrivacyPolicy({Key? key}) : super(key: key);

  @override
  _ForumsPrivacyPolicyState createState() => _ForumsPrivacyPolicyState();
}

class _ForumsPrivacyPolicyState extends State<ForumsPrivacyPolicy> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xffe5eef8),
      body: Stack(
        children: [
          //Middle Content
          SingleChildScrollView(
            child: Container(
              margin: EdgeInsets.only(top: 90, bottom: 40),
              child: Center(
                child: Column(
                  children: [
                    Container(
                      margin: EdgeInsets.only(left: 15, right: 15),
                      child: SelectableText.rich(
                        TextSpan(
                          children: [
                            TextSpan(
                              text:
                                  "To protect users' privacy rights while using our services, we have specially formulated this Privacy Policy to explain how we collect, use, store, and share your personal information when using the Organ Donor App services, as well as what rights you have over your data when you accept the Organ Donor App services, regardless of whether the data is voluntarily provided by you or collected by us. This Privacy Statement applies to all the services we offer. We will process and safeguard your personal information when you use our services in line with our Privacy Policy.",
                              style: GoogleFonts.roboto(
                                  color: Colors.black87,
                                  fontSize: 14,
                                  height: 1.7),
                            ),
                            TextSpan(
                              children: [
                                TextSpan(
                                  text:
                                      "\n\nTo open an Organ Donor App user account, you must be a valid donor or patient.",
                                  style: GoogleFonts.roboto(
                                      color: Colors.black87,
                                      fontSize: 14,
                                      height: 1.7),
                                ),
                                TextSpan(
                                  text:
                                      "\n\nBy using our application, you indicate that you have read, understood, and agreed to the terms listed below. If you do not agree with any of the conditions, you may discontinue using our services immediately.",
                                  style: GoogleFonts.roboto(
                                      color: Colors.black87,
                                      fontSize: 14,
                                      height: 1.7),
                                ),
                                TextSpan(
                                  text:
                                      "\n\nWe retain the right to alter, adjust, or revise the terms of this Privacy Policy at any time. We will notify you of any revised conditions to get your permission once more. You agree to examine these conditions on a frequent basis, as they may be amended from time to time. Your continued use of Organ Donor App services indicates your agreement to how we will collect, process, and use your personal data in accordance with the updated Privacy Policy.",
                                  style: GoogleFonts.roboto(
                                      color: Colors.black87,
                                      fontSize: 14,
                                      height: 1.7),
                                ),
                                TextSpan(
                                  text: "\n\n1.	What do we Collect and Process",
                                  style: GoogleFonts.roboto(
                                      color: Colors.black87,
                                      fontSize: 14,
                                      height: 1.7,
                                      fontWeight: FontWeight.w700),
                                ),
                                TextSpan(
                                  text:
                                      "\n\n1) Personal information that you provide us",
                                  style: GoogleFonts.roboto(
                                      color: Colors.black87,
                                      fontSize: 14,
                                      height: 1.7),
                                ),
                                TextSpan(
                                  text:
                                      "\n\nWe will collect content, communications, and other information that you give when you use our products, including information supplied when you register for an account, create, or share content, and communicate with others. This includes information contained in or connected to the content you supply (such as metadata), such as the date a file was generated. It also includes content that you see or utilize through the functionalities we provide. You certify that if you choose to provide any personal data, your contribution is subject to this Privacy Policy, and you agree to its conditions. ",
                                  style: GoogleFonts.roboto(
                                      color: Colors.black87,
                                      fontSize: 14,
                                      height: 1.7),
                                ),
                                TextSpan(
                                  text:
                                      "\n\n\t\t\t\t\tFullname (Optional)\n\t\t\t\t\tEmail Address(Optional)\n\t\t\t\t\tMobile Number (Required)\n\t\t\t\t\tAddress (Required)\n\t\t\t\t\tGender (Required)\n\t\t\t\t\tAge (Required)\n\t\t\t\t\tKidney Size (Required)\n\t\t\t\t\tBlood Type (Required)",
                                  style: GoogleFonts.roboto(
                                      color: Colors.black87,
                                      fontSize: 14,
                                      height: 1.7),
                                ),
                                TextSpan(
                                  text: "\n\n2)	Data we collect automatically",
                                  style: GoogleFonts.roboto(
                                      color: Colors.black87,
                                      fontSize: 14,
                                      height: 1.7),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ),
                    //Number 1.2 Content
                    Container(
                      margin: EdgeInsets.only(left: 45, right: 15),
                      child: SelectableText.rich(
                        TextSpan(
                          children: [
                            TextSpan(
                              text:
                                  "\ni)	Log data includes your IP address, browser type, operating system, website being used, reference web pages, pages visited, location, your mobile network operator, device information, search keyword, etc. and Cookie’s information.",
                              style: GoogleFonts.roboto(
                                  color: Colors.black87,
                                  fontSize: 14,
                                  height: 1.7),
                            ),
                            TextSpan(
                              children: [
                                TextSpan(
                                  text: "\n\nii)	Time zone information",
                                  style: GoogleFonts.roboto(
                                      color: Colors.black87,
                                      fontSize: 14,
                                      height: 1.7),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ),
                    //Number 1.3 Title
                    Container(
                      alignment: Alignment.topLeft,
                      margin: EdgeInsets.only(left: 15, right: 15),
                      child: SelectableText.rich(
                        TextSpan(
                          children: [
                            TextSpan(
                              text: "\n3) Data Shared by Third Parties",
                              style: GoogleFonts.roboto(
                                  color: Colors.black87,
                                  fontSize: 14,
                                  height: 1.7),
                            ),
                          ],
                        ),
                      ),
                    ),
                    //Number 1.3 Content
                    Container(
                      margin: EdgeInsets.only(left: 45, right: 15),
                      child: SelectableText.rich(
                        TextSpan(
                          children: [
                            TextSpan(
                              text: "\ni) Third-party SDK data",
                              style: GoogleFonts.roboto(
                                  color: Colors.black87,
                                  fontSize: 14,
                                  height: 1.7),
                            ),
                            TextSpan(
                              children: [
                                TextSpan(
                                  text:
                                      "\n\nFirebase. Google is a third-party SDK operator that assists us in gathering the information that we require in our application as well as some secondary information.",
                                  style: GoogleFonts.roboto(
                                      color: Colors.black87,
                                      fontSize: 14,
                                      height: 1.7),
                                ),
                              ],
                            ),
                            TextSpan(
                              children: [
                                TextSpan(
                                  text:
                                      "\n\n2)	Third-party data is used for advertising and research to provide you with better services.",
                                  style: GoogleFonts.roboto(
                                      color: Colors.black87,
                                      fontSize: 14,
                                      height: 1.7),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ),
                    //Number 2 Title
                    Container(
                      alignment: Alignment.topLeft,
                      margin: EdgeInsets.only(left: 15, right: 15),
                      child: SelectableText.rich(
                        TextSpan(
                          children: [
                            TextSpan(
                              text:
                                  "\n2. Why do We Collect Some of Your Personal Data",
                              style: GoogleFonts.roboto(
                                  color: Colors.black87,
                                  fontSize: 14,
                                  height: 1.7,
                                  fontWeight: FontWeight.w700),
                            ),
                          ],
                        ),
                      ),
                    ),
                    //Number 2 Content
                    Container(
                      margin: EdgeInsets.only(left: 15, right: 15),
                      child: SelectableText.rich(
                        TextSpan(
                          children: [
                            TextSpan(
                              text:
                                  "\nWe gather and process personal data for the following objectives in order to provide you with high-quality services. If you do not submit some data necessary for services, you may be unable to create a user account, some or all of the goods or services we offer will be unavailable, or the services may not work properly.",
                              style: GoogleFonts.roboto(
                                  color: Colors.black87,
                                  fontSize: 14,
                                  height: 1.7),
                            ),
                            TextSpan(
                              children: [
                                TextSpan(
                                  text:
                                      "\n\n1)	To deliver, personalize, and improve our goods.",
                                  style: GoogleFonts.roboto(
                                      color: Colors.black87,
                                      fontSize: 14,
                                      height: 1.7),
                                ),
                              ],
                            ),
                            TextSpan(
                              children: [
                                TextSpan(
                                  text:
                                      "\n2) To improve the safety and security of our services.",
                                  style: GoogleFonts.roboto(
                                      color: Colors.black87,
                                      fontSize: 14,
                                      height: 1.7),
                                ),
                              ],
                            ),
                            TextSpan(
                              children: [
                                TextSpan(
                                  text:
                                      "\n3) To interact with you and send out surveys",
                                  style: GoogleFonts.roboto(
                                      color: Colors.black87,
                                      fontSize: 14,
                                      height: 1.7),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ),
                    //Number 3 Title
                    Container(
                      alignment: Alignment.topLeft,
                      margin: EdgeInsets.only(left: 15, right: 15),
                      child: SelectableText.rich(
                        TextSpan(
                          children: [
                            TextSpan(
                              text:
                                  "\n\n3.	The Legal Basis of Our Data Collection and Processing",
                              style: GoogleFonts.roboto(
                                  color: Colors.black87,
                                  fontSize: 14,
                                  height: 1.7,
                                  fontWeight: FontWeight.w700),
                            ),
                          ],
                        ),
                      ),
                    ),
                    //Number 3 Content
                    Container(
                      margin: EdgeInsets.only(left: 45, right: 15),
                      child: SelectableText.rich(
                        TextSpan(
                          children: [
                            TextSpan(
                              text: "\n●",
                              style: GoogleFonts.roboto(
                                  color: Colors.black87,
                                  fontSize: 10,
                                  height: 1.7),
                            ),
                            TextSpan(
                              children: [
                                TextSpan(
                                  text:
                                      "\tRequired for us to meet the terms of our service agreement.",
                                  style: GoogleFonts.roboto(
                                      color: Colors.black87,
                                      fontSize: 14,
                                      height: 1.7),
                                ),
                              ],
                            ),
                            TextSpan(
                              text: "\n●",
                              style: GoogleFonts.roboto(
                                  color: Colors.black87,
                                  fontSize: 10,
                                  height: 1.7),
                            ),
                            TextSpan(
                              children: [
                                TextSpan(
                                  text:
                                      "\tRequired for us to fulfill our legal duties",
                                  style: GoogleFonts.roboto(
                                      color: Colors.black87,
                                      fontSize: 14,
                                      height: 1.7),
                                ),
                              ],
                            ),
                            TextSpan(
                              text: "\n●",
                              style: GoogleFonts.roboto(
                                  color: Colors.black87,
                                  fontSize: 10,
                                  height: 1.7),
                            ),
                            TextSpan(
                              children: [
                                TextSpan(
                                  text:
                                      "\tWe must defend your rights as well as the rights of others",
                                  style: GoogleFonts.roboto(
                                      color: Colors.black87,
                                      fontSize: 14,
                                      height: 1.7),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ),
                    //Number 4
                    Container(
                      alignment: Alignment.topLeft,
                      margin: EdgeInsets.only(left: 15, right: 15),
                      child: SelectableText.rich(
                        TextSpan(
                          children: [
                            TextSpan(
                              text: "\n\n4.	Security of Your Personal Data",
                              style: GoogleFonts.roboto(
                                  color: Colors.black87,
                                  fontSize: 14,
                                  height: 1.7,
                                  fontWeight: FontWeight.w700),
                            ),
                            TextSpan(
                              text:
                                  "\n\nThe security of Your Personal Data is important to Us, but remember that no method of transmission over the Internet, or method of electronic storage is 100% secure. While We strive to use commercially acceptable means to protect Your Personal Data, We cannot guarantee its absolute security.",
                              style: GoogleFonts.roboto(
                                color: Colors.black87,
                                fontSize: 14,
                                height: 1.7,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    //Number 5
                    Container(
                      alignment: Alignment.topLeft,
                      margin: EdgeInsets.only(left: 15, right: 15),
                      child: SelectableText.rich(
                        TextSpan(
                          children: [
                            TextSpan(
                              text: "\n\n5.	Contact Us",
                              style: GoogleFonts.roboto(
                                  color: Colors.black87,
                                  fontSize: 14,
                                  height: 1.7,
                                  fontWeight: FontWeight.w700),
                            ),
                            TextSpan(
                              text:
                                  "\n\nIn order to secure the personal information of users. Please contact us if you have any concerns about our Privacy Policy or if you have any requests for addressing issues with your personal data. You can us the developers at any of our email:",
                              style: GoogleFonts.roboto(
                                color: Colors.black87,
                                fontSize: 14,
                                height: 1.7,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    //End Title
                    Container(
                      alignment: Alignment.center,
                      margin: EdgeInsets.only(left: 15, right: 15),
                      child: SelectableText.rich(
                        TextSpan(
                          children: [
                            TextSpan(
                              text: "\n\nDevelopers:",
                              style: GoogleFonts.roboto(
                                  color: Colors.black87,
                                  fontSize: 14,
                                  height: 1.7,
                                  fontWeight: FontWeight.w600),
                            ),
                          ],
                        ),
                      ),
                    ),
                    //End Line
                    Container(
                      alignment: Alignment.center,
                      margin: EdgeInsets.only(left: 15, right: 15, bottom: 40),
                      child: SelectableText.rich(
                        TextSpan(
                          children: [
                            TextSpan(
                              text: "Espiritu, Justin Lloyd:",
                              style: GoogleFonts.roboto(
                                color: Colors.black87,
                                fontSize: 14,
                                height: 1.7,
                              ),
                            ),
                            TextSpan(
                              text: "\t\t\t\t\t\t\t\t\tmjlespiritu@tip.edu.ph",
                              style: GoogleFonts.roboto(
                                color: Colors.red,
                                fontSize: 14,
                                height: 1.7,
                              ),
                            ),
                            TextSpan(
                              text: "\nMayor, Aldrin:",
                              style: GoogleFonts.roboto(
                                color: Colors.black87,
                                fontSize: 14,
                                height: 1.7,
                              ),
                            ),
                            TextSpan(
                              text:
                                  "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tmadmayor@tip.edu.ph",
                              style: GoogleFonts.roboto(
                                color: Colors.red,
                                fontSize: 14,
                                height: 1.7,
                              ),
                            ),
                            TextSpan(
                              text: "\nRivera, Debbie Cheyenne:",
                              style: GoogleFonts.roboto(
                                color: Colors.black87,
                                fontSize: 14,
                                height: 1.7,
                              ),
                            ),
                            TextSpan(
                              text: "\t\t\t\tmdcarivera@tip.edu.ph",
                              style: GoogleFonts.roboto(
                                color: Colors.red,
                                fontSize: 14,
                                height: 1.7,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
          //Header
          Container(
            decoration: BoxDecoration(color: Color(0xffe5eef8)),
            height: 80,
            margin: EdgeInsets.only(top: 0, left: 0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                InkWell(
                  onTap: () {
                    Navigator.pushAndRemoveUntil(
                        context,
                        MaterialPageRoute(builder: (builder) => AboutPage()),
                        (route) => false);
                  },
                  child: Padding(
                    padding: const EdgeInsets.only(top: 20, left: 20),
                    child: Icon(Icons.arrow_back_ios_outlined, size: 25.0),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 20),
                  child: Text(
                    "Organ Donor App Privacy Policy",
                    style: GoogleFonts.roboto(
                        color: Colors.black87,
                        fontWeight: FontWeight.w400,
                        fontSize: 18),
                  ),
                ),
                Container(width: 32.0, height: 0.0),
              ],
            ),
          ),
          //Bottom Button
          Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              Container(
                decoration: BoxDecoration(
                  boxShadow: [BoxShadow(color: Colors.grey.withOpacity(0.5))],
                  color: Color(0xffe5eef8),
                ),
                padding: EdgeInsets.only(top: 10, bottom: 10),
                child: Column(
                  children: [
                    Container(
                      height: 30,
                      width: double.infinity,
                      padding: EdgeInsets.only(
                        left: 135,
                        right: 135,
                        top: 3,
                        bottom: 3,
                      ),
                      child: InkWell(
                        onTap: () {
                          Navigator.pushAndRemoveUntil(
                              context,
                              MaterialPageRoute(
                                  builder: (builder) => AboutPage()),
                              (route) => false);
                        },
                        child: Container(
                          decoration: BoxDecoration(
                              color: Colors.red[100],
                              borderRadius: BorderRadius.circular(18)),
                          child: Center(
                            child: Text(
                              "Back",
                              style: TextStyle(
                                  color: Colors.red[400], fontSize: 17),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
