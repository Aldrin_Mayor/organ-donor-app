import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:organ_donor_app/pages/DashboardProfilePage.dart';
import 'package:organ_donor_app/Service/Auth_Service.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:fluttertoast/fluttertoast.dart';

class ProfileInformation extends StatefulWidget {
  ProfileInformation({Key? key}) : super(key: key);

  @override
  _ProfileInformationState createState() => _ProfileInformationState();
}

class _ProfileInformationState extends State<ProfileInformation> {
  // Update Database
  final firestoreInstance = FirebaseFirestore.instance;

//FireStore
//holds Patient Document
  updatePatient() async {
    User? user = FirebaseAuth.instance.currentUser;
    await FirebaseFirestore.instance
        .collection("Users")
        .doc("Patient")
        .collection("Lists")
        .doc(user!.uid)
        .update({
      'FullName': _fullName.text,
      'UserName': _userName.text,
      'Gender': _gender.text,
      'Age': _age.text,
      'Blood Type': _bloodType.text,
      'Kidney Size': _kidneySize.text,
      'Signature': _signature.text,
    });
  }

  //holds Patient Match Document
  updatePatientMatch() async {
    User? user = FirebaseAuth.instance.currentUser;
    await FirebaseFirestore.instance
        .collection("Match Users")
        .doc("Patient")
        .collection("Lists")
        .doc(user!.uid)
        .update({
      'FullName': _fullName.text,
      'UserName': _userName.text,
      'Gender': _gender.text,
      'Age': _age.text,
      'Blood Type': _bloodType.text,
      'Kidney Size': _kidneySize.text,
      'Signature': _signature.text,
    });
  }

  //holds Donor Document
  updateDonor() async {
    User? user = FirebaseAuth.instance.currentUser;
    await FirebaseFirestore.instance
        .collection("Users")
        .doc("Donor")
        .collection("Lists")
        .doc(user!.uid)
        .update({
      'FullName': _fullName.text,
      'UserName': _userName.text,
      'Gender': _gender.text,
      'Age': _age.text,
      'Blood Type': _bloodType.text,
      'Kidney Size': _kidneySize.text,
      'Signature': _signature.text,
    });
  }

  //holds Donor Match Document
  updateDonorMatch() async {
    User? user = FirebaseAuth.instance.currentUser;
    await FirebaseFirestore.instance
        .collection("Match Users")
        .doc("Donor")
        .collection("Lists")
        .doc(user!.uid)
        .update({
      'FullName': _fullName.text,
      'UserName': _userName.text,
      'Gender': _gender.text,
      'Age': _age.text,
      'Blood Type': _bloodType.text,
      'Kidney Size': _kidneySize.text,
      'Signature': _signature.text,
    });
  }

  //All Donor Profile Document
  updateAllDonor() async {
    User? user = FirebaseAuth.instance.currentUser;
    await FirebaseFirestore.instance
        .collection('Users')
        .doc('All')
        .collection('Lists')
        .doc(user!.uid)
        .update({
      'FullName': _fullName.text,
      'UserName': _userName.text,
      'Gender': _gender.text,
      'Age': _age.text,
      'Blood Type': _bloodType.text,
      'Kidney Size': _kidneySize.text,
      'Signature': _signature.text,
    });
  }

  //All Donor Match Profile Document
  updateAllDonorMatch() async {
    User? user = FirebaseAuth.instance.currentUser;
    await FirebaseFirestore.instance
        .collection('Match Users')
        .doc('All')
        .collection('Lists')
        .doc(user!.uid)
        .update({
      'FullName': _fullName.text,
      'UserName': _userName.text,
      'Gender': _gender.text,
      'Age': _age.text,
      'Blood Type': _bloodType.text,
      'Kidney Size': _kidneySize.text,
      'Signature': _signature.text,
    });
  }

  //All Patient Document
  updateAllPatient() async {
    User? user = FirebaseAuth.instance.currentUser;
    await FirebaseFirestore.instance
        .collection('Users')
        .doc('All')
        .collection('Lists')
        .doc(user!.uid)
        .update({
      'FullName': _fullName.text,
      'UserName': _userName.text,
      'Gender': _gender.text,
      'Age': _age.text,
      'Blood Type': _bloodType.text,
      'Kidney Size': _kidneySize.text,
      'Signature': _signature.text,
    });
  }

  //All Patient Match Document
  updateAllPatientMatch() async {
    User? user = FirebaseAuth.instance.currentUser;
    await FirebaseFirestore.instance
        .collection('Match Users')
        .doc('All')
        .collection('Lists')
        .doc(user!.uid)
        .update({
      'FullName': _fullName.text,
      'UserName': _userName.text,
      'Gender': _gender.text,
      'Age': _age.text,
      'Blood Type': _bloodType.text,
      'Kidney Size': _kidneySize.text,
      'Signature': _signature.text,
    });
  }

  String? donorFullName;
  String? donorUserName;
  String? donorGender;
  String? donorUserType;
  String? donorSignature;
  String? donorAge;
  String? donorBloodType;
  String? donorKidneySize;

  String? patientFullName;
  String? patientUserName;
  String? patientGender;
  String? patientUserType;
  String? patientSignature;
  String? patientAge;
  String? patientBloodType;
  String? patientKidneySize;

  AuthClass authClass = AuthClass();

  String? usersValue;

  TextEditingController _fullName = TextEditingController();
  TextEditingController _userName = TextEditingController();
  TextEditingController _gender = TextEditingController();
  TextEditingController _age = TextEditingController();
  TextEditingController _kidneySize = TextEditingController();
  TextEditingController _bloodType = TextEditingController();
  TextEditingController _signature = TextEditingController();

  //Toast Message

  final toast = FToast();

  @override
  void initState() {
    super.initState();

    toast.init(context);
  }

  bool isButtonEnabled = false;
  bool isFieldEnabled = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: Color(0xffe5eef8),
      body: Stack(
        children: [
          //Content
          SingleChildScrollView(
            child: Column(
              children: [
                Container(
                  margin: EdgeInsets.only(top: 30),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Container(
                        padding: EdgeInsets.all(2),
                        decoration: BoxDecoration(
                            shape: BoxShape.circle, color: Colors.red[100]),
                        margin: EdgeInsets.only(top: 100),
                        child: Image.asset(
                          'assets/user.png',
                          height: 100,
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(
                      top: 20, left: 15, right: 15, bottom: 300),
                  height: 580,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    boxShadow: [
                      BoxShadow(
                        color: Colors.grey.withOpacity(0.5),
                      ),
                    ],
                    color: Colors.white,
                  ),
                  padding: EdgeInsets.only(top: 10, bottom: 10),
                  child: Column(
                    children: [
                      //Donor and Patient FullName
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Container(
                            margin: EdgeInsets.only(top: 20, left: 20),
                            child: Text(
                              'Identifier',
                              style: GoogleFonts.roboto(
                                  color: Colors.black54, fontSize: 14),
                            ),
                          ),
                          //Edit Button
                          InkWell(
                            onTap: () {
                              setState(() {
                                isButtonEnabled = true;
                                isFieldEnabled = true;
                                showEditToast();
                              });
                            },
                            child: Container(
                              margin: EdgeInsets.only(left: 170),
                              child: Icon(
                                Icons.edit,
                                color: Colors.red[400],
                              ),
                            ),
                          ),

                          //Cancel Button
                          Container(
                            margin: EdgeInsets.only(right: 10),
                            child: IconButton(
                              onPressed: isButtonEnabled
                                  ? () {
                                      setState(() {
                                        isFieldEnabled = false;
                                        isButtonEnabled = false;
                                        showCancelToast();
                                      });
                                    }
                                  : null,
                              icon: Icon(
                                Icons.cancel_outlined,
                                color: Colors.red[400],
                              ),
                            ),
                          )
                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Container(
                            margin: EdgeInsets.only(right: 20, left: 20),
                            height: 32,
                            child: FutureBuilder(
                              future: getUserData(),
                              builder: (context, snapshot) {
                                if (snapshot.connectionState !=
                                    ConnectionState.done) return Text("");
                                if (donorFullName != null) {
                                  usersValue = donorUserType;
                                  return Container(
                                    height: 20,
                                    width: 240,
                                    child: TextFormField(
                                      controller: _fullName
                                        ..text = donorFullName!,
                                      enabled: isFieldEnabled,
                                      style: GoogleFonts.roboto(
                                          color: Colors.black54, fontSize: 14),
                                    ),
                                  );
                                }
                                if (patientFullName != null) {
                                  usersValue = patientUserType;
                                  return Container(
                                    height: 20,
                                    width: 240,
                                    child: TextFormField(
                                      controller: _fullName
                                        ..text = patientFullName!,
                                      enabled: isFieldEnabled,
                                      style: GoogleFonts.roboto(
                                          color: Colors.black54, fontSize: 14),
                                    ),
                                  );
                                } else {
                                  return Text(
                                    "null",
                                    style: GoogleFonts.roboto(
                                        color: Colors.black54, fontSize: 14),
                                  );
                                }
                              },
                            ),
                          ),
                        ],
                      ),
                      //Donor or Patient User Name
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Container(
                            margin: EdgeInsets.only(top: 20, left: 20),
                            child: Text(
                              'User Name',
                              style: GoogleFonts.roboto(
                                  color: Colors.black54, fontSize: 14),
                            ),
                          ),
                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Container(
                            margin: EdgeInsets.only(right: 20, left: 20),
                            height: 32,
                            child: FutureBuilder(
                              future: getUserData(),
                              builder: (context, snapshot) {
                                if (snapshot.connectionState !=
                                    ConnectionState.done) return Text("");
                                if (donorUserName != null) {
                                  usersValue = donorUserType;
                                  return Container(
                                    height: 20,
                                    width: 240,
                                    child: TextFormField(
                                      controller: _userName
                                        ..text = donorUserName!,
                                      enabled: isFieldEnabled,
                                      style: GoogleFonts.roboto(
                                          color: Colors.black54, fontSize: 14),
                                    ),
                                  );
                                }
                                if (patientUserName != null) {
                                  usersValue = patientUserType;
                                  return Container(
                                    height: 20,
                                    width: 240,
                                    child: TextFormField(
                                      controller: _userName
                                        ..text = patientUserName!,
                                      enabled: isFieldEnabled,
                                      style: GoogleFonts.roboto(
                                          color: Colors.black54, fontSize: 14),
                                    ),
                                  );
                                } else {
                                  return Text(
                                    "null",
                                    style: GoogleFonts.roboto(
                                        color: Colors.black54, fontSize: 14),
                                  );
                                }
                              },
                            ),
                          ),
                        ],
                      ),
                      //Donor or Patient Gender
                      InkWell(
                        onTap: isButtonEnabled
                            ? () {
                                showModalBottomSheet(
                                  context: context,
                                  builder: (context) => buildGenderSheet(),
                                );
                              }
                            : null,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Container(
                                  margin: EdgeInsets.only(top: 20, left: 20),
                                  child: Text(
                                    'Gender',
                                    style: GoogleFonts.roboto(
                                        color: Colors.black54, fontSize: 14),
                                  ),
                                ),
                                Container(
                                  margin: EdgeInsets.only(top: 5, left: 20),
                                  child: FutureBuilder(
                                    future: getUserData(),
                                    builder: (context, snapshot) {
                                      if (snapshot.connectionState !=
                                          ConnectionState.done) return Text("");
                                      if (donorGender != null) {
                                        usersValue = donorUserType;
                                        return Container(
                                          height: 20,
                                          width: 240,
                                          child: TextFormField(
                                            controller: _gender
                                              ..text = donorGender!,
                                            enabled: false,
                                            style: GoogleFonts.roboto(
                                                color: Colors.black54,
                                                fontSize: 14),
                                          ),
                                        );
                                      }
                                      if (patientGender != null) {
                                        usersValue = patientUserType;
                                        return Container(
                                          height: 20,
                                          width: 240,
                                          child: TextFormField(
                                            controller: _gender
                                              ..text = patientGender!,
                                            enabled: false,
                                            style: GoogleFonts.roboto(
                                                color: Colors.black54,
                                                fontSize: 14),
                                          ),
                                        );
                                      } else {
                                        return Text(
                                          "null",
                                          style: GoogleFonts.roboto(
                                              color: Colors.black54,
                                              fontSize: 14),
                                        );
                                      }
                                    },
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                      //Donor and Patient Age
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Container(
                            margin: EdgeInsets.only(top: 20, left: 20),
                            child: Text(
                              'Age',
                              style: GoogleFonts.roboto(
                                  color: Colors.black54, fontSize: 14),
                            ),
                          ),
                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Container(
                            margin: EdgeInsets.only(right: 20, left: 20),
                            height: 32,
                            child: FutureBuilder(
                              future: getUserData(),
                              builder: (context, snapshot) {
                                if (snapshot.connectionState !=
                                    ConnectionState.done) return Text("");
                                if (donorAge != null) {
                                  usersValue = donorUserType;
                                  return Container(
                                    height: 20,
                                    width: 240,
                                    child: TextFormField(
                                      controller: _age..text = donorAge!,
                                      enabled: isFieldEnabled,
                                      keyboardType: TextInputType.number,
                                      maxLength: 2,
                                      maxLengthEnforcement:
                                          MaxLengthEnforcement.enforced,
                                      decoration: InputDecoration(
                                        hintText:
                                            "Determines age of Donor's Kidney",
                                        counterText: '',
                                      ),
                                      style: GoogleFonts.roboto(
                                          color: Colors.black54, fontSize: 14),
                                    ),
                                  );
                                }
                                if (patientAge != null) {
                                  usersValue = patientUserType;
                                  return Container(
                                    height: 20,
                                    width: 240,
                                    child: TextFormField(
                                      controller: _age..text = patientAge!,
                                      enabled: isFieldEnabled,
                                      keyboardType: TextInputType.number,
                                      maxLength: 2,
                                      decoration: InputDecoration(
                                          hintText:
                                              "Determines age of Patient's Kidney",
                                          counterText: ''),
                                      style: GoogleFonts.roboto(
                                          color: Colors.black54, fontSize: 14),
                                    ),
                                  );
                                } else {
                                  return Text(
                                    "null",
                                    style: GoogleFonts.roboto(
                                        color: Colors.black54, fontSize: 14),
                                  );
                                }
                              },
                            ),
                          ),
                        ],
                      ),
                      // User Type
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Container(
                            margin: EdgeInsets.only(top: 20, left: 20),
                            child: Text(
                              'Identifier Type',
                              style: GoogleFonts.roboto(
                                  color: Colors.black54, fontSize: 14),
                            ),
                          ),
                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Container(
                            margin: EdgeInsets.only(top: 5, left: 20),
                            child: FutureBuilder(
                              future: getUserData(),
                              builder: (context, snapshot) {
                                if (snapshot.connectionState !=
                                    ConnectionState.done) return Text("");
                                if (donorUserType != null) {
                                  usersValue = donorUserType;
                                  return Text(
                                    "$donorUserType",
                                    style: GoogleFonts.roboto(
                                        color: Colors.black54, fontSize: 14),
                                  );
                                }
                                if (patientUserType != null) {
                                  usersValue = patientUserType;
                                  return Text(
                                    "$patientUserType",
                                    style: GoogleFonts.roboto(
                                        color: Colors.black54, fontSize: 14),
                                  );
                                } else {
                                  return Text(
                                    "null",
                                    style: GoogleFonts.roboto(
                                        color: Colors.black54,
                                        fontSize: 18,
                                        fontWeight: FontWeight.bold),
                                  );
                                }
                              },
                            ),
                          ),
                        ],
                      ),
                      //Donor and Patient Blood Type
                      InkWell(
                        onTap: isButtonEnabled
                            ? () {
                                showModalBottomSheet(
                                  context: context,
                                  builder: (context) => buildBloodTypeSheet(),
                                );
                              }
                            : null,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Container(
                                  margin: EdgeInsets.only(top: 20, left: 20),
                                  child: Text(
                                    'Blood Type',
                                    style: GoogleFonts.roboto(
                                        color: Colors.black54, fontSize: 14),
                                  ),
                                ),
                                Container(
                                  margin: EdgeInsets.only(top: 5, left: 20),
                                  child: FutureBuilder(
                                    future: getUserData(),
                                    builder: (context, snapshot) {
                                      if (snapshot.connectionState !=
                                          ConnectionState.done) return Text("");
                                      if (donorBloodType != null) {
                                        usersValue = donorUserType;
                                        return Container(
                                          margin: EdgeInsets.only(top: 5),
                                          height: 20,
                                          width: 240,
                                          child: TextFormField(
                                            controller: _bloodType
                                              ..text = donorBloodType!,
                                            enabled: false,
                                            decoration: InputDecoration(
                                              hintText: 'Select Blood type',
                                            ),
                                            style: GoogleFonts.roboto(
                                                color: Colors.black54,
                                                fontSize: 14),
                                          ),
                                        );
                                      }
                                      if (patientBloodType != null) {
                                        usersValue = patientUserType;
                                        return Container(
                                          margin: EdgeInsets.only(top: 5),
                                          height: 20,
                                          width: 240,
                                          child: TextFormField(
                                            controller: _bloodType
                                              ..text = patientBloodType!,
                                            enabled: false,
                                            decoration: InputDecoration(
                                              hintText: 'Select Blood type',
                                            ),
                                            style: GoogleFonts.roboto(
                                                color: Colors.black54,
                                                fontSize: 14),
                                          ),
                                        );
                                      } else {
                                        return Text(
                                          "null",
                                          style: GoogleFonts.roboto(
                                              color: Colors.black54,
                                              fontSize: 14),
                                        );
                                      }
                                    },
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                      //Kidney Size
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Container(
                            margin: EdgeInsets.only(top: 20, left: 20),
                            child: Text(
                              'Kidney Size',
                              style: GoogleFonts.roboto(
                                  color: Colors.black54, fontSize: 14),
                            ),
                          ),
                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Container(
                            margin: EdgeInsets.only(right: 20, left: 20),
                            height: 32,
                            child: FutureBuilder(
                              future: getUserData(),
                              builder: (context, snapshot) {
                                if (snapshot.connectionState !=
                                    ConnectionState.done) return Text("");
                                if (donorKidneySize != null) {
                                  usersValue = donorUserType;
                                  return Container(
                                    height: 20,
                                    width: 240,
                                    child: TextFormField(
                                      controller: _kidneySize
                                        ..text = donorKidneySize!,
                                      enabled: isFieldEnabled,
                                      maxLength: 2,
                                      decoration: InputDecoration(
                                          hintText: '(cm)', counterText: ''),
                                      style: GoogleFonts.roboto(
                                          color: Colors.black54, fontSize: 14),
                                    ),
                                  );
                                }
                                if (patientKidneySize != null) {
                                  usersValue = patientUserType;
                                  return Container(
                                    height: 20,
                                    width: 240,
                                    child: TextFormField(
                                      controller: _kidneySize
                                        ..text = patientKidneySize!,
                                      enabled: isFieldEnabled,
                                      maxLength: 2,
                                      decoration: InputDecoration(
                                          hintText: '(cm)', counterText: ''),
                                      style: GoogleFonts.roboto(
                                          color: Colors.black54, fontSize: 14),
                                    ),
                                  );
                                } else {
                                  return Text(
                                    "null",
                                    style: GoogleFonts.roboto(
                                        color: Colors.black54, fontSize: 14),
                                  );
                                }
                              },
                            ),
                          ),
                        ],
                      ),
                      //Signature
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Container(
                            margin: EdgeInsets.only(top: 20, left: 20),
                            child: Text(
                              'Signature',
                              style: GoogleFonts.roboto(
                                  color: Colors.black54, fontSize: 14),
                            ),
                          ),
                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Container(
                            margin:
                                EdgeInsets.only(top: 20, right: 20, left: 20),
                            height: 32,
                            child: FutureBuilder(
                              future: getUserData(),
                              builder: (context, snapshot) {
                                if (snapshot.connectionState !=
                                    ConnectionState.done) return Text("");
                                if (donorSignature != null) {
                                  usersValue = donorUserType;
                                  return Container(
                                    height: 20,
                                    width: 240,
                                    child: TextFormField(
                                      controller: _signature
                                        ..text = donorSignature!,
                                      enabled: isFieldEnabled,
                                      maxLength: 30,
                                      decoration: InputDecoration(
                                        hintText:
                                            'Default signature given to everyone~',
                                      ),
                                      style: GoogleFonts.roboto(
                                          color: Colors.black54, fontSize: 14),
                                    ),
                                  );
                                }
                                if (patientSignature != null) {
                                  usersValue = patientUserType;
                                  return Container(
                                    height: 20,
                                    width: 240,
                                    child: TextFormField(
                                      controller: _signature
                                        ..text = patientSignature!,
                                      enabled: isFieldEnabled,
                                      decoration: InputDecoration(
                                        hintText:
                                            'Default signature given to everyone~',
                                      ),
                                      style: GoogleFonts.roboto(
                                          color: Colors.black54, fontSize: 14),
                                    ),
                                  );
                                } else {
                                  return Text(
                                    "null",
                                    style: GoogleFonts.roboto(
                                        color: Colors.black54, fontSize: 14),
                                  );
                                }
                              },
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          //Header
          Container(
            color: Color(0xffe5eef8),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                InkWell(
                  onTap: () {
                    Navigator.pushAndRemoveUntil(
                        context,
                        MaterialPageRoute(
                            builder: (builder) => DashboardProfilePage()),
                        (route) => false);
                  },
                  child: Container(
                    margin: EdgeInsets.only(left: 20, top: 40, bottom: 30),
                    child: Icon(Icons.arrow_back_ios_outlined, size: 25.0),
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(top: 40, bottom: 30),
                  child: Text(
                    "Profile Information",
                    style: GoogleFonts.roboto(
                        color: Colors.black87,
                        fontWeight: FontWeight.bold,
                        fontSize: 18),
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(top: 40, right: 20, bottom: 30),
                  child: ElevatedButton(
                    style: ElevatedButton.styleFrom(primary: Color(0xffe5eef8)),
                    onPressed: isButtonEnabled
                        ? () {
                            if (usersValue == 'I am a Donor') {
                              updateDonor();
                              updateAllDonor();
                              updateDonorMatch();
                              updateAllDonorMatch();
                              showSaveToast();
                              Navigator.pushAndRemoveUntil(
                                  context,
                                  MaterialPageRoute(
                                      builder: (builder) =>
                                          DashboardProfilePage()),
                                  (route) => false);
                            } else {
                              updatePatient();
                              updateAllPatient();
                              updatePatientMatch();
                              updateAllPatientMatch();
                              showSaveToast();
                              Navigator.pushAndRemoveUntil(
                                  context,
                                  MaterialPageRoute(
                                      builder: (builder) =>
                                          DashboardProfilePage()),
                                  (route) => false);
                            }
                          }
                        : null,
                    child: Text(
                      'Save',
                      style:
                          GoogleFonts.roboto(color: Colors.red, fontSize: 18),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  // Bottom Sheet For Gender
  Widget buildGenderSheet() => Container(
        height: 300,
        child: ListView(
          children: [
            //Header Gender
            ListTile(
              title: Text(
                'Gender',
                style: GoogleFonts.roboto(
                    color: Colors.black87,
                    fontSize: 15,
                    fontWeight: FontWeight.w500),
              ),
            ),
            //Selection
            ListTile(
              title: Text(
                'Male',
                style: GoogleFonts.roboto(color: Colors.black87, fontSize: 15),
              ),
              onTap: () {
                if (usersValue == "I am a Donor") {
                  _gender..text = 'Male';
                  Navigator.of(context).pop();
                } else {
                  _gender..text = 'Male';
                  Navigator.of(context).pop();
                }
              },
            ),

            ListTile(
              title: Text(
                'Female',
                style: GoogleFonts.roboto(color: Colors.black87, fontSize: 15),
              ),
              onTap: () {
                if (usersValue == "I am a Donor") {
                  _gender..text = 'Female';
                  Navigator.of(context).pop();
                } else {
                  _gender..text = 'Female';
                  Navigator.of(context).pop();
                }
              },
            ),
            ListTile(
              title: Text(
                'Other',
                style: GoogleFonts.roboto(color: Colors.black87, fontSize: 15),
              ),
              onTap: () {
                if (usersValue == "I am a Donor") {
                  _gender..text = 'Other';
                  Navigator.of(context).pop();
                } else {
                  _gender..text = 'Other';
                  Navigator.of(context).pop();
                }
              },
            ),
            ListTile(
              title: Text(
                'Prefer not to say',
                style: GoogleFonts.roboto(color: Colors.black87, fontSize: 15),
              ),
              onTap: () {
                if (usersValue == "I am a Donor") {
                  _gender..text = 'Prefer not to say';
                  Navigator.of(context).pop();
                } else {
                  _gender..text = 'Prefer not to say';
                  Navigator.of(context).pop();
                }
              },
            ),
          ],
        ),
      );

  // Bottom Sheet For Blood Type
  Widget buildBloodTypeSheet() => Container(
        height: 300,
        child: ListView(
          children: [
            //Header Blood type
            ListTile(
              title: Text(
                'Choose your blood type',
                style: GoogleFonts.roboto(
                    color: Colors.black87,
                    fontSize: 15,
                    fontWeight: FontWeight.w500),
              ),
            ),
            //Selection
            ListTile(
              title: Text(
                'A+',
                style: GoogleFonts.roboto(color: Colors.black87, fontSize: 15),
              ),
              onTap: () {
                if (usersValue == "I am a Donor") {
                  _bloodType.text = 'A+';
                  Navigator.of(context).pop();
                } else {
                  _bloodType.text = 'A+';
                  Navigator.of(context).pop();
                }
              },
            ),
            ListTile(
              title: Text(
                'A-',
                style: GoogleFonts.roboto(color: Colors.black87, fontSize: 15),
              ),
              onTap: () {
                if (usersValue == "I am a Donor") {
                  _bloodType.text = 'A-';
                  Navigator.of(context).pop();
                } else {
                  _bloodType.text = 'A-';
                  Navigator.of(context).pop();
                }
              },
            ),
            ListTile(
              title: Text(
                'B+',
                style: GoogleFonts.roboto(color: Colors.black87, fontSize: 15),
              ),
              onTap: () {
                if (usersValue == "I am a Donor") {
                  _bloodType.text = 'B+';
                  Navigator.of(context).pop();
                } else {
                  _bloodType.text = 'B+';
                  Navigator.of(context).pop();
                }
              },
            ),
            ListTile(
              title: Text(
                'B-',
                style: GoogleFonts.roboto(color: Colors.black87, fontSize: 15),
              ),
              onTap: () {
                if (usersValue == "I am a Donor") {
                  _bloodType.text = 'B-';
                  Navigator.of(context).pop();
                } else {
                  _bloodType.text = 'B-';
                  Navigator.of(context).pop();
                }
              },
            ),
            ListTile(
              title: Text(
                'AB+',
                style: GoogleFonts.roboto(color: Colors.black87, fontSize: 15),
              ),
              onTap: () {
                if (usersValue == "I am a Donor") {
                  _bloodType.text = 'AB+';
                  Navigator.of(context).pop();
                } else {
                  _bloodType.text = 'AB+';
                  Navigator.of(context).pop();
                }
              },
            ),
            ListTile(
              title: Text(
                'AB-',
                style: GoogleFonts.roboto(color: Colors.black87, fontSize: 15),
              ),
              onTap: () {
                if (usersValue == "I am a Donor") {
                  _bloodType.text = 'AB-';
                  Navigator.of(context).pop();
                } else {
                  _bloodType.text = 'AB-';
                  Navigator.of(context).pop();
                }
              },
            ),
            ListTile(
              title: Text(
                'O+',
                style: GoogleFonts.roboto(color: Colors.black87, fontSize: 15),
              ),
              onTap: () {
                if (usersValue == "I am a Donor") {
                  _bloodType.text = 'O+';
                  Navigator.of(context).pop();
                } else {
                  _bloodType.text = 'O+';
                  Navigator.of(context).pop();
                }
              },
            ),
            ListTile(
              title: Text(
                'O-',
                style: GoogleFonts.roboto(color: Colors.black87, fontSize: 15),
              ),
              onTap: () {
                if (usersValue == "I am a Donor") {
                  _bloodType.text = 'O-';
                  Navigator.of(context).pop();
                } else {
                  _bloodType.text = 'O-';
                  Navigator.of(context).pop();
                }
              },
            ),
          ],
        ),
      );

  getUserData() async {
    final donor = FirebaseAuth.instance.currentUser;
    if (donor != null) {
      await FirebaseFirestore.instance
          .collection('Users')
          .doc("Donor")
          .collection("Lists")
          .doc(donor.uid)
          .get()
          .then((value) {
        donorFullName = value.data()!['FullName'];
        donorUserName = value.data()!['UserName'];
        donorGender = value.data()!['Gender'];
        donorAge = value.data()!['Age'];
        donorUserType = value.data()!['User Type'];
        donorBloodType = value.data()!['Blood Type'];
        donorKidneySize = value.data()!['Kidney Size'];
        donorSignature = value.data()!['Signature'];
      }).catchError((e) {
        print(e);
      });
    }

    final patient = FirebaseAuth.instance.currentUser;
    if (patient != null) {
      await FirebaseFirestore.instance
          .collection('Users')
          .doc("Patient")
          .collection("Lists")
          .doc(patient.uid)
          .get()
          .then((value) {
        patientFullName = value.data()!['FullName'];
        patientUserName = value.data()!['UserName'];
        patientGender = value.data()!['Gender'];
        patientAge = value.data()!['Age'];
        patientUserType = value.data()!['User Type'];
        patientBloodType = value.data()!['Blood Type'];
        patientKidneySize = value.data()!['Kidney Size'];
        patientSignature = value.data()!['Signature'];
      }).catchError((e) {
        print(e);
      });
    }
  }

  //Show Toast Message
  void showSaveToast() =>
      toast.showToast(child: buildSaveToast(), gravity: ToastGravity.CENTER);

  Widget buildSaveToast() => Container(
        padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
        decoration: BoxDecoration(
          color: Color(0xFF111111).withOpacity(0.6),
          borderRadius: BorderRadius.circular(5),
        ),
        child: Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            Text(
              "Saved",
              style: TextStyle(color: Colors.white, fontSize: 16),
            ),
          ],
        ),
      );

  //Show Edit Toast Message
  void showEditToast() =>
      toast.showToast(child: buildEditToast(), gravity: ToastGravity.CENTER);

  Widget buildEditToast() => Container(
        padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
        decoration: BoxDecoration(
          color: Color(0xFF111111).withOpacity(0.6),
          borderRadius: BorderRadius.circular(5),
        ),
        child: Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            Text(
              "Edit Mode",
              style: TextStyle(color: Colors.white, fontSize: 16),
            ),
          ],
        ),
      );

  //Show Edit Toast Message
  void showCancelToast() =>
      toast.showToast(child: buildCancelToast(), gravity: ToastGravity.CENTER);

  Widget buildCancelToast() => Container(
        padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
        decoration: BoxDecoration(
          color: Color(0xFF111111).withOpacity(0.6),
          borderRadius: BorderRadius.circular(5),
        ),
        child: Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            Text(
              "Action Canceled",
              style: TextStyle(color: Colors.white, fontSize: 16),
            ),
          ],
        ),
      );
}
