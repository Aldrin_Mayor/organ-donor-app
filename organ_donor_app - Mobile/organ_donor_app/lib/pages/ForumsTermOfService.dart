import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:organ_donor_app/pages/AboutPage.dart';

class ForumsTermOfService extends StatefulWidget {
  ForumsTermOfService({Key? key}) : super(key: key);

  @override
  _ForumsTermOfServiceState createState() => _ForumsTermOfServiceState();
}

class _ForumsTermOfServiceState extends State<ForumsTermOfService> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xffe5eef8),
      body: Stack(
        children: [
          //Middle Content
          SingleChildScrollView(
            child: Container(
              margin: EdgeInsets.only(top: 90, bottom: 40),
              child: Center(
                child: Column(
                  children: [
                    Container(
                      margin: EdgeInsets.only(left: 20, right: 20),
                      child: SelectableText.rich(
                        TextSpan(
                          children: [
                            TextSpan(
                              text:
                                  'This service agreement (hereafter referred to as the "Agreement") is a legally binding contract between Organ Donor App ("Organ Donor App," "us," "our," and "we") and you ("you," "user"). Whether you are a guest/visitor or a registered user, your activities on the  ',
                              style: GoogleFonts.roboto(
                                  color: Colors.black87,
                                  fontSize: 14,
                                  height: 1.7),
                            ),
                            TextSpan(
                              text: "Organ Donor App, ",
                              style: GoogleFonts.roboto(
                                  color: Colors.black87,
                                  fontSize: 14,
                                  height: 1.7,
                                  fontWeight: FontWeight.w700),
                            ),
                            TextSpan(
                              text:
                                  'forum, our active websites, customer support, social media, community platforms, and/or any online services provided by Organ Donor App, or third parties authorized by Organ Donor App (collectively referred to as "Organ Donor App Services") are governed by the terms and conditions of this Agreement.',
                              style: GoogleFonts.roboto(
                                  color: Colors.black87,
                                  fontSize: 14,
                                  height: 1.7),
                            ),
                            TextSpan(
                              children: [
                                TextSpan(
                                  text:
                                      "\n\nBy registering or applying for an account, accessing ",
                                  style: GoogleFonts.roboto(
                                      color: Colors.black87,
                                      fontSize: 14,
                                      height: 1.7),
                                ),
                                TextSpan(
                                  text: "Organ Donor App, ",
                                  style: GoogleFonts.roboto(
                                      color: Colors.black87,
                                      fontSize: 14,
                                      height: 1.7,
                                      fontWeight: FontWeight.w700),
                                ),
                                TextSpan(
                                  text: "using ",
                                  style: GoogleFonts.roboto(
                                      color: Colors.black87,
                                      fontSize: 14,
                                      height: 1.7),
                                ),
                                TextSpan(
                                  text: "Organ Donor App, ",
                                  style: GoogleFonts.roboto(
                                      color: Colors.black87,
                                      fontSize: 14,
                                      height: 1.7,
                                      fontWeight: FontWeight.w700),
                                ),
                                TextSpan(
                                  text: "or visiting any services offered ",
                                  style: GoogleFonts.roboto(
                                      color: Colors.black87,
                                      fontSize: 14,
                                      height: 1.7),
                                ),
                                TextSpan(
                                  text: "Organ Donor App, ",
                                  style: GoogleFonts.roboto(
                                      color: Colors.black87,
                                      fontSize: 14,
                                      height: 1.7,
                                      fontWeight: FontWeight.w700),
                                ),
                                TextSpan(
                                  text:
                                      "by other methods, you are deemed to have read, understood, and accepted all the conditions included in this Agreement.",
                                  style: GoogleFonts.roboto(
                                      color: Colors.black87,
                                      fontSize: 14,
                                      height: 1.7),
                                ),
                                TextSpan(
                                  text: "\n\nOrgan Donor App ",
                                  style: GoogleFonts.roboto(
                                      color: Colors.black87,
                                      fontSize: 14,
                                      height: 1.7,
                                      fontWeight: FontWeight.w700),
                                ),
                                TextSpan(
                                  text:
                                      "retains the right to change or modify this Agreement at any time, and you undertake to check it on a regular basis for new terms.",
                                  style: GoogleFonts.roboto(
                                      color: Colors.black87,
                                      fontSize: 14,
                                      height: 1.7),
                                ),
                                TextSpan(
                                  text:
                                      "\n\nPlease keep in mind that your continuing use of Organ Donor App Services indicates your acceptance of the conditions of the revised Agreement.",
                                  style: GoogleFonts.roboto(
                                      color: Colors.black87,
                                      fontSize: 14,
                                      height: 1.7,
                                      fontWeight: FontWeight.w700),
                                ),
                                TextSpan(
                                  text:
                                      "\n\nIf you do not agree to any of the conditions in this Agreement, please do not use or use Organ Donor App Services in any manner, directly or indirectly.",
                                  style: GoogleFonts.roboto(
                                      color: Colors.black87,
                                      fontSize: 14,
                                      height: 1.7,
                                      fontWeight: FontWeight.w700),
                                ),
                                TextSpan(
                                  text: "\n\n\n1. Eligibility",
                                  style: GoogleFonts.roboto(
                                      color: Colors.black87,
                                      fontSize: 14,
                                      height: 1.7,
                                      fontWeight: FontWeight.w700),
                                ),
                                TextSpan(
                                  text:
                                      "\n\nTo utilize our services under any circumstances, you must be a credible kidney donor or kidney patient. Have a good motive for wanting to use the service. You may use the services only if you agree to these Terms of Service and are not a person barred from doing so by the laws of the applicable country.",
                                  style: GoogleFonts.roboto(
                                    color: Colors.black87,
                                    fontSize: 14,
                                    height: 1.7,
                                  ),
                                ),
                                TextSpan(
                                  text: "\n\n\n2. Content of the Services",
                                  style: GoogleFonts.roboto(
                                      color: Colors.black87,
                                      fontSize: 14,
                                      height: 1.7,
                                      fontWeight: FontWeight.w700),
                                ),
                                TextSpan(
                                  text: "\n\n(1). User Submissions",
                                  style: GoogleFonts.roboto(
                                      color: Colors.black87,
                                      fontSize: 14,
                                      height: 1.7,
                                      fontWeight: FontWeight.w700),
                                ),
                                TextSpan(
                                  text:
                                      '\n\n1)	Organ Donor App Services may encompass a wide range of websites, forums, communities, the internet, or other kinds of communication. This enables you to submit, publish, display, or transmit any type of material ("User Submissions") on or through Organ Donor App Services, including but not limited to text, forum posts, chat posts, personal information, modules, information, hyperlinks, feedback, e-mail, music, audio files, graphics, images, videos, code, any visuals or sounds, or any other materials that appear on Organ Donor App Services or hyperlinks that lead to Organ Donor App Services. You agree and warrant that the image-related publications are your original works and that you have exclusive ownership of the material.',
                                  style: GoogleFonts.roboto(
                                    color: Colors.black87,
                                    fontSize: 14,
                                    height: 1.7,
                                  ),
                                ),
                                TextSpan(
                                  text:
                                      "\n\n2)	We retain the right, without prior warning, to monitor, filter, evaluate, edit, and/or remove User Submissions that are offensive or inappropriate. Organ Donor App has the right to terminate or suspend your access to any user submission at any time and for any reason without prior notice.",
                                  style: GoogleFonts.roboto(
                                    color: Colors.black87,
                                    fontSize: 14,
                                    height: 1.7,
                                  ),
                                ),
                                TextSpan(
                                  text:
                                      "\n\n3)	User Submissions must not be unlawful, fraudulent, misleading, obscene, threatening, defamatory, infringing of intellectual property rights, detrimental to other parties, or inappropriate or objectionable, and must not contain viruses, malicious scripts, plug-ins, software, or programs. You agree that you will be solely responsible for your conduct when using Organ Donor App Services, including, but not limited to, any User Submissions you make. You also understand that Organ Donor App will not be held liable for any User Submissions you have submitted. You also confirm that Organ Donor App does not endorse or regulate any User Submissions. ",
                                  style: GoogleFonts.roboto(
                                    color: Colors.black87,
                                    fontSize: 14,
                                    height: 1.7,
                                  ),
                                ),
                                TextSpan(
                                  text:
                                      "\n\n4)	You declare and warrant that the information you have supplied is correct, not false, or misleading, and does not infringe on the rights of Organ Donor App or any other third party (including but not limited to intellectual property rights).",
                                  style: GoogleFonts.roboto(
                                    color: Colors.black87,
                                    fontSize: 14,
                                    height: 1.7,
                                  ),
                                ),
                                TextSpan(
                                  text: "\n\n\n3.) Rules of Use",
                                  style: GoogleFonts.roboto(
                                    color: Colors.black87,
                                    fontSize: 14,
                                    height: 1.7,
                                    fontWeight: FontWeight.w700,
                                  ),
                                ),
                                TextSpan(
                                  text: "\n\n(1) Rules and Policies",
                                  style: GoogleFonts.roboto(
                                    color: Colors.black87,
                                    fontSize: 14,
                                    height: 1.7,
                                    fontWeight: FontWeight.w700,
                                  ),
                                ),
                                TextSpan(
                                  text:
                                      "\n\n1)	You recognize and accept that by using Organ Donor App Services, you agree to be bound by this Agreement and the Privacy Policy, both of which may be changed by Organ Donor App at any time in line with its policies.",
                                  style: GoogleFonts.roboto(
                                    color: Colors.black87,
                                    fontSize: 14,
                                    height: 1.7,
                                  ),
                                ),
                                TextSpan(
                                  text:
                                      "\n\n2)	You recognize and warrant that you will not take or attempt to do any of the following acts in connection with the use of any Organ Donor App Services, either directly or indirectly:",
                                  style: GoogleFonts.roboto(
                                    color: Colors.black87,
                                    fontSize: 14,
                                    height: 1.7,
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ),
                    //Number 3.1.2 Contents
                    Container(
                      alignment: Alignment.topLeft,
                      margin: EdgeInsets.only(left: 45, right: 15),
                      child: SelectableText.rich(
                        TextSpan(
                          children: [
                            TextSpan(
                              text:
                                  '\ni)	Except as specifically authorized by this agreement, you may not duplicate the Service, or any Forum Health content or materials contained therein ("Organ Donor App Content").',
                              style: GoogleFonts.roboto(
                                  color: Colors.black87,
                                  fontSize: 14,
                                  height: 1.7),
                            ),
                            TextSpan(
                              text:
                                  "\n\nii)	Resell or commercially exploit the Service or any Organ Donor App Content (unless as explicitly permitted above).",
                              style: GoogleFonts.roboto(
                                  color: Colors.black87,
                                  fontSize: 14,
                                  height: 1.7),
                            ),
                            TextSpan(
                              text:
                                  "\n\niii)	Slander, ridicule, stalk, threaten, harass, intimidate, express hatred, express racial prejudice, or engage in any other behavior that is offensive to any member of the community.",
                              style: GoogleFonts.roboto(
                                  color: Colors.black87,
                                  fontSize: 14,
                                  height: 1.7),
                            ),
                            TextSpan(
                              text:
                                  "\n\niv)	reverse engineer, disassemble, decompile, decode, or otherwise try to extract or obtain access to the Service's or any element of its source code.",
                              style: GoogleFonts.roboto(
                                  color: Colors.black87,
                                  fontSize: 14,
                                  height: 1.7),
                            ),
                            TextSpan(
                              text:
                                  "\n\nv)	utilize data mining, robotics, or other similar data collection or extraction methods.",
                              style: GoogleFonts.roboto(
                                  color: Colors.black87,
                                  fontSize: 14,
                                  height: 1.7),
                            ),
                            TextSpan(
                              text:
                                  "\n\nvi)	remove, delete, alter, or conceal any trademarks or notices of copyright, trademark, patent, or other intellectual property or proprietary rights from the Service, including any copy thereof.",
                              style: GoogleFonts.roboto(
                                  color: Colors.black87,
                                  fontSize: 14,
                                  height: 1.7),
                            ),
                            TextSpan(
                              text:
                                  "\n\nvii)	Use, export, re-export, or otherwise violate relevant laws or local rules, regardless of whether the action is taken or no action is taken in response to such activity.",
                              style: GoogleFonts.roboto(
                                  color: Colors.black87,
                                  fontSize: 14,
                                  height: 1.7),
                            ),
                            TextSpan(
                              text:
                                  "\n\nviii)	Allow or assist any third party in carrying out any of the activities.",
                              style: GoogleFonts.roboto(
                                  color: Colors.black87,
                                  fontSize: 14,
                                  height: 1.7),
                            ),
                          ],
                        ),
                      ),
                    ),
                    //Number 3.1.3 and 3.1.4 Contents
                    Container(
                      margin: EdgeInsets.only(left: 15, right: 15),
                      child: SelectableText.rich(
                        TextSpan(
                          children: [
                            TextSpan(
                              text:
                                  '\n3)	You also agree to respect local customs and traditions and to follow all applicable laws and regulations while using Organ Donor App Services. You further affirm that if required by local laws or regulations, you will promptly cease using or accessing Organ Donor App Services.',
                              style: GoogleFonts.roboto(
                                  color: Colors.black87,
                                  fontSize: 14,
                                  height: 1.7),
                            ),
                            TextSpan(
                              text:
                                  "\n\n4)	You may not create copies of all or any portion of the content in the Organ Donor App Services and related materials, unless specifically allowed by this Agreement. You agree to the following since the Organ Donor App Services include proprietary information: a)	Maintain the absolute confidentiality of all code and technological aspects of our Services, b)	We will not show, report, publish, divulge, or otherwise transmit any of our nonpublic information, c)	Subject to any relevant laws, you agree not to use Organ Donor App Services for anything other than personal amusement and non-commercial reasons.",
                              style: GoogleFonts.roboto(
                                  color: Colors.black87,
                                  fontSize: 14,
                                  height: 1.7),
                            ),
                            TextSpan(
                              text: "\n\n\n(2) Inappropriate User Behavior",
                              style: GoogleFonts.roboto(
                                  color: Colors.black87,
                                  fontSize: 14,
                                  height: 1.7,
                                  fontWeight: FontWeight.w700),
                            ),
                            TextSpan(
                              text:
                                  "\n\n1.) Organ Donor App holds you accountable for your behavior and language and encourages users to use Organ Donor App in a civilized manner. It is also strictly forbidden to show inappropriate or offensive behavior. You may not represent yourself as an employee of Organ Donor App to Organ Donor App or third parties, distribute false or vulgar information, publish illegal websites, spam advertising emails or information relating to drugs and narcotics, or otherwise attack, threaten, or insult Organ Donor App or all or any part of its users. You shall not distribute plug-ins, Trojan horse programs, or any other type of virus. ",
                              style: GoogleFonts.roboto(
                                  color: Colors.black87,
                                  fontSize: 14,
                                  height: 1.7,
                                  fontWeight: FontWeight.w700),
                            ),
                            TextSpan(
                              text:
                                  "\n\n2.) You are not permitted to participate in the following activities, either directly or indirectly:",
                              style: GoogleFonts.roboto(
                                  color: Colors.black87,
                                  fontSize: 14,
                                  height: 1.7,
                                  fontWeight: FontWeight.w700),
                            ),
                          ],
                        ),
                      ),
                    ),
                    //Number 3.2.2 Contents
                    Container(
                      alignment: Alignment.topLeft,
                      margin: EdgeInsets.only(left: 45, right: 15),
                      child: SelectableText.rich(
                        TextSpan(
                          children: [
                            TextSpan(
                              text:
                                  "\ni) Disseminating information or materials that contravene the public's normal behaviors,religious beliefs, conventions, or societal values in whole or in part.",
                              style: GoogleFonts.roboto(
                                  color: Colors.black87,
                                  fontSize: 14,
                                  height: 1.7,
                                  fontWeight: FontWeight.w700),
                            ),
                            TextSpan(
                              text:
                                  "\n\nii) Any content connected to account trading or selling should be published or discussed.",
                              style: GoogleFonts.roboto(
                                  color: Colors.black87,
                                  fontSize: 14,
                                  height: 1.7,
                                  fontWeight: FontWeight.w700),
                            ),
                            TextSpan(
                              text:
                                  "\n\niii) The period of the License begins when you download, install, or use the Service and continues until terminated by you or the Company as set out in these Terms of Use.",
                              style: GoogleFonts.roboto(
                                  color: Colors.black87,
                                  fontSize: 14,
                                  height: 1.7,
                                  fontWeight: FontWeight.w700),
                            ),
                            TextSpan(
                              text:
                                  "\n\niv) You may cancel this License at any time by discontinuing usage of the Website and deleting your account.",
                              style: GoogleFonts.roboto(
                                  color: Colors.black87,
                                  fontSize: 14,
                                  height: 1.7,
                                  fontWeight: FontWeight.w700),
                            ),
                            TextSpan(
                              text:
                                  "\n\nv) Organ Donor App may terminate this Agreement at any time and in its sole discretion, including, but not limited to, if it stops to support the Service. Furthermore, if you break any of the terms and conditions of the License or the Terms of Use, the License will terminate immediately and automatically without notice.",
                              style: GoogleFonts.roboto(
                                  color: Colors.black87,
                                  fontSize: 14,
                                  height: 1.7,
                                  fontWeight: FontWeight.w700),
                            ),
                            TextSpan(
                              text:
                                  "\n\nvi) Any improper conduct or statements that might jeopardize Organ Donor App' reputation.",
                              style: GoogleFonts.roboto(
                                  color: Colors.black87,
                                  fontSize: 14,
                                  height: 1.7,
                                  fontWeight: FontWeight.w700),
                            ),
                          ],
                        ),
                      ),
                    ),
                    //Number 4 and 5
                    Container(
                      margin: EdgeInsets.only(left: 15, right: 15),
                      child: SelectableText.rich(
                        TextSpan(
                          children: [
                            TextSpan(
                              text: '\n\n\n4)	Privacy Protection',
                              style: GoogleFonts.roboto(
                                  color: Colors.black87,
                                  fontSize: 14,
                                  height: 1.7,
                                  fontWeight: FontWeight.w700),
                            ),
                            TextSpan(
                              text:
                                  "\n\nOur privacy policy describes how we will process the information you supply us when using our services. By using our services, you understand and accept that we may collect, and use said information (as indicated in the privacy policy), including the transfer of this information to storage and use by and its subsidiaries.",
                              style: GoogleFonts.roboto(
                                  color: Colors.black87,
                                  fontSize: 14,
                                  height: 1.7),
                            ),
                            TextSpan(
                              text:
                                  '\n\n\n5)	Disclaimer and Limitation Liability',
                              style: GoogleFonts.roboto(
                                  color: Colors.black87,
                                  fontSize: 14,
                                  height: 1.7,
                                  fontWeight: FontWeight.w700),
                            ),
                            TextSpan(
                              text: '\n\n1)	Reservation Rights',
                              style: GoogleFonts.roboto(
                                  color: Colors.black87,
                                  fontSize: 14,
                                  height: 1.7,
                                  fontWeight: FontWeight.w700),
                            ),
                            TextSpan(
                              text:
                                  "\n\nYou recognize and accept that the Service is offered to you under license rather than being sold to you. Under these Terms of Use, you do not acquire any ownership interest in the Service or any other rights to it other than to use it in accordance with the license granted and subject to all terms, conditions, and limitations set out in these Terms of Use. Forum Health retains and shall maintain all right, title, and interest in and to the Service, including all copyrights, trademarks, and other intellectual property rights therein or related thereby, save as expressly given to you in these Terms of Use.",
                              style: GoogleFonts.roboto(
                                  color: Colors.black87,
                                  fontSize: 14,
                                  height: 1.7),
                            ),
                            TextSpan(
                              text: '\n\n2)	Limitation of Liability',
                              style: GoogleFonts.roboto(
                                  color: Colors.black87,
                                  fontSize: 14,
                                  height: 1.7,
                                  fontWeight: FontWeight.w700),
                            ),
                            TextSpan(
                              text:
                                  "\n\nTo the fullest extent permitted by applicable law, Organ Donor App, or its affiliates, or any of their respective licensors or service providers, shall have no responsibility arising from or connected to your use of or inability to utilize the service or the content and services for:",
                              style: GoogleFonts.roboto(
                                  color: Colors.black87,
                                  fontSize: 14,
                                  height: 1.7),
                            ),
                          ],
                        ),
                      ),
                    ),
                    //Number 5.2 Contents
                    Container(
                      alignment: Alignment.topLeft,
                      margin: EdgeInsets.only(left: 45, right: 15),
                      child: SelectableText.rich(
                        TextSpan(
                          children: [
                            TextSpan(
                              text:
                                  '\ni) Personal Injury, Property Damage, Cost of Substitute Goods or Services, Data Loss, Goodwill Loss, Computer Failure or Malfunction, or any other incidental, consequential, indirect, exemplary, special, or punitive damages.',
                              style: GoogleFonts.roboto(
                                  color: Colors.black87,
                                  fontSize: 14,
                                  height: 1.7),
                            ),
                            TextSpan(
                              text:
                                  "\n\nii) Any mistakes, omissions, or inaccuracies in Organ Donor App Services.",
                              style: GoogleFonts.roboto(
                                  color: Colors.black87,
                                  fontSize: 14,
                                  height: 1.7),
                            ),
                            TextSpan(
                              text:
                                  "\n\niii)	Organ Donor App Services may be interrupted, suspended, or terminated at any time.",
                              style: GoogleFonts.roboto(
                                  color: Colors.black87,
                                  fontSize: 14,
                                  height: 1.7),
                            ),
                            TextSpan(
                              text:
                                  "\n\niv) Third-party mistakes, viruses, or associated linkages sent on or through Organ Donor App Services.",
                              style: GoogleFonts.roboto(
                                  color: Colors.black87,
                                  fontSize: 14,
                                  height: 1.7),
                            ),
                            TextSpan(
                              text:
                                  "\n\nv)	Any software having the intent to harm, deliberately meddle with, covertly intercept, or destroy your personal information.",
                              style: GoogleFonts.roboto(
                                  color: Colors.black87,
                                  fontSize: 14,
                                  height: 1.7),
                            ),
                            TextSpan(
                              text:
                                  "\n\nvi)  Any loss resulting from or related to unanticipated situations or other reasons beyond our reasonable control.",
                              style: GoogleFonts.roboto(
                                  color: Colors.black87,
                                  fontSize: 14,
                                  height: 1.7),
                            ),
                          ],
                        ),
                      ),
                    ),
                    //Number 5.2.2, 5.2.3 to 6
                    Container(
                      margin: EdgeInsets.only(left: 15, right: 15),
                      child: SelectableText.rich(
                        TextSpan(
                          children: [
                            TextSpan(
                              text:
                                  '\nOrgan Donor App is not liable for the activities of other users or other parties, including, but not limited to, third-party sites or services reached via links from Organ Donor App Services. Organ Donor App has no control over any third-party websites that you may visit, including any connected to our website. As a result, Organ Donor App is not responsible for the content or functioning of such sites and is not liable for any aspect of such third-party sites that you visit directly or through our websites or software features. Your use of such third-party sites will be governed by the appropriate Terms of Service and Privacy Policy.',
                              style: GoogleFonts.roboto(
                                  color: Colors.black87,
                                  fontSize: 14,
                                  height: 1.7),
                            ),
                            TextSpan(
                              text: "\n\n\n3)	Limitation of Claims",
                              style: GoogleFonts.roboto(
                                  color: Colors.black87,
                                  fontSize: 14,
                                  height: 1.7,
                                  fontWeight: FontWeight.w700),
                            ),
                            TextSpan(
                              text:
                                  '\n\nTo the greatest extent permissible by relevant law, any claims arising out of or connected to this Agreement and/or Organ Donor App Services must be litigated within a year of becoming aware of the litigation reason, or within a year after the litigated reason has happened. If you do not make your requests known within the time frame specified, the claim will be permanently barred.',
                              style: GoogleFonts.roboto(
                                  color: Colors.black87,
                                  fontSize: 14,
                                  height: 1.7),
                            ),
                            TextSpan(
                              text: '\n\n\n6)	General Provisions',
                              style: GoogleFonts.roboto(
                                  color: Colors.black87,
                                  fontSize: 14,
                                  height: 1.7,
                                  fontWeight: FontWeight.w700),
                            ),
                            TextSpan(
                              text: "\n\n1) Third-Party Content",
                              style: GoogleFonts.roboto(
                                  color: Colors.black87,
                                  fontSize: 14,
                                  height: 1.7,
                                  fontWeight: FontWeight.w700),
                            ),
                            TextSpan(
                              text:
                                  '\n\nThird-party content (including data, information, apps, and other goods, services, and/or materials) or connections to third-party websites or services may be displayed, included, or made accessible through the Service, including through third-party advertising ("Third-Party Materials"). You recognize and accept that Organ Donor App is not responsible for Third-Party Materials in any way, including their accuracy, completeness, timeliness, validity, copyright compliance, legality, decency, quality, or any other aspect. Forum Health does not assume and will not bear any liability or obligation for any Third-Party Materials to you or any other person or organization.',
                              style: GoogleFonts.roboto(
                                  color: Colors.black87,
                                  fontSize: 14,
                                  height: 1.7),
                            ),
                            TextSpan(
                              text: '\n\n\n2)	Updates',
                              style: GoogleFonts.roboto(
                                  color: Colors.black87,
                                  fontSize: 14,
                                  height: 1.7,
                                  fontWeight: FontWeight.w700),
                            ),
                            TextSpan(
                              text:
                                  '\n\nOrgan Donor App reserves the right to create and deliver Service updates in its sole discretion, which may include upgrades, bug repairs, patches, other error corrections, and/or new features (collectively, "Updates" and related documentation). Certain features and functionality may also be modified or completely removed with updates. You acknowledge and agree that Forum Health is under no obligation to deliver any Updates or to continue to provide or activate any specific features or functionality. You also agree that such Updates will be considered part of the Service and will be subject to all of the terms and restrictions of this Agreement.',
                              style: GoogleFonts.roboto(
                                  color: Colors.black87,
                                  fontSize: 14,
                                  height: 1.7),
                            ),
                            TextSpan(
                              text: '\n\n\n3)	Severability',
                              style: GoogleFonts.roboto(
                                  color: Colors.black87,
                                  fontSize: 14,
                                  height: 1.7,
                                  fontWeight: FontWeight.w700),
                            ),
                            TextSpan(
                              text:
                                  '\n\nIf any part of this Agreement is found to be unlawful or deemed illegal, void, or unenforceable for any reason, such provision shall be severed and removed from this Agreement, and the remainder of this Agreement shall be unaffected and continue in effect.',
                              style: GoogleFonts.roboto(
                                  color: Colors.black87,
                                  fontSize: 14,
                                  height: 1.7),
                            ),
                            TextSpan(
                              text:
                                  '\n\n\n4) Content to Use of Data and Mobile Communications ',
                              style: GoogleFonts.roboto(
                                  color: Colors.black87,
                                  fontSize: 14,
                                  height: 1.7,
                                  fontWeight: FontWeight.w700),
                            ),
                            TextSpan(
                              text:
                                  "\n\nUse You agree to Organ Donor App communicating with you through SMS, text message, email, social media, or other electronic means about the Service or its features, operations, and activities. These interactions will be subject to your carrier's standard message, data, and other rates and costs.",
                              style: GoogleFonts.roboto(
                                  color: Colors.black87,
                                  fontSize: 14,
                                  height: 1.7),
                            ),
                            TextSpan(
                              text: '\n\n\n5)	Form Submission',
                              style: GoogleFonts.roboto(
                                  color: Colors.black87,
                                  fontSize: 14,
                                  height: 1.7,
                                  fontWeight: FontWeight.w700),
                            ),
                            TextSpan(
                              text:
                                  '\n\nBy submitting a form on Organ Donor App application, I acknowledge that I am a eligible user and that Forum Health, its affiliates, subsidiaries, and providers, may contact me about their services. I also confirm that submitting this form does not start a patient care relationship.',
                              style: GoogleFonts.roboto(
                                  color: Colors.black87,
                                  fontSize: 14,
                                  height: 1.7),
                            ),
                            TextSpan(
                              text: '\n\n\n6)	Others',
                              style: GoogleFonts.roboto(
                                  color: Colors.black87,
                                  fontSize: 14,
                                  height: 1.7,
                                  fontWeight: FontWeight.w700),
                            ),
                            TextSpan(
                              text:
                                  '\n\n1) This Agreement replaces any earlier written or oral agreement (if any) between you and Organ Donor App and covers the complete contract of the parties on the topic of this Agreement.',
                              style: GoogleFonts.roboto(
                                  color: Colors.black87,
                                  fontSize: 14,
                                  height: 1.7),
                            ),
                            TextSpan(
                              text:
                                  '\n\n2) All references to "including" in this Agreement are intended to mean "including but not limited to.',
                              style: GoogleFonts.roboto(
                                  color: Colors.black87,
                                  fontSize: 14,
                                  height: 1.7),
                            ),
                            TextSpan(
                              text:
                                  '\n\n3) You recognize that you are entering into this Agreement fully freely, and that you do not expect to be compensated in any way other than as specifically specified in this Agreement.',
                              style: GoogleFonts.roboto(
                                  color: Colors.black87,
                                  fontSize: 14,
                                  height: 1.7),
                            ),
                          ],
                        ),
                      ),
                    ),
                    //End Line
                    Container(
                      alignment: Alignment.topRight,
                      margin: EdgeInsets.only(left: 15, right: 15, bottom: 40),
                      child: SelectableText.rich(
                        TextSpan(
                          children: [
                            TextSpan(
                              text: "\n\nDevelopers:",
                              style: GoogleFonts.roboto(
                                color: Colors.black87,
                                fontSize: 14,
                                height: 1.7,
                                fontWeight: FontWeight.w700,
                              ),
                            ),
                            TextSpan(
                              text: "\nEspiritu, Justin Lloyd:",
                              style: GoogleFonts.roboto(
                                color: Colors.black87,
                                fontSize: 14,
                                height: 1.7,
                              ),
                            ),
                            TextSpan(
                              text: "\nMayor, Aldrin:",
                              style: GoogleFonts.roboto(
                                color: Colors.black87,
                                fontSize: 14,
                                height: 1.7,
                              ),
                            ),
                            TextSpan(
                              text: "\nRivera, Debbie Cheyenne:",
                              style: GoogleFonts.roboto(
                                color: Colors.black87,
                                fontSize: 14,
                                height: 1.7,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
          //Header
          Container(
            decoration: BoxDecoration(color: Color(0xffe5eef8)),
            height: 80,
            margin: EdgeInsets.only(top: 0, left: 0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                InkWell(
                  onTap: () {
                    Navigator.pushAndRemoveUntil(
                        context,
                        MaterialPageRoute(builder: (builder) => AboutPage()),
                        (route) => false);
                  },
                  child: Padding(
                    padding: const EdgeInsets.only(top: 20, left: 20),
                    child: Icon(Icons.arrow_back_ios_outlined, size: 25.0),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 20),
                  child: Text(
                    "Forum Term of Service",
                    style: GoogleFonts.roboto(
                        color: Colors.black87,
                        fontWeight: FontWeight.w400,
                        fontSize: 18),
                  ),
                ),
                Container(width: 32.0, height: 0.0),
              ],
            ),
          ),
          //Bottom Button
          Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              Container(
                decoration: BoxDecoration(
                  boxShadow: [BoxShadow(color: Colors.grey.withOpacity(0.5))],
                  color: Color(0xffe5eef8),
                ),
                padding: EdgeInsets.only(top: 10, bottom: 10),
                child: Column(
                  children: [
                    Container(
                      height: 30,
                      width: double.infinity,
                      padding: EdgeInsets.only(
                        left: 135,
                        right: 135,
                        top: 3,
                        bottom: 3,
                      ),
                      child: InkWell(
                        onTap: () {
                          Navigator.pushAndRemoveUntil(
                              context,
                              MaterialPageRoute(
                                  builder: (builder) => AboutPage()),
                              (route) => false);
                        },
                        child: Container(
                          decoration: BoxDecoration(
                              color: Colors.red[100],
                              borderRadius: BorderRadius.circular(18)),
                          child: Center(
                            child: Text(
                              "Back",
                              style: TextStyle(
                                  color: Colors.red[400], fontSize: 17),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
