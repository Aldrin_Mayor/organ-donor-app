import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:organ_donor_app/Service/Auth_Service.dart';
import 'package:organ_donor_app/pages/SignUpPage.dart';
import 'package:otp_text_field/otp_field.dart';
import 'package:otp_text_field/otp_field_style.dart';
import 'package:otp_text_field/style.dart';
import 'package:google_fonts/google_fonts.dart';

class Verification extends StatefulWidget {
  final String mobileNumber;

  Verification({
    required this.mobileNumber,
  });

  @override
  _VerificationState createState() => _VerificationState();
}

class _VerificationState extends State<Verification> {
  String? mobileNumber;

  _VerificationState({this.mobileNumber});

  int start = 300;
  bool wait = false;
  String buttonName = "Send OTP Code";
  AuthClass authClass = AuthClass();
  String verificationIdFinal = "";

  String smsCode = "";
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: Stack(
        children: [
          Container(
            decoration: BoxDecoration(
              image: DecorationImage(
                  image: AssetImage('assets/5.png'), fit: BoxFit.cover),
            ),
          ),
          SingleChildScrollView(
            child: Container(
              margin: EdgeInsets.only(top: 30),
              child: Center(
                child: Column(
                  children: [
                    Row(
                      children: [
                        IconButton(
                          icon: SvgPicture.asset('assets/back.svg'),
                          iconSize: 5,
                          onPressed: () {
                            Navigator.of(context).push(MaterialPageRoute(
                                builder: (context) => SignUpPage()));
                          },
                        ),
                      ],
                    ),
                    Container(
                      width: 150,
                      height: 150,
                      padding: EdgeInsets.all(20),
                      decoration: BoxDecoration(
                          shape: BoxShape.circle, color: Colors.grey.shade200),
                      child: Transform.rotate(
                        angle: 38,
                        child: Image(image: AssetImage('assets/email.png')),
                      ),
                    ),
                    SizedBox(height: 25),
                    Text(
                      'Verification',
                      style:
                          TextStyle(fontSize: 26, fontWeight: FontWeight.bold),
                    ),
                    SizedBox(height: 25),
                    Text(
                      'Please Enter the Code that will be sent to \n' +
                          '(+63) ' +
                          widget.mobileNumber,
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          fontSize: 16, color: Colors.grey[500], height: 1.5),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 20),
                      decoration: BoxDecoration(
                          color: Colors.red[100],
                          borderRadius: BorderRadius.circular(10)),
                      child: InkWell(
                        onTap: wait
                            ? null
                            : () async {
                                startTimer();
                                setState(() {
                                  start = 300;
                                  wait = true;
                                  buttonName = "Resend OTP Code";
                                });
                                await authClass.verifyPhoneNumber(
                                    "+63 ${widget.mobileNumber}",
                                    context,
                                    setData);
                              },
                        child: Container(
                          margin: EdgeInsets.only(
                              top: 5, bottom: 5, left: 10, right: 10),
                          child: Text(
                            buttonName,
                            style: TextStyle(
                                fontSize: 20,
                                fontWeight: FontWeight.w600,
                                color: wait ? Colors.grey : Colors.red[400],
                                height: 1.5),
                          ),
                        ),
                      ),
                    ),
                    SizedBox(height: 25),
                    SizedBox(
                      height: 55,
                      width: 400,
                      child: OTPTextField(
                        length: 6,
                        width: MediaQuery.of(context).size.width - 34,
                        fieldWidth: 48,
                        otpFieldStyle: OtpFieldStyle(
                            backgroundColor: Colors.red.shade50,
                            borderColor: Colors.black),
                        style: TextStyle(fontSize: 17),
                        textFieldAlignment: MainAxisAlignment.spaceEvenly,
                        fieldStyle: FieldStyle.box,
                        onCompleted: (pin) {
                          print("Completed: " + pin);
                          setState(() {
                            smsCode = pin;
                          });
                        },
                      ),
                    ),
                    SizedBox(height: 25),
                    RichText(
                      text: TextSpan(
                        children: [
                          TextSpan(
                            text: "Send OTP-Code again in",
                            style: TextStyle(fontSize: 16, color: Colors.black),
                          ),
                          TextSpan(
                            text: " $start s",
                            style: TextStyle(fontSize: 16, color: Colors.black),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(height: 25),
                    SizedBox(
                      height: 55,
                      width: 300,
                      child: InkWell(
                        onTap: () async {
                          await authClass.signInwithPhoneNumber(
                              verificationIdFinal,
                              smsCode,
                              context,
                              widget.mobileNumber);
                        },
                        child: Container(
                          height: 60,
                          width: MediaQuery.of(context).size.width - 60,
                          decoration: BoxDecoration(
                              color: Colors.red,
                              borderRadius: BorderRadius.circular(20)),
                          child: Center(
                            child: Text(
                              "Verify Account",
                              style: GoogleFonts.roboto(
                                  color: Colors.white,
                                  fontWeight: FontWeight.w500,
                                  fontSize: 20),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  void startTimer() {
    const onsec = Duration(seconds: 1);
    Timer timer = Timer.periodic(onsec, (timer) {
      if (start == 0) {
        setState(() {
          timer.cancel();
          wait = false;
        });
      } else {
        setState(() {
          start--;
        });
      }
    });
  }

  void setData(String verificationId) {
    setState(() {
      verificationIdFinal = verificationId;
    });
    startTimer();
  }
}
