import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:organ_donor_app/pages/DasboardHomePage.dart';
import 'package:organ_donor_app/Service/Auth_Service.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:intl/intl.dart';
import 'package:organ_donor_app/pages/api/notification_api.dart';

class MakePosts extends StatefulWidget {
  MakePosts({Key? key}) : super(key: key);

  @override
  _MakePostsState createState() => _MakePostsState();
}

class _MakePostsState extends State<MakePosts> {
  String? donorFullName;
  String? donorUserType;
  String? donorUserName;
  String? donorCountPosts;

  String? patientUserName;
  String? patientFullName;
  String? patientUserType;
  String? patientCountPosts;

  AuthClass authClass = AuthClass();

  String? usersValue;
  String? fullName;
  String? userName;

  String? title = "";
  String? textTitle = "";
  final formKey = GlobalKey<FormState>();

  TextEditingController _title = TextEditingController();
  TextEditingController _content = TextEditingController();

  final toast = FToast();

  @override
  void initState() {
    super.initState();
    toast.init(context);
    NotificationApi.init();
  }

  //Time and Date
  var now = new DateTime.now();
  var formatter = new DateFormat('dd-MM-yyyy');

  //Initialized
  String? countPost = "0";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: Color(0xffe5eef8),
      body: Stack(
        children: [
          //Header
          Container(
            decoration: BoxDecoration(
              color: Color(0xffe5eef8),
              boxShadow: [
                BoxShadow(
                  color: Colors.grey.withOpacity(0.5),
                  blurRadius: 8,
                ),
              ],
            ),
            height: 80,
            margin: EdgeInsets.only(top: 0, left: 0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                InkWell(
                  onTap: () {
                    Navigator.of(context).pop();
                  },
                  child: Padding(
                    padding: const EdgeInsets.only(top: 26, left: 20),
                    child: Row(
                      children: [
                        Icon(Icons.close_outlined, size: 25.0),
                        Container(
                          margin: EdgeInsets.only(left: 20),
                          child: Text(
                            "Make Post",
                            style: GoogleFonts.roboto(
                              color: Colors.black87,
                              fontWeight: FontWeight.w500,
                              fontSize: 18,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                InkWell(
                  onTap: () {
                    final isValid = formKey.currentState!.validate();

                    if (isValid) {
                      formKey.currentState!.save();

                      if (usersValue == 'I am a Donor') {
                        //Post Count

                        int x = int.parse(donorCountPosts!) + 1;
                        countPost = x.toString();
                        postAddDonor();
                        postAddAll();

                        //Make Post
                        donorPosts();
                        allPosts();
                        postSpecificDonor();
                        postSpecificAll();
                        Navigator.pushAndRemoveUntil(
                            context,
                            MaterialPageRoute(
                                builder: (builder) => DashboardHomePage()),
                            (route) => false);
                        showToast();
                        NotificationApi.showNotification(
                          title: "Post Success!",
                          body: _title.text,
                        );
                      } else {
                        //Post Count

                        int x = int.parse(patientCountPosts!) + 1;
                        countPost = x.toString();
                        postAddPatient();
                        postAddAll();

                        //Make Post
                        patientPosts();
                        allPosts();
                        postSpecificPatient();
                        postSpecificAll();
                        Navigator.pushAndRemoveUntil(
                            context,
                            MaterialPageRoute(
                                builder: (builder) => DashboardHomePage()),
                            (route) => false);
                        showToast();
                        NotificationApi.showNotification(
                          title: "Post Success!",
                          body: _title.text,
                        );
                      }
                    }
                  },
                  child: Padding(
                    padding: const EdgeInsets.only(top: 26, right: 20),
                    child: Text(
                      'Post',
                      style: GoogleFonts.roboto(
                        color: Colors.black54,
                        fontWeight: FontWeight.w500,
                        fontSize: 18,
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
          //Content
          Form(
            key: formKey,
            child: Column(
              children: [
                //Post Title
                Container(
                  margin: EdgeInsets.only(top: 150, left: 15, right: 15),
                  child: InkWell(
                    onTap: () {
                      if (usersValue == 'I am a Donor') {
                        showModalBottomSheet(
                          context: context,
                          builder: (context) => buildDonorTitleSheet(),
                        );
                      } else {
                        showModalBottomSheet(
                          context: context,
                          builder: (context) => buildPatientTitleSheet(),
                        );
                      }
                    },
                    child: TextFormField(
                      decoration: InputDecoration(
                        filled: true,
                        fillColor: Colors.white,
                        errorStyle:
                            TextStyle(fontSize: 14, color: Colors.red[400]),
                        hintText: 'An Interesting Title',
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10.0),
                          borderSide: BorderSide.none,
                        ),
                      ),

                      controller: _title..text = title!,
                      key: Key(title.toString()), // <- Magic!
                      maxLines: 2,
                      readOnly: true,
                      enabled: false,
                      validator: (value) {
                        if (value!.isEmpty) {
                          return 'This Section is Required';
                        } else {
                          return null;
                        }
                      },
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 15, right: 15),
                  child: Divider(color: Colors.grey),
                ),
                //Text Post
                Container(
                  margin: EdgeInsets.only(top: 10, left: 15, right: 15),
                  child: InkWell(
                    onTap: () {
                      if (usersValue == 'I am a Donor') {
                        showModalBottomSheet(
                          context: context,
                          builder: (context) => buildDonorTextTitleSheet(),
                        );
                      } else {
                        showModalBottomSheet(
                          context: context,
                          builder: (context) => buildPatientTextTitleSheet(),
                        );
                      }
                    },
                    child: TextFormField(
                      decoration: InputDecoration(
                          filled: true,
                          fillColor: Colors.white,
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10.0),
                            borderSide: BorderSide.none,
                          ),
                          hintText: 'Your text post (Optional)'),
                      controller: _content..text = textTitle!,
                      key: Key(textTitle.toString()), // <- Magic!
                      maxLines: 5,
                      enabled: false,
                    ),
                  ),
                ),
                // Checks if User is Donor or Patient
                FutureBuilder(
                  future: getUserData(),
                  builder: (context, snapshot) {
                    if (snapshot.connectionState != ConnectionState.done)
                      return Text("");
                    if (donorUserType != null) {
                      usersValue = donorUserType;
                      countPost = donorCountPosts;
                      return Container(
                          height: 20,
                          width: 240,
                          child: Text(
                            '',
                            style: GoogleFonts.roboto(
                                color: Colors.black87,
                                fontSize: 15,
                                fontWeight: FontWeight.w500),
                          ));
                    }
                    if (patientUserType != null) {
                      usersValue = patientUserType;
                      countPost = patientCountPosts;
                      return Container(
                          height: 20,
                          width: 240,
                          child: Text(
                            '',
                            style: GoogleFonts.roboto(
                                color: Colors.black87,
                                fontSize: 15,
                                fontWeight: FontWeight.w500),
                          ));
                    } else {
                      return Text(
                        "",
                        style: GoogleFonts.roboto(
                            color: Colors.black54, fontSize: 14),
                      );
                    }
                  },
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  // Bottom Sheet For Donor Title
  Widget buildDonorTitleSheet() => Container(
        height: 300,
        child: ListView(
          children: [
            //Header Donor Title
            ListTile(
              title: Row(
                children: [
                  Text(
                    'Choose a Title',
                    style: GoogleFonts.roboto(
                        color: Colors.black87,
                        fontSize: 15,
                        fontWeight: FontWeight.w500),
                  ),
                  Container(
                    margin: EdgeInsets.only(left: 10),
                    child: Icon(
                      Icons.select_all_outlined,
                      color: Colors.red,
                    ),
                  ),
                ],
              ),
            ),
            //Donor Selection
            ListTile(
              title: Container(
                padding: EdgeInsets.all(15),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5),
                    color: Colors.red[100]),
                child: Text(
                  'Donor: Looking for a Kidney patient',
                  style:
                      GoogleFonts.roboto(color: Colors.black87, fontSize: 15),
                ),
              ),
              onTap: () {
                setState(() {
                  title = "[Donor]: Looking for a Kidney patient";
                  Navigator.of(context).pop();
                });
              },
            ),

            ListTile(
              title: Container(
                padding: EdgeInsets.all(15),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5),
                    color: Colors.red[100]),
                child: Text(
                  'Donor: Looking for available Kidney patient on the waiting list',
                  style:
                      GoogleFonts.roboto(color: Colors.black87, fontSize: 15),
                ),
              ),
              onTap: () {
                setState(() {
                  title =
                      "[Donor]: Looking for available Kidney patient on the waiting list";
                  Navigator.of(context).pop();
                });
              },
            ),
            ListTile(
              title: Container(
                padding: EdgeInsets.all(15),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5),
                    color: Colors.red[100]),
                child: Text(
                  'Donor: Looking for best match Kidney patient for me',
                  style:
                      GoogleFonts.roboto(color: Colors.black87, fontSize: 15),
                ),
              ),
              onTap: () {
                setState(() {
                  title =
                      "[Donor]: Looking for best match Kidney patient for me";
                  Navigator.of(context).pop();
                });
              },
            ),
          ],
        ),
      );

  // Bottom Sheet For Patient Title
  Widget buildPatientTitleSheet() => Container(
        height: 300,
        child: ListView(
          children: [
            //Header Patient Title
            ListTile(
              title: Row(
                children: [
                  Text(
                    'Choose a Title',
                    style: GoogleFonts.roboto(
                        color: Colors.black87,
                        fontSize: 15,
                        fontWeight: FontWeight.w500),
                  ),
                  Container(
                    margin: EdgeInsets.only(left: 10),
                    child: Icon(
                      Icons.select_all_outlined,
                      color: Colors.red,
                    ),
                  )
                ],
              ),
            ),
            //Patient Selection
            ListTile(
              title: Container(
                padding: EdgeInsets.all(15),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5),
                    color: Colors.red[100]),
                child: Text(
                  'Patient: Looking for a Kidney Donor',
                  style:
                      GoogleFonts.roboto(color: Colors.black87, fontSize: 15),
                ),
              ),
              onTap: () {
                setState(() {
                  title = "[Patient]: Looking for a Kidney Donor";
                  Navigator.of(context).pop();
                });
              },
            ),
            ListTile(
              title: Container(
                padding: EdgeInsets.all(15),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5),
                    color: Colors.red[100]),
                child: Text(
                  'Patient: Looking available donor for me?',
                  style:
                      GoogleFonts.roboto(color: Colors.black87, fontSize: 15),
                ),
              ),
              onTap: () {
                setState(() {
                  title = "[Patient]: Looking available donor for me?";
                  Navigator.of(context).pop();
                });
              },
            ),
            ListTile(
              title: Container(
                padding: EdgeInsets.all(15),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(5),
                  color: Colors.red[100],
                ),
                child: Text(
                  'Patient: Looking for best match Kidney donor for me',
                  style:
                      GoogleFonts.roboto(color: Colors.black87, fontSize: 15),
                ),
              ),
              onTap: () {
                setState(() {
                  title =
                      "[Patient]: Looking for best match Kidney donor for me";
                  Navigator.of(context).pop();
                });
              },
            ),
          ],
        ),
      );

  // Bottom Sheet For Donor Text Title
  Widget buildDonorTextTitleSheet() => Container(
        height: 350,
        child: ListView(
          children: [
            //Header Donor
            ListTile(
              title: Row(
                children: [
                  Text(
                    'Choose a Message',
                    style: GoogleFonts.roboto(
                        color: Colors.black87,
                        fontSize: 15,
                        fontWeight: FontWeight.w500),
                  ),
                  Container(
                    margin: EdgeInsets.only(left: 10),
                    child: Icon(
                      Icons.message_outlined,
                      color: Colors.red,
                    ),
                  ),
                ],
              ),
            ),
            //Donor Text Selection
            ListTile(
              title: Container(
                padding: EdgeInsets.all(15),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5),
                    color: Colors.red[100]),
                child: Text(
                  'Hello Doctor, I am Looking for a patient to donote my Kidney organ. Thank you',
                  style:
                      GoogleFonts.roboto(color: Colors.black87, fontSize: 15),
                ),
              ),
              onTap: () {
                setState(() {
                  textTitle =
                      "Hello Doctor, I am Looking for a patient to donote my Kidney organ. Thank you";
                  Navigator.of(context).pop();
                });
              },
            ),

            ListTile(
              title: Container(
                padding: EdgeInsets.all(15),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5),
                    color: Colors.red[100]),
                child: Text(
                  'Hello Doctor, I just want to know if there is an available Kidney patient on your waiting list. Thank you.',
                  style:
                      GoogleFonts.roboto(color: Colors.black87, fontSize: 15),
                ),
              ),
              onTap: () {
                setState(() {
                  textTitle =
                      "Hello Doctor, I just want to know if there is an available Kidney patient on your waiting list. Thank you.";
                  Navigator.of(context).pop();
                });
              },
            ),
            ListTile(
              title: Container(
                padding: EdgeInsets.all(15),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5),
                    color: Colors.red[100]),
                child: Text(
                  'Hello Doctor, Is there an available best match Kidney patient for me on your waiting list? Thank you.',
                  style:
                      GoogleFonts.roboto(color: Colors.black87, fontSize: 15),
                ),
              ),
              onTap: () {
                setState(() {
                  textTitle =
                      "Hello Doctor, Is there an available best match Kidney patient for me on your waiting list? Thank you.";
                  Navigator.of(context).pop();
                });
              },
            ),
          ],
        ),
      );
  // Bottom Sheet For Patient Text Title
  Widget buildPatientTextTitleSheet() => Container(
        height: 350,
        child: ListView(
          children: [
            //Header Patient
            ListTile(
              title: Row(
                children: [
                  Text(
                    'Choose a Message',
                    style: GoogleFonts.roboto(
                        color: Colors.black87,
                        fontSize: 15,
                        fontWeight: FontWeight.w500),
                  ),
                  Container(
                    margin: EdgeInsets.only(left: 10),
                    child: Icon(
                      Icons.message_outlined,
                      color: Colors.red,
                    ),
                  ),
                ],
              ),
            ),
            //Selection
            ListTile(
              title: Container(
                padding: EdgeInsets.all(15),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5),
                    color: Colors.red[100]),
                child: Text(
                  'Hello Doctor, I am Looking for a donor who is willing to donate a Kidney organ. Thank you.',
                  style:
                      GoogleFonts.roboto(color: Colors.black87, fontSize: 15),
                ),
              ),
              onTap: () {
                setState(() {
                  textTitle =
                      "Hello Doctor, I am Looking for a donor who is willing to donate a Kidney organ. Thank you.";
                  Navigator.of(context).pop();
                });
              },
            ),
            ListTile(
              title: Container(
                padding: EdgeInsets.all(15),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5),
                    color: Colors.red[100]),
                child: Text(
                  'Hello Doctor, I just want to know if there is an available Kidney Donor for me? Thank you.',
                  style:
                      GoogleFonts.roboto(color: Colors.black87, fontSize: 15),
                ),
              ),
              onTap: () {
                setState(() {
                  textTitle =
                      "Hello Doctor, I just want to know if there is an available Kidney Donor for me? Thank you.";
                  Navigator.of(context).pop();
                });
              },
            ),
            ListTile(
              title: Container(
                padding: EdgeInsets.all(15),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5),
                    color: Colors.red[100]),
                child: Text(
                  'Hello Doctor, Is there an available best match Kidney donor for me? Thank you',
                  style:
                      GoogleFonts.roboto(color: Colors.black87, fontSize: 15),
                ),
              ),
              onTap: () {
                setState(() {
                  textTitle =
                      "Hello Doctor, Is there an available best match Kidney donor for me? Thank you";
                  Navigator.of(context).pop();
                });
              },
            ),
          ],
        ),
      );

  //Show Toast Message
  void showToast() =>
      toast.showToast(child: buildToast(), gravity: ToastGravity.CENTER);

  Widget buildToast() => Container(
        padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
        decoration: BoxDecoration(
          color: Color(0xFF111111).withOpacity(0.6),
          borderRadius: BorderRadius.circular(5),
        ),
        child: Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            Text(
              "Posted",
              style: TextStyle(color: Colors.white, fontSize: 16),
            ),
          ],
        ),
      );

  //Get User Information
  getUserData() async {
    final donor = FirebaseAuth.instance.currentUser;
    if (donor != null) {
      await FirebaseFirestore.instance
          .collection('Users')
          .doc("Donor")
          .collection("Lists")
          .doc(donor.uid)
          .get()
          .then((value) {
        donorFullName = value.data()!['FullName'];
        donorUserName = value.data()!['UserName'];
        donorUserType = value.data()!['User Type'];
        donorCountPosts = value.data()!['Posts'];
        fullName = donorFullName;
        userName = donorUserName;
        print(donorFullName);
        print(donorUserType);
      }).catchError((e) {
        print(e);
      });
    }

    final patient = FirebaseAuth.instance.currentUser;
    if (patient != null) {
      await FirebaseFirestore.instance
          .collection('Users')
          .doc("Patient")
          .collection("Lists")
          .doc(patient.uid)
          .get()
          .then((value) {
        patientFullName = value.data()!['FullName'];
        patientUserName = value.data()!['UserName'];
        patientUserType = value.data()!['User Type'];
        patientCountPosts = value.data()!['Posts'];
        fullName = patientFullName;
        userName = patientUserName;
        print(patientFullName);
        print(patientUserType);
      }).catchError((e) {
        print(e);
      });
    }
  }

  donorPosts() async {
    User? user = FirebaseAuth.instance.currentUser;
    String formattedTime = DateFormat('kk:mm:s:a').format(now);
    String formattedDate = formatter.format(now);
    await FirebaseFirestore.instance
        .collection('Posts')
        .doc('Donor')
        .collection('All Posts')
        .doc(formattedTime)
        .set({
      'UserID': user!.uid,
      'FullName': donorFullName,
      'UserName': donorUserName,
      'User Type': usersValue,
      'Post Title': _title.text,
      'Post Content': _content.text,
      'Date': formattedDate,
      'Time': formattedTime,
      'Views': '0',
      'Comments': '0',
      'Likes': '0',
      'Notifications': '0'
    });
  }

  patientPosts() async {
    User? user = FirebaseAuth.instance.currentUser;
    String formattedTime = DateFormat('kk:mm:s:a').format(now);
    String formattedDate = formatter.format(now);
    await FirebaseFirestore.instance
        .collection('Posts')
        .doc('Patient')
        .collection('All Posts')
        .doc(formattedTime)
        .set({
      'UserID': user!.uid,
      'FullName': patientFullName,
      'UserName': patientUserName,
      'User Type': usersValue,
      'Post Title': _title.text,
      'Post Content': _content.text,
      'Date': formattedDate,
      'Time': formattedTime,
      'Views': '0',
      'Comments': '0',
      'Likes': '0',
      'Notifications': '0'
    });
  }

  allPosts() async {
    User? user = FirebaseAuth.instance.currentUser;
    String formattedTime = DateFormat('kk:mm:s:a').format(now);
    String formattedDate = formatter.format(now);
    await FirebaseFirestore.instance
        .collection('Posts')
        .doc('All')
        .collection('All Posts')
        .doc(formattedTime)
        .set({
      'UserID': user!.uid,
      'FullName': fullName,
      'UserName': userName,
      'User Type': usersValue,
      'Post Title': _title.text,
      'Post Content': _content.text,
      'Date': formattedDate,
      'Time': formattedTime,
      'Views': '0',
      'Comments': '0',
      'Likes': '0',
      'Notifications': '0'
    });
  }

  //Post to Current Donor Document

  postSpecificDonor() async {
    User? user = FirebaseAuth.instance.currentUser;
    String formattedTime = DateFormat('kk:mm:s:a').format(now);
    String formattedDate = formatter.format(now);
    await FirebaseFirestore.instance
        .collection('Users')
        .doc('Donor')
        .collection('Lists')
        .doc(user!.uid)
        .collection('Posts')
        .doc(formattedTime)
        .set({
      'UserID': user.uid,
      'FullName': donorFullName,
      'UserName': donorUserName,
      'User Type': usersValue,
      'Post Title': _title.text,
      'Post Content': _content.text,
      'Date': formattedDate,
      'Time': formattedTime,
      'Views': '0',
      'Comments': '0',
      'Likes': '0',
      'Notifications': '0'
    });
  }

  postSpecificPatient() async {
    User? user = FirebaseAuth.instance.currentUser;
    String formattedTime = DateFormat('kk:mm:s:a').format(now);
    String formattedDate = formatter.format(now);
    await FirebaseFirestore.instance
        .collection('Users')
        .doc('Patient')
        .collection('Lists')
        .doc(user!.uid)
        .collection('Posts')
        .doc(formattedTime)
        .set({
      'UserID': user.uid,
      'FullName': patientFullName,
      'UserName': patientUserName,
      'User Type': usersValue,
      'Post Title': _title.text,
      'Post Content': _content.text,
      'Date': formattedDate,
      'Time': formattedTime,
      'Views': '0',
      'Comments': '0',
      'Likes': '0',
      'Notifications': '0'
    });
  }

  postSpecificAll() async {
    User? user = FirebaseAuth.instance.currentUser;
    String formattedTime = DateFormat('kk:mm:s:a').format(now);
    String formattedDate = formatter.format(now);
    await FirebaseFirestore.instance
        .collection('Users')
        .doc('All')
        .collection('Lists')
        .doc(user!.uid)
        .collection('Posts')
        .doc(formattedTime)
        .set({
      'UserID': user.uid,
      'FullName': fullName,
      'UserName': userName,
      'User Type': usersValue,
      'Post Title': _title.text,
      'Post Content': _content.text,
      'Date': formattedDate,
      'Time': formattedTime,
      'Views': '0',
      'Comments': '0',
      'Likes': '0',
      'Notifications': '0'
    });
  }

  //Count Posts
  postAddDonor() async {
    User? user = FirebaseAuth.instance.currentUser;
    await FirebaseFirestore.instance
        .collection("Users")
        .doc('Donor')
        .collection('Lists')
        .doc(user!.uid)
        .update({
      'Posts': countPost,
    });
  }

  postAddPatient() async {
    User? user = FirebaseAuth.instance.currentUser;
    await FirebaseFirestore.instance
        .collection("Users")
        .doc('Patient')
        .collection('Lists')
        .doc(user!.uid)
        .update({
      'Posts': countPost,
    });
  }

  postAddAll() async {
    User? user = FirebaseAuth.instance.currentUser;
    await FirebaseFirestore.instance
        .collection("Users")
        .doc('All')
        .collection('Lists')
        .doc(user!.uid)
        .update({
      'Posts': countPost,
    });
  }
}
