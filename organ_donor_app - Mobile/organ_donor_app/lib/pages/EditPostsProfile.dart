import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:intl/intl.dart';
import 'package:organ_donor_app/pages/DashboardProfilePage.dart';

class EditPostsProfile extends StatefulWidget {
  final String? getUserName;
  final String? getFullName;
  final String? getUserID;
  final String? getUserType;
  final String? getDate;
  final String? getTime;
  final String? getPostTitle;
  final String? getPostContent;

  EditPostsProfile({
    required this.getUserName,
    required this.getFullName,
    required this.getUserID,
    required this.getUserType,
    required this.getDate,
    required this.getTime,
    required this.getPostTitle,
    required this.getPostContent,
  });

  @override
  _EditPostsProfileState createState() => _EditPostsProfileState();
}

class _EditPostsProfileState extends State<EditPostsProfile> {
  String? getUserName;
  String? getFullName;
  String? getUserID;
  String? getUserType;
  String? getDate;
  String? getTime;
  String? getPostTitle;
  String? getPostContent;
  String? getPostViews;
  String? getPostComments;
  String? getPostLikes;
  _EditPostsProfileState({
    this.getUserName,
    this.getFullName,
    this.getUserID,
    this.getUserType,
    this.getDate,
    this.getTime,
    this.getPostTitle,
    this.getPostContent,
  });

  //Time and Date
  var now = new DateTime.now();
  var formatter = new DateFormat('dd-MM-yyyy');

  //Toast Message
  final toast = FToast();
  @override
  void initState() {
    super.initState();

    toast.init(context);
  }

  String? title = "";
  String? textTitle = "";
  final formKey = GlobalKey<FormState>();

  TextEditingController _title = TextEditingController();
  TextEditingController _content = TextEditingController();

  String? donorFullName;
  String? donorUserType;
  String? donorUserName;

  String? patientUserName;
  String? patientFullName;
  String? patientUserType;
  String? usersValue;
  String? fullName;
  String? userName;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: Color(0xffe5eef8),
      body: Stack(
        children: [
          //Header
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                decoration: BoxDecoration(
                  color: Color(0xffe5eef8),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey.withOpacity(0.5),
                      blurRadius: 8,
                    ),
                  ],
                ),
                height: 80,
                margin: EdgeInsets.only(top: 0, left: 0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    InkWell(
                      onTap: () {
                        Navigator.of(context).pop();
                      },
                      child: Padding(
                        padding: const EdgeInsets.only(top: 26, left: 20),
                        child: Row(
                          children: [
                            Icon(Icons.close_outlined, size: 25.0),
                            Container(
                              margin: EdgeInsets.only(left: 20),
                              child: Text(
                                "Edit Post",
                                style: GoogleFonts.roboto(
                                  color: Colors.black87,
                                  fontWeight: FontWeight.w500,
                                  fontSize: 18,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    InkWell(
                      onTap: () {
                        final isValid = formKey.currentState!.validate();

                        if (isValid) {
                          formKey.currentState!.save();
                          if (widget.getUserType == 'I am a Donor') {
                            editDonorPost();
                            editAllPost();
                            editSpecificDonorPost();
                            editSpecificAllPost();
                            Navigator.pushAndRemoveUntil(
                                context,
                                MaterialPageRoute(
                                    builder: (builder) =>
                                        DashboardProfilePage()),
                                (route) => false);
                            showToast();
                          } else {
                            editPatientPost();
                            editAllPost();
                            editSpecificPatientPost();
                            editSpecificAllPost();
                            Navigator.pushAndRemoveUntil(
                                context,
                                MaterialPageRoute(
                                    builder: (builder) =>
                                        DashboardProfilePage()),
                                (route) => false);
                            showToast();
                          }
                        }
                      },
                      child: Padding(
                        padding: const EdgeInsets.only(top: 26, right: 20),
                        child: Text(
                          'Save',
                          style: GoogleFonts.roboto(
                            color: Colors.black54,
                            fontWeight: FontWeight.w500,
                            fontSize: 18,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              //Profile Details
              Container(
                margin: EdgeInsets.only(top: 20, left: 20),
                child: Row(
                  children: [
                    Image.asset(
                      'assets/user.png',
                      height: 45,
                    ),
                    Container(
                      margin: EdgeInsets.only(left: 10),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                            child: Text(
                              widget.getUserName!,
                              style: GoogleFonts.roboto(
                                color: Colors.black87,
                                fontWeight: FontWeight.w500,
                                fontSize: 18,
                              ),
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.only(top: 5),
                            child: Text(widget.getUserType!),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: 20, left: 20),
                child: Row(
                  children: [
                    Text(
                      'Date Posted:',
                      style: GoogleFonts.roboto(
                        color: Colors.grey,
                        fontWeight: FontWeight.w500,
                        fontSize: 14,
                      ),
                    ),
                    Text(
                      "\t" + widget.getDate!,
                      style: GoogleFonts.roboto(
                        color: Colors.grey,
                        fontWeight: FontWeight.w500,
                        fontSize: 14,
                      ),
                    ),
                    Text(
                      "\t" + widget.getTime!,
                      style: GoogleFonts.roboto(
                        color: Colors.grey,
                        fontWeight: FontWeight.w500,
                        fontSize: 14,
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),

          //Content
          Form(
            key: formKey,
            child: Column(
              children: [
                //Post Title
                Container(
                  margin: EdgeInsets.only(top: 200, left: 15, right: 15),
                  child: InkWell(
                    onTap: () {
                      if (widget.getUserType == 'I am a Donor') {
                        showModalBottomSheet(
                          context: context,
                          builder: (context) => buildDonorTitleSheet(),
                        );
                      } else {
                        showModalBottomSheet(
                          context: context,
                          builder: (context) => buildPatientTitleSheet(),
                        );
                      }
                    },
                    child: TextFormField(
                      decoration: InputDecoration(
                        filled: true,
                        fillColor: Colors.white,
                        errorStyle:
                            TextStyle(fontSize: 14, color: Colors.red[400]),
                        hintText: 'An Interesting Title',
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10.0),
                          borderSide: BorderSide.none,
                        ),
                      ),

                      controller: _title..text = title!,
                      key: Key(title.toString()), // <- Magic!
                      maxLines: 2,
                      readOnly: true,
                      enabled: false,
                      validator: (value) {
                        if (value!.isEmpty) {
                          return 'This Section is Required';
                        } else {
                          return null;
                        }
                      },
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 15, right: 15),
                  child: Divider(color: Colors.grey),
                ),
                //Text Post
                Container(
                  margin: EdgeInsets.only(top: 10, left: 15, right: 15),
                  child: InkWell(
                    onTap: () {
                      if (usersValue == 'I am a Donor') {
                        showModalBottomSheet(
                          context: context,
                          builder: (context) => buildDonorTextTitleSheet(),
                        );
                      } else {
                        showModalBottomSheet(
                          context: context,
                          builder: (context) => buildPatientTextTitleSheet(),
                        );
                      }
                    },
                    child: TextFormField(
                      decoration: InputDecoration(
                          filled: true,
                          fillColor: Colors.white,
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10.0),
                            borderSide: BorderSide.none,
                          ),
                          hintText: 'Your text post (Optional)'),
                      controller: _content..text = textTitle!,
                      key: Key(textTitle.toString()), // <- Magic!
                      maxLines: 5,
                      enabled: false,
                    ),
                  ),
                ),
                // Checks if User is Donor or Patient
                FutureBuilder(
                  future: getUserData(),
                  builder: (context, snapshot) {
                    if (snapshot.connectionState != ConnectionState.done)
                      return Text("");
                    if (donorUserType != null) {
                      usersValue = donorUserType;
                      return Container(
                          height: 20,
                          width: 240,
                          child: Text(
                            '',
                            style: GoogleFonts.roboto(
                                color: Colors.black87,
                                fontSize: 15,
                                fontWeight: FontWeight.w500),
                          ));
                    }
                    if (patientUserType != null) {
                      usersValue = patientUserType;
                      return Container(
                          height: 20,
                          width: 240,
                          child: Text(
                            '',
                            style: GoogleFonts.roboto(
                                color: Colors.black87,
                                fontSize: 15,
                                fontWeight: FontWeight.w500),
                          ));
                    } else {
                      return Text(
                        "",
                        style: GoogleFonts.roboto(
                            color: Colors.black54, fontSize: 14),
                      );
                    }
                  },
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  // Bottom Sheet For Donor Title
  Widget buildDonorTitleSheet() => Container(
        height: 300,
        child: ListView(
          children: [
            //Header Donor Title
            ListTile(
              title: Row(
                children: [
                  Text(
                    'Choose a Title',
                    style: GoogleFonts.roboto(
                        color: Colors.black87,
                        fontSize: 15,
                        fontWeight: FontWeight.w500),
                  ),
                  Container(
                    margin: EdgeInsets.only(left: 10),
                    child: Icon(
                      Icons.select_all_outlined,
                      color: Colors.red,
                    ),
                  ),
                ],
              ),
            ),
            //Donor Selection
            ListTile(
              title: Container(
                padding: EdgeInsets.all(15),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5),
                    color: Colors.red[100]),
                child: Text(
                  'Donor: Looking for a Kidney patient',
                  style:
                      GoogleFonts.roboto(color: Colors.black87, fontSize: 15),
                ),
              ),
              onTap: () {
                setState(() {
                  title = "[Donor]: Looking for a Kidney patient";
                  Navigator.of(context).pop();
                });
              },
            ),

            ListTile(
              title: Container(
                padding: EdgeInsets.all(15),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5),
                    color: Colors.red[100]),
                child: Text(
                  'Donor: Looking for available Kidney patient on the waiting list',
                  style:
                      GoogleFonts.roboto(color: Colors.black87, fontSize: 15),
                ),
              ),
              onTap: () {
                setState(() {
                  title =
                      "[Donor]: Looking for available Kidney patient on the waiting list";
                  Navigator.of(context).pop();
                });
              },
            ),
            ListTile(
              title: Container(
                padding: EdgeInsets.all(15),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5),
                    color: Colors.red[100]),
                child: Text(
                  'Donor: Looking for best match Kidney patient for me',
                  style:
                      GoogleFonts.roboto(color: Colors.black87, fontSize: 15),
                ),
              ),
              onTap: () {
                setState(() {
                  title =
                      "[Donor]: Looking for best match Kidney patient for me";
                  Navigator.of(context).pop();
                });
              },
            ),
          ],
        ),
      );

  // Bottom Sheet For Patient Title
  Widget buildPatientTitleSheet() => Container(
        height: 300,
        child: ListView(
          children: [
            //Header Patient Title
            ListTile(
              title: Row(
                children: [
                  Text(
                    'Choose a Title',
                    style: GoogleFonts.roboto(
                        color: Colors.black87,
                        fontSize: 15,
                        fontWeight: FontWeight.w500),
                  ),
                  Container(
                    margin: EdgeInsets.only(left: 10),
                    child: Icon(
                      Icons.select_all_outlined,
                      color: Colors.red,
                    ),
                  )
                ],
              ),
            ),
            //Patient Selection
            ListTile(
              title: Container(
                padding: EdgeInsets.all(15),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5),
                    color: Colors.red[100]),
                child: Text(
                  'Patient: Looking for a Kidney Donor',
                  style:
                      GoogleFonts.roboto(color: Colors.black87, fontSize: 15),
                ),
              ),
              onTap: () {
                setState(() {
                  title = "[Patient]: Looking for a Kidney Donor";
                  Navigator.of(context).pop();
                });
              },
            ),
            ListTile(
              title: Container(
                padding: EdgeInsets.all(15),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5),
                    color: Colors.red[100]),
                child: Text(
                  'Patient: Looking available donor for me?',
                  style:
                      GoogleFonts.roboto(color: Colors.black87, fontSize: 15),
                ),
              ),
              onTap: () {
                setState(() {
                  title = "[Patient]: Looking available donor for me?";
                  Navigator.of(context).pop();
                });
              },
            ),
            ListTile(
              title: Container(
                padding: EdgeInsets.all(15),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(5),
                  color: Colors.red[100],
                ),
                child: Text(
                  'Patient: Looking for best match Kidney donor for me',
                  style:
                      GoogleFonts.roboto(color: Colors.black87, fontSize: 15),
                ),
              ),
              onTap: () {
                setState(() {
                  title =
                      "[Patient]: Looking for best match Kidney donor for me";
                  Navigator.of(context).pop();
                });
              },
            ),
          ],
        ),
      );

  // Bottom Sheet For Donor Text Title
  Widget buildDonorTextTitleSheet() => Container(
        height: 350,
        child: ListView(
          children: [
            //Header Donor
            ListTile(
              title: Row(
                children: [
                  Text(
                    'Choose a Message',
                    style: GoogleFonts.roboto(
                        color: Colors.black87,
                        fontSize: 15,
                        fontWeight: FontWeight.w500),
                  ),
                  Container(
                    margin: EdgeInsets.only(left: 10),
                    child: Icon(
                      Icons.message_outlined,
                      color: Colors.red,
                    ),
                  ),
                ],
              ),
            ),
            //Donor Text Selection
            ListTile(
              title: Container(
                padding: EdgeInsets.all(15),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5),
                    color: Colors.red[100]),
                child: Text(
                  'Hello Doctor, I am Looking for a patient to donote my Kidney organ. Thank you',
                  style:
                      GoogleFonts.roboto(color: Colors.black87, fontSize: 15),
                ),
              ),
              onTap: () {
                setState(() {
                  textTitle =
                      "Hello Doctor, I am Looking for a patient to donote my Kidney organ. Thank you";
                  Navigator.of(context).pop();
                });
              },
            ),

            ListTile(
              title: Container(
                padding: EdgeInsets.all(15),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5),
                    color: Colors.red[100]),
                child: Text(
                  'Hello Doctor, I just want to know if there is an available Kidney patient on your waiting list. Thank you.',
                  style:
                      GoogleFonts.roboto(color: Colors.black87, fontSize: 15),
                ),
              ),
              onTap: () {
                setState(() {
                  textTitle =
                      "Hello Doctor, I just want to know if there is an available Kidney patient on your waiting list. Thank you.";
                  Navigator.of(context).pop();
                });
              },
            ),
            ListTile(
              title: Container(
                padding: EdgeInsets.all(15),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5),
                    color: Colors.red[100]),
                child: Text(
                  'Hello Doctor, Is there an available best match Kidney patient for me on your waiting list? Thank you.',
                  style:
                      GoogleFonts.roboto(color: Colors.black87, fontSize: 15),
                ),
              ),
              onTap: () {
                setState(() {
                  textTitle =
                      "Hello Doctor, Is there an available best match Kidney patient for me on your waiting list? Thank you.";
                  Navigator.of(context).pop();
                });
              },
            ),
          ],
        ),
      );
  // Bottom Sheet For Patient Text Title
  Widget buildPatientTextTitleSheet() => Container(
        height: 350,
        child: ListView(
          children: [
            //Header Patient
            ListTile(
              title: Row(
                children: [
                  Text(
                    'Choose a Message',
                    style: GoogleFonts.roboto(
                        color: Colors.black87,
                        fontSize: 15,
                        fontWeight: FontWeight.w500),
                  ),
                  Container(
                    margin: EdgeInsets.only(left: 10),
                    child: Icon(
                      Icons.message_outlined,
                      color: Colors.red,
                    ),
                  ),
                ],
              ),
            ),
            //Selection
            ListTile(
              title: Container(
                padding: EdgeInsets.all(15),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5),
                    color: Colors.red[100]),
                child: Text(
                  'Hello Doctor, I am Looking for a donor who is willing to donate a Kidney organ. Thank you.',
                  style:
                      GoogleFonts.roboto(color: Colors.black87, fontSize: 15),
                ),
              ),
              onTap: () {
                setState(() {
                  textTitle =
                      "Hello Doctor, I am Looking for a donor who is willing to donate a Kidney organ. Thank you.";
                  Navigator.of(context).pop();
                });
              },
            ),
            ListTile(
              title: Container(
                padding: EdgeInsets.all(15),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5),
                    color: Colors.red[100]),
                child: Text(
                  'Hello Doctor, I just want to know if there is an available Kidney Donor for me? Thank you.',
                  style:
                      GoogleFonts.roboto(color: Colors.black87, fontSize: 15),
                ),
              ),
              onTap: () {
                setState(() {
                  textTitle =
                      "Hello Doctor, I just want to know if there is an available Kidney Donor for me? Thank you.";
                  Navigator.of(context).pop();
                });
              },
            ),
            ListTile(
              title: Container(
                padding: EdgeInsets.all(15),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5),
                    color: Colors.red[100]),
                child: Text(
                  'Hello Doctor, Is there an available best match Kidney donor for me? Thank you',
                  style:
                      GoogleFonts.roboto(color: Colors.black87, fontSize: 15),
                ),
              ),
              onTap: () {
                setState(() {
                  textTitle =
                      "Hello Doctor, Is there an available best match Kidney donor for me? Thank you";
                  Navigator.of(context).pop();
                });
              },
            ),
          ],
        ),
      );

  //Show Toast Message
  void showToast() =>
      toast.showToast(child: buildToast(), gravity: ToastGravity.CENTER);

  Widget buildToast() => Container(
        padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
        decoration: BoxDecoration(
          color: Color(0xFF111111).withOpacity(0.6),
          borderRadius: BorderRadius.circular(5),
        ),
        child: Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            Text(
              "Saved",
              style: TextStyle(color: Colors.white, fontSize: 16),
            ),
          ],
        ),
      );

  //Get User Information
  getUserData() async {
    final donor = FirebaseAuth.instance.currentUser;
    if (donor != null) {
      await FirebaseFirestore.instance
          .collection('Users')
          .doc("Donor")
          .collection("Lists")
          .doc(donor.uid)
          .get()
          .then((value) {
        donorFullName = value.data()!['FullName'];
        donorUserName = value.data()!['UserName'];
        donorUserType = value.data()!['User Type'];
        fullName = donorFullName;
        userName = donorUserName;
        print(donorFullName);
        print(donorUserType);
      }).catchError((e) {
        print(e);
      });
    }

    final patient = FirebaseAuth.instance.currentUser;
    if (patient != null) {
      await FirebaseFirestore.instance
          .collection('Users')
          .doc("Patient")
          .collection("Lists")
          .doc(patient.uid)
          .get()
          .then((value) {
        patientFullName = value.data()!['FullName'];
        patientUserName = value.data()!['UserName'];
        patientUserType = value.data()!['User Type'];
        fullName = patientFullName;
        userName = patientUserName;
        print(patientFullName);
        print(patientUserType);
      }).catchError((e) {
        print(e);
      });
    }
  }

  //Edit
  editDonorPost() async {
    String? did = widget.getTime;
    await FirebaseFirestore.instance
        .collection("Posts")
        .doc('Donor')
        .collection('All Posts')
        .doc(did)
        .update({
      'Post Title': _title.text,
      'Post Content': _content.text,
    });
  }

  editPatientPost() async {
    String? did = widget.getTime;
    await FirebaseFirestore.instance
        .collection("Posts")
        .doc('Patient')
        .collection('All Posts')
        .doc(did)
        .update({
      'Post Title': _title.text,
      'Post Content': _content.text,
    });
  }

  editAllPost() async {
    String? did = widget.getTime;
    await FirebaseFirestore.instance
        .collection("Posts")
        .doc('All')
        .collection('All Posts')
        .doc(did)
        .update({
      'Post Title': _title.text,
      'Post Content': _content.text,
    });
  }

  editSpecificDonorPost() async {
    String? uid = widget.getUserID;
    String? did = widget.getTime;
    await FirebaseFirestore.instance
        .collection("Users")
        .doc('Donor')
        .collection('Lists')
        .doc(uid)
        .collection('Posts')
        .doc(did)
        .update({
      'Post Title': _title.text,
      'Post Content': _content.text,
    });
  }

  editSpecificPatientPost() async {
    String? uid = widget.getUserID;
    String? did = widget.getTime;
    await FirebaseFirestore.instance
        .collection("Users")
        .doc('Patient')
        .collection('Lists')
        .doc(uid)
        .collection('Posts')
        .doc(did)
        .update({
      'Post Title': _title.text,
      'Post Content': _content.text,
    });
  }

  editSpecificAllPost() async {
    String? uid = widget.getUserID;
    String? did = widget.getTime;
    await FirebaseFirestore.instance
        .collection("Users")
        .doc('All')
        .collection('Lists')
        .doc(uid)
        .collection('Posts')
        .doc(did)
        .update({
      'Post Title': _title.text,
      'Post Content': _content.text,
    });
  }
}
