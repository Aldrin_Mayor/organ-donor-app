import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:organ_donor_app/pages/AboutPage.dart';
import 'package:organ_donor_app/pages/DashboardProfilePage.dart';
import 'package:organ_donor_app/pages/InformationManagementPage.dart';
import 'package:organ_donor_app/Service/Auth_Service.dart';
import 'package:organ_donor_app/pages/SignInPage.dart';
import 'dart:io';
import 'package:organ_donor_app/widget/info_widget.dart';
import 'dart:ui';

class ProfileSettings extends StatefulWidget {
  ProfileSettings({Key? key}) : super(key: key);

  @override
  _ProfileSettingsState createState() => _ProfileSettingsState();
}

class _ProfileSettingsState extends State<ProfileSettings> {
  AuthClass authClass = AuthClass();

  final toast = FToast();

  @override
  void initState() {
    super.initState();

    toast.init(context);
    init();
  }

  Map<String, dynamic> mapos = {};

  Future init() async {
    final operatingSystem = await DeviceInfoApi.getOperatingSystem();

    if (!mounted) return;

    setState(() => mapos = {
          '': operatingSystem,
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xffe5eef8),
      body: Stack(
        children: [
          InfoWidgetOs(mapos: mapos),
          Container(
            margin: EdgeInsets.only(top: 40, left: 20),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                InkWell(
                  onTap: () {
                    Navigator.pushAndRemoveUntil(
                        context,
                        MaterialPageRoute(
                            builder: (builder) => DashboardProfilePage()),
                        (route) => false);
                  },
                  child: Icon(Icons.arrow_back_ios_outlined, size: 25.0),
                ),
                Text(
                  "Settings",
                  style: GoogleFonts.roboto(
                      color: Colors.black87,
                      fontWeight: FontWeight.bold,
                      fontSize: 18),
                ),
                Container(width: 32.0, height: 0.0),
              ],
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 50),
            child: ListView(
              children: [
                //Information Management
                Container(
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(10)),
                  margin: EdgeInsets.all(20),
                  child: Column(
                    children: [
                      ListTile(
                        trailing: Icon(
                          Icons.arrow_forward_ios_outlined,
                          color: Colors.grey,
                        ),
                        title: Text(
                          'Information Management',
                          style: GoogleFonts.roboto(
                              color: Colors.black87, fontSize: 15),
                        ),
                        onTap: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) =>
                                    InformationManagementPage()),
                          );
                        },
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 8, right: 8),
                        child: Divider(
                          color: Colors.black26,
                        ),
                      ),
                      ListTile(
                        trailing: Icon(
                          Icons.arrow_forward_ios_outlined,
                          color: Colors.grey,
                        ),
                        title: Text(
                          'About Organ Donor App',
                          style: GoogleFonts.roboto(
                              color: Colors.black87, fontSize: 15),
                        ),
                        onTap: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => AboutPage()),
                          );
                        },
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 8, right: 8),
                        child: Divider(
                          color: Colors.black26,
                        ),
                      ),
                      //Device Os

                      //Check for Updates
                      ListTile(
                        trailing: Icon(
                          Icons.arrow_forward_ios_outlined,
                          color: Colors.grey,
                        ),
                        title: Text(
                          'Check for Updates',
                          style: GoogleFonts.roboto(
                              color: Colors.black87, fontSize: 15),
                        ),
                        onTap: showToast,
                      ),
                    ],
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(left: 20, top: 8),
                  child: ListTile(
                    title: Text(
                      'Running On',
                      style: GoogleFonts.roboto(
                          color: Colors.black87, fontSize: 15),
                    ),
                    onTap: () {},
                  ),
                ),
              ],
            ),
          ),
          Container(
            margin: EdgeInsets.only(bottom: 10),
            alignment: Alignment.bottomCenter,
            child: InkWell(
              onTap: () async {
                showCustomDialog(context);
              },
              child: Container(
                height: 50,
                width: MediaQuery.of(context).size.width - 60,
                decoration: BoxDecoration(
                    color: Colors.red[100],
                    borderRadius: BorderRadius.circular(18)),
                child: Center(
                  child: Text(
                    "Log Out",
                    style: TextStyle(
                        color: Colors.grey[700],
                        fontSize: 17,
                        fontWeight: FontWeight.w600),
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  //Alert Dialog Method
  void showCustomDialog(BuildContext context) => showDialog(
      context: context,
      builder: (BuildContext context) {
        return Dialog(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20.0)), //this right here
          child: Container(
            height: 200,
            width: 100,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  "Log out",
                  style: GoogleFonts.roboto(
                      color: Colors.black87,
                      fontWeight: FontWeight.bold,
                      fontSize: 18),
                ),
                Text(
                  "\nAre you sure you want to log out?",
                  style:
                      GoogleFonts.roboto(color: Colors.grey[500], fontSize: 14),
                ),
                Container(
                  margin: EdgeInsets.only(top: 40),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Container(
                        child: InkWell(
                          onTap: () {
                            Navigator.of(context).pop();
                          },
                          child: Container(
                            height: 35,
                            width: 100,
                            decoration: BoxDecoration(
                                color: Colors.red[50],
                                borderRadius: BorderRadius.circular(18)),
                            child: Center(
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Text(
                                  "Cancel",
                                  style: TextStyle(
                                      color: Colors.grey[600], fontSize: 17),
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                      Container(
                        child: InkWell(
                          onTap: () async {
                            await authClass.logout();
                            Navigator.of(context).push(MaterialPageRoute(
                                builder: (context) => SignInPage()));
                          },
                          child: Container(
                            height: 35,
                            width: 100,
                            decoration: BoxDecoration(
                                color: Colors.red[200],
                                borderRadius: BorderRadius.circular(18)),
                            child: Center(
                              child: Text(
                                "Confirm",
                                style: TextStyle(
                                    color: Colors.red[400], fontSize: 17),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        );
      });
  //Show Toast Message
  void showToast() =>
      toast.showToast(child: buildToast(), gravity: ToastGravity.CENTER);

  Widget buildToast() => Container(
        padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
        decoration: BoxDecoration(
          color: Color(0xFF111111).withOpacity(0.6),
          borderRadius: BorderRadius.circular(5),
        ),
        child: Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            Text(
              "This is the latest version",
              style: TextStyle(color: Colors.white, fontSize: 16),
            ),
          ],
        ),
      );
}

// Show Os system
class DeviceInfoApi {
  static Future<String> getOperatingSystem() async => Platform.operatingSystem;
}
