import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class CreatePostsPage extends StatefulWidget {
  CreatePostsPage({Key? key}) : super(key: key);

  @override
  _CreatePostsPageState createState() => _CreatePostsPageState();
}

class _CreatePostsPageState extends State<CreatePostsPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xffe5eef8),
      body: Center(
        child: Text(
          "Congratulations! You Have Found this Section. Dont Tell the Others...",
          style: GoogleFonts.roboto(
              color: Colors.black, fontWeight: FontWeight.bold, fontSize: 80),
        ),
      ),
    );
  }
}
