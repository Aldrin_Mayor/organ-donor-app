import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:organ_donor_app/pages/TermAndCondtionsIntro.dart';

class TermsOfUse extends StatefulWidget {
  TermsOfUse({Key? key}) : super(key: key);

  @override
  _TermsOfUseState createState() => _TermsOfUseState();
}

class _TermsOfUseState extends State<TermsOfUse> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: Stack(
        children: [
          Container(
            margin: EdgeInsets.only(top: 30),
            child: Center(
              child: Column(
                children: [
                  Container(
                    width: 200,
                    height: 200,
                    child: Transform.rotate(
                      angle: 0,
                      child: Image(image: AssetImage('assets/logo.png')),
                    ),
                  ),
                  Text(
                    "Terms of Use",
                    style: GoogleFonts.roboto(
                        color: Colors.black87,
                        fontWeight: FontWeight.bold,
                        fontSize: 18),
                  ),
                  SizedBox(height: 25),
                  Container(
                    margin: EdgeInsets.only(left: 20, right: 20),
                    child: RichText(
                      text: TextSpan(
                        children: [
                          TextSpan(
                            children: [
                              TextSpan(
                                text:
                                    "To find the best match for kidney donors by registering to this application.",
                                style: GoogleFonts.roboto(
                                    color: Colors.black54,
                                    fontSize: 12,
                                    height: 1.5),
                              ),
                              TextSpan(
                                text:
                                    "\n\nThese kidney donor apps are designed to ease the process of organ donation from a donor and recipient.",
                                style: GoogleFonts.roboto(
                                    color: Colors.black54,
                                    fontSize: 12,
                                    height: 1.5),
                              ),
                              TextSpan(
                                text:
                                    "\n\nYou can register as a donor or a patient in this application and collect your data to continue to use Organ Donor App. Using analytics on the donor and recipient kidney characteristics to facilitate the best match of a kidney donor. The patient won’t identify the donors for safety and protect their privacy.",
                                style: GoogleFonts.roboto(
                                    color: Colors.black54,
                                    fontSize: 12,
                                    height: 1.5),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              Container(
                decoration: BoxDecoration(
                  boxShadow: [BoxShadow(color: Colors.grey.withOpacity(0.5))],
                  color: Colors.white,
                ),
                padding: EdgeInsets.only(top: 10, bottom: 10),
                child: Column(
                  children: [
                    Container(
                      height: 50,
                      width: double.infinity,
                      padding: EdgeInsets.only(left: 30, right: 30),
                      child: ElevatedButton(
                        style: ElevatedButton.styleFrom(
                          primary: Colors.red[400],
                        ),
                        onPressed: () {
                          Navigator.of(context).push(MaterialPageRoute(
                              builder: (context) => TermAndConditionsIntro()));
                        },
                        child: Text(
                          'Accept Terms of Use',
                          style: GoogleFonts.roboto(
                              color: Colors.white, fontSize: 14),
                        ),
                      ),
                    ),
                    Container(
                      height: 50,
                      width: double.infinity,
                      padding: EdgeInsets.only(left: 30, right: 30, top: 10),
                      child: ElevatedButton(
                        style: ElevatedButton.styleFrom(
                          primary: Colors.red[100],
                        ),
                        onPressed: () {
                          SystemNavigator.pop();
                        },
                        child: Text('Not Now',
                            style: GoogleFonts.roboto(
                                color: Colors.red[400], fontSize: 14)),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
