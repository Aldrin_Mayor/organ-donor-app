import 'package:flutter/material.dart';
import 'dart:ui';
import 'package:google_fonts/google_fonts.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:organ_donor_app/Service/Auth_Service.dart';
import 'package:organ_donor_app/pages/CommentsPageProfile.dart';
import 'package:organ_donor_app/pages/EditPostsProfile.dart';
import 'package:organ_donor_app/pages/ProfileInformation.dart';
import 'package:organ_donor_app/pages/ProfileSettings.dart';
import 'package:fluttertoast/fluttertoast.dart';

class ProfilePage extends StatefulWidget {
  ProfilePage({Key? key}) : super(key: key);

  @override
  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  final firestoreInstance = FirebaseFirestore.instance;

  //holds Patient Document Card
  patientCard() async {
    User? user = FirebaseAuth.instance.currentUser;
    await FirebaseFirestore.instance
        .collection("Donor Card")
        .doc("Patient")
        .collection("Lists")
        .doc(user!.uid)
        .set({
      'User Type': usersValue,
      'Date': date,
      'Choice': finalChoice,
      'Donor': donor,
      'BirthDate': birthDate,
      'Address': address,
      'Telephone': telephone,
      'First Witness': firstWitness,
      'Second Witness': secondWitness,
    });
  }

  //holds Donor Document Card
  donorCard() async {
    User? user = FirebaseAuth.instance.currentUser;
    await FirebaseFirestore.instance
        .collection("Donor Card")
        .doc("Donor")
        .collection("Lists")
        .doc(user!.uid)
        .set({
      'Date': date,
      'Choice': finalChoice,
      'Donor': donor,
      'BirthDate': birthDate,
      'Address': address,
      'Telephone': telephone,
      'First Witness': firstWitness,
      'Second Witness': secondWitness,
    });
  }

  //holds Patient Document Card
  updatePatientCard() async {
    User? user = FirebaseAuth.instance.currentUser;
    await FirebaseFirestore.instance
        .collection("Donor Card")
        .doc("Patient")
        .collection("Lists")
        .doc(user!.uid)
        .update({
      'User Type': usersValue,
      'Date': date,
      'Choice': finalChoice,
      'Donor': donor,
      'BirthDate': birthDate,
      'Address': address,
      'Telephone': telephone,
      'First Witness': firstWitness,
      'Second Witness': secondWitness,
    });
  }

  //holds Donor Document Card
  updateDonorCard() async {
    User? user = FirebaseAuth.instance.currentUser;
    await FirebaseFirestore.instance
        .collection("Donor Card")
        .doc("Donor")
        .collection("Lists")
        .doc(user!.uid)
        .update({
      'Date': date,
      'Choice': finalChoice,
      'Donor': donor,
      'BirthDate': birthDate,
      'Address': address,
      'Telephone': telephone,
      'First Witness': firstWitness,
      'Second Witness': secondWitness,
    });
  }

  String? donorFullName;
  String? donorUserType;
  String? donorSignature;
  String? donorPosts;
  String? donorUserName;
  String? donorDonated;
  String? donorReceived;
  String? donorLikes;

  String? patientFullName;
  String? patientUserType;
  String? patientSignature;
  String? patientPosts;
  String? patientUserName;
  String? patientDonated;
  String? patientReceived;
  String? patientLikes;

  String? usersValue;
  AuthClass authClass = AuthClass();

  //Donor Card
  String? date,
      donor,
      birthDate,
      telephone,
      address,
      firstWitness,
      secondWitness;
  String? finalChoice;
  String? choice1 = 'Kidneys';
  String? choice2 =
      'Any needed parts for transplantation, research or Education';
  String choice3 = 'Kidney & Other for transplantation, research or Education';

  bool kidneys = false;
  bool any = false;

  TextEditingController _date = TextEditingController();
  TextEditingController _donor = TextEditingController();
  TextEditingController _telephone = TextEditingController();
  TextEditingController _birthDate = TextEditingController();
  TextEditingController _address = TextEditingController();
  TextEditingController _firstWitness = TextEditingController();
  TextEditingController _secondWitness = TextEditingController();

  //Hold Donor and Patient Card Values
  String? finalDate,
      finalFinalChoice,
      finalDonor,
      finalBirthdate,
      finalAddress,
      finalTelephone,
      finalFirstWitness,
      finalSecondWitness;

  //Get Donor and Patient individual data

  //Donor
  String? donorDate,
      donorName,
      donorChoice,
      donorBirthdate,
      donorAddress,
      donorTelephone,
      donorFirstWitness,
      donorSecondWitness;
  //Patient
  String? patientDate,
      patientName,
      patientChoice,
      patientBirthdate,
      patientAddress,
      patientTelephone,
      patientFirstWitness,
      patientSecondWitness;

  bool isEnabled = false;
  bool isSaveEnabled = true;
  bool isUpdateEnabled = false;
  bool isFieldEnabled = true;

  final toast = FToast();

  TextEditingController _signature = TextEditingController();
  @override
  void initState() {
    super.initState();

    toast.init(context);
  }

  // For Passing the data
  String? getFullName;
  String? getUserName;
  String? getDate;
  String? getTime;
  String? getUserID;
  String? getPostTitle;
  String? getPostContent;
  String? getUserType;
  String? getPostViews;
  String? getPostComments;
  String? getNotification;
  String? getPostLikes;

  String? currentUserName;
  String? currentUserType;
  String? currentSignature;
  String? currentPosts;

  String? donorUserID;
  String? donorCountPosts;
  String? patientUserID;
  String? patientCountPosts;
  String? currentUserID;
  String? countPost = "0";

  String? countNotification;

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 1,
      child: Scaffold(
        backgroundColor: Color(0xffe5eef8),
        body: CustomScrollView(
          slivers: [
            SliverPersistentHeader(
              delegate: CustomSliverAppBarDelegate(
                expandedHeight: 200,
              ),
              pinned: true,
            ),
            buildProfileContent(context),
            buildSliverTabBarContent(),
          ],
        ),
      ),
    );
  }

  //Profile Info
  Widget buildProfileContent(BuildContext context) => SliverList(
        delegate: SliverChildListDelegate(
          [
            Container(
              margin: EdgeInsets.only(top: 20, left: 15, right: 15),
              height: 200,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey.withOpacity(0.5),
                  ),
                ],
                color: Colors.white,
              ),
              padding: EdgeInsets.only(top: 10, bottom: 10),
              child: Column(
                children: [
                  //Edit Button
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      InkWell(
                        onTap: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => ProfileInformation()),
                          );
                        },
                        child: Container(
                          margin: EdgeInsets.only(right: 20),
                          padding: EdgeInsets.all(10),
                          height: 35,
                          width: 100,
                          decoration: BoxDecoration(
                            color: Colors.red[100],
                            borderRadius: BorderRadius.circular(18),
                          ),
                          child: Text(
                            'Edit',
                            style: GoogleFonts.roboto(
                                color: Colors.red[900], fontSize: 14),
                            textAlign: TextAlign.center,
                          ),
                        ),
                      ),
                    ],
                  ),
                  //Donor or Patient Name
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Container(
                        margin: EdgeInsets.only(top: 20, left: 20),
                        child: FutureBuilder(
                          future: getUserData(),
                          builder: (context, snapshot) {
                            if (snapshot.connectionState !=
                                ConnectionState.done) return Text("");
                            if (donorUserName != null)
                              return Text(
                                "$donorUserName",
                                style: GoogleFonts.roboto(
                                    color: Colors.black87,
                                    fontSize: 18,
                                    fontWeight: FontWeight.bold),
                              );
                            if (patientUserName != null)
                              return Text(
                                "$patientUserName",
                                style: GoogleFonts.roboto(
                                    color: Colors.black87,
                                    fontSize: 18,
                                    fontWeight: FontWeight.bold),
                              );
                            else {
                              return Text(
                                "null",
                                style: GoogleFonts.roboto(
                                    color: Colors.black87,
                                    fontSize: 18,
                                    fontWeight: FontWeight.bold),
                              );
                            }
                          },
                        ),
                      ),
                    ],
                  ),
                  //Signature
                  InkWell(
                    onTap: () {
                      showCustomDialog(context);
                    },
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Container(
                          margin: EdgeInsets.only(top: 10, left: 20),
                          child: Icon(
                            Icons.message_outlined,
                            size: 14,
                            color: Colors.grey,
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(top: 10, left: 10),
                          child: FutureBuilder(
                            future: getUserData(),
                            builder: (context, snapshot) {
                              if (snapshot.connectionState !=
                                  ConnectionState.done) return Text("");
                              if (donorSignature != null) {
                                return Container(
                                  margin: EdgeInsets.only(top: 10),
                                  height: 20,
                                  width: 240,
                                  child: TextFormField(
                                    controller: _signature
                                      ..text = donorSignature!,
                                    decoration: InputDecoration(
                                        hintText:
                                            'Default signature given to everyone~',
                                        enabled: false,
                                        border: InputBorder.none),
                                    style: GoogleFonts.roboto(
                                        color: Colors.black54, fontSize: 14),
                                  ),
                                );
                              }
                              if (patientSignature != null) {
                                return Container(
                                  margin: EdgeInsets.only(top: 10),
                                  height: 20,
                                  width: 240,
                                  child: TextFormField(
                                    controller: _signature
                                      ..text = patientSignature!,
                                    enabled: false,
                                    decoration: InputDecoration(
                                        hintText:
                                            'Default signature given to everyone~',
                                        border: InputBorder.none),
                                    style: GoogleFonts.roboto(
                                        color: Colors.black54, fontSize: 14),
                                  ),
                                );
                              } else {
                                return Text(
                                  "null",
                                  style: GoogleFonts.roboto(
                                      color: Colors.black54,
                                      fontSize: 18,
                                      fontWeight: FontWeight.bold),
                                );
                              }
                            },
                          ),
                        ),
                      ],
                    ),
                  ),
                  // Analytics
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      //Posts
                      Container(
                        margin: EdgeInsets.only(top: 30),
                        child: FutureBuilder(
                          future: getUserData(),
                          builder: (context, snapshot) {
                            if (snapshot.connectionState !=
                                ConnectionState.done) return Text("");
                            if (donorUserName != null) {
                              return Container(
                                  child: RichText(
                                textAlign: TextAlign.center,
                                text: TextSpan(
                                  children: [
                                    TextSpan(
                                      text: "$donorPosts",
                                      style: GoogleFonts.roboto(
                                          color: Colors.black87,
                                          fontSize: 14,
                                          fontWeight: FontWeight.bold),
                                    ),
                                    TextSpan(
                                      text: "\nPosts",
                                      style: GoogleFonts.roboto(
                                          color: Colors.grey[600],
                                          fontSize: 14),
                                    ),
                                  ],
                                ),
                              ));
                            }
                            if (patientUserName != null) {
                              return Container(
                                  child: RichText(
                                textAlign: TextAlign.center,
                                text: TextSpan(
                                  children: [
                                    TextSpan(
                                      text: "$patientPosts",
                                      style: GoogleFonts.roboto(
                                          color: Colors.black87,
                                          fontSize: 14,
                                          fontWeight: FontWeight.bold),
                                    ),
                                    TextSpan(
                                      text: "\nPosts",
                                      style: GoogleFonts.roboto(
                                          color: Colors.grey[600],
                                          fontSize: 14),
                                    ),
                                  ],
                                ),
                              ));
                            } else {
                              return Text(
                                "",
                                style: GoogleFonts.roboto(
                                    color: Colors.black54, fontSize: 14),
                              );
                            }
                          },
                        ),
                      ),

                      //Donated
                      Container(
                        margin: EdgeInsets.only(top: 30),
                        child: FutureBuilder(
                          future: getUserData(),
                          builder: (context, snapshot) {
                            if (snapshot.connectionState !=
                                ConnectionState.done) return Text("");
                            if (donorUserName != null) {
                              return Container(
                                  child: RichText(
                                textAlign: TextAlign.center,
                                text: TextSpan(
                                  children: [
                                    TextSpan(
                                      text: "$donorDonated",
                                      style: GoogleFonts.roboto(
                                          color: Colors.black87,
                                          fontSize: 14,
                                          fontWeight: FontWeight.bold),
                                    ),
                                    TextSpan(
                                      text: "\nDonated",
                                      style: GoogleFonts.roboto(
                                          color: Colors.grey[600],
                                          fontSize: 14),
                                    ),
                                  ],
                                ),
                              ));
                            }
                            if (patientUserName != null) {
                              return Container(
                                  child: RichText(
                                textAlign: TextAlign.center,
                                text: TextSpan(
                                  children: [
                                    TextSpan(
                                      text: "$patientDonated",
                                      style: GoogleFonts.roboto(
                                          color: Colors.black87,
                                          fontSize: 14,
                                          fontWeight: FontWeight.bold),
                                    ),
                                    TextSpan(
                                      text: "\nDonated",
                                      style: GoogleFonts.roboto(
                                          color: Colors.grey[600],
                                          fontSize: 14),
                                    ),
                                  ],
                                ),
                              ));
                            } else {
                              return Text(
                                "",
                                style: GoogleFonts.roboto(
                                    color: Colors.black54, fontSize: 14),
                              );
                            }
                          },
                        ),
                      ),

                      //Received
                      Container(
                        margin: EdgeInsets.only(top: 30),
                        child: FutureBuilder(
                          future: getUserData(),
                          builder: (context, snapshot) {
                            if (snapshot.connectionState !=
                                ConnectionState.done) return Text("");
                            if (donorUserName != null) {
                              return Container(
                                  child: RichText(
                                textAlign: TextAlign.center,
                                text: TextSpan(
                                  children: [
                                    TextSpan(
                                      text: "$donorReceived",
                                      style: GoogleFonts.roboto(
                                          color: Colors.black87,
                                          fontSize: 14,
                                          fontWeight: FontWeight.bold),
                                    ),
                                    TextSpan(
                                      text: "\nReceived",
                                      style: GoogleFonts.roboto(
                                          color: Colors.grey[600],
                                          fontSize: 14),
                                    ),
                                  ],
                                ),
                              ));
                            }
                            if (patientUserName != null) {
                              return Container(
                                  child: RichText(
                                textAlign: TextAlign.center,
                                text: TextSpan(
                                  children: [
                                    TextSpan(
                                      text: "$patientReceived",
                                      style: GoogleFonts.roboto(
                                          color: Colors.black87,
                                          fontSize: 14,
                                          fontWeight: FontWeight.bold),
                                    ),
                                    TextSpan(
                                      text: "\nReceived",
                                      style: GoogleFonts.roboto(
                                          color: Colors.grey[600],
                                          fontSize: 14),
                                    ),
                                  ],
                                ),
                              ));
                            } else {
                              return Text(
                                "",
                                style: GoogleFonts.roboto(
                                    color: Colors.black54, fontSize: 14),
                              );
                            }
                          },
                        ),
                      ),

                      //Likes
                      Container(
                        margin: EdgeInsets.only(top: 30),
                        child: FutureBuilder(
                          future: getUserData(),
                          builder: (context, snapshot) {
                            if (snapshot.connectionState !=
                                ConnectionState.done) return Text("");
                            if (donorUserName != null) {
                              return Container(
                                  child: RichText(
                                textAlign: TextAlign.center,
                                text: TextSpan(
                                  children: [
                                    TextSpan(
                                      text: "$donorLikes",
                                      style: GoogleFonts.roboto(
                                          color: Colors.black87,
                                          fontSize: 14,
                                          fontWeight: FontWeight.bold),
                                    ),
                                    TextSpan(
                                      text: "\nLikes",
                                      style: GoogleFonts.roboto(
                                          color: Colors.grey[600],
                                          fontSize: 14),
                                    ),
                                  ],
                                ),
                              ));
                            }
                            if (patientUserName != null) {
                              return Container(
                                  child: RichText(
                                textAlign: TextAlign.center,
                                text: TextSpan(
                                  children: [
                                    TextSpan(
                                      text: "$patientLikes",
                                      style: GoogleFonts.roboto(
                                          color: Colors.black87,
                                          fontSize: 14,
                                          fontWeight: FontWeight.bold),
                                    ),
                                    TextSpan(
                                      text: "\nLikes",
                                      style: GoogleFonts.roboto(
                                          color: Colors.grey[600],
                                          fontSize: 14),
                                    ),
                                  ],
                                ),
                              ));
                            } else {
                              return Text(
                                "",
                                style: GoogleFonts.roboto(
                                    color: Colors.black54, fontSize: 14),
                              );
                            }
                          },
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
            //Header
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  margin: EdgeInsets.only(top: 30),
                  child: RichText(
                    text: TextSpan(
                      children: [
                        WidgetSpan(
                          child: Image.asset(
                            'assets/idcard.png',
                            height: 18,
                          ),
                        ),
                        TextSpan(
                          text: "\t\tMy Virtually Signed Organ Donor Card",
                          style: GoogleFonts.roboto(
                              color: Colors.black87, fontSize: 18),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
            Container(
              margin: EdgeInsets.only(top: 20, left: 50, right: 50),
              child: RichText(
                textAlign: TextAlign.center,
                text: TextSpan(
                  children: [
                    TextSpan(
                      text:
                          "The Following Content is not displayed to the public. Fill the Information required if you are willing to give.",
                      style: GoogleFonts.roboto(
                          color: Colors.grey[600], fontSize: 12, height: 1.5),
                    ),
                    TextSpan(
                      text:
                          "\n\nAttention:\nThis is Optional and Not Mandatory",
                      style: GoogleFonts.roboto(
                          color: Colors.indigo[400], fontSize: 13, height: 1.5),
                    ),
                  ],
                ),
              ),
            ),
            //Front Card
            Container(
              margin: EdgeInsets.only(top: 20, left: 15, right: 15),
              height: 550,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey.withOpacity(0.5),
                  ),
                ],
                color: Colors.blue,
              ),
              padding: EdgeInsets.all(10),
              child: Card(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(50),
                ),
                color: Colors.yellow,
                child: Column(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        //First
                        Container(
                          margin: EdgeInsets.only(left: 15, top: 30),
                          child: RichText(
                            text: TextSpan(
                              children: [
                                TextSpan(
                                  text: 'Organ',
                                  style: GoogleFonts.roboto(
                                      color: Colors.red,
                                      fontSize: 50,
                                      fontWeight: FontWeight.w900),
                                ),
                                TextSpan(
                                  text: '\nDonor',
                                  style: GoogleFonts.roboto(
                                      color: Colors.red,
                                      fontSize: 50,
                                      fontWeight: FontWeight.w900),
                                ),
                                TextSpan(
                                  text: '\nCard',
                                  style: GoogleFonts.roboto(
                                      color: Colors.red,
                                      fontSize: 50,
                                      fontWeight: FontWeight.w900),
                                ),
                              ],
                            ),
                          ),
                        ),
                        //Second
                        Container(
                          margin: EdgeInsets.only(top: 50),
                          child: Container(
                            width: 150,
                            height: 150,
                            child: Transform.rotate(
                              angle: 38,
                              child:
                                  Image(image: AssetImage('assets/logo.png')),
                            ),
                          ),
                        ),
                      ],
                    ),
                    Row(
                      children: [
                        Container(
                          margin: EdgeInsets.only(left: 50, top: 30),
                          child: RichText(
                            text: TextSpan(
                              children: [
                                TextSpan(
                                  text: 'Keep this card'.toUpperCase(),
                                  style: GoogleFonts.roboto(
                                      color: Colors.black87,
                                      fontSize: 20,
                                      fontWeight: FontWeight.w700),
                                ),
                                TextSpan(
                                  text: '\nwith you where'.toUpperCase(),
                                  style: GoogleFonts.roboto(
                                      color: Colors.black87,
                                      fontSize: 20,
                                      fontWeight: FontWeight.w700),
                                ),
                                TextSpan(
                                  text: '\nit can be found'.toUpperCase(),
                                  style: GoogleFonts.roboto(
                                      color: Colors.black87,
                                      fontSize: 20,
                                      fontWeight: FontWeight.w700),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                    //Fourth
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Container(
                          margin: EdgeInsets.only(top: 40),
                          child: RichText(
                            textAlign: TextAlign.center,
                            text: TextSpan(
                              children: [
                                TextSpan(
                                  text: 'I would like to help',
                                  style: GoogleFonts.roboto(
                                      color: Colors.blue,
                                      fontSize: 25,
                                      fontWeight: FontWeight.w700),
                                ),
                                TextSpan(
                                  text: '\nsomeone to live',
                                  style: GoogleFonts.roboto(
                                      color: Colors.blue,
                                      fontSize: 25,
                                      fontWeight: FontWeight.w700),
                                ),
                                TextSpan(
                                  text: '\nafter my death',
                                  style: GoogleFonts.roboto(
                                      color: Colors.blue,
                                      fontSize: 25,
                                      fontWeight: FontWeight.w700),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ],
                    )
                  ],
                ),
              ),
            ),
            //Back of Card

            Container(
              margin: EdgeInsets.only(top: 20),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  //Save button
                  Container(
                    child: ElevatedButton(
                      child: Text(
                        'Save',
                        style: GoogleFonts.roboto(
                            fontSize: 15, fontWeight: FontWeight.w700),
                      ),
                      onPressed: isSaveEnabled
                          ? () {
                              date = _date.text;
                              donor = _donor.text;
                              birthDate = _birthDate.text;
                              address = _address.text;
                              telephone = _telephone.text;
                              firstWitness = _firstWitness.text;
                              secondWitness = _secondWitness.text;

                              //Checkbox Condition
                              if ((kidneys == true && any == true)) {
                                finalChoice = choice3;
                                print(finalChoice);
                              } else if (kidneys == true) {
                                finalChoice = choice1;
                                print(finalChoice);
                              } else if (any == true) {
                                finalChoice = choice2;
                                print(finalChoice);
                              } else {
                                finalChoice = 'No Option Selected';
                                print(finalChoice);
                              }

                              //Check if Donor or Patient
                              if (usersValue == 'I am a Donor') {
                                donorCard();
                                showSaveToast();
                                isFieldEnabled = false;
                                isUpdateEnabled = false;
                              } else {
                                patientCard();
                                showSaveToast();
                                isFieldEnabled = false;
                                isUpdateEnabled = false;
                              }
                            }
                          : null,
                      style: ButtonStyle(
                          backgroundColor:
                              MaterialStateProperty.all(Colors.red),
                          textStyle: MaterialStateProperty.all(
                              TextStyle(fontSize: 30))),
                    ),
                  ),
                  //View Button
                  Container(
                    child: ElevatedButton(
                      child: Text(
                        'View',
                        style: GoogleFonts.roboto(
                            fontSize: 15, fontWeight: FontWeight.w700),
                      ),
                      onPressed: () {
                        showViewToast();
                        isFieldEnabled = false;
                        enableButton(context);
                        disableSaveButton(context);
                        refresh();
                        _date..text = finalDate!;
                        _donor..text = finalDonor!;
                        _birthDate..text = finalBirthdate!;
                        _address..text = finalAddress!;
                        _telephone..text = finalTelephone!;
                        _firstWitness..text = finalFirstWitness!;
                        _secondWitness..text = finalSecondWitness!;
                        isFieldEnabled = false;
                        isUpdateEnabled = false;
                        if (finalFinalChoice ==
                            'Kidney & Other for transplantation, research or Education') {
                          bool value = true;
                          setState(() {
                            this.kidneys = value;
                            this.any = value;
                          });
                        } else if (finalFinalChoice == 'Kidneys') {
                          bool value = true;
                          setState(() {
                            this.kidneys = value;
                          });
                        } else if (finalFinalChoice ==
                            'Any needed parts for transplantation, research or Education') {
                          bool value = true;
                          setState(() {
                            this.any = value;
                          });
                        }
                      },
                      style: ButtonStyle(
                          backgroundColor:
                              MaterialStateProperty.all(Colors.red),
                          textStyle: MaterialStateProperty.all(
                              TextStyle(fontSize: 30))),
                    ),
                  ),
                  //Update Button
                  Container(
                    child: ElevatedButton(
                      child: Text(
                        'Update',
                        style: GoogleFonts.roboto(
                            fontSize: 15, fontWeight: FontWeight.w700),
                      ),
                      onPressed: isUpdateEnabled
                          ? () {
                              enableSaveButton(context);
                              disableButton(context);
                              isFieldEnabled = false;
                              isUpdateEnabled = false;

                              date = _date.text;
                              donor = _donor.text;
                              birthDate = _birthDate.text;
                              address = _address.text;
                              telephone = _telephone.text;
                              firstWitness = _firstWitness.text;
                              secondWitness = _secondWitness.text;

                              //Checkbox Condition
                              if ((kidneys == true && any == true)) {
                                finalChoice = choice3;
                                print(finalChoice);
                              } else if (kidneys == true) {
                                finalChoice = choice1;
                                print(finalChoice);
                              } else if (any == true) {
                                finalChoice = choice2;
                                print(finalChoice);
                              } else {
                                finalChoice = 'No Option Selected';
                                print(finalChoice);
                              }

                              //Check if Donor or Patient
                              if (usersValue == 'I am a Donor') {
                                updateDonorCard();
                                showUpdateToast();
                              } else {
                                updatePatientCard();
                                showUpdateToast();
                              }
                            }
                          : null,
                      style: ButtonStyle(
                          backgroundColor:
                              MaterialStateProperty.all(Colors.red),
                          textStyle: MaterialStateProperty.all(
                              TextStyle(fontSize: 30))),
                    ),
                  ),
                  //Edit Button
                  Container(
                    child: ElevatedButton(
                      child: Text(
                        'Edit',
                        style: GoogleFonts.roboto(
                            fontSize: 15, fontWeight: FontWeight.w700),
                      ),
                      onPressed: isEnabled
                          ? () {
                              showEditToast();
                              setState(() {
                                isFieldEnabled = true;
                                isUpdateEnabled = true;
                              });
                            }
                          : null,
                      style: ButtonStyle(
                          backgroundColor:
                              MaterialStateProperty.all(Colors.red),
                          textStyle: MaterialStateProperty.all(
                              TextStyle(fontSize: 30))),
                    ),
                  ),
                  //Delete Button
                  Container(
                    child: ElevatedButton(
                      child: Text(
                        'Clear',
                        style: GoogleFonts.roboto(
                            fontSize: 15, fontWeight: FontWeight.w700),
                      ),
                      onPressed: isEnabled
                          ? () {
                              showClearToast();
                              enableSaveButton(context);
                              disableButton(context);
                              isFieldEnabled = true;
                              isUpdateEnabled = false;
                              _date..text = '';
                              _donor..text = '';
                              _birthDate..text = '';
                              _address..text = '';
                              _telephone..text = '';
                              _firstWitness..text = '';
                              _secondWitness..text = '';

                              date = _date.text;
                              donor = _donor.text;
                              birthDate = _birthDate.text;
                              address = _address.text;
                              telephone = _telephone.text;
                              firstWitness = _firstWitness.text;
                              secondWitness = _secondWitness.text;

                              if (finalFinalChoice ==
                                  'Kidney & Other for transplantation, research or Education') {
                                bool value = false;
                                setState(() {
                                  this.kidneys = value;
                                  this.any = value;
                                });
                              } else if (finalFinalChoice == 'Kidneys') {
                                bool value = false;
                                setState(() {
                                  this.kidneys = value;
                                });
                              } else if (finalFinalChoice ==
                                  'Any needed parts for transplantation, research or Education') {
                                bool value = false;
                                setState(() {
                                  this.any = value;
                                });
                              }
                            }
                          : null,
                      style: ButtonStyle(
                          backgroundColor:
                              MaterialStateProperty.all(Colors.red),
                          textStyle: MaterialStateProperty.all(
                              TextStyle(fontSize: 30))),
                    ),
                  ),
                ],
              ),
            ),

            Container(
              margin: EdgeInsets.only(top: 20, left: 15, right: 15, bottom: 20),
              height: 570,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: Colors.blue,
              ),
              padding: EdgeInsets.all(10),
              child: Card(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(50),
                ),
                color: Colors.yellow,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    //Date
                    Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[
                        Text(
                          'Date\t'.toUpperCase(),
                          style: GoogleFonts.roboto(
                              color: Colors.black87,
                              fontSize: 14,
                              fontWeight: FontWeight.w700),
                        ),
                        Flexible(
                          child: Container(
                            margin: EdgeInsets.only(right: 20),
                            height: 22,
                            width: 130,
                            child: TextFormField(
                              style: new TextStyle(fontSize: 14),
                              controller: _date,
                              enabled: isFieldEnabled,
                            ),
                          ),
                        ),
                      ],
                    ),
                    //Header
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Container(
                          margin: EdgeInsets.only(top: 10, left: 20, right: 20),
                          child: Text(
                            'In the Hope that i may help the others,\nI wish to donate after my death my:'
                                .toUpperCase(),
                            style: GoogleFonts.roboto(
                                color: Colors.black87,
                                fontSize: 12,
                                fontWeight: FontWeight.w700),
                          ),
                        ),
                      ],
                    ),
                    //First Row Checkbox
                    Row(
                      children: [
                        Container(
                          margin: EdgeInsets.only(left: 20),
                          child: Text(
                            'A.',
                            style: GoogleFonts.roboto(
                                color: Colors.black87,
                                fontSize: 12,
                                fontWeight: FontWeight.w700),
                          ),
                        ),
                        //Kidney Checkbox
                        Checkbox(
                          value: kidneys,
                          onChanged: isFieldEnabled
                              ? (value) => setState(
                                    () => this.kidneys = value!,
                                  )
                              : null,
                        ),
                        Container(
                          child: Text(
                            'Kidneys'.toUpperCase(),
                            style: GoogleFonts.roboto(
                                color: Colors.black87,
                                fontSize: 12,
                                fontWeight: FontWeight.w700),
                          ),
                        ),
                      ],
                    ),
                    Row(
                      children: [
                        Container(
                          margin: EdgeInsets.only(left: 20),
                          child: Text(
                            'B.',
                            style: GoogleFonts.roboto(
                                color: Colors.black87,
                                fontSize: 12,
                                fontWeight: FontWeight.w700),
                          ),
                        ),
                        //Any Checkbox
                        Checkbox(
                          value: any,
                          onChanged: isFieldEnabled
                              ? (value) => setState(() => this.any = value!)
                              : null,
                        ),
                        Container(
                          child: Text(
                            'Any needed parts or organs to \nbe used for transplantation, \nresearch or education'
                                .toUpperCase(),
                            style: GoogleFonts.roboto(
                                color: Colors.black87,
                                fontSize: 12,
                                fontWeight: FontWeight.w700),
                          ),
                        ),
                      ],
                    ),
                    //Contents
                    //Name
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Container(
                          margin: EdgeInsets.only(top: 10, left: 15),
                          child: Text(
                            'Donor\t\t\t\t\t\t\t\t\t\t\t'.toUpperCase(),
                            style: GoogleFonts.roboto(
                                color: Colors.black87,
                                fontSize: 14,
                                fontWeight: FontWeight.w700),
                          ),
                        ),
                        Flexible(
                          child: Container(
                            margin: EdgeInsets.only(right: 20),
                            height: 30,
                            child: TextFormField(
                              style: new TextStyle(fontSize: 14),
                              controller: _donor,
                              enabled: isFieldEnabled,
                            ),
                          ),
                        ),
                      ],
                    ),
                    //Birthdate
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Container(
                          margin: EdgeInsets.only(top: 10, left: 15),
                          child: Text(
                            'Birthdate\t\t\t\t'.toUpperCase(),
                            style: GoogleFonts.roboto(
                                color: Colors.black87,
                                fontSize: 14,
                                fontWeight: FontWeight.w700),
                          ),
                        ),
                        Flexible(
                          child: Container(
                            margin: EdgeInsets.only(right: 20),
                            height: 30,
                            child: TextFormField(
                              style: new TextStyle(
                                fontSize: 14,
                              ),
                              controller: _birthDate,
                              enabled: isFieldEnabled,
                            ),
                          ),
                        ),
                      ],
                    ),
                    //Address
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Container(
                          margin: EdgeInsets.only(top: 10, left: 15),
                          child: Text(
                            'Address\t\t\t\t\t\t\t'.toUpperCase(),
                            style: GoogleFonts.roboto(
                                color: Colors.black87,
                                fontSize: 14,
                                fontWeight: FontWeight.w700),
                          ),
                        ),
                        Flexible(
                          child: Container(
                            margin: EdgeInsets.only(right: 20),
                            height: 30,
                            child: TextFormField(
                              style: new TextStyle(fontSize: 14),
                              controller: _address,
                              enabled: isFieldEnabled,
                            ),
                          ),
                        ),
                      ],
                    ),
                    //Telephone
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Container(
                          margin: EdgeInsets.only(top: 10, left: 15),
                          child: Text(
                            'Telephone\t\t\t'.toUpperCase(),
                            style: GoogleFonts.roboto(
                                color: Colors.black87,
                                fontSize: 14,
                                fontWeight: FontWeight.w700),
                          ),
                        ),
                        Flexible(
                          child: Container(
                            margin: EdgeInsets.only(right: 20),
                            height: 30,
                            child: TextFormField(
                              style: new TextStyle(fontSize: 14),
                              controller: _telephone,
                              enabled: isFieldEnabled,
                            ),
                          ),
                        ),
                      ],
                    ),
                    //First Witness
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Container(
                          margin: EdgeInsets.only(top: 10, left: 15),
                          child: Text(
                            'Witness(1)\t\t\t'.toUpperCase(),
                            style: GoogleFonts.roboto(
                                color: Colors.black87,
                                fontSize: 14,
                                fontWeight: FontWeight.w700),
                          ),
                        ),
                        Flexible(
                          child: Container(
                            margin: EdgeInsets.only(right: 20),
                            height: 30,
                            child: TextFormField(
                              style: new TextStyle(fontSize: 14),
                              controller: _firstWitness,
                              enabled: isFieldEnabled,
                            ),
                          ),
                        ),
                      ],
                    ),
                    //Second Witness
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Container(
                          margin: EdgeInsets.only(top: 10, left: 15),
                          child: Text(
                            'Witness(2)\t\t\t'.toUpperCase(),
                            style: GoogleFonts.roboto(
                                color: Colors.black87,
                                fontSize: 14,
                                fontWeight: FontWeight.w700),
                          ),
                        ),
                        Flexible(
                          child: Container(
                            margin: EdgeInsets.only(right: 20),
                            height: 30,
                            child: TextFormField(
                              style: new TextStyle(fontSize: 14),
                              controller: _secondWitness,
                              enabled: isFieldEnabled,
                            ),
                          ),
                        ),
                      ],
                    ),
                    //Info Text
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Container(
                          margin: EdgeInsets.only(top: 30),
                          child: RichText(
                            text: TextSpan(
                              children: [
                                TextSpan(
                                  text: 'For Further Information, Call'
                                      .toUpperCase(),
                                  style: GoogleFonts.roboto(
                                      color: Colors.red[400],
                                      fontSize: 16,
                                      fontWeight: FontWeight.w500),
                                ),
                                TextSpan(
                                  text: '\n\nDevelopers:'.toUpperCase(),
                                  style: GoogleFonts.roboto(
                                      color: Colors.red[400],
                                      fontSize: 20,
                                      fontWeight: FontWeight.bold),
                                ),
                                TextSpan(
                                  text:
                                      '\nEspiritu, Justin Lloyd: \t\t\t\t\t\t09310441638',
                                  style: GoogleFonts.roboto(
                                      color: Colors.red[400], fontSize: 16),
                                ),
                                TextSpan(
                                  text:
                                      '\nMayor, Aldrin: \t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t09558074717',
                                  style: GoogleFonts.roboto(
                                      color: Colors.red[400], fontSize: 16),
                                ),
                                TextSpan(
                                  text:
                                      '\nRivera, Debbie Cheyenne: \t09065180962',
                                  style: GoogleFonts.roboto(
                                      color: Colors.red[400], fontSize: 16),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                    // Checks if User is Donor or Patient
                    FutureBuilder(
                      future: getUserData(),
                      builder: (context, snapshot) {
                        if (snapshot.connectionState != ConnectionState.done)
                          return Text("");
                        if (donorUserType != null) {
                          usersValue = donorUserType;
                          return Container(
                              height: 20,
                              width: 240,
                              child: Text(
                                '',
                                style: GoogleFonts.roboto(
                                    color: Colors.black87,
                                    fontSize: 15,
                                    fontWeight: FontWeight.w500),
                              ));
                        }
                        if (patientUserType != null) {
                          usersValue = patientUserType;
                          return Container(
                              height: 20,
                              width: 240,
                              child: Text(
                                '',
                                style: GoogleFonts.roboto(
                                    color: Colors.black87,
                                    fontSize: 15,
                                    fontWeight: FontWeight.w500),
                              ));
                        } else {
                          return Text(
                            "",
                            style: GoogleFonts.roboto(
                                color: Colors.black54, fontSize: 14),
                          );
                        }
                      },
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      );
  //Show Save Toast Message
  void showSaveToast() =>
      toast.showToast(child: buildSaveToast(), gravity: ToastGravity.CENTER);

  Widget buildSaveToast() => Container(
        padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
        decoration: BoxDecoration(
          color: Color(0xFF111111).withOpacity(0.6),
          borderRadius: BorderRadius.circular(5),
        ),
        child: Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            Text(
              "Information Save Successfull",
              style: TextStyle(color: Colors.white, fontSize: 16),
            ),
          ],
        ),
      );

  //Show View Toast Message
  void showViewToast() =>
      toast.showToast(child: buildViewToast(), gravity: ToastGravity.CENTER);

  Widget buildViewToast() => Container(
        padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
        decoration: BoxDecoration(
          color: Color(0xFF111111).withOpacity(0.6),
          borderRadius: BorderRadius.circular(5),
        ),
        child: Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            Text(
              "View Mode",
              style: TextStyle(color: Colors.white, fontSize: 16),
            ),
          ],
        ),
      );

//Show View Null Toast Message
  void showViewNullToast() => toast.showToast(
      child: buildViewNullToast(), gravity: ToastGravity.CENTER);

  Widget buildViewNullToast() => Container(
        padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
        decoration: BoxDecoration(
          color: Color(0xFF111111).withOpacity(0.6),
          borderRadius: BorderRadius.circular(5),
        ),
        child: Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            Text(
              "Error. No record of data!",
              style: TextStyle(color: Colors.white, fontSize: 16),
            ),
          ],
        ),
      );

  //Show Update Toast Message
  void showUpdateToast() =>
      toast.showToast(child: buildUpdateToast(), gravity: ToastGravity.CENTER);

  Widget buildUpdateToast() => Container(
        padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
        decoration: BoxDecoration(
          color: Color(0xFF111111).withOpacity(0.6),
          borderRadius: BorderRadius.circular(5),
        ),
        child: Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            Text(
              "Details Updated Successfull",
              style: TextStyle(color: Colors.white, fontSize: 16),
            ),
          ],
        ),
      );

  //Show Edit Toast Message
  void showEditToast() =>
      toast.showToast(child: buildEditToast(), gravity: ToastGravity.CENTER);

  Widget buildEditToast() => Container(
        padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
        decoration: BoxDecoration(
          color: Color(0xFF111111).withOpacity(0.6),
          borderRadius: BorderRadius.circular(5),
        ),
        child: Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            Text(
              "Edit Mode",
              style: TextStyle(color: Colors.white, fontSize: 16),
            ),
          ],
        ),
      );

  //Show Clear Toast Message
  void showClearToast() =>
      toast.showToast(child: buildClearToast(), gravity: ToastGravity.CENTER);

  Widget buildClearToast() => Container(
        padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
        decoration: BoxDecoration(
          color: Color(0xFF111111).withOpacity(0.6),
          borderRadius: BorderRadius.circular(5),
        ),
        child: Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            Text(
              "Entry Fields are now Empty!",
              style: TextStyle(color: Colors.white, fontSize: 16),
            ),
          ],
        ),
      );
  //Enable and Disable Buttons
  void enableButton(BuildContext context) {
    setState(() {
      isEnabled = true;
    });
  }

  void disableButton(BuildContext context) {
    setState(() {
      isEnabled = false;
    });
  }

  void enableSaveButton(BuildContext context) {
    setState(() {
      isSaveEnabled = true;
    });
  }

  void disableSaveButton(BuildContext context) {
    setState(() {
      isSaveEnabled = false;
    });
  }

  // Date Picker
  Future pickDate(BuildContext context) async {
    final initialDate = DateTime.now();
    final newDate = await showDatePicker(
      context: context,
      initialDate: initialDate,
      firstDate: DateTime(DateTime.now().year - 10),
      lastDate: DateTime(DateTime.now().year + 10),
    );

    if (newDate == null) return;
  }

  //Tab bar Info
  Widget buildSliverTabBarContent() => SliverList(
        delegate: SliverChildListDelegate(
          [
            //Tab Bar Itself
            Padding(
              padding: EdgeInsets.only(right: 250),
              child: Container(
                height: 50,
                decoration: BoxDecoration(
                  color: Color(0xffe5eef8), //(0xffe5eef8)
                ),
                child: TabBar(
                  indicatorColor: Colors.indigo,
                  indicatorWeight: 3,
                  indicatorSize: TabBarIndicatorSize.label,
                  unselectedLabelColor: Colors.grey[500],
                  labelColor: Colors.black87,
                  tabs: [
                    Tab(
                      child: Row(
                        children: [
                          Icon(
                            Icons.post_add_outlined,
                            size: 24,
                            color: Colors.pink,
                          ),
                          Text(
                            '\tPosts',
                            style: GoogleFonts.roboto(
                              fontSize: 18,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
            //Posts
            Container(
              margin: EdgeInsets.only(top: 20, left: 15, right: 15, bottom: 80),
              height: 450,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: Color(0xffe5eef8),
              ),
              padding: EdgeInsets.only(top: 10, bottom: 10),
              child: TabBarView(
                children: [
                  Container(
                    child: FutureBuilder(
                      future: getUserPostsData(),
                      builder: (context, AsyncSnapshot snapshot) {
                        if (currentPosts == "0") {
                          return Container(
                            margin: EdgeInsets.only(top: 150),
                            child: Column(
                              children: [
                                Image.asset(
                                  'assets/box.png',
                                  height: 150,
                                ),
                                Text(
                                  "\n\nThere's nothing here...",
                                  style: GoogleFonts.roboto(
                                    color: Colors.grey[500],
                                    fontSize: 14,
                                  ),
                                )
                              ],
                            ),
                          );
                        } else {
                          return ListView.builder(
                              itemCount: snapshot.data!.length,
                              itemBuilder: (context, index) {
                                DocumentSnapshot userData =
                                    snapshot.data[index];
                                return Stack(
                                  children: [
                                    Container(
                                      decoration: BoxDecoration(
                                          color: Colors.white,
                                          borderRadius:
                                              BorderRadius.circular(10)),
                                      margin: EdgeInsets.only(top: 10),
                                      child: ListTile(
                                        title: Column(
                                          children: [
                                            Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment
                                                      .spaceBetween,
                                              children: [
                                                Column(
                                                  children: [
                                                    Row(
                                                      children: [
                                                        Container(
                                                          margin:
                                                              EdgeInsets.only(
                                                                  top: 10),
                                                          child: Image.asset(
                                                            'assets/user.png',
                                                            height: 30,
                                                          ),
                                                        ),
                                                        Container(
                                                          margin:
                                                              EdgeInsets.only(
                                                                  top: 20,
                                                                  left: 10),
                                                          child: Column(
                                                            crossAxisAlignment:
                                                                CrossAxisAlignment
                                                                    .start,
                                                            children: [
                                                              //UserName
                                                              Container(
                                                                child: Row(
                                                                  children: [
                                                                    Text(
                                                                      userData[
                                                                          "UserName"],
                                                                      style: GoogleFonts.roboto(
                                                                          color: Colors
                                                                              .black87,
                                                                          fontWeight: FontWeight
                                                                              .w600,
                                                                          fontSize:
                                                                              14),
                                                                    ),
                                                                    //Verified Logo
                                                                    Container(
                                                                      margin: EdgeInsets.only(
                                                                          left:
                                                                              5),
                                                                      child: Icon(
                                                                          Icons
                                                                              .verified_outlined,
                                                                          size:
                                                                              20,
                                                                          color:
                                                                              Colors.green),
                                                                    ),
                                                                  ],
                                                                ),
                                                              ),
                                                              Container(
                                                                margin: EdgeInsets
                                                                    .only(
                                                                        top: 5),
                                                                child: Row(
                                                                  children: [
                                                                    Text(
                                                                      userData[
                                                                          "Date"],
                                                                      style: GoogleFonts.roboto(
                                                                          color: Colors
                                                                              .black54,
                                                                          fontWeight: FontWeight
                                                                              .w600,
                                                                          fontSize:
                                                                              12),
                                                                    ),
                                                                    Container(
                                                                      margin: EdgeInsets.only(
                                                                          left:
                                                                              2),
                                                                      child:
                                                                          Text(
                                                                        userData[
                                                                            "Time"],
                                                                        style: GoogleFonts.roboto(
                                                                            color:
                                                                                Colors.black54,
                                                                            fontWeight: FontWeight.w600,
                                                                            fontSize: 12),
                                                                      ),
                                                                    ),
                                                                  ],
                                                                ),
                                                              ),
                                                            ],
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                                  ],
                                                ),

                                                //Setting Action
                                                InkWell(
                                                  onTap: () {
                                                    getUserID =
                                                        userData["UserID"];
                                                    getUserName =
                                                        userData["UserName"];
                                                    getFullName =
                                                        userData["FullName"];
                                                    getUserType =
                                                        userData["User Type"];
                                                    getDate = userData["Date"];
                                                    getTime = userData["Time"];
                                                    getPostTitle =
                                                        userData["Post Title"];
                                                    getPostContent = userData[
                                                        "Post Content"];
                                                    showModalBottomSheet(
                                                      context: context,
                                                      builder: (context) =>
                                                          buildCurrentUserSheet(),
                                                    );
                                                  },
                                                  child: Icon(
                                                      Icons.more_vert_outlined,
                                                      size: 20,
                                                      color: Colors.grey),
                                                ),
                                              ],
                                            ),
                                            Row(
                                              children: [
                                                Container(
                                                  width: MediaQuery.of(context)
                                                          .size
                                                          .width *
                                                      0.82,
                                                  margin: EdgeInsets.only(
                                                      top: 20, left: 0),
                                                  child: Text(
                                                    userData["Post Title"],
                                                    style: GoogleFonts.roboto(
                                                        color: Colors.black87,
                                                        fontWeight:
                                                            FontWeight.w600,
                                                        fontSize: 16),
                                                  ),
                                                ),
                                              ],
                                            ),
                                            Row(
                                              children: [
                                                Container(
                                                  width: MediaQuery.of(context)
                                                          .size
                                                          .width *
                                                      0.82,
                                                  margin: EdgeInsets.only(
                                                    top: 20,
                                                    left: 0,
                                                  ),
                                                  child: Text(
                                                    userData["Post Content"],
                                                    style: GoogleFonts.roboto(
                                                        color: Colors.black87,
                                                        fontWeight:
                                                            FontWeight.w400,
                                                        fontSize: 14),
                                                  ),
                                                ),
                                              ],
                                            ),
                                            //views, comment and Like
                                            Container(
                                              margin: EdgeInsets.only(
                                                  top: 20, bottom: 10),
                                              child: Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment
                                                        .spaceEvenly,
                                                children: [
                                                  //Views
                                                  InkWell(
                                                    onTap: () {},
                                                    child: Row(
                                                      children: [
                                                        //Views Icon
                                                        Icon(
                                                          Icons
                                                              .remove_red_eye_rounded,
                                                          color: Colors.grey,
                                                        ),
                                                        //Views Count
                                                        Container(
                                                          margin:
                                                              EdgeInsets.only(
                                                                  left: 10),
                                                          child: Text(
                                                              userData["Views"],
                                                              style: GoogleFonts.roboto(
                                                                  color: Colors
                                                                      .grey,
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .w400,
                                                                  fontSize:
                                                                      14)),
                                                        ),
                                                      ],
                                                    ),
                                                  ),
                                                  //Comments
                                                  InkWell(
                                                    onTap: () {
                                                      getUserID =
                                                          userData["UserID"];
                                                      getUserName =
                                                          userData["UserName"];
                                                      getFullName =
                                                          userData["FullName"];
                                                      getUserType =
                                                          userData["User Type"];
                                                      getDate =
                                                          userData["Date"];
                                                      getTime =
                                                          userData["Time"];
                                                      getPostTitle = userData[
                                                          "Post Title"];
                                                      getPostContent = userData[
                                                          "Post Content"];
                                                      getPostViews =
                                                          userData["Views"];
                                                      getPostComments =
                                                          userData["Comments"];
                                                      getPostLikes =
                                                          userData["Likes"];

                                                      // For counting Views to Posts
                                                      if (getUserType ==
                                                          'I am a Donor') {
                                                        setState(() {
                                                          int x = int.parse(
                                                                  getPostViews!) +
                                                              1;
                                                          getPostViews =
                                                              x.toString();

                                                          viewsAddDonor();
                                                          viewsAddAll();
                                                          viewsAddSpecificDonor();
                                                          viewsAddSpecificAll();
                                                        });
                                                      } else {
                                                        setState(() {
                                                          int x = int.parse(
                                                                  getPostViews!) +
                                                              1;
                                                          getPostViews =
                                                              x.toString();

                                                          viewsAddPatient();
                                                          viewsAddAll();
                                                          viewsAddSpecificPatient();
                                                          viewsAddSpecificAll();
                                                        });
                                                      }
                                                      Navigator.of(context).push(MaterialPageRoute(
                                                          builder: (context) => CommentsPageProfile(
                                                              getUserName:
                                                                  getUserName,
                                                              getFullName:
                                                                  getFullName,
                                                              getUserID:
                                                                  getUserID,
                                                              getUserType:
                                                                  getUserType,
                                                              getDate: getDate,
                                                              getTime: getTime,
                                                              getPostTitle:
                                                                  getPostTitle,
                                                              getPostContent:
                                                                  getPostContent,
                                                              getPostViews:
                                                                  getPostViews,
                                                              getPostComments:
                                                                  getPostComments,
                                                              getPostLikes:
                                                                  getPostLikes)));
                                                    },
                                                    child: Row(
                                                      children: [
                                                        //Comments Icon
                                                        Icon(
                                                          Icons
                                                              .comment_outlined,
                                                          color: Colors.grey,
                                                        ),
                                                        //Comments Count
                                                        Container(
                                                          margin:
                                                              EdgeInsets.only(
                                                                  left: 10),
                                                          child: Text(
                                                              userData[
                                                                  "Comments"],
                                                              style: GoogleFonts.roboto(
                                                                  color: Colors
                                                                      .grey,
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .w400,
                                                                  fontSize:
                                                                      14)),
                                                        ),
                                                      ],
                                                    ),
                                                  ),
                                                  //Likes
                                                  InkWell(
                                                    onTap: () {
                                                      getUserID =
                                                          userData["UserID"];
                                                      getUserName =
                                                          userData["UserName"];
                                                      getFullName =
                                                          userData["FullName"];
                                                      getUserType =
                                                          userData["User Type"];
                                                      getDate =
                                                          userData["Date"];
                                                      getTime =
                                                          userData["Time"];
                                                      getPostTitle = userData[
                                                          "Post Title"];
                                                      getPostContent = userData[
                                                          "Post Content"];
                                                      getPostViews =
                                                          userData["Views"];
                                                      getPostComments =
                                                          userData["Comments"];
                                                      getPostLikes =
                                                          userData["Likes"];
                                                      getNotification =
                                                          userData[
                                                              "Notifications"];

                                                      if (getUserType ==
                                                          'I am a Donor') {
                                                        setState(() {
                                                          int x = int.parse(
                                                                  getPostLikes!) +
                                                              1;
                                                          getPostLikes =
                                                              x.toString();
                                                          likesAddDonor();
                                                          likesAddAll();
                                                          likesAddSpecificDonor();
                                                          likesAddSpecificAll();
                                                          likesAddSpecificProfileDonor();
                                                          likesAddSpecificProfileAll();
                                                          //Notification count
                                                          int y = int.parse(
                                                                  getNotification!) +
                                                              1;
                                                          countNotification =
                                                              y.toString();
                                                          notificationAddDonor();
                                                          notificationAddAll();
                                                          notificationAddSpecificDonor();
                                                          notificationAddSpecificAll();
                                                        });
                                                      } else {
                                                        setState(() {
                                                          int x = int.parse(
                                                                  getPostLikes!) +
                                                              1;
                                                          getPostLikes =
                                                              x.toString();
                                                          likesAddPatient();
                                                          likesAddAll();
                                                          likesAddSpecificPatient();
                                                          likesAddSpecificAll();
                                                          likesAddSpecificProfilePatient();
                                                          likesAddSpecificProfileAll();
                                                          //Notification count
                                                          int y = int.parse(
                                                                  getNotification!) +
                                                              1;
                                                          countNotification =
                                                              y.toString();
                                                          notificationAddPatient();
                                                          notificationAddAll();
                                                          notificationAddSpecificPatient();
                                                          notificationAddSpecificAll();
                                                        });
                                                      }
                                                    },
                                                    child: Row(
                                                      children: [
                                                        //Likes Icon
                                                        Icon(
                                                          Icons
                                                              .thumb_up_outlined,
                                                          color: Colors.grey,
                                                        ),
                                                        //Likes Count
                                                        Container(
                                                          margin:
                                                              EdgeInsets.only(
                                                                  left: 10),
                                                          child: Text(
                                                              userData["Likes"],
                                                              style: GoogleFonts.roboto(
                                                                  color: Colors
                                                                      .grey,
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .w400,
                                                                  fontSize:
                                                                      14)),
                                                        ),
                                                      ],
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            )
                                          ],
                                        ),
                                      ),
                                    ),
                                  ],
                                );
                              });
                        }
                      },
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      );

//Alert Dialog Method
  void showCustomDialog(BuildContext context) => showDialog(
      context: context,
      builder: (BuildContext context) {
        return Dialog(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20.0)), //this right here
          child: Container(
            height: 180,
            width: 100,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    InkWell(
                      onTap: () {
                        Navigator.of(context).pop();
                      },
                      child: Container(
                        margin: EdgeInsets.only(right: 10),
                        child: Icon(Icons.close_outlined),
                      ),
                    ),
                  ],
                ),
                Text(
                  "Signature",
                  style: GoogleFonts.roboto(
                      color: Colors.black87,
                      fontWeight: FontWeight.w500,
                      fontSize: 18),
                ),
                Container(
                  margin: EdgeInsets.only(top: 20),
                  child: FutureBuilder(
                    future: getUserData(),
                    builder: (context, snapshot) {
                      if (snapshot.connectionState != ConnectionState.done)
                        return Text("");
                      if (donorSignature != null) {
                        return Container(
                          margin: EdgeInsets.only(top: 10),
                          height: 20,
                          width: 240,
                          child: TextFormField(
                            controller: _signature..text = donorSignature!,
                            textAlign: TextAlign.center,
                            decoration: InputDecoration(
                                hintText:
                                    'Default signature given to everyone~',
                                enabled: false,
                                border: InputBorder.none),
                            style: GoogleFonts.roboto(
                                color: Colors.black54, fontSize: 14),
                          ),
                        );
                      }
                      if (patientSignature != null) {
                        return Container(
                          margin: EdgeInsets.only(top: 10),
                          height: 20,
                          child: TextFormField(
                            controller: _signature..text = patientSignature!,
                            textAlign: TextAlign.center,
                            decoration: InputDecoration(
                                hintText:
                                    'Default signature given to everyone~',
                                enabled: false,
                                border: InputBorder.none),
                            style: GoogleFonts.roboto(
                                color: Colors.black54, fontSize: 14),
                          ),
                        );
                      } else {
                        return Text(
                          "null",
                          style: GoogleFonts.roboto(
                              color: Colors.black54,
                              fontSize: 18,
                              fontWeight: FontWeight.bold),
                        );
                      }
                    },
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(top: 20),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Container(
                        child: InkWell(
                          onTap: () {
                            Navigator.of(context).pop();
                          },
                          child: Container(
                            height: 35,
                            width: 230,
                            decoration: BoxDecoration(
                                color: Colors.red[50],
                                borderRadius: BorderRadius.circular(18)),
                            child: Center(
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Text(
                                  "Close",
                                  style: TextStyle(
                                      color: Colors.red[400], fontSize: 17),
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        );
      });

  //Get Donor and Patient Information
  getUserData() async {
    final donor = FirebaseAuth.instance.currentUser;
    //Get Donor Profile Details
    if (donor != null) {
      await FirebaseFirestore.instance
          .collection('Users')
          .doc("Donor")
          .collection("Lists")
          .doc(donor.uid)
          .get()
          .then((value) {
        donorUserName = value.data()!['UserName'];
        donorUserType = value.data()!['User Type'];
        donorSignature = value.data()!['Signature'];
        donorUserID = value.data()!['UserID'];
        donorCountPosts = value.data()!['Posts'];
        donorDonated = value.data()!['Donated'];
        donorReceived = value.data()!['Received'];
        donorLikes = value.data()!['Likes'];
        donorPosts = value.data()!['Posts'];
      }).catchError((e) {
        print(e);
      });
    }

    //Get Donor Card Details
    if (donor != null) {
      await FirebaseFirestore.instance
          .collection('Donor Card')
          .doc("Donor")
          .collection("Lists")
          .doc(donor.uid)
          .get()
          .then((value) {
        donorDate = value.data()!['Date'];
        donorName = value.data()!['Donor'];
        donorChoice = value.data()!['Choice'];
        donorBirthdate = value.data()!['BirthDate'];
        donorAddress = value.data()!['Address'];
        donorTelephone = value.data()!['Telephone'];
        donorFirstWitness = value.data()!['First Witness'];
        donorSecondWitness = value.data()!['Second Witness'];
      }).catchError((e) {
        print(e);
      });
    }

    //Get Patient Profile Details
    final patient = FirebaseAuth.instance.currentUser;
    if (patient != null) {
      await FirebaseFirestore.instance
          .collection('Users')
          .doc("Patient")
          .collection("Lists")
          .doc(patient.uid)
          .get()
          .then((value) {
        patientUserName = value.data()!['UserName'];
        patientUserType = value.data()!['User Type'];
        patientSignature = value.data()!['Signature'];
        patientDonated = value.data()!['Donated'];
        patientReceived = value.data()!['Received'];
        patientLikes = value.data()!['Likes'];
        patientPosts = value.data()!['Posts'];
        patientUserID = value.data()!['UserID'];
        patientCountPosts = value.data()!['Posts'];
      }).catchError((e) {
        print(e);
      });
    }
    //Get Patient Donor Card Details
    if (patient != null) {
      await FirebaseFirestore.instance
          .collection('Donor Card')
          .doc("Patient")
          .collection("Lists")
          .doc(patient.uid)
          .get()
          .then((value) {
        patientDate = value.data()!['Date'];
        patientName = value.data()!['Donor'];
        patientChoice = value.data()!['Choice'];
        patientBirthdate = value.data()!['BirthDate'];
        patientAddress = value.data()!['Address'];
        patientTelephone = value.data()!['Telephone'];
        patientFirstWitness = value.data()!['First Witness'];
        patientSecondWitness = value.data()!['Second Witness'];
      }).catchError((e) {
        print(e);
      });
    }
  }

  void refresh() {
    if (usersValue == 'I am a Donor') {
      finalDate = donorDate;
      finalDonor = donorName;
      finalFinalChoice = donorChoice;
      finalBirthdate = donorBirthdate;
      finalAddress = donorAddress;
      finalTelephone = donorTelephone;
      finalFirstWitness = donorFirstWitness;
      finalSecondWitness = donorSecondWitness;
    } else {
      finalDate = patientDate;
      finalDonor = patientName;
      finalFinalChoice = patientChoice;
      finalBirthdate = patientBirthdate;
      finalAddress = patientAddress;
      finalTelephone = patientTelephone;
      finalFirstWitness = patientFirstWitness;
      finalSecondWitness = patientSecondWitness;
    }
  }

  //get User Posts
  getUserPostsData() async {
    //Get Patient Profile Details
    final currentUser = FirebaseAuth.instance.currentUser;

    if (currentUser != null) {
      //Initialize Value
      await FirebaseFirestore.instance
          .collection('Users')
          .doc("All")
          .collection("Lists")
          .doc(currentUser.uid)
          .get()
          .then((value) {
        currentUserName = value.data()!['UserName'];
        currentUserType = value.data()!['User Type'];
        currentSignature = value.data()!['Signature'];
        currentPosts = value.data()!['Posts'];
      }).catchError((e) {
        print(e);
      });

      //Display Lists
      final firestore = FirebaseFirestore.instance;
      QuerySnapshot user = await firestore
          .collection('Users')
          .doc('All')
          .collection('Lists')
          .doc(currentUser.uid)
          .collection('Posts')
          .get();
      return user.docs;
    }
  }

  Widget buildCurrentUserSheet() => Container(
        height: 130,
        child: ListView(
          children: [
            //Edit
            ListTile(
              title: Row(
                children: [
                  Text(
                    'Edit Post',
                    style: GoogleFonts.roboto(
                        color: Colors.black87,
                        fontSize: 20,
                        fontWeight: FontWeight.w400),
                  ),
                  Container(
                    margin: EdgeInsets.only(left: 10),
                    child: Icon(
                      Icons.reply_outlined,
                      color: Colors.red[100],
                    ),
                  ),
                ],
              ),
              onTap: () {
                Navigator.of(context).pop();
                Navigator.of(context).push(MaterialPageRoute(
                    builder: (context) => EditPostsProfile(
                        getUserName: getUserName,
                        getFullName: getFullName,
                        getUserID: getUserID,
                        getUserType: getUserType,
                        getDate: getDate,
                        getTime: getTime,
                        getPostTitle: getPostTitle,
                        getPostContent: getPostContent)));
              },
            ),
            //Delete
            ListTile(
              title: Row(
                children: [
                  Text(
                    'Delete Post',
                    style: GoogleFonts.roboto(
                        color: Colors.black87,
                        fontSize: 20,
                        fontWeight: FontWeight.w400),
                  ),
                  Container(
                    margin: EdgeInsets.only(left: 10),
                    child: Icon(
                      Icons.delete_outline,
                      color: Colors.red[100],
                    ),
                  ),
                ],
              ),
              onTap: () {
                setState(() {
                  if (getUserType == 'I am a Donor') {
                    //Post Count Minus

                    int x = int.parse(donorCountPosts!) - 1;
                    countPost = x.toString();
                    postMinusDonor();
                    postMinusAll();

                    //Delete
                    deleteDonorPost();
                    deleteAlltPost();
                    deleteSpecificDonorPost();
                    deleteSpecificAllPost();
                    Navigator.of(context).pop();
                    showDeleteToast();
                  } else {
                    //Post Count Minus

                    int x = int.parse(patientCountPosts!) - 1;
                    countPost = x.toString();
                    postMinusPatient();
                    postMinusAll();

                    //Delete
                    deletePatientPost();
                    deleteAlltPost();
                    deleteSpecificPatientPost();
                    deleteSpecificAllPost();
                    Navigator.of(context).pop();
                    showDeleteToast();
                  }
                });
              },
            ),
          ],
        ),
      );

  //Delete
  deleteDonorPost() async {
    String? uid = getTime;
    await FirebaseFirestore.instance
        .collection("Posts")
        .doc('Donor')
        .collection('All Posts')
        .doc(uid)
        .delete();
  }

  deletePatientPost() async {
    String? uid = getTime;
    await FirebaseFirestore.instance
        .collection("Posts")
        .doc('Patient')
        .collection('All Posts')
        .doc(uid)
        .delete();
  }

  deleteAlltPost() async {
    String? uid = getTime;
    await FirebaseFirestore.instance
        .collection("Posts")
        .doc('All')
        .collection('All Posts')
        .doc(uid)
        .delete();
  }

  deleteSpecificDonorPost() async {
    String? uid = getUserID;
    String? did = getTime;
    await FirebaseFirestore.instance
        .collection("Users")
        .doc('Donor')
        .collection('Lists')
        .doc(uid)
        .collection('Posts')
        .doc(did)
        .delete();
  }

  deleteSpecificPatientPost() async {
    String? uid = getUserID;
    String? did = getTime;
    await FirebaseFirestore.instance
        .collection("Users")
        .doc('Patient')
        .collection('Lists')
        .doc(uid)
        .collection('Posts')
        .doc(did)
        .delete();
  }

  deleteSpecificAllPost() async {
    String? uid = getUserID;
    String? did = getTime;
    await FirebaseFirestore.instance
        .collection("Users")
        .doc('All')
        .collection('Lists')
        .doc(uid)
        .collection('Posts')
        .doc(did)
        .delete();
  }

  //Show Delete Success
  void showDeleteToast() =>
      toast.showToast(child: buildDeleteToast(), gravity: ToastGravity.CENTER);

  Widget buildDeleteToast() => Container(
        padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
        decoration: BoxDecoration(
          color: Colors.green[300],
          borderRadius: BorderRadius.circular(5),
        ),
        child: Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            Text(
              "Deleted",
              style: TextStyle(color: Colors.black87, fontSize: 16),
            ),
          ],
        ),
      );

  //Count Posts
  postMinusDonor() async {
    User? user = FirebaseAuth.instance.currentUser;
    await FirebaseFirestore.instance
        .collection("Users")
        .doc('Donor')
        .collection('Lists')
        .doc(user!.uid)
        .update({
      'Posts': countPost,
    });
  }

  postMinusPatient() async {
    User? user = FirebaseAuth.instance.currentUser;
    await FirebaseFirestore.instance
        .collection("Users")
        .doc('Patient')
        .collection('Lists')
        .doc(user!.uid)
        .update({
      'Posts': countPost,
    });
  }

  postMinusAll() async {
    User? user = FirebaseAuth.instance.currentUser;
    await FirebaseFirestore.instance
        .collection("Users")
        .doc('All')
        .collection('Lists')
        .doc(user!.uid)
        .update({
      'Posts': countPost,
    });
  }

  //Post Utilities

  //Views Count
  viewsAddDonor() async {
    String? did = getTime;
    await FirebaseFirestore.instance
        .collection("Posts")
        .doc('Donor')
        .collection('All Posts')
        .doc(did)
        .update({
      'Views': getPostViews,
    });
  }

  viewsAddPatient() async {
    String? did = getTime;
    await FirebaseFirestore.instance
        .collection("Posts")
        .doc('Patient')
        .collection('All Posts')
        .doc(did)
        .update({
      'Views': getPostViews,
    });
  }

  viewsAddAll() async {
    String? did = getTime;
    await FirebaseFirestore.instance
        .collection("Posts")
        .doc('All')
        .collection('All Posts')
        .doc(did)
        .update({
      'Views': getPostViews,
    });
  }

  viewsAddSpecificDonor() async {
    String? uid = getUserID;
    String? did = getTime;
    await FirebaseFirestore.instance
        .collection("Users")
        .doc('Donor')
        .collection('Lists')
        .doc(uid)
        .collection('Posts')
        .doc(did)
        .update({
      'Views': getPostViews,
    });
  }

  viewsAddSpecificPatient() async {
    String? uid = getUserID;
    String? did = getTime;
    await FirebaseFirestore.instance
        .collection("Users")
        .doc('Patient')
        .collection('Lists')
        .doc(uid)
        .collection('Posts')
        .doc(did)
        .update({
      'Views': getPostViews,
    });
  }

  viewsAddSpecificAll() async {
    String? uid = getUserID;
    String? did = getTime;
    await FirebaseFirestore.instance
        .collection("Users")
        .doc('All')
        .collection('Lists')
        .doc(uid)
        .collection('Posts')
        .doc(did)
        .update({
      'Views': getPostViews,
    });
  }

  //Likes Counter
  likesAddDonor() async {
    String? did = getTime;
    await FirebaseFirestore.instance
        .collection("Posts")
        .doc('Donor')
        .collection('All Posts')
        .doc(did)
        .update({
      'Likes': getPostLikes,
    });
  }

  likesAddPatient() async {
    String? did = getTime;
    await FirebaseFirestore.instance
        .collection("Posts")
        .doc('Patient')
        .collection('All Posts')
        .doc(did)
        .update({
      'Likes': getPostLikes,
    });
  }

  likesAddAll() async {
    String? did = getTime;
    await FirebaseFirestore.instance
        .collection("Posts")
        .doc('All')
        .collection('All Posts')
        .doc(did)
        .update({
      'Likes': getPostLikes,
    });
  }

  likesAddSpecificDonor() async {
    String? uid = getUserID;
    String? did = getTime;
    await FirebaseFirestore.instance
        .collection("Users")
        .doc('Donor')
        .collection('Lists')
        .doc(uid)
        .collection('Posts')
        .doc(did)
        .update({
      'Likes': getPostLikes,
    });
  }

  likesAddSpecificPatient() async {
    String? uid = getUserID;
    String? did = getTime;
    await FirebaseFirestore.instance
        .collection("Users")
        .doc('Patient')
        .collection('Lists')
        .doc(uid)
        .collection('Posts')
        .doc(did)
        .update({
      'Likes': getPostLikes,
    });
  }

  likesAddSpecificAll() async {
    String? uid = getUserID;
    String? did = getTime;
    await FirebaseFirestore.instance
        .collection("Users")
        .doc('All')
        .collection('Lists')
        .doc(uid)
        .collection('Posts')
        .doc(did)
        .update({
      'Likes': getPostLikes,
    });
  }

  //Likes added to User Profile
  likesAddSpecificProfileDonor() async {
    String? uid = getUserID;

    await FirebaseFirestore.instance
        .collection("Users")
        .doc('Donor')
        .collection('Lists')
        .doc(uid)
        .update({
      'Likes': getPostLikes,
    });
  }

  likesAddSpecificProfilePatient() async {
    String? uid = getUserID;

    await FirebaseFirestore.instance
        .collection("Users")
        .doc('Patient')
        .collection('Lists')
        .doc(uid)
        .update({
      'Likes': getPostLikes,
    });
  }

  likesAddSpecificProfileAll() async {
    String? uid = getUserID;

    await FirebaseFirestore.instance
        .collection("Users")
        .doc('All')
        .collection('Lists')
        .doc(uid)
        .update({
      'Likes': getPostLikes,
    });
  }

  //Notification Count
  notificationAddDonor() async {
    String? did = getTime;
    await FirebaseFirestore.instance
        .collection("Posts")
        .doc('Donor')
        .collection('All Posts')
        .doc(did)
        .update({
      'Notifications': countNotification,
    });
  }

  notificationAddPatient() async {
    String? did = getTime;
    await FirebaseFirestore.instance
        .collection("Posts")
        .doc('Patient')
        .collection('All Posts')
        .doc(did)
        .update({
      'Notifications': countNotification,
    });
  }

  notificationAddAll() async {
    String? did = getTime;
    await FirebaseFirestore.instance
        .collection("Posts")
        .doc('All')
        .collection('All Posts')
        .doc(did)
        .update({
      'Notifications': countNotification,
    });
  }

  notificationAddSpecificDonor() async {
    String? uid = getUserID;
    await FirebaseFirestore.instance
        .collection("Users")
        .doc('Donor')
        .collection('Lists')
        .doc(uid)
        .update({
      'Notifications': countNotification,
    });
  }

  notificationAddSpecificPatient() async {
    String? uid = getUserID;
    await FirebaseFirestore.instance
        .collection("Users")
        .doc('Patient')
        .collection('Lists')
        .doc(uid)
        .update({
      'Notifications': countNotification,
    });
  }

  notificationAddSpecificAll() async {
    String? uid = getUserID;
    await FirebaseFirestore.instance
        .collection("Users")
        .doc('All')
        .collection('Lists')
        .doc(uid)
        .update({
      'Notifications': countNotification,
    });
  }
}

//Second Class
class CustomSliverAppBarDelegate extends SliverPersistentHeaderDelegate {
  final double expandedHeight;
  late final String? donorFullName;
  late final String? patientFullName;

  CustomSliverAppBarDelegate({
    required this.expandedHeight,
  });

  @override
  Widget build(
      BuildContext context, double shrinkOffset, bool overlapsContent) {
    final size = 60;
    final top = expandedHeight - shrinkOffset - size / 2;

    return Stack(
      fit: StackFit.expand,
      overflow: Overflow.visible,
      children: [
        buildBackground(shrinkOffset),
        buildAppBar(context, shrinkOffset),
        Positioned(
          top: top,
          left: 20,
          right: 20,
          child: buildFloating(shrinkOffset),
        ),
      ],
    );
  }

  double appear(double shrinkOffset) => shrinkOffset / expandedHeight;

  double disappear(double shrinkOffset) => 1 - shrinkOffset / expandedHeight;

  Widget buildAppBar(BuildContext context, double shrinkOffset) => Opacity(
        opacity: appear(shrinkOffset),
        child: AppBar(
          title: Row(
            children: [
              Image.asset(
                'assets/user.png',
                height: 25,
              ),
              Text(
                "\t\t My Profile ",
                style: GoogleFonts.roboto(
                  color: Colors.white,
                  fontSize: 18,
                ),
              ),
            ],
          ),
          backgroundColor: Colors.red[400],
          actions: [
            InkWell(
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => ProfileSettings()),
                );
              },
              child: Padding(
                padding: const EdgeInsets.only(right: 20),
                child: Icon(Icons.settings_outlined),
              ),
            ),
          ],
        ),
      );

  Widget buildBackground(double shrinkOffset) => Opacity(
        opacity: disappear(shrinkOffset),
        child: ClipRRect(
          borderRadius: BorderRadius.vertical(
            bottom: Radius.circular(40),
          ),
          child: Image.asset(
            'assets/kidneybackground.jpg',
            fit: BoxFit.cover,
          ),
        ),
      );

  Widget buildFloating(double shrinkOffset) => Opacity(
        opacity: disappear(shrinkOffset),
        child: Container(
          alignment: Alignment.centerLeft,
          margin: EdgeInsets.only(left: 20),
          child: Image.asset(
            'assets/user.png',
            height: 100,
          ),
        ),
      );

  @override
  double get maxExtent => expandedHeight;

  @override
  double get minExtent => kToolbarHeight + 30;

  @override
  bool shouldRebuild(SliverPersistentHeaderDelegate oldDelegate) => true;

  getUserData() async {
    final donor = FirebaseAuth.instance.currentUser;
    if (donor != null) {
      await FirebaseFirestore.instance
          .collection('Users')
          .doc("Donor")
          .collection("Lists")
          .doc(donor.uid)
          .get()
          .then((value) {
        donorFullName = value.data()!['FullName'];

        print(donorFullName);
      }).catchError((e) {
        print(e);
      });
    }

    final patient = FirebaseAuth.instance.currentUser;
    if (patient != null) {
      await FirebaseFirestore.instance
          .collection('Users')
          .doc("Patient")
          .collection("Lists")
          .doc(patient.uid)
          .get()
          .then((value) {
        patientFullName = value.data()!['FullName'];
        print(patientFullName);
      }).catchError((e) {
        print(e);
      });
    }
  }
}
