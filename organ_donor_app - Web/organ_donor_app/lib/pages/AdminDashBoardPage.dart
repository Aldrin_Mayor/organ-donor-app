import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:syncfusion_flutter_charts/charts.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:intl/intl.dart';

class AdminDashBoardPage extends StatefulWidget {
  AdminDashBoardPage({Key? key}) : super(key: key);

  @override
  _AdminDashBoardPageState createState() => _AdminDashBoardPageState();
}

class _AdminDashBoardPageState extends State<AdminDashBoardPage> {
  //For Total Users
  int initialTotalDonorCount = 0;
  int initialTotalPatientCount = 0;
  int initialTotalArchivedDonorCount = 0;
  int initialTotalArchivedPatientCount = 0;
  int initialTotalTaggedDonorCount = 0;
  int initialTotalTaggedPatientCount = 0;
  int initialTotalTaggedArchivedDonorCount = 0;
  int initialTotalTaggedArchivedPatientCount = 0;
  int totalUsers = 0;
  //Percentage
  double donorPercentage = 0;
  double patientPercentage = 0;
  double donorTaggedPercentage = 0;
  double patientTaggedPercentage = 0;
  double donorRegisteredPercentage = 0;
  double patientRegisteredPercentage = 0;
  double totalRegisteredUsersPercentage = 0;
  double totalTaggedUsersPercentage = 0;

  //Total Registered Users
  int totalRegisteredUsers = 0;

  //Total Tagged Users
  int totalTaggedUsers = 0;

  //For Normal Donor
  int initialDonorCount = 0;
  int initialArchivedDonorCount = 0;
  int totalDonorCount = 0;

  //For Normal Patient
  int initialPatientCount = 0;
  int initialArchivedPatientCount = 0;
  int totalPatientCount = 0;

  //For Tagged Donor
  int initialTaggedDonorCount = 0;
  int initialTaggedArchivedDonorCount = 0;
  int totalTaggedDonorCount = 0;

  //For Normal Patient
  int initialTaggedPatientCount = 0;
  int initialTaggedArchivedPatientCount = 0;
  int totalTaggedPatientCount = 0;

  //Posts Count and Kidney Match
  int initialPostsCount = 0;
  int initialKidneyMatchCount = 0;

  double? rating = 0;
  String? getRating;

  //For Admin RadialBar Graph
  late List<adminData> _adminChartData;
  late TooltipBehavior _tooltipBehavior;
  final toast = FToast();
  @override
  void initState() {
    _adminChartData = getAdminChartData();

    _tooltipBehavior = TooltipBehavior(enable: true);
    //Toast
    toast.init(context);
    super.initState();
  }

  String? getUserID;
  String? getUserName;

  //Time and Date
  var now = new DateTime.now();
  var formatter = new DateFormat('dd-MM-yyyy');
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xffe5eef8),
      body: SingleChildScrollView(
        child: Wrap(
          spacing: 8.0, // gap between adjacent chips
          runSpacing: 4.0, // gap between lines
          direction: Axis.horizontal,
          children: <Widget>[
            //Admin and Users Count

            Row(
              children: [
                //Admin
                Container(
                  margin: EdgeInsets.only(top: 50, left: 50),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    color: Colors.white,
                  ),
                  child: FutureBuilder(
                    future: getDonorDocument(),
                    builder: (context, snapshot) {
                      if (snapshot.connectionState != ConnectionState.done) {
                        return Text("");
                      }
                      return Container(
                        width: MediaQuery.of(context).size.width * 0.355,
                        padding: EdgeInsets.all(20),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(20)),
                        child: Row(
                          children: [
                            //Image
                            Container(
                              margin: EdgeInsets.only(left: 50),
                              child: Image.asset(
                                'assets/admin_doctor.png',
                                height: 100,
                              ),
                            ),
                            Column(
                              children: [
                                //Admin Count
                                Container(
                                  height: 150,
                                  margin: EdgeInsets.only(left: 40, top: 20),
                                  child: SelectableText.rich(
                                    TextSpan(
                                      children: [
                                        TextSpan(
                                          text: '3',
                                          style: GoogleFonts.roboto(
                                              color: Colors.indigo[600],
                                              fontWeight: FontWeight.bold,
                                              fontSize: 80),
                                        ),
                                        TextSpan(
                                          text: '\nAdmin (Doctors)',
                                          style: GoogleFonts.roboto(
                                              color: Colors.grey, fontSize: 20),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                            //Chart
                            SafeArea(
                              child: Container(
                                height: 350,
                                child: SfCircularChart(
                                  title: ChartTitle(
                                    text: 'Admin Overview',
                                    textStyle: GoogleFonts.roboto(
                                      fontWeight: FontWeight.w600,
                                    ),
                                  ),
                                  legend: Legend(
                                      isVisible: true,
                                      overflowMode:
                                          LegendItemOverflowMode.scroll),
                                  tooltipBehavior: _tooltipBehavior,
                                  series: <CircularSeries>[
                                    RadialBarSeries<adminData, String>(
                                        dataSource: _adminChartData,
                                        xValueMapper: (adminData data, _) =>
                                            data.title,
                                        yValueMapper: (adminData data, _) =>
                                            data.digits,
                                        dataLabelSettings: DataLabelSettings(
                                          isVisible: false,
                                        ),
                                        enableTooltip: true,
                                        maximumValue: 5),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                      );
                    },
                  ),
                ),

                //Users
                Container(
                  margin: EdgeInsets.only(top: 50, left: 50),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    color: Colors.white,
                  ),
                  child: FutureBuilder(
                    future: getTotalUsers(),
                    builder: (context, snapshot) {
                      if (snapshot.connectionState != ConnectionState.done) {
                        return Text("");
                      }

                      return Container(
                        width: MediaQuery.of(context).size.width * 0.355,
                        padding: EdgeInsets.all(20),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(20)),
                        child: Row(
                          children: [
                            //Image
                            Container(
                              margin: EdgeInsets.only(left: 50),
                              child: Image.asset(
                                'assets/admin_users.png',
                                height: 100,
                              ),
                            ),
                            Column(
                              children: [
                                //Users Count
                                Container(
                                  height: 150,
                                  margin: EdgeInsets.only(left: 40, top: 20),
                                  child: SelectableText.rich(
                                    TextSpan(
                                      children: [
                                        TextSpan(
                                          text: totalUsers.toString(),
                                          style: GoogleFonts.roboto(
                                              color: Colors.indigo[600],
                                              fontWeight: FontWeight.bold,
                                              fontSize: 80),
                                        ),
                                        TextSpan(
                                          text: '\nUsers',
                                          style: GoogleFonts.roboto(
                                              color: Colors.grey, fontSize: 20),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                            //Chart
                            Column(
                              children: [
                                Container(
                                  margin: EdgeInsets.only(top: 20),
                                  child: Text(
                                    'Total Users Overview',
                                    style: GoogleFonts.roboto(
                                      fontSize: 18,
                                      fontWeight: FontWeight.w600,
                                    ),
                                  ),
                                ),
                                Row(
                                  children: [
                                    //Title
                                    Container(
                                      margin: EdgeInsets.only(
                                          top: 40, left: 30, bottom: 50),
                                      color: Colors.white,
                                      width: 70,
                                      height: 220,
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        mainAxisAlignment:
                                            MainAxisAlignment.end,
                                        children: [
                                          Container(
                                            margin: EdgeInsets.only(bottom: 15),
                                            child: Text(
                                              'Patient Tagged',
                                              style: GoogleFonts.roboto(
                                                  color: Colors.grey,
                                                  fontSize: 14,
                                                  height: 1.3),
                                            ),
                                          ),
                                          Container(
                                            margin: EdgeInsets.only(bottom: 15),
                                            child: Text(
                                              'Donor Tagged',
                                              style: GoogleFonts.roboto(
                                                  color: Colors.grey,
                                                  fontSize: 14,
                                                  height: 1.3),
                                            ),
                                          ),
                                          Container(
                                            margin: EdgeInsets.only(bottom: 15),
                                            child: Text(
                                              'Patient Registered',
                                              style: GoogleFonts.roboto(
                                                  color: Colors.grey,
                                                  fontSize: 14,
                                                  height: 1.3),
                                            ),
                                          ),
                                          Container(
                                            margin: EdgeInsets.only(bottom: 15),
                                            child: Text(
                                              'Donor Registered',
                                              style: GoogleFonts.roboto(
                                                  color: Colors.grey,
                                                  fontSize: 14,
                                                  height: 1.3),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                    //Graph Data
                                    Container(
                                      width: 220,
                                      height: 220,
                                      margin: EdgeInsets.only(
                                          top: 40, left: 10, bottom: 50),
                                      decoration: BoxDecoration(
                                          color: Colors.red[50],
                                          borderRadius:
                                              BorderRadius.circular(5)),
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        mainAxisAlignment:
                                            MainAxisAlignment.end,
                                        children: [
                                          //Total Patient Tagged
                                          Row(
                                            children: [
                                              Container(
                                                margin:
                                                    EdgeInsets.only(bottom: 20),
                                                color: Colors.green,
                                                width: double.parse(
                                                        initialTotalTaggedPatientCount
                                                            .toString()) *
                                                    10,
                                                height: 30,
                                              ),
                                              Container(
                                                margin: EdgeInsets.only(
                                                    bottom: 20, left: 5),
                                                child: Row(
                                                  children: [
                                                    Text(
                                                      initialTotalTaggedPatientCount
                                                          .toString(),
                                                      style: GoogleFonts.roboto(
                                                          color: Colors.black87,
                                                          fontSize: 14,
                                                          fontWeight:
                                                              FontWeight.w700),
                                                    ),
                                                    Container(
                                                      margin: EdgeInsets.only(
                                                          left: 5),
                                                      child: Text(
                                                        "(" +
                                                            patientTaggedPercentage
                                                                .toStringAsFixed(
                                                                    2) +
                                                            " % " +
                                                            ")",
                                                        style:
                                                            GoogleFonts.roboto(
                                                                color: Colors
                                                                    .red[900],
                                                                fontSize: 14,
                                                                fontWeight:
                                                                    FontWeight
                                                                        .w300),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            ],
                                          ),
                                          // Total Donor Tagged
                                          Row(
                                            children: [
                                              Container(
                                                margin:
                                                    EdgeInsets.only(bottom: 20),
                                                color: Colors.orange,
                                                width: double.parse(
                                                        initialTotalTaggedDonorCount
                                                            .toString()) *
                                                    10,
                                                height: 30,
                                              ),
                                              Container(
                                                margin: EdgeInsets.only(
                                                    bottom: 20, left: 5),
                                                child: Row(
                                                  children: [
                                                    Text(
                                                      initialTotalTaggedDonorCount
                                                          .toString(),
                                                      style: GoogleFonts.roboto(
                                                          color: Colors.black87,
                                                          fontSize: 14,
                                                          fontWeight:
                                                              FontWeight.w700),
                                                    ),
                                                    Container(
                                                      margin: EdgeInsets.only(
                                                          left: 5),
                                                      child: Text(
                                                        "(" +
                                                            donorTaggedPercentage
                                                                .toStringAsFixed(
                                                                    2) +
                                                            " % " +
                                                            ")",
                                                        style:
                                                            GoogleFonts.roboto(
                                                                color: Colors
                                                                    .red[900],
                                                                fontSize: 14,
                                                                fontWeight:
                                                                    FontWeight
                                                                        .w300),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            ],
                                          ),
                                          //Total Patient Count
                                          Row(
                                            children: [
                                              Container(
                                                margin:
                                                    EdgeInsets.only(bottom: 22),
                                                color: Colors.yellow,
                                                width: double.parse(
                                                        initialTotalPatientCount
                                                            .toString()) *
                                                    10,
                                                height: 30,
                                              ),
                                              Container(
                                                margin: EdgeInsets.only(
                                                    bottom: 22, left: 5),
                                                child: Row(
                                                  children: [
                                                    Text(
                                                      initialTotalPatientCount
                                                          .toString(),
                                                      style: GoogleFonts.roboto(
                                                          color: Colors.black87,
                                                          fontSize: 14,
                                                          fontWeight:
                                                              FontWeight.w700),
                                                    ),
                                                    Container(
                                                      margin: EdgeInsets.only(
                                                          left: 5),
                                                      child: Text(
                                                        "(" +
                                                            patientPercentage
                                                                .toStringAsFixed(
                                                                    2) +
                                                            " % " +
                                                            ")",
                                                        style:
                                                            GoogleFonts.roboto(
                                                                color: Colors
                                                                    .red[900],
                                                                fontSize: 14,
                                                                fontWeight:
                                                                    FontWeight
                                                                        .w300),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            ],
                                          ),
                                          //Total Donor Count
                                          Row(
                                            children: [
                                              Container(
                                                margin:
                                                    EdgeInsets.only(bottom: 18),
                                                color: Colors.blue,
                                                width: double.parse(
                                                        initialTotalDonorCount
                                                            .toString()) *
                                                    10,
                                                height: 30,
                                              ),
                                              Container(
                                                margin: EdgeInsets.only(
                                                    bottom: 18, left: 5),
                                                child: Row(
                                                  children: [
                                                    Text(
                                                      initialTotalDonorCount
                                                          .toString(),
                                                      style: GoogleFonts.roboto(
                                                          color: Colors.black87,
                                                          fontSize: 14,
                                                          fontWeight:
                                                              FontWeight.w700),
                                                    ),
                                                    Container(
                                                      margin: EdgeInsets.only(
                                                          left: 5),
                                                      child: Text(
                                                        "(" +
                                                            donorPercentage
                                                                .toStringAsFixed(
                                                                    2) +
                                                            " % " +
                                                            ")",
                                                        style:
                                                            GoogleFonts.roboto(
                                                                color: Colors
                                                                    .red[900],
                                                                fontSize: 14,
                                                                fontWeight:
                                                                    FontWeight
                                                                        .w300),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            ],
                                          ),
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ],
                        ),
                      );
                    },
                  ),
                ),
              ],
            ),

            //Registered and Tagged Count
            Row(
              children: [
                //Registered
                Container(
                  margin: EdgeInsets.only(top: 50, left: 50),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    color: Colors.white,
                  ),
                  child: FutureBuilder(
                    future: getTotalUsers(),
                    builder: (context, snapshot) {
                      if (snapshot.connectionState != ConnectionState.done) {
                        return Text("");
                      }
                      return Container(
                        width: MediaQuery.of(context).size.width * 0.355,
                        padding: EdgeInsets.all(20),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(20)),
                        child: Row(
                          children: [
                            //Image
                            Container(
                              margin: EdgeInsets.only(left: 100),
                              child: Image.asset(
                                'assets/admin_registered.png',
                                height: 100,
                              ),
                            ),
                            Column(
                              children: [
                                //Registered Count
                                Container(
                                  height: 150,
                                  margin: EdgeInsets.only(left: 50, top: 20),
                                  child: SelectableText.rich(
                                    TextSpan(
                                      children: [
                                        TextSpan(
                                          text: totalRegisteredUsers.toString(),
                                          style: GoogleFonts.roboto(
                                              color: Colors.indigo[600],
                                              fontWeight: FontWeight.bold,
                                              fontSize: 80),
                                        ),
                                        TextSpan(
                                          text: '\nRegistered',
                                          style: GoogleFonts.roboto(
                                              color: Colors.grey, fontSize: 20),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Container(
                                  margin: EdgeInsets.only(top: 20),
                                  child: Text(
                                    'Registered Users Overview',
                                    style: GoogleFonts.roboto(
                                      fontSize: 18,
                                      fontWeight: FontWeight.w600,
                                    ),
                                  ),
                                ),
                                //Chart
                                Container(
                                  margin: EdgeInsets.only(
                                    top: 50,
                                    left: 30,
                                  ),
                                  width: 250,
                                  height: 220,
                                  decoration: BoxDecoration(
                                      color: Colors.red[50],
                                      borderRadius: BorderRadius.circular(5)),
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.end,
                                    children: [
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceEvenly,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.end,
                                        children: [
                                          Column(
                                            children: [
                                              Text(
                                                "(" +
                                                    donorRegisteredPercentage
                                                        .toStringAsFixed(2) +
                                                    "%)",
                                                style: GoogleFonts.roboto(
                                                    color: Colors.red[900],
                                                    fontSize: 14,
                                                    fontWeight:
                                                        FontWeight.w300),
                                              ),
                                              Text(
                                                  initialTotalDonorCount
                                                      .toString(),
                                                  style: GoogleFonts.roboto(
                                                      color: Colors.black87,
                                                      fontSize: 14,
                                                      fontWeight:
                                                          FontWeight.w700)),
                                              Container(
                                                width: 50,
                                                height: double.parse(
                                                        initialTotalDonorCount
                                                            .toString()) *
                                                    10,
                                                color: Colors.green,
                                              ),
                                            ],
                                          ),
                                          Column(
                                            children: [
                                              Text(
                                                "(" +
                                                    patientRegisteredPercentage
                                                        .toStringAsFixed(2) +
                                                    "%)",
                                                style: GoogleFonts.roboto(
                                                    color: Colors.red[900],
                                                    fontSize: 14,
                                                    fontWeight:
                                                        FontWeight.w300),
                                              ),
                                              Text(
                                                  initialTotalPatientCount
                                                      .toString(),
                                                  style: GoogleFonts.roboto(
                                                      color: Colors.black87,
                                                      fontSize: 14,
                                                      fontWeight:
                                                          FontWeight.w700)),
                                              Container(
                                                width: 50,
                                                height: double.parse(
                                                        initialTotalPatientCount
                                                            .toString()) *
                                                    10,
                                                color: Colors.blue,
                                              ),
                                            ],
                                          )
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                                Container(
                                  width: 250,
                                  margin: EdgeInsets.only(
                                      top: 10, bottom: 40, left: 30),
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceEvenly,
                                    children: [
                                      Text(
                                        'Donor',
                                        style: GoogleFonts.roboto(
                                          color: Colors.grey,
                                          fontSize: 14,
                                        ),
                                      ),
                                      Text(
                                        '  Patient',
                                        style: GoogleFonts.roboto(
                                          color: Colors.grey,
                                          fontSize: 14,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      );
                    },
                  ),
                ),
                //Tagged Count
                Container(
                  margin: EdgeInsets.only(top: 50, left: 50),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    color: Colors.white,
                  ),
                  child: FutureBuilder(
                    future: getTotalUsers(),
                    builder: (context, snapshot) {
                      if (snapshot.connectionState != ConnectionState.done) {
                        return Text("");
                      }
                      return Container(
                        width: MediaQuery.of(context).size.width * 0.355,
                        padding: EdgeInsets.all(20),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(20)),
                        child: Row(
                          children: [
                            //Image
                            Container(
                              margin: EdgeInsets.only(left: 100),
                              child: Image.asset(
                                'assets/admin_add.png',
                                height: 100,
                              ),
                            ),
                            Column(
                              children: [
                                //Tagged Count
                                Container(
                                  height: 150,
                                  margin: EdgeInsets.only(left: 50, top: 20),
                                  child: SelectableText.rich(
                                    TextSpan(
                                      children: [
                                        TextSpan(
                                          text: totalTaggedUsers.toString(),
                                          style: GoogleFonts.roboto(
                                              color: Colors.indigo[600],
                                              fontWeight: FontWeight.bold,
                                              fontSize: 80),
                                        ),
                                        TextSpan(
                                          text: '\nTagged',
                                          style: GoogleFonts.roboto(
                                              color: Colors.grey, fontSize: 20),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Container(
                                  margin: EdgeInsets.only(top: 20),
                                  child: Text(
                                    'Tagged Users Overview',
                                    style: GoogleFonts.roboto(
                                      fontSize: 18,
                                      fontWeight: FontWeight.w600,
                                    ),
                                  ),
                                ),
                                //Chart
                                Container(
                                  margin: EdgeInsets.only(
                                    top: 50,
                                    left: 30,
                                  ),
                                  width: 250,
                                  height: 220,
                                  decoration: BoxDecoration(
                                      color: Colors.red[50],
                                      borderRadius: BorderRadius.circular(5)),
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.end,
                                    children: [
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceEvenly,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.end,
                                        children: [
                                          Column(
                                            children: [
                                              Text(
                                                "(" +
                                                    donorTaggedPercentage
                                                        .toStringAsFixed(2) +
                                                    "%)",
                                                style: GoogleFonts.roboto(
                                                    color: Colors.red[900],
                                                    fontSize: 14,
                                                    fontWeight:
                                                        FontWeight.w300),
                                              ),
                                              Text(
                                                  initialTotalTaggedDonorCount
                                                      .toString(),
                                                  style: GoogleFonts.roboto(
                                                      color: Colors.black87,
                                                      fontSize: 14,
                                                      fontWeight:
                                                          FontWeight.w700)),
                                              Container(
                                                width: 50,
                                                height: double.parse(
                                                        initialTotalTaggedDonorCount
                                                            .toString()) *
                                                    20,
                                                color: Colors.purple,
                                              ),
                                            ],
                                          ),
                                          Column(
                                            children: [
                                              Text(
                                                "(" +
                                                    patientTaggedPercentage
                                                        .toStringAsFixed(2) +
                                                    "%)",
                                                style: GoogleFonts.roboto(
                                                    color: Colors.red[900],
                                                    fontSize: 14,
                                                    fontWeight:
                                                        FontWeight.w300),
                                              ),
                                              Text(
                                                  initialTotalTaggedPatientCount
                                                      .toString(),
                                                  style: GoogleFonts.roboto(
                                                      color: Colors.black87,
                                                      fontSize: 14,
                                                      fontWeight:
                                                          FontWeight.w700)),
                                              Container(
                                                width: 50,
                                                height: double.parse(
                                                        initialTotalTaggedPatientCount
                                                            .toString()) *
                                                    10,
                                                color: Colors.blue[200],
                                              ),
                                            ],
                                          )
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                                Container(
                                  width: 250,
                                  margin: EdgeInsets.only(
                                      top: 10, bottom: 40, left: 30),
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceEvenly,
                                    children: [
                                      Text(
                                        'Donor',
                                        style: GoogleFonts.roboto(
                                          color: Colors.grey,
                                          fontSize: 14,
                                        ),
                                      ),
                                      Text(
                                        '  Patient',
                                        style: GoogleFonts.roboto(
                                          color: Colors.grey,
                                          fontSize: 14,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      );
                    },
                  ),
                ),
              ],
            ),

            //Donor and Patient Count
            Row(
              children: [
                //Donor
                Container(
                  margin: EdgeInsets.only(top: 50, left: 50),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    color: Colors.white,
                  ),
                  child: FutureBuilder(
                    future: getTotalUsers(),
                    builder: (context, snapshot) {
                      if (snapshot.connectionState != ConnectionState.done) {
                        return Text("");
                      }
                      return Container(
                        width: MediaQuery.of(context).size.width * 0.355,
                        padding: EdgeInsets.all(20),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(20)),
                        child: Row(
                          children: [
                            //Image
                            Container(
                              margin: EdgeInsets.only(left: 100),
                              child: Image.asset(
                                'assets/admin_donor.png',
                                height: 100,
                              ),
                            ),
                            Column(
                              children: [
                                //Donor Count
                                Container(
                                  height: 150,
                                  margin: EdgeInsets.only(left: 80, top: 20),
                                  child: SelectableText.rich(
                                    TextSpan(
                                      children: [
                                        TextSpan(
                                          text: totalDonorCount.toString(),
                                          style: GoogleFonts.roboto(
                                              color: Colors.indigo[600],
                                              fontWeight: FontWeight.bold,
                                              fontSize: 80),
                                        ),
                                        TextSpan(
                                          text: '\nDonors',
                                          style: GoogleFonts.roboto(
                                              color: Colors.grey, fontSize: 20),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Container(
                                  margin: EdgeInsets.only(top: 20),
                                  child: Text(
                                    'Donor Registered Overview',
                                    style: GoogleFonts.roboto(
                                      fontSize: 18,
                                      fontWeight: FontWeight.w600,
                                    ),
                                  ),
                                ),
                                //Chart
                                Container(
                                  margin: EdgeInsets.only(
                                    top: 50,
                                    left: 30,
                                  ),
                                  width: 250,
                                  height: 220,
                                  decoration: BoxDecoration(
                                      color: Colors.red[50],
                                      borderRadius: BorderRadius.circular(5)),
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.end,
                                    children: [
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceEvenly,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.end,
                                        children: [
                                          //Total Users Count
                                          Column(
                                            children: [
                                              Text(
                                                "(100%)",
                                                style: GoogleFonts.roboto(
                                                    color: Colors.red[900],
                                                    fontSize: 14,
                                                    fontWeight:
                                                        FontWeight.w300),
                                              ),
                                              Text(totalUsers.toString(),
                                                  style: GoogleFonts.roboto(
                                                      color: Colors.black87,
                                                      fontSize: 14,
                                                      fontWeight:
                                                          FontWeight.w700)),
                                              Container(
                                                width: 50,
                                                height: double.parse(
                                                        totalUsers.toString()) *
                                                    10,
                                                color: Colors.orange,
                                              ),
                                            ],
                                          ),
                                          // Total Registered
                                          Column(
                                            children: [
                                              Text(
                                                "(" +
                                                    totalRegisteredUsersPercentage
                                                        .toStringAsFixed(2) +
                                                    "%)",
                                                style: GoogleFonts.roboto(
                                                    color: Colors.red[900],
                                                    fontSize: 14,
                                                    fontWeight:
                                                        FontWeight.w300),
                                              ),
                                              Text(
                                                  totalRegisteredUsers
                                                      .toString(),
                                                  style: GoogleFonts.roboto(
                                                      color: Colors.black87,
                                                      fontSize: 14,
                                                      fontWeight:
                                                          FontWeight.w700)),
                                              Container(
                                                width: 50,
                                                height: double.parse(
                                                        totalRegisteredUsers
                                                            .toString()) *
                                                    10,
                                                color: Colors.yellow,
                                              ),
                                            ],
                                          ),
                                          //Donor Count
                                          Column(
                                            children: [
                                              Text(
                                                "(" +
                                                    donorPercentage
                                                        .toStringAsFixed(2) +
                                                    "%)",
                                                style: GoogleFonts.roboto(
                                                    color: Colors.red[900],
                                                    fontSize: 14,
                                                    fontWeight:
                                                        FontWeight.w300),
                                              ),
                                              Text(
                                                  initialTotalDonorCount
                                                      .toString(),
                                                  style: GoogleFonts.roboto(
                                                      color: Colors.black87,
                                                      fontSize: 14,
                                                      fontWeight:
                                                          FontWeight.w700)),
                                              Container(
                                                width: 50,
                                                height: double.parse(
                                                        initialTotalDonorCount
                                                            .toString()) *
                                                    10,
                                                color: Colors.blue,
                                              ),
                                            ],
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                                Container(
                                  width: 250,
                                  margin: EdgeInsets.only(
                                      top: 10, bottom: 40, left: 30),
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceEvenly,
                                    children: [
                                      Text(
                                        'Total\nUsers',
                                        style: GoogleFonts.roboto(
                                            color: Colors.grey,
                                            fontSize: 14,
                                            height: 1.3),
                                      ),
                                      Text(
                                        'Registered',
                                        style: GoogleFonts.roboto(
                                          color: Colors.grey,
                                          fontSize: 14,
                                        ),
                                      ),
                                      Text(
                                        'Donor',
                                        style: GoogleFonts.roboto(
                                          color: Colors.grey,
                                          fontSize: 14,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      );
                    },
                  ),
                ),
                //Patient Count
                Container(
                  margin: EdgeInsets.only(top: 50, left: 50),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    color: Colors.white,
                  ),
                  child: FutureBuilder(
                    future: getTotalUsers(),
                    builder: (context, snapshot) {
                      if (snapshot.connectionState != ConnectionState.done) {
                        return Text("");
                      }
                      return Container(
                        width: MediaQuery.of(context).size.width * 0.355,
                        padding: EdgeInsets.all(20),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(20)),
                        child: Row(
                          children: [
                            //Image
                            Container(
                              margin: EdgeInsets.only(left: 100),
                              child: Image.asset(
                                'assets/admin_patient.png',
                                height: 100,
                              ),
                            ),
                            Column(
                              children: [
                                //Patient Count
                                Container(
                                  height: 150,
                                  margin: EdgeInsets.only(left: 80, top: 20),
                                  child: SelectableText.rich(
                                    TextSpan(
                                      children: [
                                        TextSpan(
                                          text: initialTotalPatientCount
                                              .toString(),
                                          style: GoogleFonts.roboto(
                                              color: Colors.indigo[600],
                                              fontWeight: FontWeight.bold,
                                              fontSize: 80),
                                        ),
                                        TextSpan(
                                          text: '\nPatients',
                                          style: GoogleFonts.roboto(
                                              color: Colors.grey, fontSize: 20),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Container(
                                  margin: EdgeInsets.only(top: 20),
                                  child: Text(
                                    'Patient Registered Overview',
                                    style: GoogleFonts.roboto(
                                      fontSize: 18,
                                      fontWeight: FontWeight.w600,
                                    ),
                                  ),
                                ),
                                //Chart
                                Container(
                                  margin: EdgeInsets.only(
                                    top: 50,
                                    left: 30,
                                  ),
                                  width: 250,
                                  height: 220,
                                  decoration: BoxDecoration(
                                      color: Colors.red[50],
                                      borderRadius: BorderRadius.circular(5)),
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.end,
                                    children: [
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceEvenly,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.end,
                                        children: [
                                          //Total Users Count
                                          Column(
                                            children: [
                                              Text(
                                                "(100%)",
                                                style: GoogleFonts.roboto(
                                                    color: Colors.red[900],
                                                    fontSize: 14,
                                                    fontWeight:
                                                        FontWeight.w300),
                                              ),
                                              Text(totalUsers.toString(),
                                                  style: GoogleFonts.roboto(
                                                      color: Colors.black87,
                                                      fontSize: 14,
                                                      fontWeight:
                                                          FontWeight.w700)),
                                              Container(
                                                width: 50,
                                                height: double.parse(
                                                        totalUsers.toString()) *
                                                    10,
                                                color: Colors.orange,
                                              ),
                                            ],
                                          ),
                                          // Total Registered
                                          Column(
                                            children: [
                                              Text(
                                                "(" +
                                                    totalRegisteredUsersPercentage
                                                        .toStringAsFixed(2) +
                                                    "%)",
                                                style: GoogleFonts.roboto(
                                                    color: Colors.red[900],
                                                    fontSize: 14,
                                                    fontWeight:
                                                        FontWeight.w300),
                                              ),
                                              Text(
                                                  totalRegisteredUsers
                                                      .toString(),
                                                  style: GoogleFonts.roboto(
                                                      color: Colors.black87,
                                                      fontSize: 14,
                                                      fontWeight:
                                                          FontWeight.w700)),
                                              Container(
                                                width: 50,
                                                height: double.parse(
                                                        totalRegisteredUsers
                                                            .toString()) *
                                                    10,
                                                color: Colors.yellow,
                                              ),
                                            ],
                                          ),
                                          //Patient Count
                                          Column(
                                            children: [
                                              Text(
                                                "(" +
                                                    patientPercentage
                                                        .toStringAsFixed(2) +
                                                    "%)",
                                                style: GoogleFonts.roboto(
                                                    color: Colors.red[900],
                                                    fontSize: 14,
                                                    fontWeight:
                                                        FontWeight.w300),
                                              ),
                                              Text(
                                                  initialTotalPatientCount
                                                      .toString(),
                                                  style: GoogleFonts.roboto(
                                                      color: Colors.black87,
                                                      fontSize: 14,
                                                      fontWeight:
                                                          FontWeight.w700)),
                                              Container(
                                                width: 50,
                                                height: double.parse(
                                                        initialTotalPatientCount
                                                            .toString()) *
                                                    10,
                                                color: Colors.green,
                                              ),
                                            ],
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                                Container(
                                  width: 250,
                                  margin: EdgeInsets.only(
                                      top: 10, bottom: 40, left: 30),
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceEvenly,
                                    children: [
                                      Text(
                                        'Total\nUsers',
                                        style: GoogleFonts.roboto(
                                            color: Colors.grey,
                                            fontSize: 14,
                                            height: 1.3),
                                      ),
                                      Text(
                                        'Registered',
                                        style: GoogleFonts.roboto(
                                          color: Colors.grey,
                                          fontSize: 14,
                                        ),
                                      ),
                                      Text(
                                        'Patient',
                                        style: GoogleFonts.roboto(
                                          color: Colors.grey,
                                          fontSize: 14,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      );
                    },
                  ),
                ),
              ],
            ),

            //TaggedDonor and Patient Count
            Row(
              children: [
                //Tagged Donor
                Container(
                  margin: EdgeInsets.only(top: 50, left: 50),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    color: Colors.white,
                  ),
                  child: FutureBuilder(
                    future: getTotalUsers(),
                    builder: (context, snapshot) {
                      if (snapshot.connectionState != ConnectionState.done) {
                        return Text("");
                      }
                      return Container(
                        width: MediaQuery.of(context).size.width * 0.355,
                        padding: EdgeInsets.all(20),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(20)),
                        child: Row(
                          children: [
                            //Image
                            Container(
                              margin: EdgeInsets.only(left: 60),
                              child: Image.asset(
                                'assets/admin_tagdonor.png',
                                height: 100,
                              ),
                            ),
                            Column(
                              children: [
                                //Tagged Donor Count
                                Container(
                                  height: 150,
                                  margin: EdgeInsets.only(left: 40, top: 20),
                                  child: SelectableText.rich(
                                    TextSpan(
                                      children: [
                                        TextSpan(
                                          text: initialTotalTaggedDonorCount
                                              .toString(),
                                          style: GoogleFonts.roboto(
                                              color: Colors.indigo[600],
                                              fontWeight: FontWeight.bold,
                                              fontSize: 80),
                                        ),
                                        TextSpan(
                                          text: '\nTagged Donors',
                                          style: GoogleFonts.roboto(
                                              color: Colors.grey, fontSize: 20),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Container(
                                  margin: EdgeInsets.only(top: 20),
                                  child: Text(
                                    'Donor Tagged Overview',
                                    style: GoogleFonts.roboto(
                                      fontSize: 18,
                                      fontWeight: FontWeight.w600,
                                    ),
                                  ),
                                ),
                                //Chart
                                Container(
                                  margin: EdgeInsets.only(
                                    top: 50,
                                    left: 30,
                                  ),
                                  width: 250,
                                  height: 220,
                                  decoration: BoxDecoration(
                                      color: Colors.red[50],
                                      borderRadius: BorderRadius.circular(5)),
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.end,
                                    children: [
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceEvenly,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.end,
                                        children: [
                                          //Total Users Count
                                          Column(
                                            children: [
                                              Text(
                                                "(100%)",
                                                style: GoogleFonts.roboto(
                                                    color: Colors.red[900],
                                                    fontSize: 14,
                                                    fontWeight:
                                                        FontWeight.w300),
                                              ),
                                              Text(totalUsers.toString(),
                                                  style: GoogleFonts.roboto(
                                                      color: Colors.black87,
                                                      fontSize: 14,
                                                      fontWeight:
                                                          FontWeight.w700)),
                                              Container(
                                                width: 50,
                                                height: double.parse(
                                                        totalUsers.toString()) *
                                                    10,
                                                color: Colors.red,
                                              ),
                                            ],
                                          ),
                                          // Total Tagged
                                          Column(
                                            children: [
                                              Text(
                                                "(" +
                                                    totalTaggedUsersPercentage
                                                        .toStringAsFixed(2) +
                                                    "%)",
                                                style: GoogleFonts.roboto(
                                                    color: Colors.red[900],
                                                    fontSize: 14,
                                                    fontWeight:
                                                        FontWeight.w300),
                                              ),
                                              Text(totalTaggedUsers.toString(),
                                                  style: GoogleFonts.roboto(
                                                      color: Colors.black87,
                                                      fontSize: 14,
                                                      fontWeight:
                                                          FontWeight.w700)),
                                              Container(
                                                width: 50,
                                                height: double.parse(
                                                        totalTaggedUsers
                                                            .toString()) *
                                                    10,
                                                color: Colors.orange,
                                              ),
                                            ],
                                          ),
                                          //Donor Tagged Count
                                          Column(
                                            children: [
                                              Text(
                                                "(" +
                                                    donorTaggedPercentage
                                                        .toStringAsFixed(2) +
                                                    "%)",
                                                style: GoogleFonts.roboto(
                                                    color: Colors.red[900],
                                                    fontSize: 14,
                                                    fontWeight:
                                                        FontWeight.w300),
                                              ),
                                              Text(
                                                  initialTotalTaggedDonorCount
                                                      .toString(),
                                                  style: GoogleFonts.roboto(
                                                      color: Colors.black87,
                                                      fontSize: 14,
                                                      fontWeight:
                                                          FontWeight.w700)),
                                              Container(
                                                width: 50,
                                                height: double.parse(
                                                        initialTotalTaggedDonorCount
                                                            .toString()) *
                                                    10,
                                                color: Colors.green,
                                              ),
                                            ],
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                                Container(
                                  width: 250,
                                  margin: EdgeInsets.only(
                                      top: 10, bottom: 40, left: 30),
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceEvenly,
                                    children: [
                                      Text(
                                        'Total\nUsers',
                                        style: GoogleFonts.roboto(
                                            color: Colors.grey,
                                            fontSize: 14,
                                            height: 1.3),
                                      ),
                                      Text(
                                        'Tagged',
                                        style: GoogleFonts.roboto(
                                          color: Colors.grey,
                                          fontSize: 14,
                                        ),
                                      ),
                                      Text(
                                        'Tagged\nDonor',
                                        style: GoogleFonts.roboto(
                                          color: Colors.grey,
                                          fontSize: 14,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      );
                    },
                  ),
                ),
                //Tagged Patient Count
                Container(
                  margin: EdgeInsets.only(top: 50, left: 50),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    color: Colors.white,
                  ),
                  child: FutureBuilder(
                    future: getTotalUsers(),
                    builder: (context, snapshot) {
                      if (snapshot.connectionState != ConnectionState.done) {
                        return Text("");
                      }
                      return Container(
                        width: MediaQuery.of(context).size.width * 0.355,
                        padding: EdgeInsets.all(20),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(20)),
                        child: Row(
                          children: [
                            //Image
                            Container(
                              margin: EdgeInsets.only(left: 60),
                              child: Image.asset(
                                'assets/admin_tagpatient.png',
                                height: 100,
                              ),
                            ),
                            Column(
                              children: [
                                //Tagged Patient Count
                                Container(
                                  height: 150,
                                  margin: EdgeInsets.only(left: 40, top: 20),
                                  child: SelectableText.rich(
                                    TextSpan(
                                      children: [
                                        TextSpan(
                                          text: initialTotalTaggedPatientCount
                                              .toString(),
                                          style: GoogleFonts.roboto(
                                              color: Colors.indigo[600],
                                              fontWeight: FontWeight.bold,
                                              fontSize: 80),
                                        ),
                                        TextSpan(
                                          text: '\nTagged Patients',
                                          style: GoogleFonts.roboto(
                                              color: Colors.grey, fontSize: 20),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Container(
                                  margin: EdgeInsets.only(top: 20),
                                  child: Text(
                                    'Patient Tagged Overview',
                                    style: GoogleFonts.roboto(
                                      fontSize: 18,
                                      fontWeight: FontWeight.w600,
                                    ),
                                  ),
                                ),
                                //Chart
                                Container(
                                  margin: EdgeInsets.only(
                                    top: 50,
                                    left: 30,
                                  ),
                                  width: 250,
                                  height: 220,
                                  decoration: BoxDecoration(
                                      color: Colors.red[50],
                                      borderRadius: BorderRadius.circular(5)),
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.end,
                                    children: [
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceEvenly,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.end,
                                        children: [
                                          //Total Users Count
                                          Column(
                                            children: [
                                              Text(
                                                "(100%)",
                                                style: GoogleFonts.roboto(
                                                    color: Colors.red[900],
                                                    fontSize: 14,
                                                    fontWeight:
                                                        FontWeight.w300),
                                              ),
                                              Text(totalUsers.toString(),
                                                  style: GoogleFonts.roboto(
                                                      color: Colors.black87,
                                                      fontSize: 14,
                                                      fontWeight:
                                                          FontWeight.w700)),
                                              Container(
                                                width: 50,
                                                height: double.parse(
                                                        totalUsers.toString()) *
                                                    10,
                                                color: Colors.red,
                                              ),
                                            ],
                                          ),
                                          // Total Tagged
                                          Column(
                                            children: [
                                              Text(
                                                "(" +
                                                    totalTaggedUsersPercentage
                                                        .toStringAsFixed(2) +
                                                    "%)",
                                                style: GoogleFonts.roboto(
                                                    color: Colors.red[900],
                                                    fontSize: 14,
                                                    fontWeight:
                                                        FontWeight.w300),
                                              ),
                                              Text(totalTaggedUsers.toString(),
                                                  style: GoogleFonts.roboto(
                                                      color: Colors.black87,
                                                      fontSize: 14,
                                                      fontWeight:
                                                          FontWeight.w700)),
                                              Container(
                                                  width: 50,
                                                  height: double.parse(
                                                          totalTaggedUsers
                                                              .toString()) *
                                                      10,
                                                  color: Colors.orange),
                                            ],
                                          ),
                                          //Donor Tagged Count
                                          Column(
                                            children: [
                                              Text(
                                                "(" +
                                                    patientTaggedPercentage
                                                        .toStringAsFixed(2) +
                                                    "%)",
                                                style: GoogleFonts.roboto(
                                                    color: Colors.red[900],
                                                    fontSize: 14,
                                                    fontWeight:
                                                        FontWeight.w300),
                                              ),
                                              Text(
                                                  initialTotalTaggedPatientCount
                                                      .toString(),
                                                  style: GoogleFonts.roboto(
                                                      color: Colors.black87,
                                                      fontSize: 14,
                                                      fontWeight:
                                                          FontWeight.w700)),
                                              Container(
                                                width: 50,
                                                height: double.parse(
                                                        initialTotalTaggedPatientCount
                                                            .toString()) *
                                                    10,
                                                color: Colors.purple,
                                              ),
                                            ],
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                                Container(
                                  width: 250,
                                  margin: EdgeInsets.only(
                                      top: 10, bottom: 40, left: 30),
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceEvenly,
                                    children: [
                                      Text(
                                        'Total\nUsers',
                                        style: GoogleFonts.roboto(
                                            color: Colors.grey,
                                            fontSize: 14,
                                            height: 1.3),
                                      ),
                                      Text(
                                        'Tagged',
                                        style: GoogleFonts.roboto(
                                          color: Colors.grey,
                                          fontSize: 14,
                                        ),
                                      ),
                                      Text(
                                        'Tagged\nPatient',
                                        style: GoogleFonts.roboto(
                                          color: Colors.grey,
                                          fontSize: 14,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      );
                    },
                  ),
                ),
              ],
            ),

            //Matches and Posts
            Row(
              children: [
                //Matches
                Container(
                  margin: EdgeInsets.only(top: 50, left: 50),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    color: Colors.white,
                  ),
                  child: FutureBuilder(
                    future: getKidneyMatchDocument(),
                    builder: (context, snapshot) {
                      if (snapshot.connectionState != ConnectionState.done)
                        return Text("");
                      return Container(
                        width: MediaQuery.of(context).size.width * 0.355,
                        padding: EdgeInsets.all(20),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(20)),
                        child: Row(
                          children: [
                            //Image
                            Container(
                              margin: EdgeInsets.only(left: 100),
                              child: Image.asset(
                                'assets/admin_match.png',
                                height: 100,
                              ),
                            ),
                            Column(
                              children: [
                                //Matches Count
                                Container(
                                  height: 150,
                                  margin: EdgeInsets.only(left: 80, top: 20),
                                  child: SelectableText.rich(
                                    TextSpan(
                                      children: [
                                        TextSpan(
                                          text: initialKidneyMatchCount
                                              .toString(),
                                          style: GoogleFonts.roboto(
                                              color: Colors.indigo[600],
                                              fontWeight: FontWeight.bold,
                                              fontSize: 80),
                                        ),
                                        TextSpan(
                                          text: '\nMatches',
                                          style: GoogleFonts.roboto(
                                              color: Colors.grey, fontSize: 20),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      );
                    },
                  ),
                ),
                //Posts
                Container(
                  margin: EdgeInsets.only(top: 50, left: 50),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    color: Colors.white,
                  ),
                  child: FutureBuilder(
                    future: getPostsDocument(),
                    builder: (context, snapshot) {
                      if (snapshot.connectionState != ConnectionState.done)
                        return Text("");
                      return Container(
                        width: MediaQuery.of(context).size.width * 0.355,
                        padding: EdgeInsets.all(20),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(20)),
                        child: Row(
                          children: [
                            //Image
                            Container(
                              margin: EdgeInsets.only(left: 100),
                              child: Image.asset(
                                'assets/admin_post.png',
                                height: 100,
                              ),
                            ),
                            Column(
                              children: [
                                //Posts Count
                                Container(
                                  height: 150,
                                  margin: EdgeInsets.only(left: 80, top: 20),
                                  child: SelectableText.rich(
                                    TextSpan(
                                      children: [
                                        TextSpan(
                                          text: initialPostsCount.toString(),
                                          style: GoogleFonts.roboto(
                                              color: Colors.indigo[600],
                                              fontWeight: FontWeight.bold,
                                              fontSize: 80),
                                        ),
                                        TextSpan(
                                          text: '\nPosts',
                                          style: GoogleFonts.roboto(
                                              color: Colors.grey, fontSize: 20),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      );
                    },
                  ),
                ),
              ],
            ),

            Row(
              children: [
                Container(
                  margin: EdgeInsets.only(top: 50, left: 50, bottom: 150),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    color: Colors.white,
                  ),
                  child: Container(
                    height: MediaQuery.of(context).size.height * 0.7,
                    width: MediaQuery.of(context).size.width * 0.736,
                    child: Container(
                      margin: EdgeInsets.only(top: 50, left: 50, right: 50),
                      child: Stack(
                        children: [
                          Text(
                            'Users Feedback Overview',
                            style: GoogleFonts.roboto(
                                color: Colors.indigo[600],
                                fontSize: 40,
                                fontWeight: FontWeight.w600),
                          ),
                          //Contents
                          Container(
                            margin: EdgeInsets.only(top: 100),
                            child: FutureBuilder(
                              future: getFeedbacksData(),
                              builder: (context, AsyncSnapshot snapshot) {
                                if (snapshot.hasData) {
                                  return ListView.builder(
                                    itemCount: snapshot.data!.length,
                                    itemBuilder: (context, index) {
                                      DocumentSnapshot userData =
                                          snapshot.data[index];

                                      return Stack(
                                        children: [
                                          Container(
                                            decoration: BoxDecoration(
                                                color: Colors.blue[50],
                                                borderRadius:
                                                    BorderRadius.circular(5)),
                                            margin: EdgeInsets.only(
                                                top: 10, left: 10, right: 10),
                                            child: ListTile(
                                              title: Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: [
                                                  //Profile
                                                  Row(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment
                                                            .spaceBetween,
                                                    children: [
                                                      Container(
                                                        margin: EdgeInsets.only(
                                                            top: 20, left: 50),
                                                        child: Row(
                                                          children: [
                                                            Image.asset(
                                                              'assets/user.png',
                                                              height: 30,
                                                            ),
                                                            Container(
                                                              margin: EdgeInsets
                                                                  .only(
                                                                      left: 20),
                                                              child: Text(
                                                                userData[
                                                                    "UserName"],
                                                                style: GoogleFonts.roboto(
                                                                    color: Colors
                                                                        .black87,
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .w400,
                                                                    fontSize:
                                                                        16),
                                                              ),
                                                            ),
                                                          ],
                                                        ),
                                                      ),
                                                      InkWell(
                                                        onTap: () {
                                                          getUserID = userData[
                                                              "UserID"];
                                                          getUserName =
                                                              userData[
                                                                  "UserName"];
                                                          showModalBottomSheet(
                                                            context: context,
                                                            builder: (context) =>
                                                                buildCurrentUserSheet(),
                                                          );
                                                        },
                                                        child: Icon(Icons
                                                            .more_horiz_outlined),
                                                      )
                                                    ],
                                                  ),
                                                  //Rating and Date
                                                  Container(
                                                    margin: EdgeInsets.only(
                                                        top: 20, left: 50),
                                                    child: Row(
                                                      children: [
                                                        RatingBar.builder(
                                                          initialRating: double
                                                              .parse(userData[
                                                                  "Rating"]),
                                                          minRating: 1,
                                                          itemSize: 30,
                                                          itemPadding: EdgeInsets
                                                              .symmetric(
                                                                  horizontal:
                                                                      4),
                                                          itemBuilder:
                                                              (context, _) =>
                                                                  Icon(
                                                            Icons
                                                                .stars_outlined,
                                                            color: Colors.green,
                                                          ),
                                                          updateOnDrag: true,
                                                          onRatingUpdate:
                                                              (rating) =>
                                                                  setState(() {
                                                            this.rating = double
                                                                .parse(userData[
                                                                    "Rating"]);
                                                          }),
                                                        ),
                                                        Container(
                                                          margin:
                                                              EdgeInsets.only(
                                                                  left: 10),
                                                          child: Text(
                                                            userData[
                                                                "Feedback Date"],
                                                            style: GoogleFonts.roboto(
                                                                color: Colors
                                                                    .black87,
                                                                fontWeight:
                                                                    FontWeight
                                                                        .w400,
                                                                fontSize: 16),
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                                  ),
                                                  Container(
                                                    margin: EdgeInsets.only(
                                                        top: 20,
                                                        left: 50,
                                                        bottom: 20),
                                                    child: Text(
                                                      userData["Feedback"],
                                                      style: GoogleFonts.roboto(
                                                          color: Colors.black87,
                                                          fontWeight:
                                                              FontWeight.w400,
                                                          fontSize: 16,
                                                          height: 1.5),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ),
                                        ],
                                      );
                                    },
                                  );
                                } else {
                                  return Container(
                                    alignment: Alignment.center,
                                    child: CircularProgressIndicator(),
                                  );
                                }
                              },
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  Widget buildCurrentUserSheet() => Container(
        height: 70,
        child: ListView(
          children: [
            //Delete
            ListTile(
              title: Row(
                children: [
                  Text(
                    'Delete Feedback',
                    style: GoogleFonts.roboto(
                        color: Colors.black87,
                        fontSize: 20,
                        fontWeight: FontWeight.w400),
                  ),
                  Container(
                    margin: EdgeInsets.only(left: 10),
                    child: Icon(
                      Icons.delete_outline,
                      color: Colors.red[100],
                    ),
                  ),
                ],
              ),
              onTap: () {
                showCustomDialog(context);
              },
            ),
          ],
        ),
      );

  //Alert Dialog For Delete
  void showCustomDialog(BuildContext context) => showDialog(
      context: context,
      builder: (BuildContext context) {
        return Dialog(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20.0)), //this right here
          child: Container(
            height: 200,
            width: 100,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  "Delete Feedback",
                  style: GoogleFonts.roboto(
                      color: Colors.black87,
                      fontWeight: FontWeight.bold,
                      fontSize: 18),
                ),
                Text(
                  "\nAre you sure you want to delete?",
                  style:
                      GoogleFonts.roboto(color: Colors.grey[500], fontSize: 14),
                ),
                Container(
                  margin: EdgeInsets.only(top: 40),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Container(
                        child: InkWell(
                          onTap: () {
                            Navigator.of(context).pop();
                          },
                          child: Container(
                            height: 35,
                            width: 100,
                            decoration: BoxDecoration(
                                color: Colors.red[50],
                                borderRadius: BorderRadius.circular(18)),
                            child: Center(
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Text(
                                  "Cancel",
                                  style: TextStyle(
                                      color: Colors.grey[600], fontSize: 17),
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                      Container(
                        child: InkWell(
                          onTap: () {
                            setState(() {
                              deleteFeedbacks();
                              Navigator.of(context).pop();
                              Navigator.of(context).pop();
                              showDeleteToast();
                              logAdminDeleteFeedback();
                            });
                          },
                          child: Container(
                            height: 35,
                            width: 100,
                            decoration: BoxDecoration(
                                color: Colors.red[200],
                                borderRadius: BorderRadius.circular(18)),
                            child: Center(
                              child: Text(
                                "Confirm",
                                style: TextStyle(
                                    color: Colors.red[400], fontSize: 17),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        );
      });

  //Show Delete Succes
  void showDeleteToast() =>
      toast.showToast(child: buildDeleteToast(), gravity: ToastGravity.CENTER);

  Widget buildDeleteToast() => Container(
        padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
        decoration: BoxDecoration(
          color: Colors.green[300],
          borderRadius: BorderRadius.circular(5),
        ),
        child: Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            Text(
              "Feedback removed!",
              style: TextStyle(color: Colors.black87, fontSize: 16),
            ),
          ],
        ),
      );

  //All Users(Registered and Tagged)
  getTotalUsers() async {
    //Normal Donor Document
    await FirebaseFirestore.instance
        .collection('Users')
        .doc('Donor')
        .collection('Lists')
        .get()
        .then((myDocuments) {
      print("${myDocuments.docs.length}");
      int totalDonorCount = myDocuments.docs.length;
      initialTotalDonorCount = totalDonorCount;
    });

    //Normal Patient Document
    await FirebaseFirestore.instance
        .collection('Users')
        .doc('Patient')
        .collection('Lists')
        .get()
        .then((myDocuments) {
      print("${myDocuments.docs.length}");
      int totalPatientCount = myDocuments.docs.length;
      initialTotalPatientCount = totalPatientCount;
    });

    //Archived Donor Document
    await FirebaseFirestore.instance
        .collection('Archived')
        .doc('Users')
        .collection('Registered')
        .doc('Donor')
        .collection('Lists')
        .get()
        .then((myDocuments) {
      print("${myDocuments.docs.length}");
      int archivedTotalDonorCount = myDocuments.docs.length;
      initialTotalArchivedDonorCount = archivedTotalDonorCount;
    });

    //Archived Patient Document
    await FirebaseFirestore.instance
        .collection('Archived')
        .doc('Users')
        .collection('Registered')
        .doc('Patient')
        .collection('Lists')
        .get()
        .then((myDocuments) {
      print("${myDocuments.docs.length}");
      int archivedTotalPatientCount = myDocuments.docs.length;
      initialTotalArchivedPatientCount = archivedTotalPatientCount;
    });

    //Normal Tagged Donor Document
    await FirebaseFirestore.instance
        .collection('Tagged Users')
        .doc('Donor')
        .collection('Lists')
        .get()
        .then((myDocuments) {
      print("${myDocuments.docs.length}");
      int totalTaggedDonorCount = myDocuments.docs.length;
      initialTotalTaggedDonorCount = totalTaggedDonorCount;
    });

    //Normal Tagged Patient Document
    await FirebaseFirestore.instance
        .collection('Tagged Users')
        .doc('Patient')
        .collection('Lists')
        .get()
        .then((myDocuments) {
      print("${myDocuments.docs.length}");
      int taggedTotalPatientCount = myDocuments.docs.length;
      initialTotalTaggedPatientCount = taggedTotalPatientCount;
    });

    //Archived Tagged Donor Document
    await FirebaseFirestore.instance
        .collection('Archived')
        .doc('Users')
        .collection('Tagged Users')
        .doc('Donor')
        .collection('Lists')
        .get()
        .then((myDocuments) {
      print("${myDocuments.docs.length}");
      int archivedTotalTaggedDonorCount = myDocuments.docs.length;
      initialTotalTaggedArchivedDonorCount = archivedTotalTaggedDonorCount;
    });

    //Archived Tagged Patient Document
    await FirebaseFirestore.instance
        .collection('Archived')
        .doc('Users')
        .collection('Tagged Users')
        .doc('Patient')
        .collection('Lists')
        .get()
        .then((myDocuments) {
      print("${myDocuments.docs.length}");
      int taggedTotalArchivedPatientCount = myDocuments.docs.length;
      initialTotalTaggedArchivedPatientCount = taggedTotalArchivedPatientCount;
    });

    totalUsers = initialTotalDonorCount +
        initialTotalPatientCount +
        initialTotalArchivedDonorCount +
        initialTotalArchivedPatientCount +
        initialTotalTaggedDonorCount +
        initialTotalTaggedPatientCount +
        initialTotalTaggedArchivedDonorCount +
        initialTotalTaggedArchivedPatientCount;

    donorPercentage = (double.parse(initialTotalDonorCount.toString()) /
            double.parse(totalUsers.toString())) *
        100;
    patientPercentage = (double.parse(initialTotalPatientCount.toString()) /
            double.parse(totalUsers.toString())) *
        100;
    donorTaggedPercentage =
        (double.parse(initialTotalTaggedDonorCount.toString()) /
                double.parse(totalUsers.toString())) *
            100;
    patientTaggedPercentage =
        (double.parse(initialTotalTaggedPatientCount.toString()) /
                double.parse(totalUsers.toString())) *
            100;
    donorRegisteredPercentage =
        (double.parse(initialTotalDonorCount.toString()) /
            double.parse(totalRegisteredUsers.toString()) *
            100);
    patientRegisteredPercentage =
        (double.parse(initialTotalPatientCount.toString()) /
            double.parse(totalRegisteredUsers.toString()) *
            100);
    donorTaggedPercentage =
        (double.parse(initialTotalTaggedDonorCount.toString()) /
            double.parse(totalUsers.toString()) *
            100);
    patientTaggedPercentage =
        (double.parse(initialTotalTaggedPatientCount.toString()) /
            double.parse(totalUsers.toString()) *
            100);
    totalRegisteredUsersPercentage =
        (double.parse(totalRegisteredUsers.toString()) /
            double.parse(totalUsers.toString()) *
            100);
    totalTaggedUsersPercentage = (double.parse(totalTaggedUsers.toString()) /
        double.parse(totalUsers.toString()) *
        100);
    //Total Registered Users
    totalRegisteredUsers = initialTotalDonorCount + initialTotalPatientCount;
    //Total Tagged Users
    totalTaggedUsers =
        initialTotalTaggedDonorCount + initialTotalTaggedPatientCount;
  }

  //Display Number Of Donors
  getDonorDocument() async {
    //Normal Document
    await FirebaseFirestore.instance
        .collection('Users')
        .doc('Donor')
        .collection('Lists')
        .get()
        .then((myDocuments) {
      print("${myDocuments.docs.length}");
      int donorCount = myDocuments.docs.length;
      initialDonorCount = donorCount;
    });

    //Archived Document
    await FirebaseFirestore.instance
        .collection('Archived')
        .doc('Users')
        .collection('Registered')
        .doc('Donor')
        .collection('Lists')
        .get()
        .then((myDocuments) {
      print("${myDocuments.docs.length}");
      int donorArchivedCount = myDocuments.docs.length;
      initialArchivedDonorCount = donorArchivedCount;
    });

    totalDonorCount = initialDonorCount + initialArchivedDonorCount;
  }

  //Display Number Of Patients
  getPatientDocument() async {
    await FirebaseFirestore.instance
        .collection('Users')
        .doc('Patient')
        .collection('Lists')
        .get()
        .then((myDocuments) {
      print("${myDocuments.docs.length}");
      int patientCount = myDocuments.docs.length;
      initialPatientCount = patientCount;
    });

    //Archived Document
    await FirebaseFirestore.instance
        .collection('Archived')
        .doc('Users')
        .collection('Registered')
        .doc('Patient')
        .collection('Lists')
        .get()
        .then((myDocuments) {
      print("${myDocuments.docs.length}");
      int patientArchivedCount = myDocuments.docs.length;
      initialArchivedPatientCount = patientArchivedCount;

      totalPatientCount = initialPatientCount + initialArchivedPatientCount;
    });
  }

  //Display Number Of Tagged Donors
  getTaggedDonorDocument() async {
    //Normal Document
    await FirebaseFirestore.instance
        .collection('Tagged Users')
        .doc('Donor')
        .collection('Lists')
        .get()
        .then((myDocuments) {
      print("${myDocuments.docs.length}");
      int donorTaggedCount = myDocuments.docs.length;
      initialTaggedDonorCount = donorTaggedCount;
    });

    //Archived Document
    await FirebaseFirestore.instance
        .collection('Archived')
        .doc('Users')
        .collection('Tagged Users')
        .doc('Donor')
        .collection('Lists')
        .get()
        .then((myDocuments) {
      print("${myDocuments.docs.length}");
      int donorTaggedArchivedCount = myDocuments.docs.length;
      initialTaggedArchivedDonorCount = donorTaggedArchivedCount;
    });

    totalTaggedDonorCount =
        initialTaggedDonorCount + initialTaggedArchivedDonorCount;
  }

  //Display Number Of  Tagged Patients
  getTaggedPatientDocument() async {
    await FirebaseFirestore.instance
        .collection('Tagged Users')
        .doc('Patient')
        .collection('Lists')
        .get()
        .then((myDocuments) {
      print("${myDocuments.docs.length}");
      int patientTaggedCount = myDocuments.docs.length;
      initialTaggedPatientCount = patientTaggedCount;
    });

    //Archived Document
    await FirebaseFirestore.instance
        .collection('Archived')
        .doc('Users')
        .collection('Tagged Users')
        .doc('Patient')
        .collection('Lists')
        .get()
        .then((myDocuments) {
      print("${myDocuments.docs.length}");
      int patientTaggedArchivedCount = myDocuments.docs.length;
      initialTaggedArchivedPatientCount = patientTaggedArchivedCount;

      totalTaggedPatientCount =
          initialTaggedPatientCount + initialTaggedArchivedPatientCount;
    });
  }

  //Display Posts Count
  getPostsDocument() async {
    //Normal Document
    await FirebaseFirestore.instance
        .collection('Posts')
        .doc('All')
        .collection('All Posts')
        .get()
        .then((myDocuments) {
      print("${myDocuments.docs.length}");
      int postsCount = myDocuments.docs.length;
      initialPostsCount = postsCount;
    });

    //Archived Document
    await FirebaseFirestore.instance
        .collection('Archived')
        .doc('Users')
        .collection('Registered')
        .doc('Donor')
        .collection('Lists')
        .get()
        .then((myDocuments) {
      print("${myDocuments.docs.length}");
      int donorArchivedCount = myDocuments.docs.length;
      initialArchivedDonorCount = donorArchivedCount;
    });

    totalDonorCount = initialDonorCount + initialArchivedDonorCount;
  }

  //Display Matches Count
  getKidneyMatchDocument() async {
    //Normal Document
    await FirebaseFirestore.instance
        .collection('Kidney Match')
        .get()
        .then((myDocuments) {
      print("${myDocuments.docs.length}");
      int kidneyMatchCount = myDocuments.docs.length;
      initialKidneyMatchCount = kidneyMatchCount;
    });

    //Archived Document
    await FirebaseFirestore.instance
        .collection('Archived')
        .doc('Users')
        .collection('Registered')
        .doc('Donor')
        .collection('Lists')
        .get()
        .then((myDocuments) {
      print("${myDocuments.docs.length}");
      int donorArchivedCount = myDocuments.docs.length;
      initialArchivedDonorCount = donorArchivedCount;
    });

    totalDonorCount = initialDonorCount + initialArchivedDonorCount;
  }

  //Display Feedback Table
  getFeedbacksData() async {
    final firestore = FirebaseFirestore.instance;
    QuerySnapshot user = await firestore.collection('Feedbacks').get();
    return user.docs;
  }

  //Admin Chart Data
  List<adminData> getAdminChartData() {
    final List<adminData> adminChartData = [
      adminData('Active', 3),
      adminData('Logged In', 1),
      adminData('Max', 5),
    ];
    return adminChartData;
  }

  //Delete Feedbacks
  deleteFeedbacks() async {
    String? uid = getUserID;
    await FirebaseFirestore.instance.collection("Feedbacks").doc(uid).delete();
  }

  logAdminDeleteFeedback() async {
    String formattedTime = DateFormat('kk:mm:s:a').format(now);
    String formattedDate = formatter.format(now);
    await FirebaseFirestore.instance.collection("Logs").add({
      'Message': "Admin has removed a feedback from " + getUserName.toString(),
      'User': "Official Admin",
      'Logged Date': formattedDate,
      'Logged Time': formattedTime,
    });
  }
}

//Admin Chart Class
class adminData {
  adminData(this.title, this.digits);
  final String? title;
  final int digits;
}
