import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:intl/intl.dart';
import 'package:syncfusion_flutter_charts/charts.dart';

class AdminMatchingPage extends StatefulWidget {
  AdminMatchingPage({Key? key}) : super(key: key);

  @override
  _AdminMatchingPageState createState() => _AdminMatchingPageState();
}

class _AdminMatchingPageState extends State<AdminMatchingPage> {
  //Donor and Patient Registered
  String? getUserID;
  String? getFullName;
  String? getUserName;
  String? getUserType;
  String? getGender;
  String? getAge;
  String? getBloodType;
  String? getKidneySize;
  String? getMatchResultPercentage;

  String? getCurrentUserID;
  String? getCurrentFullName;
  String? getCurrentUserName;
  String? getCurrentEmail;
  String? getCurrentUserType;
  String? getCurrentGender;
  String? getCurrentAge;
  String? getCurrentBloodType;
  String? getCurrentKidneySize;
  String? getCurrentMobileNumber;
  String? getCurrentStatus;

  TextEditingController _userID = TextEditingController();
  TextEditingController _fullName = TextEditingController();
  TextEditingController _userName = TextEditingController();
  TextEditingController _email = TextEditingController();
  TextEditingController _userType = TextEditingController();
  TextEditingController _gender = TextEditingController();
  TextEditingController _age = TextEditingController();
  TextEditingController _bloodType = TextEditingController();
  TextEditingController _kidneySize = TextEditingController();
  TextEditingController _mobileNumber = TextEditingController();
  TextEditingController _status = TextEditingController();

  //For Matching
  double? agePerecentage;
  double? bloodTypePercentage;
  double? kidneySizePercentage;
  double? matchResultPercentage;

  TextEditingController _agePercentage = TextEditingController();
  TextEditingController _bloodTypePercentage = TextEditingController();
  TextEditingController _kidneySizePercentage = TextEditingController();
  TextEditingController _matchResultPercentage = TextEditingController();
  TextEditingController _matchResultPercentageMessage = TextEditingController();
  TextEditingController _registeredMatchList = TextEditingController();
  TextEditingController _TaggedMatchList = TextEditingController();

  bool isButtonEnabled = false;

  String? currentMatch;
  TextEditingController _currentMatch = TextEditingController();
  String? getCurrentMatch;
  String? getAllCurrentMatch;

  final toast = FToast();
  @override
  void initState() {
    super.initState();

    toast.init(context);
  }

  //Time and Date
  var now = new DateTime.now();
  var formatter = new DateFormat('dd-MM-yyyy');

  String? getDonorID;
  String? getDUserName;
  String? getPatientID;
  String? getPUserName;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xffe5eef8),
      body: SingleChildScrollView(
        child: Wrap(
          spacing: 8.0, // gap between adjacent chips
          runSpacing: 4.0, // gap between lines
          direction: Axis.horizontal,
          children: <Widget>[
            Container(
              margin: EdgeInsets.only(top: 60, left: 100),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    'Match a Donor and Patient',
                    style: GoogleFonts.roboto(
                        color: Colors.black54,
                        fontSize: 40,
                        fontWeight: FontWeight.w400),
                  ),
                  Container(
                    margin: EdgeInsets.only(right: 200),
                    height: 40,
                    width: 170,
                    decoration: BoxDecoration(
                      color: Colors.red,
                      borderRadius: BorderRadius.circular(20),
                    ),
                    child: InkWell(
                      onTap: () {
                        showMatchCustomDialog(context);
                      },
                      child: Row(
                        children: [
                          Container(
                            margin: EdgeInsets.only(left: 10),
                            child: Icon(
                              Icons.search_outlined,
                              color: Colors.red[100],
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.only(left: 10),
                            child: Text(
                              'Find match'.toUpperCase(),
                              style: GoogleFonts.roboto(
                                  color: Colors.white, fontSize: 16),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Container(
              height: MediaQuery.of(context).size.height * 0.7,
              width: MediaQuery.of(context).size.width * 0.755,
              margin: EdgeInsets.only(top: 20, left: 50),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: Colors.white,
              ),
              child: Stack(
                children: [
                  //Contents
                  FutureBuilder(
                    future: getAllMatchData(),
                    builder: (context, AsyncSnapshot snapshot) {
                      if (snapshot.hasData) {
                        return ListView.builder(
                          itemCount: snapshot.data!.length,
                          itemBuilder: (context, index) {
                            DocumentSnapshot userData = snapshot.data[index];
                            getCurrentUserName = userData["UserName"];
                            getCurrentMatch = userData["UserID"];

                            return Stack(
                              children: [
                                Container(
                                  color: Colors.red[50],
                                  margin: EdgeInsets.only(
                                      top: 20, left: 20, right: 20),
                                  child: ListTile(
                                    title: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        //Time and Date
                                        Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Container(
                                              child: Row(
                                                children: [
                                                  Container(
                                                    margin: EdgeInsets.only(
                                                        top: 50),
                                                    child: Image.asset(
                                                      "assets/admin_donor.png",
                                                      height: 100,
                                                    ),
                                                  ),
                                                  Column(
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .start,
                                                    children: [
                                                      Container(
                                                        margin: EdgeInsets.only(
                                                            top: 30),
                                                        child: Row(
                                                          children: [
                                                            Container(
                                                              margin: EdgeInsets
                                                                  .only(
                                                                      left: 20),
                                                              child: Text(
                                                                  'User Name: '),
                                                            ),
                                                            Container(
                                                              margin: EdgeInsets
                                                                  .only(
                                                                      left: 10),
                                                              child: Text(
                                                                userData[
                                                                    "UserName"],
                                                                style: GoogleFonts
                                                                    .roboto(
                                                                        color: Colors
                                                                            .grey,
                                                                        fontSize:
                                                                            14),
                                                              ),
                                                            ),
                                                          ],
                                                        ),
                                                      ),
                                                      Container(
                                                        margin: EdgeInsets.only(
                                                            top: 15),
                                                        child: Row(
                                                          children: [
                                                            Container(
                                                              margin: EdgeInsets
                                                                  .only(
                                                                      left: 20),
                                                              child: Text(
                                                                  'User Type: '),
                                                            ),
                                                            Container(
                                                              margin: EdgeInsets
                                                                  .only(
                                                                      left: 10),
                                                              child: Text(
                                                                userData[
                                                                    "User Type"],
                                                                style: GoogleFonts
                                                                    .roboto(
                                                                        color: Colors
                                                                            .grey,
                                                                        fontSize:
                                                                            14),
                                                              ),
                                                            ),
                                                          ],
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                ],
                                              ),
                                            ),
                                            //Display Matches of Current User
                                            Container(
                                              margin: EdgeInsets.only(
                                                  right: 200, bottom: 20),
                                              child: Column(
                                                children: [
                                                  Container(
                                                    margin: EdgeInsets.only(
                                                        top: 10),
                                                    child: Text(
                                                      "Summary of Results",
                                                      style: GoogleFonts.roboto(
                                                          color: Colors.black87,
                                                          fontSize: 20,
                                                          fontWeight:
                                                              FontWeight.w600,
                                                          letterSpacing: 1),
                                                    ),
                                                  ),
                                                  Container(
                                                    margin: EdgeInsets.only(
                                                        top: 10),
                                                    child: Row(
                                                      children: [
                                                        Text(
                                                          "Top",
                                                          style: GoogleFonts
                                                              .roboto(
                                                                  color: Colors
                                                                      .grey,
                                                                  fontSize: 14,
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .w600,
                                                                  letterSpacing:
                                                                      1),
                                                        ),
                                                        Container(
                                                          margin:
                                                              EdgeInsets.only(
                                                                  left: 10),
                                                          child: Text(
                                                            'Match %',
                                                            style: GoogleFonts.roboto(
                                                                color:
                                                                    Colors.grey,
                                                                fontSize: 14,
                                                                fontWeight:
                                                                    FontWeight
                                                                        .w600,
                                                                letterSpacing:
                                                                    1),
                                                          ),
                                                        ),
                                                        Container(
                                                          margin:
                                                              EdgeInsets.only(
                                                                  left: 10),
                                                          child: Text(
                                                            "User name",
                                                            style: GoogleFonts.roboto(
                                                                color:
                                                                    Colors.grey,
                                                                fontSize: 14,
                                                                fontWeight:
                                                                    FontWeight
                                                                        .w600,
                                                                letterSpacing:
                                                                    1),
                                                          ),
                                                        ),
                                                        Container(
                                                          margin:
                                                              EdgeInsets.only(
                                                                  left: 10),
                                                          child: Text(
                                                            'Age %',
                                                            style: GoogleFonts.roboto(
                                                                color:
                                                                    Colors.grey,
                                                                fontSize: 14,
                                                                fontWeight:
                                                                    FontWeight
                                                                        .w600,
                                                                letterSpacing:
                                                                    1),
                                                          ),
                                                        ),
                                                        Container(
                                                          margin:
                                                              EdgeInsets.only(
                                                                  left: 10),
                                                          child: Text(
                                                            'Blood Type %',
                                                            style: GoogleFonts.roboto(
                                                                color:
                                                                    Colors.grey,
                                                                fontSize: 14,
                                                                fontWeight:
                                                                    FontWeight
                                                                        .w600,
                                                                letterSpacing:
                                                                    1),
                                                          ),
                                                        ),
                                                        Container(
                                                          margin:
                                                              EdgeInsets.only(
                                                                  left: 10),
                                                          child: Text(
                                                            'Kidney Size %',
                                                            style: GoogleFonts.roboto(
                                                                color:
                                                                    Colors.grey,
                                                                fontSize: 14,
                                                                fontWeight:
                                                                    FontWeight
                                                                        .w600,
                                                                letterSpacing:
                                                                    1),
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                                  ),
                                                  //display Matches List of current User
                                                  Container(
                                                    decoration: BoxDecoration(
                                                      color: Colors.red[200],
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              10),
                                                    ),
                                                    width:
                                                        MediaQuery.of(context)
                                                                .size
                                                                .width *
                                                            0.25,
                                                    height: 130,
                                                    margin: EdgeInsets.only(
                                                        top: 10),
                                                    child: FutureBuilder(
                                                      future:
                                                          getAllMatchesData(),
                                                      builder: (context,
                                                          AsyncSnapshot
                                                              snapshot) {
                                                        if (snapshot.hasData) {
                                                          return ListView
                                                              .builder(
                                                            itemCount: snapshot
                                                                .data!.length,
                                                            itemBuilder:
                                                                (context,
                                                                    index) {
                                                              DocumentSnapshot
                                                                  userData =
                                                                  snapshot.data[
                                                                      index];
                                                              int x = index + 1;
                                                              index = x;
                                                              return Stack(
                                                                children: [
                                                                  Container(
                                                                    margin: EdgeInsets
                                                                        .only(
                                                                            top:
                                                                                10),
                                                                    child: Row(
                                                                      children: [
                                                                        Container(
                                                                          width:
                                                                              15,
                                                                          margin:
                                                                              EdgeInsets.only(left: 20),
                                                                          child:
                                                                              Text(
                                                                            index.toString() +
                                                                                ".",
                                                                            style:
                                                                                GoogleFonts.roboto(color: Colors.black87, fontSize: 14),
                                                                          ),
                                                                        ),
                                                                        Container(
                                                                          width:
                                                                              50,
                                                                          margin:
                                                                              EdgeInsets.only(left: 20),
                                                                          child:
                                                                              Text(
                                                                            userData["Match Rate"],
                                                                            style:
                                                                                GoogleFonts.roboto(color: Colors.black87, fontSize: 14),
                                                                          ),
                                                                        ),
                                                                        Container(
                                                                          width:
                                                                              60,
                                                                          margin:
                                                                              EdgeInsets.only(left: 25),
                                                                          child:
                                                                              Text(
                                                                            userData["UserName"],
                                                                            style:
                                                                                GoogleFonts.roboto(color: Colors.black87, fontSize: 14),
                                                                          ),
                                                                        ),
                                                                        Container(
                                                                          width:
                                                                              50,
                                                                          margin:
                                                                              EdgeInsets.only(left: 20),
                                                                          child:
                                                                              Text(
                                                                            userData["Age Percentage"],
                                                                            style:
                                                                                GoogleFonts.roboto(color: Colors.black87, fontSize: 14),
                                                                          ),
                                                                        ),
                                                                        Container(
                                                                          width:
                                                                              50,
                                                                          margin:
                                                                              EdgeInsets.only(left: 25),
                                                                          child:
                                                                              Text(
                                                                            userData["Blood Type Percentage"],
                                                                            style:
                                                                                GoogleFonts.roboto(color: Colors.black87, fontSize: 14),
                                                                          ),
                                                                        ),
                                                                        Container(
                                                                          width:
                                                                              50,
                                                                          margin:
                                                                              EdgeInsets.only(left: 50),
                                                                          child:
                                                                              Text(
                                                                            userData["Kidney Size Percentage"],
                                                                            style:
                                                                                GoogleFonts.roboto(color: Colors.black87, fontSize: 14),
                                                                          ),
                                                                        ),
                                                                      ],
                                                                    ),
                                                                  ),
                                                                ],
                                                              );
                                                            },
                                                          );
                                                        } else {
                                                          return Text(
                                                              'Users Data Not Available');
                                                        }
                                                      },
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),

                                            //Delete Button
                                            InkWell(
                                                onTap: () {
                                                  getCurrentMatch =
                                                      userData["UserID"];
                                                  getAllCurrentMatch =
                                                      userData["UserID"];
                                                  showCustomDialog(context);
                                                },
                                                child: Icon(Icons
                                                    .delete_forever_outlined)),
                                          ],
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            );
                          },
                        );
                      } else {
                        return Text('Users Data Not Available');
                      }
                    },
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  //Generate Kidney Match
  void showMatchCustomDialog(BuildContext context) => showDialog(
      context: context,
      builder: (BuildContext context) {
        return Dialog(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10.0)), //this right here
          child: Container(
            height: MediaQuery.of(context).size.height * 0.999,
            width: MediaQuery.of(context).size.width * 1,
            child: Stack(
              children: [
                //Close Icon
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    Container(
                      margin: EdgeInsets.only(top: 10, right: 20),
                      child: InkWell(
                        onTap: () {
                          setState(() {
                            Navigator.of(context).pop();
                            isButtonEnabled = false;
                            //Donor and Patient clear Fields
                            _fullName.text = "";
                            _userName.text = "";
                            _gender.text = "";
                            _age.text = "";
                            _bloodType.text = "";
                            _kidneySize.text = "";
                            _userType.text = "";

                            //Retireve all list works like magic
                            getAge!;
                          });
                        },
                        child: Icon(
                          Icons.close_outlined,
                          size: 25,
                          color: Colors.red[100],
                        ),
                      ),
                    ),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container(
                      margin: EdgeInsets.only(top: 40, left: 40),
                      child: Text(
                        'Generate Kidney Match'.toUpperCase(),
                        style: GoogleFonts.roboto(
                            color: Colors.black87,
                            fontSize: 40,
                            fontWeight: FontWeight.w700,
                            letterSpacing: 2),
                      ),
                    ),
                  ],
                ),

                //All Donor and Patient Registered
                Column(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        //All Registered
                        Column(
                          children: [
                            Container(
                              margin: EdgeInsets.only(
                                top: 100,
                              ),
                              child: Text(
                                'All Donor and Patient Registered',
                                style: GoogleFonts.roboto(
                                    color: Colors.black87,
                                    fontSize: 20,
                                    fontWeight: FontWeight.w600,
                                    letterSpacing: 1),
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(left: 20),
                              width: 450,
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceEvenly,
                                children: [
                                  Text(
                                    'Full Name',
                                    style: GoogleFonts.roboto(
                                        color: Colors.red,
                                        fontWeight: FontWeight.w700,
                                        fontSize: 14),
                                  ),
                                  Text(
                                    'User Name',
                                    style: GoogleFonts.roboto(
                                        color: Colors.red,
                                        fontWeight: FontWeight.w700,
                                        fontSize: 14),
                                  ),
                                  Text(
                                    'Gender',
                                    style: GoogleFonts.roboto(
                                        color: Colors.red,
                                        fontWeight: FontWeight.w700,
                                        fontSize: 14),
                                  ),
                                  Text(
                                    'Age',
                                    style: GoogleFonts.roboto(
                                        color: Colors.red,
                                        fontWeight: FontWeight.w700,
                                        fontSize: 14),
                                  ),
                                  Text(
                                    '\nBlood\nType',
                                    style: GoogleFonts.roboto(
                                        color: Colors.red,
                                        fontWeight: FontWeight.w700,
                                        fontSize: 14),
                                  ),
                                  Text(
                                    '\nKidney\nSize',
                                    style: GoogleFonts.roboto(
                                        color: Colors.red,
                                        fontWeight: FontWeight.w700,
                                        fontSize: 14),
                                  ),
                                ],
                              ),
                            ),
                            Container(
                              width: 460,
                              height: 700,
                              margin: EdgeInsets.only(left: 40),
                              decoration: BoxDecoration(
                                  color: Color(0xffe5eef8),
                                  borderRadius: BorderRadius.circular(10)),
                              child: FutureBuilder(
                                future: getAllRegisteredData(),
                                builder: (context, AsyncSnapshot snapshot) {
                                  if (snapshot.hasData) {
                                    return ListView.builder(
                                      itemCount: snapshot.data!.length,
                                      itemBuilder: (context, index) {
                                        DocumentSnapshot userData =
                                            snapshot.data[index];

                                        return Stack(
                                          children: [
                                            Container(
                                              margin: EdgeInsets.only(
                                                top: 10,
                                              ),
                                              child: ListTile(
                                                title: Row(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment
                                                          .spaceEvenly,
                                                  children: [
                                                    Container(
                                                      margin: EdgeInsets.only(
                                                          left: 10),
                                                      width: 65,
                                                      child: Text(
                                                        userData["FullName"],
                                                        style:
                                                            GoogleFonts.roboto(
                                                                color: Colors
                                                                    .black87,
                                                                fontWeight:
                                                                    FontWeight
                                                                        .w400,
                                                                fontSize: 14),
                                                      ),
                                                    ),
                                                    Container(
                                                      width: 70,
                                                      margin: EdgeInsets.only(
                                                          left: 20),
                                                      child: Text(
                                                        userData["UserName"],
                                                        style:
                                                            GoogleFonts.roboto(
                                                                color: Colors
                                                                    .black87,
                                                                fontWeight:
                                                                    FontWeight
                                                                        .w400,
                                                                fontSize: 14),
                                                      ),
                                                    ),
                                                    Container(
                                                      width: 55,
                                                      margin: EdgeInsets.only(
                                                          left: 25),
                                                      child: Text(
                                                        userData["Gender"],
                                                        style:
                                                            GoogleFonts.roboto(
                                                                color: Colors
                                                                    .black87,
                                                                fontWeight:
                                                                    FontWeight
                                                                        .w400,
                                                                fontSize: 14),
                                                      ),
                                                    ),
                                                    Container(
                                                      width: 20,
                                                      margin: EdgeInsets.only(
                                                          left: 10),
                                                      child: Text(
                                                        userData["Age"],
                                                        style:
                                                            GoogleFonts.roboto(
                                                                color: Colors
                                                                    .black87,
                                                                fontWeight:
                                                                    FontWeight
                                                                        .w400,
                                                                fontSize: 14),
                                                      ),
                                                    ),
                                                    Container(
                                                      width: 25,
                                                      margin: EdgeInsets.only(
                                                          left: 35),
                                                      child: Text(
                                                        userData["Blood Type"],
                                                        style:
                                                            GoogleFonts.roboto(
                                                                color: Colors
                                                                    .black87,
                                                                fontWeight:
                                                                    FontWeight
                                                                        .w400,
                                                                fontSize: 14),
                                                      ),
                                                    ),
                                                    Container(
                                                      width: 25,
                                                      margin: EdgeInsets.only(
                                                          left: 30),
                                                      child: Text(
                                                        userData["Kidney Size"],
                                                        style:
                                                            GoogleFonts.roboto(
                                                                color: Colors
                                                                    .black87,
                                                                fontWeight:
                                                                    FontWeight
                                                                        .w400,
                                                                fontSize: 14),
                                                      ),
                                                    ),
                                                    InkWell(
                                                      onTap: () {
                                                        getCurrentFullName =
                                                            userData[
                                                                "FullName"];
                                                        getCurrentUserName =
                                                            userData[
                                                                "UserName"];
                                                        getCurrentGender =
                                                            userData["Gender"];
                                                        getCurrentUserType =
                                                            userData[
                                                                "User Type"];
                                                        getCurrentAge =
                                                            userData["Age"];
                                                        getCurrentBloodType =
                                                            userData[
                                                                "Blood Type"];
                                                        getCurrentKidneySize =
                                                            userData[
                                                                "Kidney Size"];
                                                        getCurrentUserID =
                                                            userData["UserID"];
                                                        getCurrentEmail =
                                                            userData[
                                                                "Email Address"];
                                                        getCurrentMobileNumber =
                                                            userData[
                                                                "Mobile Number"];
                                                        getCurrentStatus =
                                                            userData["Status"];

                                                        _userID.text =
                                                            getCurrentUserID!;
                                                        _fullName.text =
                                                            getCurrentFullName!;
                                                        _userName.text =
                                                            getCurrentUserName!;
                                                        _userType.text =
                                                            getCurrentUserType!;
                                                        _gender.text =
                                                            getCurrentGender!;
                                                        _age.text =
                                                            getCurrentAge!;
                                                        _bloodType.text =
                                                            getCurrentBloodType!;
                                                        _kidneySize.text =
                                                            getCurrentKidneySize!;
                                                        _email.text =
                                                            getCurrentEmail!;
                                                        _mobileNumber.text =
                                                            getCurrentMobileNumber!;
                                                        _status.text =
                                                            getCurrentStatus!;

                                                        Navigator.of(context)
                                                            .pop();
                                                        showMatchCustomDialog(
                                                            context);
                                                      },
                                                      child: Container(
                                                        width: 25,
                                                        margin: EdgeInsets.only(
                                                            left: 10),
                                                        child: Icon(
                                                          Icons
                                                              .arrow_right_outlined,
                                                          size: 30,
                                                          color: Colors.blue,
                                                        ),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            ),
                                            Padding(
                                              padding:
                                                  const EdgeInsets.symmetric(
                                                      horizontal: 18),
                                              child: Divider(),
                                            ),
                                          ],
                                        );
                                      },
                                    );
                                  } else {
                                    return Container(
                                      alignment: Alignment.center,
                                      child: CircularProgressIndicator(),
                                    );
                                  }
                                },
                              ),
                            ),
                          ],
                        ),
                        //Matchesz Potential
                        Column(
                          children: [
                            Container(
                              margin: EdgeInsets.only(
                                top: 100,
                              ),
                              child: Text(
                                'List of Potential Kidney Donor and Patient Match',
                                style: GoogleFonts.roboto(
                                    color: Colors.black87,
                                    fontSize: 20,
                                    fontWeight: FontWeight.w600,
                                    letterSpacing: 1),
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(left: 10, right: 60),
                              width: 450,
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceEvenly,
                                children: [
                                  Text(
                                    'User Name',
                                    style: GoogleFonts.roboto(
                                        color: Colors.red,
                                        fontWeight: FontWeight.w700,
                                        fontSize: 14),
                                  ),
                                  Container(
                                    margin:
                                        EdgeInsets.only(left: 20, right: 20),
                                    child: Text(
                                      'User Type',
                                      style: GoogleFonts.roboto(
                                          color: Colors.red,
                                          fontWeight: FontWeight.w700,
                                          fontSize: 14),
                                    ),
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(right: 20),
                                    child: Text(
                                      'Match %',
                                      style: GoogleFonts.roboto(
                                          color: Colors.red,
                                          fontWeight: FontWeight.w700,
                                          fontSize: 14),
                                    ),
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(right: 20),
                                    child: Text(
                                      'Age %',
                                      style: GoogleFonts.roboto(
                                          color: Colors.red,
                                          fontWeight: FontWeight.w700,
                                          fontSize: 14),
                                    ),
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(right: 20),
                                    child: Text(
                                      '\nBlood\nType %',
                                      style: GoogleFonts.roboto(
                                          color: Colors.red,
                                          fontWeight: FontWeight.w700,
                                          fontSize: 14),
                                    ),
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(right: 20),
                                    child: Text(
                                      '\nKidney\nSize %',
                                      style: GoogleFonts.roboto(
                                          color: Colors.red,
                                          fontWeight: FontWeight.w700,
                                          fontSize: 14),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Container(
                              width: 460,
                              height: 700,
                              margin: EdgeInsets.only(right: 40),
                              decoration: BoxDecoration(
                                  color: Color(0xffe5eef8),
                                  borderRadius: BorderRadius.circular(10)),
                              child: FutureBuilder(
                                future: getAllRegisteredData(),
                                builder: (context, AsyncSnapshot snapshot) {
                                  if (snapshot.hasData) {
                                    return ListView.builder(
                                      itemCount: snapshot.data!.length,
                                      itemBuilder: (context, index) {
                                        DocumentSnapshot userData =
                                            snapshot.data[index];

                                        getUserID = userData["UserID"];
                                        getFullName = userData["FullName"];
                                        getUserName = userData["UserName"];
                                        getUserType = userData["User Type"];
                                        getGender = userData["Gender"];
                                        getAge = userData["Age"];
                                        getBloodType = userData["Blood Type"];
                                        getKidneySize = userData["Kidney Size"];

                                        //Remove current user from list
                                        if (_userName.text == getUserName) {
                                          getUserName = "";
                                          getUserType = "";
                                          getGender = "";
                                          getAge = "";
                                          getBloodType = "";
                                          getKidneySize = "";
                                        }
                                        //Remove similar user type from list
                                        if (_userType.text == getUserType) {
                                          getUserName = "";
                                          getUserType = "";
                                          getGender = "";
                                          getAge = "";
                                          getBloodType = "";
                                          getKidneySize = "";
                                        }

                                        //Age Computation
                                        if (_age.text.isEmpty) {
                                          int x = int.parse(userData["Age"]);
                                          getAge = x.toString();
                                        } else {
                                          int x = int.parse(_age.text);
                                          int y = int.parse(userData["Age"]);

                                          //card is less than
                                          if (x < y) {
                                            double z = (x / y) * 100;
                                            getAge = z.toStringAsFixed(2);
                                          }
                                          //card is greater than
                                          if (x > y) {
                                            double z = (y / x) * 100;
                                            getAge = z.toStringAsFixed(2);
                                          }
                                          //card is equal
                                          if (x == y) {
                                            double z = (x / y) * 100;
                                            getAge = z.toStringAsFixed(2);
                                          }

                                          if (_userName.text ==
                                              userData["UserName"]) {
                                            getAge = "";
                                            getUserType = "";
                                          }
                                          if (_userType.text ==
                                              userData["User Type"]) {
                                            getAge = "";
                                          }
                                        }

                                        //Blood Type
                                        String? dBT = _bloodType.text;
                                        String? pBT = userData["Blood Type"];
                                        //Compatible to A +
                                        if (dBT == "A+" && pBT == "A+") {
                                          //Blood Type A+
                                          bloodTypePercentage = 100.00;
                                          getBloodType = bloodTypePercentage!
                                              .toStringAsFixed(2);
                                          if (_userName.text ==
                                              userData["UserName"]) {
                                            getBloodType = "";
                                          }
                                          if (_userType.text ==
                                              userData["User Type"]) {
                                            getBloodType = "";
                                          }
                                        } else if (dBT == "A+" && pBT == "A-") {
                                          bloodTypePercentage = 100.00;
                                          getBloodType = bloodTypePercentage!
                                              .toStringAsFixed(2);
                                          if (_userName.text ==
                                              userData["UserName"]) {
                                            getBloodType = "";
                                          }
                                          if (_userType.text ==
                                              userData["User Type"]) {
                                            getBloodType = "";
                                          }
                                        } else if (dBT == "A+" &&
                                            pBT == "AB-") {
                                          bloodTypePercentage = 100.00;
                                          getBloodType = bloodTypePercentage!
                                              .toStringAsFixed(2);
                                          if (_userName.text ==
                                              userData["UserName"]) {
                                            getBloodType = "";
                                          }
                                          if (_userType.text ==
                                              userData["User Type"]) {
                                            getBloodType = "";
                                          }
                                        } else if (dBT == "A+" &&
                                            pBT == "AB+") {
                                          bloodTypePercentage = 100.00;
                                          getBloodType = bloodTypePercentage!
                                              .toStringAsFixed(2);
                                          if (_userName.text ==
                                              userData["UserName"]) {
                                            getBloodType = "";
                                          }
                                          if (_userType.text ==
                                              userData["User Type"]) {
                                            getBloodType = "";
                                          }
                                          //Not Compatible to A+
                                        } else if (dBT == "A+" && pBT == "B+") {
                                          bloodTypePercentage = 00.00;
                                          getBloodType = bloodTypePercentage!
                                              .toStringAsFixed(2);
                                          if (_userName.text ==
                                              userData["UserName"]) {
                                            getBloodType = "";
                                          }
                                          if (_userType.text ==
                                              userData["User Type"]) {
                                            getBloodType = "";
                                          }
                                        } else if (dBT == "A+" && pBT == "B-") {
                                          bloodTypePercentage = 00.00;
                                          getBloodType = bloodTypePercentage!
                                              .toStringAsFixed(2);
                                          if (_userName.text ==
                                              userData["UserName"]) {
                                            getBloodType = "";
                                          }
                                          if (_userType.text ==
                                              userData["User Type"]) {
                                            getBloodType = "";
                                          }
                                        } else if (dBT == "A+" && pBT == "O+") {
                                          bloodTypePercentage = 00.00;
                                          getBloodType = bloodTypePercentage!
                                              .toStringAsFixed(2);
                                          if (_userName.text ==
                                              userData["UserName"]) {
                                            getBloodType = "";
                                          }
                                          if (_userType.text ==
                                              userData["User Type"]) {
                                            getBloodType = "";
                                          }
                                        } else if (dBT == "A+" && pBT == "O-") {
                                          bloodTypePercentage = 00.00;
                                          getBloodType = bloodTypePercentage!
                                              .toStringAsFixed(2);
                                          if (_userName.text ==
                                              userData["UserName"]) {
                                            getBloodType = "";
                                          }
                                          if (_userType.text ==
                                              userData["User Type"]) {
                                            getBloodType = "";
                                          }
                                          //Compatible to A-
                                        } else if (dBT == "A-" && pBT == "A+") {
                                          //Blood Type A-
                                          bloodTypePercentage = 100.00;
                                          getBloodType = bloodTypePercentage!
                                              .toStringAsFixed(2);
                                          if (_userName.text ==
                                              userData["UserName"]) {
                                            getBloodType = "";
                                          }
                                          if (_userType.text ==
                                              userData["User Type"]) {
                                            getBloodType = "";
                                          }
                                        } else if (dBT == "A-" && pBT == "A-") {
                                          bloodTypePercentage = 100.00;
                                          getBloodType = bloodTypePercentage!
                                              .toStringAsFixed(2);
                                          if (_userName.text ==
                                              userData["UserName"]) {
                                            getBloodType = "";
                                          }
                                          if (_userType.text ==
                                              userData["User Type"]) {
                                            getBloodType = "";
                                          }
                                        } else if (dBT == "A-" &&
                                            pBT == "AB-") {
                                          bloodTypePercentage = 100.00;
                                          getBloodType = bloodTypePercentage!
                                              .toStringAsFixed(2);
                                          if (_userName.text ==
                                              userData["UserName"]) {
                                            getBloodType = "";
                                          }
                                          if (_userType.text ==
                                              userData["User Type"]) {
                                            getBloodType = "";
                                          }
                                        } else if (dBT == "A-" &&
                                            pBT == "AB+") {
                                          bloodTypePercentage = 100.00;
                                          getBloodType = bloodTypePercentage!
                                              .toStringAsFixed(2);
                                          if (_userName.text ==
                                              userData["UserName"]) {
                                            getBloodType = "";
                                          }
                                          if (_userType.text ==
                                              userData["User Type"]) {
                                            getBloodType = "";
                                          }
                                          //Not Compatible to A-
                                        } else if (dBT == "A-" && pBT == "B+") {
                                          bloodTypePercentage = 00.00;
                                          getBloodType = bloodTypePercentage!
                                              .toStringAsFixed(2);
                                          if (_userName.text ==
                                              userData["UserName"]) {
                                            getBloodType = "";
                                          }
                                          if (_userType.text ==
                                              userData["User Type"]) {
                                            getBloodType = "";
                                          }
                                        } else if (dBT == "A-" && pBT == "B-") {
                                          bloodTypePercentage = 00.00;
                                          getBloodType = bloodTypePercentage!
                                              .toStringAsFixed(2);
                                          if (_userName.text ==
                                              userData["UserName"]) {
                                            getBloodType = "";
                                          }
                                          if (_userType.text ==
                                              userData["User Type"]) {
                                            getBloodType = "";
                                          }
                                        } else if (dBT == "A-" && pBT == "O+") {
                                          bloodTypePercentage = 00.00;
                                          getBloodType = bloodTypePercentage!
                                              .toStringAsFixed(2);
                                          if (_userName.text ==
                                              userData["UserName"]) {
                                            getBloodType = "";
                                          }
                                          if (_userType.text ==
                                              userData["User Type"]) {
                                            getBloodType = "";
                                          }
                                        } else if (dBT == "A-" && pBT == "O-") {
                                          bloodTypePercentage = 00.00;
                                          getBloodType = bloodTypePercentage!
                                              .toStringAsFixed(2);
                                          if (_userName.text ==
                                              userData["UserName"]) {
                                            getBloodType = "";
                                          }
                                          if (_userType.text ==
                                              userData["User Type"]) {
                                            getBloodType = "";
                                          }
                                          //Compatible to B+
                                        } else if (dBT == "B+" && pBT == "B+") {
                                          //Blood Type B+
                                          bloodTypePercentage = 100.00;
                                          getBloodType = bloodTypePercentage!
                                              .toStringAsFixed(2);
                                          if (_userName.text ==
                                              userData["UserName"]) {
                                            getBloodType = "";
                                          }
                                          if (_userType.text ==
                                              userData["User Type"]) {
                                            getBloodType = "";
                                          }
                                        } else if (dBT == "B+" && pBT == "B-") {
                                          bloodTypePercentage = 100.00;
                                          getBloodType = bloodTypePercentage!
                                              .toStringAsFixed(2);
                                          if (_userName.text ==
                                              userData["UserName"]) {
                                            getBloodType = "";
                                          }
                                          if (_userType.text ==
                                              userData["User Type"]) {
                                            getBloodType = "";
                                          }
                                        } else if (dBT == "B+" &&
                                            pBT == "AB-") {
                                          bloodTypePercentage = 100.00;
                                          getBloodType = bloodTypePercentage!
                                              .toStringAsFixed(2);
                                          if (_userName.text ==
                                              userData["UserName"]) {
                                            getBloodType = "";
                                          }
                                          if (_userType.text ==
                                              userData["User Type"]) {
                                            getBloodType = "";
                                          }
                                        } else if (dBT == "B+" &&
                                            pBT == "AB+") {
                                          bloodTypePercentage = 100.00;
                                          getBloodType = bloodTypePercentage!
                                              .toStringAsFixed(2);
                                          if (_userName.text ==
                                              userData["UserName"]) {
                                            getBloodType = "";
                                          }
                                          if (_userType.text ==
                                              userData["User Type"]) {
                                            getBloodType = "";
                                          }
                                          //Not Compatible to B+
                                        } else if (dBT == "B+" && pBT == "A+") {
                                          bloodTypePercentage = 00.00;
                                          getBloodType = bloodTypePercentage!
                                              .toStringAsFixed(2);
                                          if (_userName.text ==
                                              userData["UserName"]) {
                                            getBloodType = "";
                                          }
                                          if (_userType.text ==
                                              userData["User Type"]) {
                                            getBloodType = "";
                                          }
                                        } else if (dBT == "B+" && pBT == "A-") {
                                          bloodTypePercentage = 00.00;
                                          getBloodType = bloodTypePercentage!
                                              .toStringAsFixed(2);
                                          if (_userName.text ==
                                              userData["UserName"]) {
                                            getBloodType = "";
                                          }
                                          if (_userType.text ==
                                              userData["User Type"]) {
                                            getBloodType = "";
                                          }
                                        } else if (dBT == "B+" && pBT == "O+") {
                                          bloodTypePercentage = 00.00;
                                          getBloodType = bloodTypePercentage!
                                              .toStringAsFixed(2);
                                          if (_userName.text ==
                                              userData["UserName"]) {
                                            getBloodType = "";
                                          }
                                          if (_userType.text ==
                                              userData["User Type"]) {
                                            getBloodType = "";
                                          }
                                        } else if (dBT == "B+" && pBT == "O-") {
                                          bloodTypePercentage = 00.00;
                                          getBloodType = bloodTypePercentage!
                                              .toStringAsFixed(2);
                                          if (_userName.text ==
                                              userData["UserName"]) {
                                            getBloodType = "";
                                          }
                                          if (_userType.text ==
                                              userData["User Type"]) {
                                            getBloodType = "";
                                          }
                                          //Compatible to B-
                                        } else if (dBT == "B-" && pBT == "B+") {
                                          //Blood Type B+
                                          bloodTypePercentage = 100.00;
                                          getBloodType = bloodTypePercentage!
                                              .toStringAsFixed(2);
                                          if (_userName.text ==
                                              userData["UserName"]) {
                                            getBloodType = "";
                                          }
                                          if (_userType.text ==
                                              userData["User Type"]) {
                                            getBloodType = "";
                                          }
                                        } else if (dBT == "B-" && pBT == "B-") {
                                          bloodTypePercentage = 100.00;
                                          getBloodType = bloodTypePercentage!
                                              .toStringAsFixed(2);
                                          if (_userName.text ==
                                              userData["UserName"]) {
                                            getBloodType = "";
                                          }
                                          if (_userType.text ==
                                              userData["User Type"]) {
                                            getBloodType = "";
                                          }
                                        } else if (dBT == "B-" &&
                                            pBT == "AB-") {
                                          bloodTypePercentage = 100.00;
                                          getBloodType = bloodTypePercentage!
                                              .toStringAsFixed(2);
                                          if (_userName.text ==
                                              userData["UserName"]) {
                                            getBloodType = "";
                                          }
                                          if (_userType.text ==
                                              userData["User Type"]) {
                                            getBloodType = "";
                                          }
                                        } else if (dBT == "B-" &&
                                            pBT == "AB+") {
                                          bloodTypePercentage = 100.00;
                                          getBloodType = bloodTypePercentage!
                                              .toStringAsFixed(2);
                                          if (_userName.text ==
                                              userData["UserName"]) {
                                            getBloodType = "";
                                          }
                                          if (_userType.text ==
                                              userData["User Type"]) {
                                            getBloodType = "";
                                          }
                                          //Not Compatible to B-
                                        } else if (dBT == "B-" && pBT == "A+") {
                                          bloodTypePercentage = 00.00;
                                          getBloodType = bloodTypePercentage!
                                              .toStringAsFixed(2);
                                          if (_userName.text ==
                                              userData["UserName"]) {
                                            getBloodType = "";
                                          }
                                          if (_userType.text ==
                                              userData["User Type"]) {
                                            getBloodType = "";
                                          }
                                        } else if (dBT == "B-" && pBT == "A-") {
                                          bloodTypePercentage = 00.00;
                                          getBloodType = bloodTypePercentage!
                                              .toStringAsFixed(2);
                                          if (_userName.text ==
                                              userData["UserName"]) {
                                            getBloodType = "";
                                          }
                                          if (_userType.text ==
                                              userData["User Type"]) {
                                            getBloodType = "";
                                          }
                                        } else if (dBT == "B-" && pBT == "O+") {
                                          bloodTypePercentage = 00.00;
                                          getBloodType = bloodTypePercentage!
                                              .toStringAsFixed(2);
                                          if (_userName.text ==
                                              userData["UserName"]) {
                                            getBloodType = "";
                                          }
                                          if (_userType.text ==
                                              userData["User Type"]) {
                                            getBloodType = "";
                                          }
                                        } else if (dBT == "B-" && pBT == "O-") {
                                          bloodTypePercentage = 00.00;
                                          getBloodType = bloodTypePercentage!
                                              .toStringAsFixed(2);
                                          if (_userName.text ==
                                              userData["UserName"]) {
                                            getBloodType = "";
                                          }
                                          if (_userType.text ==
                                              userData["User Type"]) {
                                            getBloodType = "";
                                          }
                                          //Compatible to AB+
                                        } else if (dBT == "AB+" &&
                                            pBT == "AB+") {
                                          //Blood Type AB+
                                          bloodTypePercentage = 100.00;
                                          getBloodType = bloodTypePercentage!
                                              .toStringAsFixed(2);
                                          if (_userName.text ==
                                              userData["UserName"]) {
                                            getBloodType = "";
                                          }
                                          if (_userType.text ==
                                              userData["User Type"]) {
                                            getBloodType = "";
                                          }
                                        } else if (dBT == "AB+" &&
                                            pBT == "AB-") {
                                          bloodTypePercentage = 100.00;
                                          getBloodType = bloodTypePercentage!
                                              .toStringAsFixed(2);
                                          if (_userName.text ==
                                              userData["UserName"]) {
                                            getBloodType = "";
                                          }
                                          if (_userType.text ==
                                              userData["User Type"]) {
                                            getBloodType = "";
                                          }
                                          //Not Compatible to AB
                                        } else if (dBT == "AB+" &&
                                            pBT == "B-") {
                                          bloodTypePercentage = 00.00;
                                          getBloodType = bloodTypePercentage!
                                              .toStringAsFixed(2);
                                          if (_userName.text ==
                                              userData["UserName"]) {
                                            getBloodType = "";
                                          }
                                          if (_userType.text ==
                                              userData["User Type"]) {
                                            getBloodType = "";
                                          }
                                        } else if (dBT == "AB+" &&
                                            pBT == "B+") {
                                          bloodTypePercentage = 00.00;
                                          getBloodType = bloodTypePercentage!
                                              .toStringAsFixed(2);
                                          if (_userName.text ==
                                              userData["UserName"]) {
                                            getBloodType = "";
                                          }
                                          if (_userType.text ==
                                              userData["User Type"]) {
                                            getBloodType = "";
                                          }
                                        } else if (dBT == "AB+" &&
                                            pBT == "A+") {
                                          bloodTypePercentage = 00.00;
                                          getBloodType = bloodTypePercentage!
                                              .toStringAsFixed(2);
                                          if (_userName.text ==
                                              userData["UserName"]) {
                                            getBloodType = "";
                                          }
                                          if (_userType.text ==
                                              userData["User Type"]) {
                                            getBloodType = "";
                                          }
                                        } else if (dBT == "AB+" &&
                                            pBT == "A-") {
                                          bloodTypePercentage = 00.00;
                                          getBloodType = bloodTypePercentage!
                                              .toStringAsFixed(2);
                                          if (_userName.text ==
                                              userData["UserName"]) {
                                            getBloodType = "";
                                          }
                                          if (_userType.text ==
                                              userData["User Type"]) {
                                            getBloodType = "";
                                          }
                                        } else if (dBT == "AB+" &&
                                            pBT == "O+") {
                                          bloodTypePercentage = 00.00;
                                          getBloodType = bloodTypePercentage!
                                              .toStringAsFixed(2);
                                          if (_userName.text ==
                                              userData["UserName"]) {
                                            getBloodType = "";
                                          }
                                          if (_userType.text ==
                                              userData["User Type"]) {
                                            getBloodType = "";
                                          }
                                        } else if (dBT == "AB+" &&
                                            pBT == "O-") {
                                          bloodTypePercentage = 00.00;
                                          getBloodType = bloodTypePercentage!
                                              .toStringAsFixed(2);
                                          if (_userName.text ==
                                              userData["UserName"]) {
                                            getBloodType = "";
                                          }
                                          if (_userType.text ==
                                              userData["User Type"]) {
                                            getBloodType = "";
                                          }
                                          //Compatible to Blood Type AB-
                                        } else if (dBT == "AB-" &&
                                            pBT == "AB+") {
                                          //Blood Type AB
                                          bloodTypePercentage = 100.00;
                                          getBloodType = bloodTypePercentage!
                                              .toStringAsFixed(2);
                                          if (_userName.text ==
                                              userData["UserName"]) {
                                            getBloodType = "";
                                          }
                                          if (_userType.text ==
                                              userData["User Type"]) {
                                            getBloodType = "";
                                          }
                                        } else if (dBT == "AB-" &&
                                            pBT == "AB-") {
                                          bloodTypePercentage = 100.00;
                                          getBloodType = bloodTypePercentage!
                                              .toStringAsFixed(2);
                                          if (_userName.text ==
                                              userData["UserName"]) {
                                            getBloodType = "";
                                          }
                                          if (_userType.text ==
                                              userData["User Type"]) {
                                            getBloodType = "";
                                          }
                                          //Not Compatible to AB
                                        } else if (dBT == "AB-" &&
                                            pBT == "B-") {
                                          bloodTypePercentage = 00.00;
                                          getBloodType = bloodTypePercentage!
                                              .toStringAsFixed(2);
                                          if (_userName.text ==
                                              userData["UserName"]) {
                                            getBloodType = "";
                                          }
                                          if (_userType.text ==
                                              userData["User Type"]) {
                                            getBloodType = "";
                                          }
                                        } else if (dBT == "AB-" &&
                                            pBT == "B+") {
                                          bloodTypePercentage = 00.00;
                                          getBloodType = bloodTypePercentage!
                                              .toStringAsFixed(2);
                                          if (_userName.text ==
                                              userData["UserName"]) {
                                            getBloodType = "";
                                          }
                                          if (_userType.text ==
                                              userData["User Type"]) {
                                            getBloodType = "";
                                          }
                                        } else if (dBT == "AB-" &&
                                            pBT == "A+") {
                                          bloodTypePercentage = 00.00;
                                          getBloodType = bloodTypePercentage!
                                              .toStringAsFixed(2);
                                          if (_userName.text ==
                                              userData["UserName"]) {
                                            getBloodType = "";
                                          }
                                          if (_userType.text ==
                                              userData["User Type"]) {
                                            getBloodType = "";
                                          }
                                        } else if (dBT == "AB-" &&
                                            pBT == "A-") {
                                          bloodTypePercentage = 00.00;
                                          getBloodType = bloodTypePercentage!
                                              .toStringAsFixed(2);
                                          if (_userName.text ==
                                              userData["UserName"]) {
                                            getBloodType = "";
                                          }
                                          if (_userType.text ==
                                              userData["User Type"]) {
                                            getBloodType = "";
                                          }
                                        } else if (dBT == "AB-" &&
                                            pBT == "O+") {
                                          bloodTypePercentage = 00.00;
                                          getBloodType = bloodTypePercentage!
                                              .toStringAsFixed(2);
                                          if (_userName.text ==
                                              userData["UserName"]) {
                                            getBloodType = "";
                                          }
                                          if (_userType.text ==
                                              userData["User Type"]) {
                                            getBloodType = "";
                                          }
                                        } else if (dBT == "AB-" &&
                                            pBT == "O-") {
                                          bloodTypePercentage = 00.00;
                                          getBloodType = bloodTypePercentage!
                                              .toStringAsFixed(2);
                                          if (_userName.text ==
                                              userData["UserName"]) {
                                            getBloodType = "";
                                          }
                                          if (_userType.text ==
                                              userData["User Type"]) {
                                            getBloodType = "";
                                          }
                                          //Compatible to Blood Type O+
                                        } else if (dBT == "O+" && pBT == "A+") {
                                          //Blood Type O+
                                          bloodTypePercentage = 100.00;
                                          getBloodType = bloodTypePercentage!
                                              .toStringAsFixed(2);
                                          if (_userName.text ==
                                              userData["UserName"]) {
                                            getBloodType = "";
                                          }
                                          if (_userType.text ==
                                              userData["User Type"]) {
                                            getBloodType = "";
                                          }
                                        } else if (dBT == "O+" && pBT == "A-") {
                                          bloodTypePercentage = 100.00;
                                          getBloodType = bloodTypePercentage!
                                              .toStringAsFixed(2);
                                          if (_userName.text ==
                                              userData["UserName"]) {
                                            getBloodType = "";
                                          }
                                          if (_userType.text ==
                                              userData["User Type"]) {
                                            getBloodType = "";
                                          }
                                        } else if (dBT == "O+" && pBT == "B+") {
                                          bloodTypePercentage = 100.00;
                                          getBloodType = bloodTypePercentage!
                                              .toStringAsFixed(2);
                                          if (_userName.text ==
                                              userData["UserName"]) {
                                            getBloodType = "";
                                          }
                                          if (_userType.text ==
                                              userData["User Type"]) {
                                            getBloodType = "";
                                          }
                                        } else if (dBT == "O+" && pBT == "B-") {
                                          bloodTypePercentage = 100.00;
                                          getBloodType = bloodTypePercentage!
                                              .toStringAsFixed(2);
                                          if (_userName.text ==
                                              userData["UserName"]) {
                                            getBloodType = "";
                                          }
                                          if (_userType.text ==
                                              userData["User Type"]) {
                                            getBloodType = "";
                                          }
                                        } else if (dBT == "O+" &&
                                            pBT == "AB+") {
                                          bloodTypePercentage = 100.00;
                                          getBloodType = bloodTypePercentage!
                                              .toStringAsFixed(2);
                                          if (_userName.text ==
                                              userData["UserName"]) {
                                            getBloodType = "";
                                          }
                                          if (_userType.text ==
                                              userData["User Type"]) {
                                            getBloodType = "";
                                          }
                                        } else if (dBT == "O+" &&
                                            pBT == "AB-") {
                                          bloodTypePercentage = 100.00;
                                          getBloodType = bloodTypePercentage!
                                              .toStringAsFixed(2);
                                          if (_userName.text ==
                                              userData["UserName"]) {
                                            getBloodType = "";
                                          }
                                          if (_userType.text ==
                                              userData["User Type"]) {
                                            getBloodType = "";
                                          }
                                        } else if (dBT == "O+" && pBT == "O+") {
                                          bloodTypePercentage = 100.00;
                                          getBloodType = bloodTypePercentage!
                                              .toStringAsFixed(2);
                                          if (_userName.text ==
                                              userData["UserName"]) {
                                            getBloodType = "";
                                          }
                                          if (_userType.text ==
                                              userData["User Type"]) {
                                            getBloodType = "";
                                          }
                                        } else if (dBT == "O+" && pBT == "O-") {
                                          bloodTypePercentage = 100.00;
                                          getBloodType = bloodTypePercentage!
                                              .toStringAsFixed(2);
                                          if (_userName.text ==
                                              userData["UserName"]) {
                                            getBloodType = "";
                                          }
                                          if (_userType.text ==
                                              userData["User Type"]) {
                                            getBloodType = "";
                                          }
                                          //Blood type O-
                                        } else if (dBT == "O-" && pBT == "A+") {
                                          //Blood Type O-
                                          bloodTypePercentage = 100.00;
                                          getBloodType = bloodTypePercentage!
                                              .toStringAsFixed(2);
                                          if (_userName.text ==
                                              userData["UserName"]) {
                                            getBloodType = "";
                                          }
                                          if (_userType.text ==
                                              userData["User Type"]) {
                                            getBloodType = "";
                                          }
                                        } else if (dBT == "O-" && pBT == "A-") {
                                          bloodTypePercentage = 100.00;
                                          getBloodType = bloodTypePercentage!
                                              .toStringAsFixed(2);
                                          if (_userName.text ==
                                              userData["UserName"]) {
                                            getBloodType = "";
                                          }
                                          if (_userType.text ==
                                              userData["User Type"]) {
                                            getBloodType = "";
                                          }
                                        } else if (dBT == "O-" && pBT == "B+") {
                                          bloodTypePercentage = 100.00;
                                          getBloodType = bloodTypePercentage!
                                              .toStringAsFixed(2);
                                          if (_userName.text ==
                                              userData["UserName"]) {
                                            getBloodType = "";
                                          }
                                          if (_userType.text ==
                                              userData["User Type"]) {
                                            getBloodType = "";
                                          }
                                        } else if (dBT == "O-" && pBT == "B-") {
                                          bloodTypePercentage = 100.00;
                                          getBloodType = bloodTypePercentage!
                                              .toStringAsFixed(2);
                                          if (_userName.text ==
                                              userData["UserName"]) {
                                            getBloodType = "";
                                          }
                                          if (_userType.text ==
                                              userData["User Type"]) {
                                            getBloodType = "";
                                          }
                                        } else if (dBT == "O-" &&
                                            pBT == "AB+") {
                                          bloodTypePercentage = 100.00;
                                          getBloodType = bloodTypePercentage!
                                              .toStringAsFixed(2);
                                          if (_userName.text ==
                                              userData["UserName"]) {
                                            getBloodType = "";
                                          }
                                          if (_userType.text ==
                                              userData["User Type"]) {
                                            getBloodType = "";
                                          }
                                        } else if (dBT == "O-" &&
                                            pBT == "AB-") {
                                          bloodTypePercentage = 100.00;
                                          getBloodType = bloodTypePercentage!
                                              .toStringAsFixed(2);
                                          if (_userName.text ==
                                              userData["UserName"]) {
                                            getBloodType = "";
                                          }
                                          if (_userType.text ==
                                              userData["User Type"]) {
                                            getBloodType = "";
                                          }
                                        } else if (dBT == "O-" && pBT == "O+") {
                                          bloodTypePercentage = 100.00;
                                          getBloodType = bloodTypePercentage!
                                              .toStringAsFixed(2);
                                          if (_userName.text ==
                                              userData["UserName"]) {
                                            getBloodType = "";
                                          }
                                          if (_userType.text ==
                                              userData["User Type"]) {
                                            getBloodType = "";
                                          }
                                        } else if (dBT == "O-" && pBT == "O-") {
                                          bloodTypePercentage = 100.00;
                                          getBloodType = bloodTypePercentage!
                                              .toStringAsFixed(2);
                                          if (_userName.text ==
                                              userData["UserName"]) {
                                            getBloodType = "";
                                          }
                                          if (_userType.text ==
                                              userData["User Type"]) {
                                            getBloodType = "";
                                          }
                                        }

                                        //Kidney Size
                                        if (_kidneySize.text.isEmpty) {
                                          int x = int.parse(
                                              userData["Kidney Size"]);
                                          getKidneySize = x.toString();
                                        } else {
                                          int x = int.parse(_kidneySize.text);
                                          int y = int.parse(
                                              userData["Kidney Size"]);

                                          //card is less than
                                          if (x < y) {
                                            double z = (x / y) * 100;
                                            getKidneySize =
                                                z.toStringAsFixed(2);
                                          }
                                          //card is greater than
                                          if (x > y) {
                                            double z = (y / x) * 100;
                                            getKidneySize =
                                                z.toStringAsFixed(2);
                                          }
                                          //card is equal
                                          if (x == y) {
                                            double z = (x / y) * 100;
                                            getKidneySize =
                                                z.toStringAsFixed(2);
                                          }

                                          if (_userName.text ==
                                              userData["UserName"]) {
                                            getKidneySize = "";
                                          }
                                          if (_userType.text ==
                                              userData["User Type"]) {
                                            getKidneySize = "";
                                          }
                                        }

                                        // Match Percentage
                                        //Validation 1
                                        if (_userName.text.isEmpty) {
                                          double x = 0.0;
                                          getMatchResultPercentage =
                                              x.toStringAsFixed(2);
                                        }

                                        //Validation 2 - Computation
                                        if (_userName.text ==
                                            userData["UserName"]) {
                                          getMatchResultPercentage = "";
                                          getBloodType = userData["Blood Type"];
                                        }

                                        //Validation 3
                                        if (_userType.text ==
                                            userData["User Type"]) {
                                          getBloodType = "";
                                          getMatchResultPercentage = "";
                                        }
                                        //Validation 4
                                        if (_userName.text.isNotEmpty &&
                                            _userType.text !=
                                                userData["User Type"]) {
                                          double x = (double.parse(getAge!) +
                                                  double.parse(getBloodType!) +
                                                  double.parse(
                                                      getKidneySize!)) /
                                              3;
                                          getMatchResultPercentage =
                                              x.toStringAsFixed(2);

                                          //Suggest Possible Match
                                          if (getBloodType! != "0.00" &&
                                              double.parse(
                                                      getMatchResultPercentage!) >=
                                                  50) {
                                            if (_userType.text ==
                                                "I am a Donor") {
                                              postPossibleMatchDonor();
                                              postPossibleMatchAll();
                                              postMatch();
                                              postPossibleKidneyMatch();
                                              archiveCurrentSelectedUser();
                                              archiveCurrentSelectedUserMatches();
                                              deleteAllArchiveCurrentSelectedUserMatches();
                                              deleteDonorArchiveCurrentSelectedUserMatches();
                                              deleteSpecficAllMatchUser();
                                              deleteSpecficDonorMatchUser();
                                            } else if (_userType.text ==
                                                "I am a Patient") {
                                              postPossibleMatchPatient();
                                              postPossibleMatchAll();
                                              postMatch();
                                              postPossibleKidneyMatch();
                                              archiveCurrentSelectedUser();
                                              archiveCurrentSelectedUserMatches();
                                              deleteAllArchiveCurrentSelectedUserMatches();
                                              deletePatientArchiveCurrentSelectedUserMatches();
                                              deleteSpecficAllMatchUser();
                                              deleteSpecficPatientMatchUser();
                                            }
                                          }
                                        }

                                        //For Blood Type Validation
                                        if (getBloodType == "0.00") {
                                          getUserName = "";
                                          getUserType = "";
                                          getMatchResultPercentage = "";
                                          getAge = "";
                                          getBloodType = "";
                                          getKidneySize = "";
                                        }

                                        return Stack(
                                          children: [
                                            Container(
                                              margin: EdgeInsets.only(
                                                top: 10,
                                              ),
                                              child: ListTile(
                                                title: Row(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment
                                                          .spaceEvenly,
                                                  children: [
                                                    Container(
                                                      margin: EdgeInsets.only(
                                                          left: 10),
                                                      width: 65,
                                                      child: Text(
                                                        getUserName!,
                                                        style:
                                                            GoogleFonts.roboto(
                                                                color: Colors
                                                                    .black87,
                                                                fontWeight:
                                                                    FontWeight
                                                                        .w400,
                                                                fontSize: 14),
                                                      ),
                                                    ),
                                                    Container(
                                                      width: 70,
                                                      margin: EdgeInsets.only(
                                                          left: 10),
                                                      child: Text(
                                                        getUserType!,
                                                        style:
                                                            GoogleFonts.roboto(
                                                                color: Colors
                                                                    .black87,
                                                                fontWeight:
                                                                    FontWeight
                                                                        .w400,
                                                                fontSize: 14),
                                                      ),
                                                    ),
                                                    Container(
                                                      width: 55,
                                                      margin: EdgeInsets.only(
                                                          left: 15),
                                                      child: Text(
                                                        getMatchResultPercentage!,
                                                        style:
                                                            GoogleFonts.roboto(
                                                                color: Colors
                                                                    .black87,
                                                                fontWeight:
                                                                    FontWeight
                                                                        .w400,
                                                                fontSize: 14),
                                                      ),
                                                    ),
                                                    Container(
                                                      width: 50,
                                                      margin: EdgeInsets.only(
                                                          left: 10),
                                                      child: Text(
                                                        getAge!,
                                                        style:
                                                            GoogleFonts.roboto(
                                                                color: Colors
                                                                    .black87,
                                                                fontWeight:
                                                                    FontWeight
                                                                        .w400,
                                                                fontSize: 14),
                                                      ),
                                                    ),
                                                    Container(
                                                      width: 50,
                                                      margin: EdgeInsets.only(
                                                          left: 15),
                                                      child: Text(
                                                        getBloodType!,
                                                        style:
                                                            GoogleFonts.roboto(
                                                                color: Colors
                                                                    .black87,
                                                                fontWeight:
                                                                    FontWeight
                                                                        .w400,
                                                                fontSize: 14),
                                                      ),
                                                    ),
                                                    Container(
                                                      width: 50,
                                                      margin: EdgeInsets.only(
                                                          left: 20),
                                                      child: Text(
                                                        getKidneySize!,
                                                        style:
                                                            GoogleFonts.roboto(
                                                                color: Colors
                                                                    .black87,
                                                                fontWeight:
                                                                    FontWeight
                                                                        .w400,
                                                                fontSize: 14),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            ),
                                            Padding(
                                              padding:
                                                  const EdgeInsets.symmetric(
                                                      horizontal: 18),
                                              child: Divider(),
                                            ),
                                          ],
                                        );
                                      },
                                    );
                                  } else {
                                    return Container(
                                      alignment: Alignment.center,
                                      child: CircularProgressIndicator(),
                                    );
                                  }
                                },
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                    Text(
                        '\nNote: Click the Arrow of selected user if possible match list and summary is not visible. The Data cannot be viewed again if you close this tab.'),
                  ],
                ),

                Column(
                  children: [
                    Container(
                      margin: EdgeInsets.only(top: 150, left: 250),
                      child: Text(
                        'Possible Match List and Summary',
                        style: GoogleFonts.roboto(
                            color: Colors.black87,
                            fontSize: 20,
                            fontWeight: FontWeight.w600,
                            letterSpacing: 1),
                      ),
                    ),
                    Row(
                      children: [
                        Container(
                          margin: EdgeInsets.only(top: 20, left: 590),
                          child: Text(
                            'Current Selected User',
                            style: GoogleFonts.roboto(
                                color: Colors.red,
                                fontWeight: FontWeight.w700,
                                fontSize: 14),
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(top: 20, left: 130),
                          child: Text(
                            'Top',
                            style: GoogleFonts.roboto(
                                color: Colors.red,
                                fontWeight: FontWeight.w700,
                                fontSize: 14),
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(top: 20, left: 25),
                          child: Text(
                            'Match %',
                            style: GoogleFonts.roboto(
                                color: Colors.red,
                                fontWeight: FontWeight.w700,
                                fontSize: 14),
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(top: 20, left: 20),
                          child: Text(
                            'User Name',
                            style: GoogleFonts.roboto(
                                color: Colors.red,
                                fontWeight: FontWeight.w700,
                                fontSize: 14),
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(top: 20, left: 30),
                          child: Text(
                            'Age %',
                            style: GoogleFonts.roboto(
                                color: Colors.red,
                                fontWeight: FontWeight.w700,
                                fontSize: 14),
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(top: 20, left: 25),
                          child: Text(
                            'Blood\nType %',
                            style: GoogleFonts.roboto(
                                color: Colors.red,
                                fontWeight: FontWeight.w700,
                                fontSize: 14),
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(top: 20, left: 25),
                          child: Text(
                            'Kidney\nSize %',
                            style: GoogleFonts.roboto(
                                color: Colors.red,
                                fontWeight: FontWeight.w700,
                                fontSize: 14),
                          ),
                        )
                      ],
                    ),
                  ],
                ),
                //Donor and Patient match Card
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    //Donor Match Card
                    Container(
                      margin: EdgeInsets.only(top: 250, left: 500),
                      width: 200,
                      height: 550,
                      decoration: BoxDecoration(
                          color: Colors.red[100],
                          borderRadius: BorderRadius.circular(10)),
                      child: Column(
                        children: [
                          Container(
                            margin: EdgeInsets.only(top: 20),
                            child: Image.asset(
                              'assets/user.png',
                              height: 50,
                            ),
                          ),
                          Container(
                            width: 120,
                            child: Column(
                              children: [
                                TextFormField(
                                  controller: _fullName..text = _fullName.text,
                                  style: GoogleFonts.roboto(
                                      color: Colors.red[400],
                                      fontWeight: FontWeight.w600,
                                      fontSize: 14),
                                  key: Key(
                                    getCurrentFullName.toString(),
                                  ),
                                ),
                                Text(
                                  'Full Name: ',
                                  style: GoogleFonts.roboto(
                                      color: Colors.grey[600],
                                      fontWeight: FontWeight.bold,
                                      fontSize: 14),
                                ),
                              ],
                            ),
                          ),
                          Container(
                            width: 120,
                            child: Column(
                              children: [
                                TextFormField(
                                  controller: _userName..text = _userName.text,
                                  style: GoogleFonts.roboto(
                                      color: Colors.red[400],
                                      fontWeight: FontWeight.w600,
                                      fontSize: 14),
                                  key: Key(
                                    getCurrentUserName.toString(),
                                  ),
                                ),
                                Text(
                                  'User Name: ',
                                  style: GoogleFonts.roboto(
                                      color: Colors.grey[600],
                                      fontWeight: FontWeight.bold,
                                      fontSize: 14),
                                ),
                              ],
                            ),
                          ),
                          Container(
                            width: 120,
                            child: Column(
                              children: [
                                TextFormField(
                                  controller: _gender..text = _gender.text,
                                  style: GoogleFonts.roboto(
                                      color: Colors.red[400],
                                      fontWeight: FontWeight.w600,
                                      fontSize: 14),
                                  key: Key(
                                    getCurrentGender.toString(),
                                  ),
                                ),
                                Text(
                                  'Gender: ',
                                  style: GoogleFonts.roboto(
                                      color: Colors.grey[600],
                                      fontWeight: FontWeight.bold,
                                      fontSize: 14),
                                ),
                              ],
                            ),
                          ),
                          Container(
                            width: 120,
                            child: Column(
                              children: [
                                TextFormField(
                                  controller: _age..text = _age.text,
                                  style: GoogleFonts.roboto(
                                      color: Colors.red[400],
                                      fontWeight: FontWeight.w600,
                                      fontSize: 14),
                                  key: Key(
                                    getCurrentAge.toString(),
                                  ),
                                ),
                                Text(
                                  'Age: ',
                                  style: GoogleFonts.roboto(
                                      color: Colors.grey[600],
                                      fontWeight: FontWeight.bold,
                                      fontSize: 14),
                                ),
                              ],
                            ),
                          ),
                          Container(
                            width: 120,
                            child: Column(
                              children: [
                                TextFormField(
                                  controller: _bloodType
                                    ..text = _bloodType.text,
                                  style: GoogleFonts.roboto(
                                      color: Colors.red[400],
                                      fontWeight: FontWeight.w600,
                                      fontSize: 14),
                                  key: Key(
                                    getCurrentBloodType.toString(),
                                  ),
                                ),
                                Text(
                                  'Blood Type: ',
                                  style: GoogleFonts.roboto(
                                      color: Colors.grey[600],
                                      fontWeight: FontWeight.bold,
                                      fontSize: 14),
                                ),
                              ],
                            ),
                          ),
                          Container(
                            width: 120,
                            child: Column(
                              children: [
                                TextFormField(
                                  controller: _kidneySize
                                    ..text = _kidneySize.text,
                                  style: GoogleFonts.roboto(
                                      color: Colors.red[400],
                                      fontWeight: FontWeight.w600,
                                      fontSize: 14),
                                  key: Key(
                                    getCurrentKidneySize.toString(),
                                  ),
                                ),
                                Text(
                                  'Kidney Size: ',
                                  style: GoogleFonts.roboto(
                                      color: Colors.grey[600],
                                      fontWeight: FontWeight.bold,
                                      fontSize: 14),
                                ),
                              ],
                            ),
                          ),
                          Container(
                            width: 120,
                            child: Column(
                              children: [
                                TextFormField(
                                  controller: _userType..text = _userType.text,
                                  style: GoogleFonts.roboto(
                                      color: Colors.red[400],
                                      fontWeight: FontWeight.w600,
                                      fontSize: 14),
                                  key: Key(
                                    getCurrentUserType.toString(),
                                  ),
                                ),
                                Text(
                                  'User Type: ',
                                  style: GoogleFonts.roboto(
                                      color: Colors.grey[600],
                                      fontWeight: FontWeight.bold,
                                      fontSize: 14),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),

                    //Possible Matches Suggested
                    Container(
                      width: 460,
                      height: 550,
                      margin: EdgeInsets.only(top: 250, right: 500),
                      decoration: BoxDecoration(
                          color: Colors.red[100],
                          borderRadius: BorderRadius.circular(10)),
                      child: FutureBuilder(
                        future: getAllCurrentMatchData(),
                        builder: (context, AsyncSnapshot snapshot) {
                          if (snapshot.hasData && _userName.text.isNotEmpty) {
                            return ListView.builder(
                              itemCount: snapshot.data!.length,
                              itemBuilder: (context, index) {
                                DocumentSnapshot userData =
                                    snapshot.data[index];

                                getUserName = userData["UserName"];
                                getMatchResultPercentage =
                                    userData["Match Rate"];
                                getAge = userData["Age Percentage"];
                                getBloodType =
                                    userData["Blood Type Percentage"];
                                getKidneySize =
                                    userData["Kidney Size Percentage"];

                                int x = index + 1;

                                if (getBloodType == "0.00") {
                                  getUserName = "";
                                  getMatchResultPercentage = "";
                                  getAge = "";
                                  getBloodType = "";
                                  getKidneySize = "";
                                }

                                return Stack(
                                  children: [
                                    Container(
                                      margin: EdgeInsets.only(
                                        top: 10,
                                      ),
                                      child: ListTile(
                                        title: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceEvenly,
                                          children: [
                                            Container(
                                              margin: EdgeInsets.only(left: 10),
                                              width: 25,
                                              child: Text(
                                                x.toString(),
                                                style: GoogleFonts.roboto(
                                                    color: Colors.black87,
                                                    fontWeight: FontWeight.w400,
                                                    fontSize: 14),
                                              ),
                                            ),
                                            Container(
                                              margin: EdgeInsets.only(left: 10),
                                              width: 55,
                                              child: Text(
                                                getMatchResultPercentage!,
                                                style: GoogleFonts.roboto(
                                                    color: Colors.black87,
                                                    fontWeight: FontWeight.w400,
                                                    fontSize: 14),
                                              ),
                                            ),
                                            Container(
                                              width: 80,
                                              margin: EdgeInsets.only(left: 10),
                                              child: Text(
                                                getUserName!,
                                                style: GoogleFonts.roboto(
                                                    color: Colors.black87,
                                                    fontWeight: FontWeight.w400,
                                                    fontSize: 14),
                                              ),
                                            ),
                                            Container(
                                              width: 55,
                                              margin: EdgeInsets.only(left: 15),
                                              child: Text(
                                                getAge!,
                                                style: GoogleFonts.roboto(
                                                    color: Colors.black87,
                                                    fontWeight: FontWeight.w400,
                                                    fontSize: 14),
                                              ),
                                            ),
                                            Container(
                                              width: 50,
                                              margin: EdgeInsets.only(left: 10),
                                              child: Text(
                                                getBloodType!,
                                                style: GoogleFonts.roboto(
                                                    color: Colors.black87,
                                                    fontWeight: FontWeight.w400,
                                                    fontSize: 14),
                                              ),
                                            ),
                                            Container(
                                              width: 50,
                                              margin: EdgeInsets.only(left: 15),
                                              child: Text(
                                                getKidneySize!,
                                                style: GoogleFonts.roboto(
                                                    color: Colors.black87,
                                                    fontWeight: FontWeight.w400,
                                                    fontSize: 14),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.symmetric(
                                          horizontal: 18),
                                      child: Divider(),
                                    ),
                                  ],
                                );
                              },
                            );
                          } else {
                            return Container(
                              alignment: Alignment.center,
                              child: Text(""),
                            );
                          }
                        },
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        );
      });

  //Alert Dialog For Delete
  void showCustomDialog(BuildContext context) => showDialog(
      context: context,
      builder: (BuildContext context) {
        return Dialog(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20.0)), //this right here
          child: Container(
            height: 200,
            width: 100,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  "Delete Match",
                  style: GoogleFonts.roboto(
                      color: Colors.black87,
                      fontWeight: FontWeight.bold,
                      fontSize: 18),
                ),
                Text(
                  "\nAre you sure you want to delete?",
                  style:
                      GoogleFonts.roboto(color: Colors.grey[500], fontSize: 14),
                ),
                Container(
                  margin: EdgeInsets.only(top: 40),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Container(
                        child: InkWell(
                          onTap: () {
                            Navigator.of(context).pop();
                          },
                          child: Container(
                            height: 35,
                            width: 100,
                            decoration: BoxDecoration(
                                color: Colors.red[50],
                                borderRadius: BorderRadius.circular(18)),
                            child: Center(
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Text(
                                  "Cancel",
                                  style: TextStyle(
                                      color: Colors.grey[600], fontSize: 17),
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                      Container(
                        child: InkWell(
                          onTap: () {
                            setState(() {
                              deleteMatch();
                              deleteAllMatch();
                              deleteArchiveMatch();
                              deleteAllArchiveMatch();
                              showDeleteToast();
                              Navigator.of(context).pop();
                            });
                          },
                          child: Container(
                            height: 35,
                            width: 100,
                            decoration: BoxDecoration(
                                color: Colors.red[200],
                                borderRadius: BorderRadius.circular(18)),
                            child: Center(
                              child: Text(
                                "Confirm",
                                style: TextStyle(
                                    color: Colors.red[400], fontSize: 17),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        );
      });

  //Show Delete Succes
  void showDeleteToast() =>
      toast.showToast(child: buildDeleteToast(), gravity: ToastGravity.CENTER);

  Widget buildDeleteToast() => Container(
        padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
        decoration: BoxDecoration(
          color: Colors.green[300],
          borderRadius: BorderRadius.circular(5),
        ),
        child: Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            Text(
              "Match removed!",
              style: TextStyle(color: Colors.black87, fontSize: 16),
            ),
          ],
        ),
      );

  //Show Match Success
  void showMatchToast() =>
      toast.showToast(child: buildMatchToast(), gravity: ToastGravity.CENTER);

  Widget buildMatchToast() => Container(
        padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
        decoration: BoxDecoration(
          color: Colors.green[300],
          borderRadius: BorderRadius.circular(5),
        ),
        child: Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            Text(
              "Success",
              style: TextStyle(color: Colors.black87, fontSize: 16),
            ),
          ],
        ),
      );

  //Show Post Result Success
  void showPostResultToast() => toast.showToast(
      child: buildPostResultToast(), gravity: ToastGravity.CENTER);

  Widget buildPostResultToast() => Container(
        padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
        decoration: BoxDecoration(
          color: Colors.green[300],
          borderRadius: BorderRadius.circular(5),
        ),
        child: Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            Text(
              "Results Posted",
              style: TextStyle(color: Colors.black87, fontSize: 16),
            ),
          ],
        ),
      );
  //Display Donor Registered Table
  getAllRegisteredData() async {
    final firestore = FirebaseFirestore.instance;
    QuerySnapshot user = await firestore
        .collection('Match Users')
        .doc('All')
        .collection('Lists')
        .get();
    return user.docs;
  }

  getAllTaggedData() async {
    final firestore = FirebaseFirestore.instance;
    QuerySnapshot user = await firestore
        .collection('Match Tagged Users')
        .doc('All Tagged')
        .collection('Lists')
        .get();
    return user.docs;
  }

  //Display All Matches Made
  getAllMatchData() async {
    final firestore = FirebaseFirestore.instance;
    QuerySnapshot user = await firestore.collection('Kidney Match').get();
    return user.docs;
  }

  getAllMatchesData() async {
    String? uid = getCurrentMatch!;
    final firestore = FirebaseFirestore.instance;
    QuerySnapshot user = await firestore
        .collection('Kidney Match')
        .doc(uid)
        .collection("Matches")
        .orderBy("Match Rate", descending: true)
        .get();
    return user.docs;
  }

  //Display User Matches in Matching Page
  getAllCurrentMatchData() async {
    String? uid = _userID.text;
    final firestore = FirebaseFirestore.instance;
    QuerySnapshot user = await firestore
        .collection('Kidney Match')
        .doc(uid)
        .collection('Matches')
        .orderBy("Match Rate", descending: true)
        .get();
    return user.docs;
  }

  postPossibleMatchDonor() async {
    String? uid = _userID.text;
    String? mid = getUserID;
    String formattedTime = DateFormat('kk:mm:s:a').format(now);
    String formattedDate = formatter.format(now);
    await FirebaseFirestore.instance
        .collection("Match Users")
        .doc("Donor")
        .collection("Lists")
        .doc(uid)
        .collection("Matches")
        .doc(mid)
        .set({
      "UserID": mid,
      "UserName": getUserName,
      "User Type": getUserType,
      "Match Rate": getMatchResultPercentage!,
      "Age Percentage": getAge!,
      "Blood Type Percentage": getBloodType!,
      "Kidney Size Percentage": getKidneySize!,
      "Match Date Time": formattedDate + " at " + formattedTime,
    });
  }

  postPossibleMatchPatient() async {
    String? uid = _userID.text;
    String? mid = getUserID;
    String formattedTime = DateFormat('kk:mm:s:a').format(now);
    String formattedDate = formatter.format(now);
    await FirebaseFirestore.instance
        .collection("Match Users")
        .doc("Patient")
        .collection("Lists")
        .doc(uid)
        .collection("Matches")
        .doc(mid)
        .set({
      "UserID": mid,
      "UserName": getUserName,
      "User Type": getUserType,
      "Match Rate": getMatchResultPercentage!,
      "Age Percentage": getAge!,
      "Blood Type Percentage": getBloodType!,
      "Kidney Size Percentage": getKidneySize!,
      "Match Date Time": formattedDate + " at " + formattedTime,
    });
  }

  postPossibleMatchAll() async {
    String? uid = _userID.text;
    String? mid = getUserID;
    String formattedTime = DateFormat('kk:mm:s:a').format(now);
    String formattedDate = formatter.format(now);
    await FirebaseFirestore.instance
        .collection("Match Users")
        .doc("All")
        .collection("Lists")
        .doc(uid)
        .collection("Matches")
        .doc(mid)
        .set({
      "UserID": mid,
      "UserName": getUserName,
      "User Type": getUserType,
      "Match Rate": getMatchResultPercentage!,
      "Age Percentage": getAge!,
      "Blood Type Percentage": getBloodType!,
      "Kidney Size Percentage": getKidneySize!,
      "Match Date Time": formattedDate + " at " + formattedTime,
    });
  }

  postMatch() async {
    String? uid = _userID.text;
    String formattedTime = DateFormat('kk:mm:s:a').format(now);
    String formattedDate = formatter.format(now);
    await FirebaseFirestore.instance.collection("Kidney Match").doc(uid).set({
      "UserID": _userID.text,
      "FullName": _fullName.text,
      "UserName": _userName.text,
      "User Type": _userType.text,
    });
  }

  postPossibleKidneyMatch() async {
    String? uid = _userID.text;
    String? mid = getUserID;
    String formattedTime = DateFormat('kk:mm:s:a').format(now);
    String formattedDate = formatter.format(now);
    await FirebaseFirestore.instance
        .collection("Kidney Match")
        .doc(uid)
        .collection("Matches")
        .doc(mid)
        .set({
      "UserID": mid,
      "UserName": getUserName,
      "User Type": getUserType,
      "Match Rate": getMatchResultPercentage!,
      "Age Percentage": getAge!,
      "Blood Type Percentage": getBloodType!,
      "Kidney Size Percentage": getKidneySize!,
      "Match Date Time": formattedDate + " at " + formattedTime,
    });
  }

  archiveCurrentSelectedUser() async {
    String? uid = _userID.text;
    String formattedTime = DateFormat('kk:mm:s:a').format(now);
    String formattedDate = formatter.format(now);
    await FirebaseFirestore.instance
        .collection("Archive Match Users")
        .doc(uid)
        .set({
      "UserID": _userID.text,
      "FullName": _fullName.text,
      "UserName": _userName.text,
      "User Type": _userType.text,
      "Gender": _gender.text,
      "Age": _age.text,
      "Blood Type": _bloodType.text,
      "Kidney Size": _kidneySize.text,
      "Status": "Potential Kidney Match Done",
      "Archive Date Time": formattedDate + " at " + formattedTime,
      "Email Address": _email.text,
      "Mobile Number": _mobileNumber.text,
    });
  }

  archiveCurrentSelectedUserMatches() async {
    String? uid = _userID.text;
    String? mid = getUserID;
    String formattedTime = DateFormat('kk:mm:s:a').format(now);
    String formattedDate = formatter.format(now);
    await FirebaseFirestore.instance
        .collection("Archive Match Users")
        .doc(uid)
        .collection("Matches")
        .doc(mid)
        .set({
      "UserID": mid,
      "UserName": getUserName,
      "User Type": getUserType,
      "Match Rate": getMatchResultPercentage!,
      "Age Percentage": getAge!,
      "Blood Type Percentage": getBloodType!,
      "Kidney Size Percentage": getKidneySize!,
      "Match Date Time": formattedDate + " at " + formattedTime,
    });
  }

  deleteAllArchiveCurrentSelectedUserMatches() async {
    String? uid = _userID.text;
    String? mid = getUserID;
    await FirebaseFirestore.instance
        .collection("Match Users")
        .doc("All")
        .collection("Lists")
        .doc(uid)
        .collection("Matches")
        .doc(mid)
        .delete();
  }

  deleteDonorArchiveCurrentSelectedUserMatches() async {
    String? uid = _userID.text;
    String? mid = getUserID;
    await FirebaseFirestore.instance
        .collection("Match Users")
        .doc("Donor")
        .collection("Lists")
        .doc(uid)
        .collection("Matches")
        .doc(mid)
        .delete();
  }

  deletePatientArchiveCurrentSelectedUserMatches() async {
    String? uid = _userID.text;
    String? mid = getUserID;
    await FirebaseFirestore.instance
        .collection("Match Users")
        .doc("Patient")
        .collection("Lists")
        .doc(uid)
        .collection("Matches")
        .doc(mid)
        .delete();
  }

  deleteSpecficAllMatchUser() async {
    String? uid = _userID.text;
    await FirebaseFirestore.instance
        .collection("Match Users")
        .doc("All")
        .collection("Lists")
        .doc(uid)
        .delete();
  }

  deleteSpecficDonorMatchUser() async {
    String? uid = _userID.text;
    await FirebaseFirestore.instance
        .collection("Match Users")
        .doc("Donor")
        .collection("Lists")
        .doc(uid)
        .delete();
  }

  deleteSpecficPatientMatchUser() async {
    String? uid = _userID.text;
    await FirebaseFirestore.instance
        .collection("Match Users")
        .doc("All")
        .collection("Lists")
        .doc(uid)
        .delete();
  }

  //

  deleteMatch() async {
    String? uid = getCurrentMatch;
    await FirebaseFirestore.instance
        .collection("Kidney Match")
        .doc(uid)
        .delete();
  }

  deleteAllMatch() async {
    String? uid = getCurrentMatch;
    String? mid = getAllCurrentMatch;
    await FirebaseFirestore.instance
        .collection("Kidney Match")
        .doc(uid)
        .collection("Matches")
        .doc(mid)
        .delete();
  }

  deleteArchiveMatch() async {
    String? uid = getCurrentMatch;

    await FirebaseFirestore.instance
        .collection("Archive Match Users")
        .doc(uid)
        .delete();
  }

  deleteAllArchiveMatch() async {
    String? uid = getCurrentMatch;
    String? mid = getAllCurrentMatch;
    await FirebaseFirestore.instance
        .collection("Archive Match Users")
        .doc(uid)
        .collection("Matches")
        .doc(mid)
        .delete();
  }

  logAdminFindMatch() async {
    String formattedTime = DateFormat('kk:mm:s:a').format(now);
    String formattedDate = formatter.format(now);
    await FirebaseFirestore.instance.collection("Logs").add({
      'Message': "Admin Match Success!",
      'User': "Official Admin",
      'Logged Date': formattedDate,
      'Logged Time': formattedTime,
    });
  }

  logAdminPostedResults() async {
    String formattedTime = DateFormat('kk:mm:s:a').format(now);
    String formattedDate = formatter.format(now);
    await FirebaseFirestore.instance.collection("Logs").add({
      'Message': "Admin Posted the Results",
      'User': "Official Admin",
      'Logged Date': formattedDate,
      'Logged Time': formattedTime,
    });
  }
}
