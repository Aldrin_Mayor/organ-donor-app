import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:intl/intl.dart';

class AdminPostsPage extends StatefulWidget {
  AdminPostsPage({Key? key}) : super(key: key);

  @override
  _AdminPostsPageState createState() => _AdminPostsPageState();
}

class _AdminPostsPageState extends State<AdminPostsPage> {
  String? usersListValue;

  final usersList = [
    'All Posts',
    'Donor Only',
    'Patient Only',
    'Admin Only',
  ];

  String? donorUserID;
  String? donorCountPosts;
  String? patientUserID;
  String? patientCountPosts;
  String? currentUserID;
  String? currentCountPosts;
  String? userCountPosts;
  String? currentUserType;
  String? currentUsersValue;

  String? getUid;

  // For Passing the data
  String? getFullName;
  String? getUserName;
  String? getDate;
  String? getTime;
  String? getUserID;
  String? getPostTitle;
  String? getPostContent;
  String? getUserType;
  String? getPostViews;
  String? getPostComments;
  String? getNotification;
  String? getPostLikes;
  String? countNotification;

  //Admin Constant Values
  String? userName = "Official Admin";
  String? usersValue = "Admin";
  String? userID = "SuperAdmin";

  //Toast
  final toast = FToast();
  @override
  void initState() {
    super.initState();

    toast.init(context);
  }

  String? countPost = "0";
  final formKey = GlobalKey<FormState>();
  String? title = "";
  String? textTitle = "";

  TextEditingController _title = TextEditingController();
  TextEditingController _content = TextEditingController();

  //Time and Date
  var now = new DateTime.now();
  var formatter = new DateFormat('dd-MM-yyyy');

  //For Reply
  TextEditingController _reply = TextEditingController();
  String? reply = "";
  bool isButtonEnabled = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xffe5eef8),
      body: SingleChildScrollView(
        child: Wrap(
          spacing: 8.0, // gap between adjacent chips
          runSpacing: 4.0, // gap between lines
          direction: Axis.horizontal,
          children: <Widget>[
            Container(
              margin: EdgeInsets.only(top: 60, left: 100),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    'Posts',
                    style: GoogleFonts.roboto(
                        color: Colors.black54,
                        fontSize: 40,
                        fontWeight: FontWeight.w400),
                  ),
                  Container(
                    margin: EdgeInsets.only(left: 800),
                    height: 40,
                    width: 170,
                    decoration: BoxDecoration(
                      color: Colors.red,
                      borderRadius: BorderRadius.circular(20),
                    ),
                    child: Row(
                      children: [
                        Container(
                          margin: EdgeInsets.only(left: 10),
                          child: Icon(
                            Icons.add,
                            color: Colors.red[100],
                          ),
                        ),
                        InkWell(
                          onTap: () {
                            showAddPostsCustomDialog(context);
                          },
                          child: Container(
                            margin: EdgeInsets.only(left: 10),
                            child: Text(
                              'Create Post'.toUpperCase(),
                              style: GoogleFonts.roboto(
                                  color: Colors.white, fontSize: 16),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(right: 70),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      color: Colors.grey[300],
                    ),
                    width: 240,
                    child: DropdownButtonHideUnderline(
                      child: Stack(
                        children: [
                          Row(
                            children: [
                              Container(
                                height: 45,
                                width: 45,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.only(
                                    topLeft: Radius.circular(10),
                                    bottomLeft: Radius.circular(10),
                                  ),
                                  color: Colors.grey[400],
                                ),
                                child: Icon(Icons.filter_alt),
                              ),
                              Expanded(
                                child: DropdownButtonFormField<String>(
                                  decoration: InputDecoration(
                                      border: InputBorder.none,
                                      fillColor: Colors.red),
                                  hint: Container(
                                    margin: EdgeInsets.only(left: 20),
                                    child: Text(
                                      "Filter Users",
                                      style: GoogleFonts.roboto(
                                          color: Colors.black54,
                                          fontWeight: FontWeight.w500,
                                          fontSize: 16),
                                    ),
                                  ),
                                  value: usersListValue,
                                  isExpanded: true,
                                  items: usersList
                                      .map(buildUserListMenuItem)
                                      .toList(),
                                  onChanged: (value) => setState(
                                    () => this.usersListValue = value,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Container(
              height: MediaQuery.of(context).size.height * 3,
              width: MediaQuery.of(context).size.width * 0.755,
              margin: EdgeInsets.only(top: 20, left: 50, bottom: 100),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: Color(0xffe5eef8),
              ),
              child: Stack(
                children: [
                  //Contents
                  FutureBuilder(
                    future: getPostsData(),
                    builder: (context, AsyncSnapshot snapshot) {
                      if (snapshot.hasData) {
                        return ListView.builder(
                          itemCount: snapshot.data!.length,
                          itemBuilder: (context, index) {
                            DocumentSnapshot userData = snapshot.data[index];
                            getUid = userData["UserID"];

                            return Stack(
                              children: [
                                Container(
                                  decoration: BoxDecoration(
                                      color: Colors.white,
                                      borderRadius: BorderRadius.circular(10)),
                                  margin: EdgeInsets.only(
                                      top: 20, left: 10, right: 10),
                                  child: ListTile(
                                    title: Column(
                                      children: [
                                        Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: [
                                            Column(
                                              children: [
                                                Row(
                                                  children: [
                                                    Container(
                                                      margin: EdgeInsets.only(
                                                          top: 10),
                                                      child: Image.asset(
                                                        'assets/user.png',
                                                        height: 30,
                                                      ),
                                                    ),
                                                    Container(
                                                      margin: EdgeInsets.only(
                                                          top: 20, left: 10),
                                                      child: Column(
                                                        crossAxisAlignment:
                                                            CrossAxisAlignment
                                                                .start,
                                                        children: [
                                                          //UserName
                                                          Container(
                                                            child: Row(
                                                              children: [
                                                                Text(
                                                                  userData[
                                                                      "UserName"],
                                                                  style: GoogleFonts.roboto(
                                                                      color: Colors
                                                                          .black87,
                                                                      fontWeight:
                                                                          FontWeight
                                                                              .w600,
                                                                      fontSize:
                                                                          14),
                                                                ),
                                                                //Verified Logo
                                                                Container(
                                                                  margin: EdgeInsets
                                                                      .only(
                                                                          left:
                                                                              5),
                                                                  child: Icon(
                                                                      Icons
                                                                          .verified_outlined,
                                                                      size: 20,
                                                                      color: Colors
                                                                          .green),
                                                                ),
                                                              ],
                                                            ),
                                                          ),
                                                          Container(
                                                            margin:
                                                                EdgeInsets.only(
                                                                    top: 5),
                                                            child: Row(
                                                              children: [
                                                                Text(
                                                                  userData[
                                                                      "Date"],
                                                                  style: GoogleFonts.roboto(
                                                                      color: Colors
                                                                          .black54,
                                                                      fontWeight:
                                                                          FontWeight
                                                                              .w600,
                                                                      fontSize:
                                                                          12),
                                                                ),
                                                                Container(
                                                                  margin: EdgeInsets
                                                                      .only(
                                                                          left:
                                                                              2),
                                                                  child: Text(
                                                                    userData[
                                                                        "Time"],
                                                                    style: GoogleFonts.roboto(
                                                                        color: Colors
                                                                            .black54,
                                                                        fontWeight:
                                                                            FontWeight
                                                                                .w600,
                                                                        fontSize:
                                                                            12),
                                                                  ),
                                                                ),
                                                              ],
                                                            ),
                                                          ),
                                                        ],
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ],
                                            ),

                                            //Setting Action
                                            InkWell(
                                              onTap: () {
                                                getUserID = userData["UserID"];
                                                getUserName =
                                                    userData["UserName"];
                                                getFullName =
                                                    userData["FullName"];
                                                getUserType =
                                                    userData["User Type"];
                                                getDate = userData["Date"];
                                                getTime = userData["Time"];
                                                getPostTitle =
                                                    userData["Post Title"];
                                                getPostContent =
                                                    userData["Post Content"];
                                                showModalBottomSheet(
                                                  context: context,
                                                  builder: (context) =>
                                                      buildCurrentUserSheet(),
                                                );
                                              },
                                              child: Icon(
                                                  Icons.more_vert_outlined,
                                                  size: 20,
                                                  color: Colors.grey),
                                            ),
                                          ],
                                        ),
                                        Row(
                                          children: [
                                            Container(
                                              width: MediaQuery.of(context)
                                                      .size
                                                      .width *
                                                  0.7,
                                              margin: EdgeInsets.only(
                                                  top: 20, left: 0),
                                              child: Text(
                                                userData["Post Title"],
                                                style: GoogleFonts.roboto(
                                                    color: Colors.black87,
                                                    fontWeight: FontWeight.w600,
                                                    fontSize: 16),
                                              ),
                                            ),
                                          ],
                                        ),
                                        Row(
                                          children: [
                                            Container(
                                              width: MediaQuery.of(context)
                                                      .size
                                                      .width *
                                                  0.7,
                                              margin: EdgeInsets.only(
                                                top: 20,
                                                left: 0,
                                              ),
                                              child: Text(
                                                userData["Post Content"],
                                                style: GoogleFonts.roboto(
                                                    color: Colors.black87,
                                                    fontWeight: FontWeight.w400,
                                                    fontSize: 14),
                                              ),
                                            ),
                                          ],
                                        ),
                                        //views, comment and Like
                                        Container(
                                          margin: EdgeInsets.only(
                                              top: 20, bottom: 10),
                                          child: Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceEvenly,
                                            children: [
                                              //Views
                                              InkWell(
                                                onTap: () {},
                                                child: Row(
                                                  children: [
                                                    //Views Icon
                                                    Icon(
                                                      Icons
                                                          .remove_red_eye_rounded,
                                                      color: Colors.grey,
                                                    ),
                                                    //Views Count
                                                    Container(
                                                      margin: EdgeInsets.only(
                                                          left: 10),
                                                      child: Text(
                                                          userData["Views"],
                                                          style: GoogleFonts
                                                              .roboto(
                                                                  color: Colors
                                                                      .grey,
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .w400,
                                                                  fontSize:
                                                                      14)),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                              //Comments
                                              InkWell(
                                                onTap: () {
                                                  getUserID =
                                                      userData["UserID"];
                                                  getUserName =
                                                      userData["UserName"];
                                                  getFullName =
                                                      userData["FullName"];
                                                  getUserType =
                                                      userData["User Type"];
                                                  getDate = userData["Date"];
                                                  getTime = userData["Time"];
                                                  getPostTitle =
                                                      userData["Post Title"];
                                                  getPostContent =
                                                      userData["Post Content"];
                                                  getPostViews =
                                                      userData["Views"];
                                                  getPostComments =
                                                      userData["Comments"];
                                                  getPostLikes =
                                                      userData["Likes"];

                                                  showCommentsCustomDialog(
                                                      context);

                                                  // For counting Views to Posts
                                                  if (getUserType ==
                                                      'I am a Donor') {
                                                    setState(() {
                                                      int x = int.parse(
                                                              getPostViews!) +
                                                          1;
                                                      getPostViews =
                                                          x.toString();

                                                      viewsAddDonor();
                                                      viewsAddAll();
                                                      viewsAddSpecificDonor();
                                                      viewsAddSpecificAll();
                                                      logAdminViewUsersPosts();
                                                    });
                                                  } else if (getUserType ==
                                                      'I am a Patient') {
                                                    setState(() {
                                                      int x = int.parse(
                                                              getPostViews!) +
                                                          1;
                                                      getPostViews =
                                                          x.toString();

                                                      viewsAddPatient();
                                                      viewsAddAll();
                                                      viewsAddSpecificPatient();
                                                      viewsAddSpecificAll();
                                                      logAdminViewUsersPosts();
                                                    });
                                                  } else {
                                                    setState(() {
                                                      int x = int.parse(
                                                              getPostViews!) +
                                                          1;
                                                      getPostViews =
                                                          x.toString();
                                                      viewsAddAdmin();
                                                      viewsAddAll();
                                                      logAdminViewAdminPosts();
                                                    });
                                                  }
                                                },
                                                child: Row(
                                                  children: [
                                                    //Comments Icon
                                                    Icon(
                                                      Icons.comment_outlined,
                                                      color: Colors.grey,
                                                    ),
                                                    //Comments Count
                                                    Container(
                                                      margin: EdgeInsets.only(
                                                          left: 10),
                                                      child: Text(
                                                          userData["Comments"],
                                                          style: GoogleFonts
                                                              .roboto(
                                                                  color: Colors
                                                                      .grey,
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .w400,
                                                                  fontSize:
                                                                      14)),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                              //Likes
                                              InkWell(
                                                onTap: () {
                                                  getUserID =
                                                      userData["UserID"];
                                                  getUserName =
                                                      userData["UserName"];
                                                  getFullName =
                                                      userData["FullName"];
                                                  getUserType =
                                                      userData["User Type"];
                                                  getDate = userData["Date"];
                                                  getTime = userData["Time"];
                                                  getPostTitle =
                                                      userData["Post Title"];
                                                  getPostContent =
                                                      userData["Post Content"];
                                                  getPostViews =
                                                      userData["Views"];
                                                  getPostComments =
                                                      userData["Comments"];
                                                  getPostLikes =
                                                      userData["Likes"];
                                                  getNotification =
                                                      userData["Notifications"];

                                                  if (getUserType ==
                                                      'I am a Donor') {
                                                    setState(() {
                                                      int x = int.parse(
                                                              getPostLikes!) +
                                                          1;
                                                      getPostLikes =
                                                          x.toString();
                                                      likesAddDonor();
                                                      likesAddAll();
                                                      likesAddSpecificDonor();
                                                      likesAddSpecificAll();
                                                      likesAddSpecificProfileDonor();
                                                      likesAddSpecificProfileAll();
                                                      logAdminLikeUsersPosts();
                                                      //Notification count
                                                      int y = int.parse(
                                                              getNotification!) +
                                                          1;
                                                      countNotification =
                                                          y.toString();
                                                      notificationAddDonor();
                                                      notificationAddAll();
                                                      notificationAddSpecificDonor();
                                                      notificationAddSpecificAll();
                                                    });
                                                  } else if (getUserType ==
                                                      'I am a Patient') {
                                                    setState(() {
                                                      int x = int.parse(
                                                              getPostLikes!) +
                                                          1;
                                                      getPostLikes =
                                                          x.toString();
                                                      likesAddPatient();
                                                      likesAddAll();
                                                      likesAddSpecificPatient();
                                                      likesAddSpecificAll();
                                                      likesAddSpecificProfilePatient();
                                                      likesAddSpecificProfileAll();
                                                      logAdminLikeUsersPosts();

                                                      //Notification count
                                                      int y = int.parse(
                                                              getNotification!) +
                                                          1;
                                                      countNotification =
                                                          y.toString();
                                                      notificationAddPatient();
                                                      notificationAddAll();
                                                      notificationAddSpecificPatient();
                                                      notificationAddSpecificAll();
                                                    });
                                                  } else {
                                                    setState(() {
                                                      int x = int.parse(
                                                              getPostLikes!) +
                                                          1;
                                                      getPostLikes =
                                                          x.toString();
                                                      likesAddAdmin();
                                                      likesAddAll();
                                                      logAdminLikeAdminPosts();
                                                    });
                                                  }
                                                },
                                                child: Row(
                                                  children: [
                                                    //Likes Icon
                                                    Icon(
                                                      Icons.thumb_up_outlined,
                                                      color: Colors.grey,
                                                    ),
                                                    //Likes Count
                                                    Container(
                                                      margin: EdgeInsets.only(
                                                          left: 10),
                                                      child: Text(
                                                          userData["Likes"],
                                                          style: GoogleFonts
                                                              .roboto(
                                                                  color: Colors
                                                                      .grey,
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .w400,
                                                                  fontSize:
                                                                      14)),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            ],
                                          ),
                                        )
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            );
                          },
                        );
                      } else {
                        return Text('Users Data Not Available');
                      }
                    },
                  ),

                  // Checks if Current user id matches with his post
                  FutureBuilder(
                    future: getUserData(),
                    builder: (context, snapshot) {
                      if (snapshot.connectionState != ConnectionState.done)
                        return Text("");
                      if (donorUserID != null) {
                        currentUserID = donorUserID;
                        return Container(
                            height: 20,
                            width: 240,
                            child: Text(
                              '',
                              style: GoogleFonts.roboto(
                                  color: Colors.black87,
                                  fontSize: 15,
                                  fontWeight: FontWeight.w500),
                            ));
                      }
                      if (patientUserID != null) {
                        currentUserID = patientUserID;
                        return Container(
                            height: 20,
                            width: 240,
                            child: Text(
                              '',
                              style: GoogleFonts.roboto(
                                  color: Colors.black87,
                                  fontSize: 15,
                                  fontWeight: FontWeight.w500),
                            ));
                      } else {
                        return Text(
                          "",
                          style: GoogleFonts.roboto(
                              color: Colors.black54, fontSize: 14),
                        );
                      }
                    },
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  //Filter Users DropdownMenu
  DropdownMenuItem<String> buildUserListMenuItem(String usersList) =>
      DropdownMenuItem(
        value: usersList,
        child: Container(
          child: Text(
            '   ' + usersList,
            style: GoogleFonts.roboto(color: Colors.black, fontSize: 15),
          ),
        ),
      );

  //Get User Information
  getUserData() async {
    final user = getUserID;
    await FirebaseFirestore.instance
        .collection('Users')
        .doc("All")
        .collection("Lists")
        .doc(user)
        .get()
        .then((value) {
      currentUserID = value.data()!['UserID'];
      currentCountPosts = value.data()!['Posts'];
      currentUserType = value.data()!['User Type'];
    }).catchError((e) {
      print(e);
    });
  }

  //Display Posts Table
  getPostsData() async {
    final firestore = FirebaseFirestore.instance;
    if (usersListValue == 'All Posts') {
      QuerySnapshot user = await firestore
          .collection('Posts')
          .doc('All')
          .collection('All Posts')
          .get();
      return user.docs;
    } else if (usersListValue == 'Donor Only') {
      QuerySnapshot user = await firestore
          .collection('Posts')
          .doc('Donor')
          .collection('All Posts')
          .get();
      return user.docs;
    } else if (usersListValue == 'Patient Only') {
      QuerySnapshot user = await firestore
          .collection('Posts')
          .doc('Patient')
          .collection('All Posts')
          .get();
      return user.docs;
    } else if (usersListValue == 'Admin Only') {
      QuerySnapshot user = await firestore
          .collection('Posts')
          .doc('Admin')
          .collection('All Posts')
          .get();
      return user.docs;
    } else {
      QuerySnapshot user = await firestore
          .collection('Posts')
          .doc('All')
          .collection('All Posts')
          .get();
      return user.docs;
    }
  }

  //Delete
  deleteDonorPost() async {
    String? uid = getTime;
    await FirebaseFirestore.instance
        .collection("Posts")
        .doc('Donor')
        .collection('All Posts')
        .doc(uid)
        .delete();
  }

  deletePatientPost() async {
    String? uid = getTime;
    await FirebaseFirestore.instance
        .collection("Posts")
        .doc('Patient')
        .collection('All Posts')
        .doc(uid)
        .delete();
  }

  deleteAlltPost() async {
    String? uid = getTime;
    await FirebaseFirestore.instance
        .collection("Posts")
        .doc('All')
        .collection('All Posts')
        .doc(uid)
        .delete();
  }

  deleteSpecificDonorPost() async {
    String? uid = getUserID;
    String? did = getTime;
    await FirebaseFirestore.instance
        .collection("Users")
        .doc('Donor')
        .collection('Lists')
        .doc(uid)
        .collection('Posts')
        .doc(did)
        .delete();
  }

  deleteSpecificPatientPost() async {
    String? uid = getUserID;
    String? did = getTime;
    await FirebaseFirestore.instance
        .collection("Users")
        .doc('Patient')
        .collection('Lists')
        .doc(uid)
        .collection('Posts')
        .doc(did)
        .delete();
  }

  deleteSpecificAllPost() async {
    String? uid = getUserID;
    String? did = getTime;
    await FirebaseFirestore.instance
        .collection("Users")
        .doc('All')
        .collection('Lists')
        .doc(uid)
        .collection('Posts')
        .doc(did)
        .delete();
  }

  deleteAdminPost() async {
    String? uid = getTime;
    await FirebaseFirestore.instance
        .collection("Posts")
        .doc('Admin')
        .collection('All Posts')
        .doc(uid)
        .delete();
  }

  Widget buildCurrentUserSheet() => Container(
        height: 130,
        child: ListView(
          children: [
            //Edit
            ListTile(
              title: Row(
                children: [
                  Text(
                    'Edit Post',
                    style: GoogleFonts.roboto(
                        color: Colors.black87,
                        fontSize: 20,
                        fontWeight: FontWeight.w400),
                  ),
                  Container(
                    margin: EdgeInsets.only(left: 10),
                    child: Icon(
                      Icons.reply_outlined,
                      color: Colors.red[100],
                    ),
                  ),
                ],
              ),
              onTap: () {
                showEditPostsCustomDialog(context);
              },
            ),
            //Delete
            ListTile(
              title: Row(
                children: [
                  Text(
                    'Delete Post',
                    style: GoogleFonts.roboto(
                        color: Colors.black87,
                        fontSize: 20,
                        fontWeight: FontWeight.w400),
                  ),
                  Container(
                    margin: EdgeInsets.only(left: 10),
                    child: Icon(
                      Icons.delete_outline,
                      color: Colors.red[100],
                    ),
                  ),
                  // Checking
                  FutureBuilder(
                    future: getUserData(),
                    builder: (context, snapshot) {
                      if (snapshot.connectionState != ConnectionState.done)
                        return Text("");

                      if (getUserID != null) {
                        userCountPosts = currentCountPosts;
                        return Container(
                            height: 20,
                            width: 240,
                            child: Text(
                              '',
                              style: GoogleFonts.roboto(
                                  color: Colors.black87,
                                  fontSize: 15,
                                  fontWeight: FontWeight.w500),
                            ));
                      } else {
                        return Text(
                          "",
                          style: GoogleFonts.roboto(
                              color: Colors.black54, fontSize: 14),
                        );
                      }
                    },
                  ),
                ],
              ),
              onTap: () {
                showCustomDialog(context);
              },
            ),
          ],
        ),
      );

  //Alert Dialog For Delete
  void showCustomDialog(BuildContext context) => showDialog(
      context: context,
      builder: (BuildContext context) {
        return Dialog(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20.0)), //this right here
          child: Container(
            height: 200,
            width: 100,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  "Delete Post",
                  style: GoogleFonts.roboto(
                      color: Colors.black87,
                      fontWeight: FontWeight.bold,
                      fontSize: 18),
                ),
                Text(
                  "\nAre you sure you want to delete?",
                  style:
                      GoogleFonts.roboto(color: Colors.grey[500], fontSize: 14),
                ),
                Container(
                  margin: EdgeInsets.only(top: 40),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Container(
                        child: InkWell(
                          onTap: () {
                            Navigator.of(context).pop();
                          },
                          child: Container(
                            height: 35,
                            width: 100,
                            decoration: BoxDecoration(
                                color: Colors.red[50],
                                borderRadius: BorderRadius.circular(18)),
                            child: Center(
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Text(
                                  "Cancel",
                                  style: TextStyle(
                                      color: Colors.grey[600], fontSize: 17),
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                      Container(
                        child: InkWell(
                          onTap: () {
                            setState(() {
                              if (getUserType == 'I am a Donor') {
                                //Post Count Minus

                                int x = int.parse(userCountPosts!) - 1;
                                countPost = x.toString();
                                postMinusDonor();
                                postMinusAll();

                                //Delete
                                deleteDonorPost();
                                deleteAlltPost();
                                deleteSpecificDonorPost();
                                deleteSpecificAllPost();
                                Navigator.of(context).pop();
                                Navigator.of(context).pop();
                                showDeleteToast();
                              } else if (getUserType == 'I am a Patient') {
                                //Post Count Minus

                                int x = int.parse(userCountPosts!) - 1;
                                countPost = x.toString();
                                postMinusPatient();
                                postMinusAll();

                                //Delete
                                deletePatientPost();
                                deleteAlltPost();
                                deleteSpecificPatientPost();
                                deleteSpecificAllPost();
                                Navigator.of(context).pop();
                                Navigator.of(context).pop();
                                showDeleteToast();
                              } else {
                                deleteAdminPost();
                                deleteAlltPost();
                                Navigator.of(context).pop();
                                Navigator.of(context).pop();
                                showDeleteToast();
                              }
                            });
                          },
                          child: Container(
                            height: 35,
                            width: 100,
                            decoration: BoxDecoration(
                                color: Colors.red[200],
                                borderRadius: BorderRadius.circular(18)),
                            child: Center(
                              child: Text(
                                "Confirm",
                                style: TextStyle(
                                    color: Colors.red[400], fontSize: 17),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        );
      });

  //Show Delete Success
  void showDeleteToast() =>
      toast.showToast(child: buildDeleteToast(), gravity: ToastGravity.CENTER);

  Widget buildDeleteToast() => Container(
        padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
        decoration: BoxDecoration(
          color: Colors.green[300],
          borderRadius: BorderRadius.circular(5),
        ),
        child: Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            Text(
              "Deleted",
              style: TextStyle(color: Colors.black87, fontSize: 16),
            ),
          ],
        ),
      );

  //Show Toast Message
  void showToast() =>
      toast.showToast(child: buildToast(), gravity: ToastGravity.CENTER);

  Widget buildToast() => Container(
        padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
        decoration: BoxDecoration(
          color: Color(0xFF111111).withOpacity(0.6),
          borderRadius: BorderRadius.circular(5),
        ),
        child: Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            Text(
              "Saved",
              style: TextStyle(color: Colors.white, fontSize: 16),
            ),
          ],
        ),
      );

  //Post Utilities

  //Views Count
  viewsAddDonor() async {
    String? did = getTime;
    await FirebaseFirestore.instance
        .collection("Posts")
        .doc('Donor')
        .collection('All Posts')
        .doc(did)
        .update({
      'Views': getPostViews,
    });
  }

  viewsAddPatient() async {
    String? did = getTime;
    await FirebaseFirestore.instance
        .collection("Posts")
        .doc('Patient')
        .collection('All Posts')
        .doc(did)
        .update({
      'Views': getPostViews,
    });
  }

  viewsAddAdmin() async {
    String? did = getTime;
    await FirebaseFirestore.instance
        .collection("Posts")
        .doc('Admin')
        .collection('All Posts')
        .doc(did)
        .update({
      'Views': getPostViews,
    });
  }

  viewsAddAll() async {
    String? did = getTime;
    await FirebaseFirestore.instance
        .collection("Posts")
        .doc('All')
        .collection('All Posts')
        .doc(did)
        .update({
      'Views': getPostViews,
    });
  }

  viewsAddSpecificDonor() async {
    String? uid = getUserID;
    String? did = getTime;
    await FirebaseFirestore.instance
        .collection("Users")
        .doc('Donor')
        .collection('Lists')
        .doc(uid)
        .collection('Posts')
        .doc(did)
        .update({
      'Views': getPostViews,
    });
  }

  viewsAddSpecificPatient() async {
    String? uid = getUserID;
    String? did = getTime;
    await FirebaseFirestore.instance
        .collection("Users")
        .doc('Patient')
        .collection('Lists')
        .doc(uid)
        .collection('Posts')
        .doc(did)
        .update({
      'Views': getPostViews,
    });
  }

  viewsAddSpecificAll() async {
    String? uid = getUserID;
    String? did = getTime;
    await FirebaseFirestore.instance
        .collection("Users")
        .doc('All')
        .collection('Lists')
        .doc(uid)
        .collection('Posts')
        .doc(did)
        .update({
      'Views': getPostViews,
    });
  }

  //Likes Counter
  likesAddDonor() async {
    String? did = getTime;
    await FirebaseFirestore.instance
        .collection("Posts")
        .doc('Donor')
        .collection('All Posts')
        .doc(did)
        .update({
      'Likes': getPostLikes,
    });
  }

  likesAddPatient() async {
    String? did = getTime;
    await FirebaseFirestore.instance
        .collection("Posts")
        .doc('Patient')
        .collection('All Posts')
        .doc(did)
        .update({
      'Likes': getPostLikes,
    });
  }

  likesAddAdmin() async {
    String? did = getTime;
    await FirebaseFirestore.instance
        .collection("Posts")
        .doc('Admin')
        .collection('All Posts')
        .doc(did)
        .update({
      'Likes': getPostLikes,
    });
  }

  likesAddAll() async {
    String? did = getTime;
    await FirebaseFirestore.instance
        .collection("Posts")
        .doc('All')
        .collection('All Posts')
        .doc(did)
        .update({
      'Likes': getPostLikes,
    });
  }

  likesAddSpecificDonor() async {
    String? uid = getUserID;
    String? did = getTime;
    await FirebaseFirestore.instance
        .collection("Users")
        .doc('Donor')
        .collection('Lists')
        .doc(uid)
        .collection('Posts')
        .doc(did)
        .update({
      'Likes': getPostLikes,
    });
  }

  likesAddSpecificPatient() async {
    String? uid = getUserID;
    String? did = getTime;
    await FirebaseFirestore.instance
        .collection("Users")
        .doc('Patient')
        .collection('Lists')
        .doc(uid)
        .collection('Posts')
        .doc(did)
        .update({
      'Likes': getPostLikes,
    });
  }

  likesAddSpecificAll() async {
    String? uid = getUserID;
    String? did = getTime;
    await FirebaseFirestore.instance
        .collection("Users")
        .doc('All')
        .collection('Lists')
        .doc(uid)
        .collection('Posts')
        .doc(did)
        .update({
      'Likes': getPostLikes,
    });
  }

  //Likes added to User Profile
  likesAddSpecificProfileDonor() async {
    String? uid = getUserID;

    await FirebaseFirestore.instance
        .collection("Users")
        .doc('Donor')
        .collection('Lists')
        .doc(uid)
        .update({
      'Likes': getPostLikes,
    });
  }

  likesAddSpecificProfilePatient() async {
    String? uid = getUserID;

    await FirebaseFirestore.instance
        .collection("Users")
        .doc('Patient')
        .collection('Lists')
        .doc(uid)
        .update({
      'Likes': getPostLikes,
    });
  }

  likesAddSpecificProfileAll() async {
    String? uid = getUserID;

    await FirebaseFirestore.instance
        .collection("Users")
        .doc('All')
        .collection('Lists')
        .doc(uid)
        .update({
      'Likes': getPostLikes,
    });
  }

  //Count Posts
  postMinusDonor() async {
    String? uid = getUserID;
    await FirebaseFirestore.instance
        .collection("Users")
        .doc('Donor')
        .collection('Lists')
        .doc(uid)
        .update({
      'Posts': countPost,
    });
  }

  postMinusPatient() async {
    String? uid = getUserID;
    await FirebaseFirestore.instance
        .collection("Users")
        .doc('Patient')
        .collection('Lists')
        .doc(uid)
        .update({
      'Posts': countPost,
    });
  }

  postMinusAll() async {
    String? uid = getUserID;
    await FirebaseFirestore.instance
        .collection("Users")
        .doc('All')
        .collection('Lists')
        .doc(uid)
        .update({
      'Posts': countPost,
    });
  }

  //Add Post by Admin
  void showAddPostsCustomDialog(BuildContext context) => showDialog(
      context: context,
      builder: (BuildContext context) {
        return Dialog(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10.0)), //this right here
          child: Container(
            height: MediaQuery.of(context).size.height * 0.6,
            width: MediaQuery.of(context).size.width * 0.4,
            child: Form(
              key: formKey,
              child: Column(
                children: [
                  //Close Icon
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      Container(
                        margin: EdgeInsets.only(top: 10, right: 20),
                        child: InkWell(
                          onTap: () {
                            setState(() {
                              Navigator.of(context).pop();
                            });
                          },
                          child: Icon(
                            Icons.close_outlined,
                            size: 25,
                            color: Colors.red[100],
                          ),
                        ),
                      ),
                    ],
                  ),
                  //Header
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Container(
                        margin: EdgeInsets.only(top: 10, left: 20),
                        child: Row(
                          children: [
                            Icon(
                              Icons.search,
                              size: 30,
                              color: Colors.red[100],
                            ),
                            Container(
                              margin: EdgeInsets.only(left: 10),
                              child: Text(
                                'Find Kidney Donor or Patient...'.toUpperCase(),
                                style: GoogleFonts.roboto(
                                    color: Colors.black87,
                                    fontSize: 30,
                                    fontWeight: FontWeight.w700,
                                    letterSpacing: 2),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                  //Post Title
                  Container(
                    margin: EdgeInsets.only(top: 50, left: 15, right: 15),
                    child: TextFormField(
                      decoration: InputDecoration(
                        filled: true,
                        fillColor: Color(0xffe5eef8),
                        errorStyle:
                            TextStyle(fontSize: 14, color: Colors.red[400]),
                        hintText: 'An Interesting Title',
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10.0),
                          borderSide: BorderSide.none,
                        ),
                      ),

                      controller: _title..text = "",
                      key: Key(title.toString()), // <- Magic!
                      maxLines: 2,

                      validator: (value) {
                        if (value!.isEmpty) {
                          return 'This Section is Required';
                        } else {
                          return null;
                        }
                      },
                    ),
                  ),

                  Padding(
                    padding: const EdgeInsets.only(left: 15, right: 15),
                    child: Divider(color: Colors.grey),
                  ),
                  //Text Post
                  Container(
                    margin: EdgeInsets.only(top: 10, left: 15, right: 15),
                    child: TextFormField(
                      decoration: InputDecoration(
                          filled: true,
                          fillColor: Color(0xffe5eef8),
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10.0),
                            borderSide: BorderSide.none,
                          ),
                          hintText: 'Your text post (Optional)'),
                      controller: _content..text = "",
                      key: Key(textTitle.toString()), // <- Magic!
                      maxLines: 5,
                    ),
                  ),

                  Container(
                    margin: EdgeInsets.only(top: 100),
                    decoration: BoxDecoration(
                        color: Colors.red[100],
                        borderRadius: BorderRadius.circular(30)),
                    height: 50,
                    width: 400,
                    child: InkWell(
                      onTap: () {
                        final isValid = formKey.currentState!.validate();

                        if (isValid) {
                          formKey.currentState!.save();
                          adminPosts();
                          adminAllPosts();
                          showPostsSuccessToast();
                          Navigator.of(context).pop();
                          logAdminCreatePosts();

                          setState(() {});
                        }
                      },
                      child: Container(
                        alignment: Alignment.center,
                        child: Text(
                          'Post',
                          style: GoogleFonts.roboto(
                              color: Colors.red[400],
                              fontSize: 18,
                              fontWeight: FontWeight.w500,
                              letterSpacing: 2),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      });

  //edit Post by Admin
  void showEditPostsCustomDialog(BuildContext context) => showDialog(
      context: context,
      builder: (BuildContext context) {
        return Dialog(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10.0)), //this right here
          child: Container(
            height: MediaQuery.of(context).size.height * 0.6,
            width: MediaQuery.of(context).size.width * 0.4,
            child: SingleChildScrollView(
              child: Column(
                children: [
                  //Header
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        decoration: BoxDecoration(
                          color: Color(0xffe5eef8),
                          borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(10),
                              topRight: Radius.circular(10)),
                          boxShadow: [
                            BoxShadow(
                              color: Colors.grey.withOpacity(0.5),
                              blurRadius: 8,
                            ),
                          ],
                        ),
                        height: 80,
                        margin: EdgeInsets.only(top: 0, left: 0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            InkWell(
                              onTap: () {
                                Navigator.of(context).pop();
                              },
                              child: Padding(
                                padding:
                                    const EdgeInsets.only(top: 26, left: 20),
                                child: Row(
                                  children: [
                                    Icon(Icons.close_outlined, size: 25.0),
                                    Container(
                                      margin: EdgeInsets.only(left: 20),
                                      child: Text(
                                        "Edit Post",
                                        style: GoogleFonts.roboto(
                                          color: Colors.black87,
                                          fontWeight: FontWeight.w500,
                                          fontSize: 18,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            InkWell(
                              onTap: () {
                                setState(() {
                                  final isValid =
                                      formKey.currentState!.validate();

                                  if (isValid) {
                                    formKey.currentState!.save();
                                    if (getUserType == 'I am a Donor') {
                                      editDonorPost();
                                      editAllPost();
                                      editSpecificDonorPost();
                                      editSpecificAllPost();
                                      Navigator.of(context).pop();
                                      Navigator.of(context).pop();
                                      showToast();
                                      logAdminEditUsersPosts();
                                    } else if (getUserType ==
                                        'I am a Patient') {
                                      editPatientPost();
                                      editAllPost();
                                      editSpecificPatientPost();
                                      editSpecificAllPost();
                                      Navigator.of(context).pop();
                                      Navigator.of(context).pop();
                                      showToast();
                                      logAdminEditUsersPosts();
                                    } else {
                                      editAdminPost();
                                      editAllPost();
                                      Navigator.of(context).pop();
                                      Navigator.of(context).pop();
                                      showToast();
                                      logAdminLikeAdminPosts();
                                    }
                                  }
                                });
                              },
                              child: Padding(
                                padding:
                                    const EdgeInsets.only(top: 26, right: 20),
                                child: Text(
                                  'Save',
                                  style: GoogleFonts.roboto(
                                    color: Colors.black54,
                                    fontWeight: FontWeight.w500,
                                    fontSize: 18,
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      //Profile Details
                      Container(
                        margin: EdgeInsets.only(top: 20, left: 20),
                        child: Row(
                          children: [
                            Image.asset(
                              'assets/user.png',
                              height: 45,
                            ),
                            Container(
                              margin: EdgeInsets.only(left: 10),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Container(
                                    child: Text(
                                      getUserName!,
                                      style: GoogleFonts.roboto(
                                        color: Colors.black87,
                                        fontWeight: FontWeight.w500,
                                        fontSize: 18,
                                      ),
                                    ),
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(top: 5),
                                    child: Text(getUserType!),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(top: 20, left: 20),
                        child: Row(
                          children: [
                            Text(
                              'Date Posted:',
                              style: GoogleFonts.roboto(
                                color: Colors.grey,
                                fontWeight: FontWeight.w500,
                                fontSize: 14,
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(left: 10),
                              child: Text(
                                getDate!,
                                style: GoogleFonts.roboto(
                                  color: Colors.grey,
                                  fontWeight: FontWeight.w500,
                                  fontSize: 14,
                                ),
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(left: 10),
                              child: Text(
                                getTime!,
                                style: GoogleFonts.roboto(
                                  color: Colors.grey,
                                  fontWeight: FontWeight.w500,
                                  fontSize: 14,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),

                  //Content
                  Form(
                    key: formKey,
                    child: Column(
                      children: [
                        //Post Title
                        Container(
                          margin: EdgeInsets.only(top: 50, left: 15, right: 15),
                          child: TextFormField(
                            decoration: InputDecoration(
                              filled: true,
                              fillColor: Color(0xffe5eef8),
                              errorStyle: TextStyle(
                                  fontSize: 14, color: Colors.red[400]),
                              hintText: 'An Interesting Title',
                              border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(10.0),
                                borderSide: BorderSide.none,
                              ),
                            ),
                            controller: _title..text = getPostTitle!,
                            maxLines: 5,
                            validator: (value) {
                              if (value!.isEmpty) {
                                return 'This Section is Required';
                              } else {
                                return null;
                              }
                            },
                          ),
                        ),

                        Padding(
                          padding: const EdgeInsets.only(left: 15, right: 15),
                          child: Divider(color: Colors.grey),
                        ),
                        //Text Post
                        Container(
                          margin: EdgeInsets.only(top: 10, left: 15, right: 15),
                          child: TextFormField(
                            decoration: InputDecoration(
                                filled: true,
                                fillColor: Color(0xffe5eef8),
                                border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(10.0),
                                  borderSide: BorderSide.none,
                                ),
                                hintText: 'Your text post (Optional)'),
                            controller: _content..text = getPostContent!,
                            maxLines: 5,
                          ),
                        ),

                        // Checks if User is Donor or Patient
                        FutureBuilder(
                          future: getUserData(),
                          builder: (context, snapshot) {
                            if (snapshot.connectionState !=
                                ConnectionState.done) return Text("");

                            if (currentUserType != null) {
                              currentUsersValue = currentUserType;
                              return Container(
                                  height: 20,
                                  width: 240,
                                  child: Text(
                                    '',
                                    style: GoogleFonts.roboto(
                                        color: Colors.black87,
                                        fontSize: 15,
                                        fontWeight: FontWeight.w500),
                                  ));
                            } else {
                              return Text(
                                "",
                                style: GoogleFonts.roboto(
                                    color: Colors.black54, fontSize: 14),
                              );
                            }
                          },
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      });

  //Add Comments by Admin
  void showCommentsCustomDialog(BuildContext context) => showDialog(
      context: context,
      builder: (BuildContext context) {
        return Dialog(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10.0)), //this right here
          child: Container(
            height: MediaQuery.of(context).size.height * 0.8,
            width: MediaQuery.of(context).size.width * 0.4,
            child: Stack(
              children: [
                SingleChildScrollView(
                  child: Column(
                    children: [
                      //Entire Posts Details
                      Container(
                        margin: EdgeInsets.only(top: 80),
                        decoration: BoxDecoration(
                          color: Colors.white,
                        ),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            //Post Title
                            Container(
                              margin:
                                  EdgeInsets.only(top: 15, left: 15, right: 15),
                              child: Text(
                                getPostTitle!,
                                style: GoogleFonts.roboto(
                                  color: Colors.black87,
                                  fontWeight: FontWeight.w600,
                                  fontSize: 22,
                                  letterSpacing: 1,
                                  height: 1.5,
                                ),
                              ),
                            ),
                            //User Profile
                            Row(
                              children: [
                                //Default Profile Image
                                Container(
                                  margin: EdgeInsets.only(
                                      top: 20, left: 20, right: 15),
                                  child: Image.asset(
                                    'assets/user.png',
                                    height: 45,
                                  ),
                                ),
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    //User Name
                                    Container(
                                      margin: EdgeInsets.only(top: 20),
                                      child: Text(
                                        getUserName!,
                                        style: GoogleFonts.roboto(
                                          color: Colors.black87,
                                          fontWeight: FontWeight.w600,
                                          fontSize: 14,
                                        ),
                                      ),
                                    ),
                                    //Date and Time
                                    Container(
                                      margin: EdgeInsets.only(top: 10),
                                      child: Row(
                                        children: [
                                          Text(
                                            getDate!,
                                            style: GoogleFonts.roboto(
                                              color: Colors.grey,
                                              fontWeight: FontWeight.w400,
                                              fontSize: 12,
                                            ),
                                          ),
                                          Container(
                                            margin: EdgeInsets.only(left: 10),
                                            child: Text(
                                              getTime!,
                                              style: GoogleFonts.roboto(
                                                color: Colors.grey,
                                                fontWeight: FontWeight.w400,
                                                fontSize: 12,
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ],
                                )
                              ],
                            ),
                            //Post Content
                            Container(
                              margin:
                                  EdgeInsets.only(top: 40, left: 15, right: 15),
                              child: Text(
                                getPostContent!,
                                style: GoogleFonts.roboto(
                                  color: Colors.black87,
                                  fontWeight: FontWeight.w400,
                                  fontSize: 16,
                                  letterSpacing: 1,
                                  height: 1.5,
                                ),
                              ),
                            ),
                            Container(
                              margin:
                                  EdgeInsets.only(top: 20, left: 15, right: 15),
                              child: Text(
                                'Organ Donor App',
                                style: GoogleFonts.roboto(
                                  color: Colors.grey[400],
                                  fontWeight: FontWeight.w400,
                                  fontSize: 12,
                                  letterSpacing: 1,
                                  height: 1.5,
                                ),
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(top: 10),
                              child: Divider(
                                color: Colors.grey,
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(top: 10, left: 10),
                              child: Text(
                                'Post Comment',
                                style: GoogleFonts.roboto(
                                  color: Colors.grey[700],
                                  fontWeight: FontWeight.w400,
                                  fontSize: 14,
                                ),
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(top: 20),
                              child: TextFormField(
                                decoration: InputDecoration(
                                  filled: true,
                                  fillColor: Colors.red[50],
                                  hintStyle: GoogleFonts.roboto(
                                    color: Colors.black54,
                                    fontWeight: FontWeight.w400,
                                    fontSize: 14,
                                  ),
                                  hintText: 'Please select and enter reply',
                                  border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(0),
                                    borderSide: BorderSide.none,
                                  ),
                                ),
                                controller: _reply..text = reply!,
                                maxLines: 5,
                              ),
                            ),

                            //Post Button
                            Container(
                              margin: EdgeInsets.only(
                                  top: 5, bottom: 10, right: 20),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.end,
                                children: [
                                  ElevatedButton(
                                    style: ElevatedButton.styleFrom(
                                      primary: Colors.red[100],
                                      shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(30),
                                      ),
                                    ),
                                    onPressed: () {
                                      setState(() {
                                        if (getUserType == 'I am a Donor') {
                                          //Count Comments

                                          int x =
                                              int.parse(getPostComments!) + 1;
                                          getPostComments = x.toString();
                                          commentAddDonor();
                                          commentAddAll();
                                          commentAddSpecificDonor();
                                          commentAddSpecificAll();
                                          //Post Reply
                                          replyDonor();
                                          replyAll();
                                          replySpecificDonor();
                                          replySpecificAll();
                                          showReplyToast();
                                          Navigator.of(context).pop();
                                          reply = "";
                                          showCommentsCustomDialog(context);
                                          logAdminReplyToUserPosts();
                                        } else if (getUserType ==
                                            'I am a Patient') {
                                          //Count Comments

                                          int x =
                                              int.parse(getPostComments!) + 1;
                                          getPostComments = x.toString();
                                          commentAddPatient();
                                          commentAddAll();
                                          commentAddSpecificPatient();
                                          commentAddSpecificAll();

                                          //Post Reply
                                          replyPatient();
                                          replyAll();
                                          replySpecificPatient();
                                          replySpecificAll();
                                          showReplyToast();
                                          Navigator.of(context).pop();
                                          reply = "";
                                          showCommentsCustomDialog(context);
                                          logAdminReplyToUserPosts();
                                        } else {
                                          //count Comments

                                          int x =
                                              int.parse(getPostComments!) + 1;
                                          getPostComments = x.toString();
                                          commentAddAdmin();
                                          commentAddAll();

                                          //Posts Reply
                                          replyAdmin();
                                          replyAll();
                                          showReplyToast();
                                          Navigator.of(context).pop();
                                          reply = "";
                                          showCommentsCustomDialog(context);
                                          logAdminReplyToOwnPosts();
                                        }
                                      });
                                    },
                                    child: Text(
                                      'Post',
                                      style: GoogleFonts.roboto(
                                        color: Colors.red[400],
                                        fontWeight: FontWeight.w400,
                                        fontSize: 14,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            )
                          ],
                        ),
                      ),
                      //All Comments
                      Container(
                        height: 450,
                        width: double.infinity,
                        margin: EdgeInsets.only(top: 0, bottom: 50),
                        color: Colors.white,
                        child: Stack(
                          children: [
                            Container(
                              margin: EdgeInsets.only(top: 15, left: 15),
                              child: Text(
                                'All Comments',
                                style: GoogleFonts.roboto(
                                    color: Colors.black87,
                                    fontSize: 14,
                                    fontWeight: FontWeight.w800),
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(top: 40),
                              child: Divider(
                                color: Colors.grey,
                              ),
                            ),
                            Container(
                              color: Colors.white, // change color here!!!!!!
                              margin: EdgeInsets.only(top: 50),
                              child: FutureBuilder(
                                future: getAllCommentsData(),
                                builder: (context, AsyncSnapshot snapshot) {
                                  if (snapshot.hasData) {
                                    return ListView.builder(
                                      itemCount: snapshot.data!.length,
                                      itemBuilder: (context, index) {
                                        DocumentSnapshot userData =
                                            snapshot.data[index];

                                        return Stack(
                                          children: [
                                            Container(
                                              color: Colors.white,
                                              margin: EdgeInsets.only(top: 10),
                                              child: ListTile(
                                                title: Column(
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.start,
                                                  children: [
                                                    Row(
                                                      children: [
                                                        //Profile Image
                                                        Container(
                                                          margin:
                                                              EdgeInsets.only(
                                                                  top: 10),
                                                          child: Image.asset(
                                                            'assets/user.png',
                                                            height: 35,
                                                          ),
                                                        ),
                                                        Container(
                                                          margin:
                                                              EdgeInsets.only(
                                                                  left: 10),
                                                          child: Column(
                                                            crossAxisAlignment:
                                                                CrossAxisAlignment
                                                                    .start,
                                                            children: [
                                                              //UserName
                                                              Container(
                                                                margin: EdgeInsets
                                                                    .only(
                                                                        top:
                                                                            10),
                                                                child: Text(
                                                                  userData[
                                                                      "UserName"],
                                                                  style: GoogleFonts.roboto(
                                                                      color: Colors
                                                                          .black87,
                                                                      fontSize:
                                                                          14,
                                                                      fontWeight:
                                                                          FontWeight
                                                                              .w600),
                                                                ),
                                                              ),
                                                              //Replied Time
                                                              Container(
                                                                margin: EdgeInsets
                                                                    .only(
                                                                        top: 3),
                                                                child: Text(
                                                                  userData[
                                                                      "Replied Time"],
                                                                  style: GoogleFonts.roboto(
                                                                      color: Colors
                                                                          .grey,
                                                                      fontSize:
                                                                          12,
                                                                      fontWeight:
                                                                          FontWeight
                                                                              .w600),
                                                                ),
                                                              ),
                                                            ],
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                                    //Reply
                                                    Container(
                                                      margin: EdgeInsets.only(
                                                          top: 20, left: 45),
                                                      child: Text(
                                                        userData["Reply"],
                                                        style:
                                                            GoogleFonts.roboto(
                                                          color: Colors.black87,
                                                          fontSize: 18,
                                                          letterSpacing: 1,
                                                          fontWeight:
                                                              FontWeight.w400,
                                                        ),
                                                      ),
                                                    ),
                                                    //Replied Date
                                                    Container(
                                                      margin: EdgeInsets.only(
                                                          top: 20, left: 45),
                                                      child: Row(
                                                        mainAxisAlignment:
                                                            MainAxisAlignment
                                                                .spaceBetween,
                                                        children: [
                                                          Text(
                                                            userData[
                                                                "Replied Date"],
                                                            style: GoogleFonts.roboto(
                                                                color:
                                                                    Colors.grey,
                                                                fontSize: 12,
                                                                fontWeight:
                                                                    FontWeight
                                                                        .w400),
                                                          ),
                                                        ],
                                                      ),
                                                    ),
                                                    //Divider Line
                                                    Container(
                                                      margin: EdgeInsets.only(
                                                          top: 10, left: 42),
                                                      child: Divider(
                                                        color: Colors.grey,
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            ),
                                          ],
                                        );
                                      },
                                    );
                                  } else {
                                    return Text('Users Data Not Available');
                                  }
                                },
                              ),
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                ),
                //Header
                Container(
                  decoration: BoxDecoration(
                    color: Color(0xffe5eef8),
                    boxShadow: [
                      BoxShadow(
                        color: Colors.grey.withOpacity(0.5),
                        blurRadius: 8,
                      ),
                    ],
                  ),
                  height: 80,
                  margin: EdgeInsets.only(top: 0, left: 0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.only(top: 26, left: 20),
                        child: Row(
                          children: [
                            InkWell(
                              onTap: () {
                                Navigator.of(context).pop();
                              },
                              child: Icon(Icons.close_outlined, size: 25.0),
                            ),
                            //Verified Icon
                            Container(
                              margin: EdgeInsets.only(left: 10),
                              child: Icon(Icons.verified_outlined,
                                  color: Colors.green),
                            ),
                            //UserName
                            Container(
                              margin: EdgeInsets.only(left: 10),
                              child: Text(
                                getUserType!,
                                style: GoogleFonts.roboto(
                                  color: Colors.black87,
                                  fontWeight: FontWeight.w500,
                                  fontSize: 18,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                //Put Comments
                Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    Container(
                      height: 50,
                      width: double.infinity,
                      decoration: BoxDecoration(
                        boxShadow: [
                          BoxShadow(color: Colors.grey.withOpacity(0.5))
                        ],
                        color: Colors.white,
                      ),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Container(
                                margin: EdgeInsets.only(
                                    top: 5, left: 20, right: 20),
                                child: Row(
                                  children: [
                                    //Number of Comments
                                    Container(
                                      margin: EdgeInsets.only(left: 20),
                                      child: Column(
                                        children: [
                                          Icon(Icons.comment_outlined),
                                          Container(
                                            child: Text(
                                              getPostComments!,
                                              style: GoogleFonts.roboto(
                                                color: Colors.grey,
                                                fontWeight: FontWeight.w400,
                                                fontSize: 12,
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),

                                    //Number of Likes
                                    Container(
                                      margin: EdgeInsets.only(left: 20),
                                      child: Column(
                                        children: [
                                          Icon(Icons.thumb_up_outlined),
                                          Text(
                                            getPostLikes!,
                                            style: GoogleFonts.roboto(
                                              color: Colors.grey,
                                              fontWeight: FontWeight.w400,
                                              fontSize: 12,
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        );
      });

  //Show Patient Tagged Success Toast Message
  void showPostsSuccessToast() => toast.showToast(
      child: buildPostsSuccessToast(), gravity: ToastGravity.CENTER);

  Widget buildPostsSuccessToast() => Container(
        padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
        decoration: BoxDecoration(
          color: Colors.green[300],
          borderRadius: BorderRadius.circular(5),
        ),
        child: Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            Text(
              "Post Success",
              style: TextStyle(color: Colors.black87, fontSize: 16),
            ),
          ],
        ),
      );

  //Show Reply Succes
  void showReplyToast() =>
      toast.showToast(child: buildReplyToast(), gravity: ToastGravity.CENTER);

  Widget buildReplyToast() => Container(
        padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
        decoration: BoxDecoration(
          color: Colors.green[300],
          borderRadius: BorderRadius.circular(5),
        ),
        child: Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            Text(
              "You have replied on this post",
              style: TextStyle(color: Colors.black87, fontSize: 16),
            ),
          ],
        ),
      );

  adminPosts() async {
    String formattedTime = DateFormat('kk:mm:s:a').format(now);
    String formattedDate = formatter.format(now);
    await FirebaseFirestore.instance
        .collection('Posts')
        .doc('Admin')
        .collection('All Posts')
        .doc(formattedTime)
        .set({
      'UserID': 'SuperAdmin',
      'FullName': 'Organ Donor App Official',
      'UserName': 'Official Admin',
      'User Type': 'Admin',
      'Post Title': _title.text,
      'Post Content': _content.text,
      'Date': formattedDate,
      'Time': formattedTime,
      'Views': '0',
      'Comments': '0',
      'Likes': '0',
      'Notifications': '0',
    });
  }

  adminAllPosts() async {
    String formattedTime = DateFormat('kk:mm:s:a').format(now);
    String formattedDate = formatter.format(now);
    await FirebaseFirestore.instance
        .collection('Posts')
        .doc('All')
        .collection('All Posts')
        .doc(formattedTime)
        .set({
      'UserID': 'SuperAdmin',
      'FullName': 'Organ Donor App Official',
      'UserName': 'Official Admin',
      'User Type': 'Admin',
      'Post Title': _title.text,
      'Post Content': _content.text,
      'Date': formattedDate,
      'Time': formattedTime,
      'Views': '0',
      'Comments': '0',
      'Likes': '0',
      'Notifications': '0',
    });
  }

  //Posts Document
  replyDonor() async {
    String? pid = getTime;

    String formattedTime = DateFormat('kk:mm:s:a').format(now);
    String formattedDate = formatter.format(now);
    await FirebaseFirestore.instance
        .collection("Posts")
        .doc('Donor')
        .collection('All Posts')
        .doc(pid)
        .collection('Comments')
        .doc(formattedTime)
        .set({
      'UserID': userID,
      'UserName': userName,
      'User Type': usersValue,
      'Reply': _reply.text,
      'Replied Date': formattedDate,
      'Replied Time': formattedTime,
      'Likes': '0',
    });
  }

  replyPatient() async {
    String? pid = getTime;
    String formattedTime = DateFormat('kk:mm:s:a').format(now);
    String formattedDate = formatter.format(now);
    await FirebaseFirestore.instance
        .collection("Posts")
        .doc('Patient')
        .collection('All Posts')
        .doc(pid)
        .collection('Comments')
        .doc(formattedTime)
        .set({
      'UserID': userID,
      'UserName': userName,
      'User Type': usersValue,
      'Reply': _reply.text,
      'Replied Date': formattedDate,
      'Replied Time': formattedTime,
      'Likes': '0',
    });
  }

  replyAdmin() async {
    String? pid = getTime;
    String formattedTime = DateFormat('kk:mm:s:a').format(now);
    String formattedDate = formatter.format(now);
    await FirebaseFirestore.instance
        .collection("Posts")
        .doc('Admin')
        .collection('All Posts')
        .doc(pid)
        .collection('Comments')
        .doc(formattedTime)
        .set({
      'UserID': userID,
      'UserName': userName,
      'User Type': usersValue,
      'Reply': _reply.text,
      'Replied Date': formattedDate,
      'Replied Time': formattedTime,
      'Likes': '0',
    });
  }

  replyAll() async {
    String? pid = getTime;
    String formattedTime = DateFormat('kk:mm:s:a').format(now);
    String formattedDate = formatter.format(now);
    await FirebaseFirestore.instance
        .collection("Posts")
        .doc('All')
        .collection('All Posts')
        .doc(pid)
        .collection('Comments')
        .doc(formattedTime)
        .set({
      'UserID': userID,
      'UserName': userName,
      'User Type': usersValue,
      'Reply': _reply.text,
      'Replied Date': formattedDate,
      'Replied Time': formattedTime,
      'Likes': '0',
    });
  }

  //Specific Donor and Patient Document
  replySpecificDonor() async {
    String? pid = getTime;
    String? did = getUserID;
    String formattedTime = DateFormat('kk:mm:s:a').format(now);
    String formattedDate = formatter.format(now);
    await FirebaseFirestore.instance
        .collection("Users")
        .doc('Donor')
        .collection('Lists')
        .doc(did)
        .collection('Posts')
        .doc(pid)
        .collection('Comments')
        .doc(formattedTime)
        .set({
      'UserID': userID,
      'UserName': userName,
      'User Type': usersValue,
      'Reply': _reply.text,
      'Replied Date': formattedDate,
      'Replied Time': formattedTime,
      'Likes': '0',
    });
  }

  replySpecificPatient() async {
    String? pid = getTime;
    String? did = getUserID;
    String formattedTime = DateFormat('kk:mm:s:a').format(now);
    String formattedDate = formatter.format(now);
    await FirebaseFirestore.instance
        .collection("Users")
        .doc('Patient')
        .collection('Lists')
        .doc(did)
        .collection('Posts')
        .doc(pid)
        .collection('Comments')
        .doc(formattedTime)
        .set({
      'UserID': userID,
      'UserName': userName,
      'User Type': usersValue,
      'Reply': _reply.text,
      'Replied Date': formattedDate,
      'Replied Time': formattedTime,
      'Likes': '0',
    });
  }

  replySpecificAll() async {
    String? pid = getTime;
    String? did = getUserID;
    String formattedTime = DateFormat('kk:mm:s:a').format(now);
    String formattedDate = formatter.format(now);
    await FirebaseFirestore.instance
        .collection("Users")
        .doc('All')
        .collection('Lists')
        .doc(did)
        .collection('Posts')
        .doc(pid)
        .collection('Comments')
        .doc(formattedTime)
        .set({
      'UserID': userID,
      'UserName': userName,
      'User Type': usersValue,
      'Reply': _reply.text,
      'Replied Date': formattedDate,
      'Replied Time': formattedTime,
      'Likes': '0',
    });
  }

  //Display From All Posts
  getAllCommentsData() async {
    String? pid = getTime;
    final firestore = FirebaseFirestore.instance;
    QuerySnapshot user = await firestore
        .collection('Posts')
        .doc('All')
        .collection('All Posts')
        .doc(pid)
        .collection('Comments')
        .get();
    return user.docs;
  }

  //CommentCount
  commentAddDonor() async {
    String? did = getTime;
    await FirebaseFirestore.instance
        .collection("Posts")
        .doc('Donor')
        .collection('All Posts')
        .doc(did)
        .update({
      'Comments': getPostComments,
    });
  }

  commentAddPatient() async {
    String? did = getTime;
    await FirebaseFirestore.instance
        .collection("Posts")
        .doc('Patient')
        .collection('All Posts')
        .doc(did)
        .update({
      'Comments': getPostComments,
    });
  }

  commentAddAdmin() async {
    String? did = getTime;
    await FirebaseFirestore.instance
        .collection("Posts")
        .doc('Admin')
        .collection('All Posts')
        .doc(did)
        .update({
      'Comments': getPostComments,
    });
  }

  commentAddAll() async {
    String? did = getTime;
    await FirebaseFirestore.instance
        .collection("Posts")
        .doc('All')
        .collection('All Posts')
        .doc(did)
        .update({
      'Comments': getPostComments,
    });
  }

  commentAddSpecificDonor() async {
    String? uid = getUserID;
    String? did = getTime;
    await FirebaseFirestore.instance
        .collection("Users")
        .doc('Donor')
        .collection('Lists')
        .doc(uid)
        .collection('Posts')
        .doc(did)
        .update({
      'Comments': getPostComments,
    });
  }

  commentAddSpecificPatient() async {
    String? uid = getUserID;
    String? did = getTime;
    await FirebaseFirestore.instance
        .collection("Users")
        .doc('Patient')
        .collection('Lists')
        .doc(uid)
        .collection('Posts')
        .doc(did)
        .update({
      'Comments': getPostComments,
    });
  }

  commentAddSpecificAll() async {
    String? uid = getUserID;
    String? did = getTime;
    await FirebaseFirestore.instance
        .collection("Users")
        .doc('All')
        .collection('Lists')
        .doc(uid)
        .collection('Posts')
        .doc(did)
        .update({
      'Comments': getPostComments,
    });
  }

  //Edit
  editDonorPost() async {
    String? did = getTime;
    await FirebaseFirestore.instance
        .collection("Posts")
        .doc('Donor')
        .collection('All Posts')
        .doc(did)
        .update({
      'Post Title': _title.text,
      'Post Content': _content.text,
    });
  }

  editPatientPost() async {
    String? did = getTime;
    await FirebaseFirestore.instance
        .collection("Posts")
        .doc('Patient')
        .collection('All Posts')
        .doc(did)
        .update({
      'Post Title': _title.text,
      'Post Content': _content.text,
    });
  }

  editAdminPost() async {
    String? did = getTime;
    await FirebaseFirestore.instance
        .collection("Posts")
        .doc('Admin')
        .collection('All Posts')
        .doc(did)
        .update({
      'Post Title': _title.text,
      'Post Content': _content.text,
    });
  }

  editAllPost() async {
    String? did = getTime;
    await FirebaseFirestore.instance
        .collection("Posts")
        .doc('All')
        .collection('All Posts')
        .doc(did)
        .update({
      'Post Title': _title.text,
      'Post Content': _content.text,
    });
  }

  editSpecificDonorPost() async {
    String? uid = getUserID;
    String? did = getTime;
    await FirebaseFirestore.instance
        .collection("Users")
        .doc('Donor')
        .collection('Lists')
        .doc(uid)
        .collection('Posts')
        .doc(did)
        .update({
      'Post Title': _title.text,
      'Post Content': _content.text,
    });
  }

  editSpecificPatientPost() async {
    String? uid = getUserID;
    String? did = getTime;
    await FirebaseFirestore.instance
        .collection("Users")
        .doc('Patient')
        .collection('Lists')
        .doc(uid)
        .collection('Posts')
        .doc(did)
        .update({
      'Post Title': _title.text,
      'Post Content': _content.text,
    });
  }

  editSpecificAllPost() async {
    String? uid = getUserID;
    String? did = getTime;
    await FirebaseFirestore.instance
        .collection("Users")
        .doc('All')
        .collection('Lists')
        .doc(uid)
        .collection('Posts')
        .doc(did)
        .update({
      'Post Title': _title.text,
      'Post Content': _content.text,
    });
  }

  //Logs in the database
  logAdminCreatePosts() async {
    String formattedTime = DateFormat('kk:mm:s:a').format(now);
    String formattedDate = formatter.format(now);
    await FirebaseFirestore.instance.collection("Logs").add({
      'Message': "Admin has made a Post",
      'User': "Official Admin",
      'Logged Date': formattedDate,
      'Logged Time': formattedTime,
    });
  }

  logAdminViewUsersPosts() async {
    String formattedTime = DateFormat('kk:mm:s:a').format(now);
    String formattedDate = formatter.format(now);
    await FirebaseFirestore.instance.collection("Logs").add({
      'Message': "Admin has viewed post of " + getUserName!,
      'User': "Official Admin",
      'Logged Date': formattedDate,
      'Logged Time': formattedTime,
    });
  }

  logAdminViewAdminPosts() async {
    String formattedTime = DateFormat('kk:mm:s:a').format(now);
    String formattedDate = formatter.format(now);
    await FirebaseFirestore.instance.collection("Logs").add({
      'Message': "Admin has viewed his post",
      'User': "Official Admin",
      'Logged Date': formattedDate,
      'Logged Time': formattedTime,
    });
  }

  logAdminLikeUsersPosts() async {
    String formattedTime = DateFormat('kk:mm:s:a').format(now);
    String formattedDate = formatter.format(now);
    await FirebaseFirestore.instance.collection("Logs").add({
      'Message': "Admin has liked post of " + getUserName!,
      'User': "Official Admin",
      'Logged Date': formattedDate,
      'Logged Time': formattedTime,
    });
  }

  logAdminLikeAdminPosts() async {
    String formattedTime = DateFormat('kk:mm:s:a').format(now);
    String formattedDate = formatter.format(now);
    await FirebaseFirestore.instance.collection("Logs").add({
      'Message': "Admin has liked his own post",
      'User': "Official Admin",
      'Logged Date': formattedDate,
      'Logged Time': formattedTime,
    });
  }

  logAdminReplyToUserPosts() async {
    String formattedTime = DateFormat('kk:mm:s:a').format(now);
    String formattedDate = formatter.format(now);
    await FirebaseFirestore.instance.collection("Logs").add({
      'Message': "Admin has replied to " + getUserName!,
      'User': "Official Admin",
      'Logged Date': formattedDate,
      'Logged Time': formattedTime,
    });
  }

  logAdminReplyToOwnPosts() async {
    String formattedTime = DateFormat('kk:mm:s:a').format(now);
    String formattedDate = formatter.format(now);
    await FirebaseFirestore.instance.collection("Logs").add({
      'Message': "Admin has replied to his own post",
      'User': "Official Admin",
      'Logged Date': formattedDate,
      'Logged Time': formattedTime,
    });
  }

  logAdminEditUsersPosts() async {
    String formattedTime = DateFormat('kk:mm:s:a').format(now);
    String formattedDate = formatter.format(now);
    await FirebaseFirestore.instance.collection("Logs").add({
      'Message': "Admin has edited" + getUserName! + " post",
      'User': "Official Admin",
      'Logged Date': formattedDate,
      'Logged Time': formattedTime,
    });
  }

  logAdminEditOwnPosts() async {
    String formattedTime = DateFormat('kk:mm:s:a').format(now);
    String formattedDate = formatter.format(now);
    await FirebaseFirestore.instance.collection("Logs").add({
      'Message': "Admin has edited his own post",
      'User': "Official Admin",
      'Logged Date': formattedDate,
      'Logged Time': formattedTime,
    });
  }

  logAdminDeleteUsersPosts() async {
    String formattedTime = DateFormat('kk:mm:s:a').format(now);
    String formattedDate = formatter.format(now);
    await FirebaseFirestore.instance.collection("Logs").add({
      'Message': "Admin has deleted " + getUserName! + " post",
      'User': "Official Admin",
      'Logged Date': formattedDate,
      'Logged Time': formattedTime,
    });
  }

  logAdminDeleteOwnPosts() async {
    String formattedTime = DateFormat('kk:mm:s:a').format(now);
    String formattedDate = formatter.format(now);
    await FirebaseFirestore.instance.collection("Logs").add({
      'Message': "Admin has deleted his own post",
      'User': "Official Admin",
      'Logged Date': formattedDate,
      'Logged Time': formattedTime,
    });
  }

  //Notification Count
  notificationAddDonor() async {
    String? did = getTime;
    await FirebaseFirestore.instance
        .collection("Posts")
        .doc('Donor')
        .collection('All Posts')
        .doc(did)
        .update({
      'Notifications': countNotification,
    });
  }

  notificationAddPatient() async {
    String? did = getTime;
    await FirebaseFirestore.instance
        .collection("Posts")
        .doc('Patient')
        .collection('All Posts')
        .doc(did)
        .update({
      'Notifications': countNotification,
    });
  }

  notificationAddAll() async {
    String? did = getTime;
    await FirebaseFirestore.instance
        .collection("Posts")
        .doc('All')
        .collection('All Posts')
        .doc(did)
        .update({
      'Notifications': countNotification,
    });
  }

  notificationAddSpecificDonor() async {
    String? uid = getUserID;
    await FirebaseFirestore.instance
        .collection("Users")
        .doc('Donor')
        .collection('Lists')
        .doc(uid)
        .update({
      'Notifications': countNotification,
    });
  }

  notificationAddSpecificPatient() async {
    String? uid = getUserID;
    await FirebaseFirestore.instance
        .collection("Users")
        .doc('Patient')
        .collection('Lists')
        .doc(uid)
        .update({
      'Notifications': countNotification,
    });
  }

  notificationAddSpecificAll() async {
    String? uid = getUserID;
    await FirebaseFirestore.instance
        .collection("Users")
        .doc('All')
        .collection('Lists')
        .doc(uid)
        .update({
      'Notifications': countNotification,
    });
  }
}
