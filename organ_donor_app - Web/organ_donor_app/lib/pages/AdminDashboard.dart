import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:organ_donor_app/pages/AdminArchivesMatchUsers.dart';
import 'package:organ_donor_app/pages/AdminArchivesPage.dart';
import 'package:organ_donor_app/pages/AdminDashBoardPage.dart';
import 'package:organ_donor_app/pages/AdminEmailPage.dart';
import 'package:organ_donor_app/pages/AdminLogin.dart';
import 'package:organ_donor_app/pages/AdminLogs.dart';
import 'package:organ_donor_app/pages/AdminMatchUsersPage.dart';
import 'package:organ_donor_app/pages/AdminMatchingPage.dart';
import 'package:organ_donor_app/pages/AdminPostsPage.dart';
import 'package:organ_donor_app/pages/AdminUsersPage.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:intl/intl.dart';
import 'package:fluttertoast/fluttertoast.dart';

class AdminDashboard extends StatefulWidget {
  AdminDashboard({Key? key}) : super(key: key);

  @override
  _AdminDashboardState createState() => _AdminDashboardState();
}

class _AdminDashboardState extends State<AdminDashboard> {
  final padding = EdgeInsets.symmetric(horizontal: 20);

  int _selectedIndex = 0;

  List<Widget> _widgetOptions = <Widget>[
    //Dashboard
    Container(
      margin: EdgeInsets.only(top: 130, left: 360),
      child: AdminDashBoardPage(),
    ),
    Container(
      margin: EdgeInsets.only(top: 130, left: 360),
      child: AdminUsersPage(),
    ),
    Container(
      margin: EdgeInsets.only(top: 130, left: 360),
      child: AdminArchivesPage(),
    ),
    Container(
      margin: EdgeInsets.only(top: 130, left: 360),
      child: AdminLogs(),
    ),
    Container(
      margin: EdgeInsets.only(top: 130, left: 360),
      child: AdminPostsPage(),
    ),

    Container(
      margin: EdgeInsets.only(top: 130, left: 360),
      child: AdminMatchingPage(),
    ),
    Container(
      margin: EdgeInsets.only(top: 130, left: 360),
      child: AdminMatchUsersPage(),
    ),
    Container(
      margin: EdgeInsets.only(top: 130, left: 360),
      child: AdminArchivesMatchUsersPage(),
    ),
    Container(
      margin: EdgeInsets.only(top: 130, left: 360),
      child: AdminEmailPage(),
    ),
  ];

  //Time and Date
  var now = new DateTime.now();
  var formatter = new DateFormat('dd-MM-yyyy');

  final formKey = GlobalKey<FormState>();

  //Set State Refresh
  final toast = FToast();
  @override
  void initState() {
    super.initState();

    toast.init(context);
  }

  TextEditingController _userName = TextEditingController();
  TextEditingController _subject = TextEditingController();
  TextEditingController _content = TextEditingController();

  String? getUserID;
  String? getUserName;
  String? getFullName;
  String? getUserType;
  String? getNotifications;
  String? getMail; //Comments in the Firebase(just temporary hold variable)

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async => false,
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        body: Stack(
          children: [
            Container(
              decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage('assets/5.png'), fit: BoxFit.cover),
              ),
            ),
            //Top Navigation Bar
            Container(
              margin: EdgeInsets.only(left: 350),
              height: 130.0,
              width: MediaQuery.of(context).size.width,
              decoration: BoxDecoration(
                color: Colors.white,
              ),
              child: Stack(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                        margin: EdgeInsets.only(top: 40, left: 40),
                        width: 700,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(32),
                        ),
                        child: Text(
                          'Welcome to the dashboard!',
                          style: GoogleFonts.roboto(
                              color: Colors.blue[900],
                              fontWeight: FontWeight.bold,
                              fontSize: 50),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(top: 40, right: 200),
                        child: Row(
                          children: [
                            Container(
                              decoration: BoxDecoration(
                                  color: Colors.red[50],
                                  borderRadius: BorderRadius.circular(20)),
                              padding: EdgeInsets.all(10),
                              child: InkWell(
                                onTap: () {
                                  showSendEmailCustomDialog(context);
                                },
                                child: Row(
                                  children: [
                                    Container(
                                      margin: EdgeInsets.only(left: 10),
                                      child: Icon(Icons.add),
                                    ),
                                    Container(
                                      margin:
                                          EdgeInsets.only(left: 10, right: 10),
                                      child: Text(
                                        'Compose',
                                        style: GoogleFonts.roboto(
                                            color: Colors.red[400],
                                            fontWeight: FontWeight.bold,
                                            fontSize: 16),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(left: 20),
                              child: IconButton(
                                icon: Icon(
                                  Icons.logout_outlined,
                                  color: Colors.grey,
                                ),
                                hoverColor: Colors.blue,
                                iconSize: 30,
                                onPressed: () {
                                  showCustomDialog(context);
                                },
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(bottom: 10),
                              width: 150,
                              height: 150,
                              padding: EdgeInsets.all(15),
                              decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  color: Colors.grey.shade200),
                              child:
                                  Image(image: AssetImage('assets/boss.png')),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),

            //Navigation Drawer
            Container(
              height: MediaQuery.of(context).size.height,
              width: 350.0,
              decoration: BoxDecoration(
                color: Colors.white,
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey.withOpacity(0.5),
                    spreadRadius: 5,
                    blurRadius: 7,
                    offset: Offset(0, 3), // changes position of shadow
                  ),
                ],
              ),
              child: Stack(
                children: [
                  Container(
                    margin: EdgeInsets.only(top: 30, left: 20),
                    child: Image.asset(
                      'assets/logo.png',
                      height: 100,
                      width: 100,
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 70, left: 110),
                    child: Text(
                      "Organ Donor Admin ",
                      style: GoogleFonts.roboto(
                          color: Colors.blue[900],
                          fontWeight: FontWeight.bold,
                          fontSize: 20),
                    ),
                  ),
                  //Navigation Drawer Items
                  Container(
                    margin: EdgeInsets.only(top: 130),
                    child: Material(
                      child: ListView(
                        padding: padding,
                        children: <Widget>[
                          const SizedBox(
                            height: 25,
                          ),
                          buildMenuItem(
                            text: 'Analytics Dashboard',
                            icon: Icons.space_dashboard_sharp,
                            onClicked: () {
                              setState(() {
                                selectedItem(context, 0);
                                logClickedDataBaseLogs();
                              });
                            },
                          ),
                          const SizedBox(
                            height: 5,
                          ),
                          buildMenuItem(
                            text: 'Users',
                            icon: Icons.people_sharp,
                            onClicked: () {
                              setState(() {
                                selectedItem(context, 1);
                                logClickedUsers();
                              });
                            },
                          ),
                          const SizedBox(
                            height: 5,
                          ),
                          buildMenuItem(
                            text: 'Archives',
                            icon: Icons.archive_outlined,
                            onClicked: () {
                              setState(() {
                                selectedItem(context, 2);
                                logClickedArchives();
                              });
                            },
                          ),
                          const SizedBox(
                            height: 5,
                          ),
                          buildMenuItem(
                            text: 'Database Logs',
                            icon: Icons.history_outlined,
                            onClicked: () {
                              setState(() {
                                selectedItem(context, 3);
                                logClickedDataBaseLogs();
                              });
                            },
                          ),
                          const SizedBox(
                            height: 15,
                          ),
                          Divider(color: Colors.blueGrey),
                          const SizedBox(
                            height: 5,
                          ),
                          buildMenuItem(
                            text: 'Posts',
                            icon: Icons.post_add_sharp,
                            onClicked: () {
                              setState(() {
                                selectedItem(context, 4);
                                logClickedPosts();
                              });
                            },
                          ),
                          const SizedBox(
                            height: 5,
                          ),
                          const SizedBox(
                            height: 5,
                          ),
                          buildMenuItem(
                            text: 'Matching',
                            icon: Icons.person_pin_sharp,
                            onClicked: () {
                              setState(() {
                                selectedItem(context, 5);
                                logClickedMatching();
                              });
                            },
                          ),
                          const SizedBox(
                            height: 5,
                          ),
                          buildMenuItem(
                            text: 'Match Users',
                            icon: Icons.people_outline_outlined,
                            onClicked: () {
                              setState(() {
                                selectedItem(context, 6);
                                logClickedMatchUsers();
                              });
                            },
                          ),
                          const SizedBox(
                            height: 5,
                          ),
                          buildMenuItem(
                            text: 'Archive Match Users',
                            icon: Icons.archive_outlined,
                            onClicked: () {
                              setState(() {
                                selectedItem(context, 7);
                                logClickedArchiveMatchUsers();
                              });
                            },
                          ),
                          const SizedBox(
                            height: 5,
                          ),
                          buildMenuItem(
                            text: 'Mails',
                            icon: Icons.mail_outline,
                            onClicked: () {
                              setState(() {
                                selectedItem(context, 8);
                                logClickedMail();
                              });
                            },
                          ),
                        ],
                      ),
                    ),
                  )
                ],
              ),
            ),
            Container(
              child: _widgetOptions.elementAt(_selectedIndex),
            ),
          ],
        ),
      ),
    );
  }

//List Navigation Method
  Widget buildMenuItem({
    required String text,
    required IconData icon,
    VoidCallback? onClicked,
  }) {
    final color = Colors.lightBlueAccent;
    final iconColor = Colors.red[200];
    final hoverColor = Colors.black45;

    return ListTile(
      leading: Icon(icon, color: iconColor),
      title: Text(
        text,
        style: TextStyle(color: color),
      ),
      hoverColor: hoverColor,
      onTap: onClicked,
    );
  }

// Select Item Method
  void selectedItem(BuildContext context, int index) {
    switch (index) {
      case 0:
        setState(() {
          _selectedIndex = index;
        });
        break;
      case 1:
        setState(() {
          _selectedIndex = index;
        });
        break;
      case 2:
        setState(() {
          _selectedIndex = index;
        });
        break;
      case 3:
        setState(() {
          _selectedIndex = index;
        });
        break;
      case 4:
        setState(() {
          _selectedIndex = index;
        });
        break;
      case 5:
        setState(() {
          _selectedIndex = index;
        });
        break;
      case 6:
        setState(() {
          _selectedIndex = index;
        });
        break;
      case 7:
        setState(() {
          _selectedIndex = index;
        });
        break;
      case 8:
        setState(() {
          _selectedIndex = index;
        });
        break;
    }
  }

  //Alert Dialog Method
  void showCustomDialog(BuildContext context) => showDialog(
      context: context,
      builder: (BuildContext context) {
        return Dialog(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20.0)), //this right here
          child: Container(
            height: 200,
            width: 100,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  "Log out",
                  style: GoogleFonts.roboto(
                      color: Colors.black87,
                      fontWeight: FontWeight.bold,
                      fontSize: 18),
                ),
                Text(
                  "\nAre you sure you want to log out?",
                  style:
                      GoogleFonts.roboto(color: Colors.grey[500], fontSize: 14),
                ),
                Container(
                  margin: EdgeInsets.only(top: 40),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Container(
                        child: InkWell(
                          onTap: () {
                            Navigator.of(context).pop();
                          },
                          child: Container(
                            height: 35,
                            width: 100,
                            decoration: BoxDecoration(
                                color: Colors.red[50],
                                borderRadius: BorderRadius.circular(18)),
                            child: Center(
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Text(
                                  "Cancel",
                                  style: TextStyle(
                                      color: Colors.grey[600], fontSize: 17),
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                      Container(
                        child: InkWell(
                          onTap: () {
                            Navigator.pushAndRemoveUntil(
                                context,
                                MaterialPageRoute(
                                    builder: (builder) => AdminLogin()),
                                (route) => false);
                            logAdminLogout();
                          },
                          child: Container(
                            height: 35,
                            width: 100,
                            decoration: BoxDecoration(
                                color: Colors.red[200],
                                borderRadius: BorderRadius.circular(18)),
                            child: Center(
                              child: Text(
                                "Confirm",
                                style: TextStyle(
                                    color: Colors.red[400], fontSize: 17),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        );
      });

  //Compose Email Dialog
  void showSendEmailCustomDialog(BuildContext context) => showDialog(
      context: context,
      builder: (BuildContext context) {
        return Dialog(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10.0)), //this right here
          child: Container(
            height: MediaQuery.of(context).size.height * 0.9,
            width: MediaQuery.of(context).size.width * 0.4,
            child: Form(
              key: formKey,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  //Title and Close Icon
                  Container(
                    decoration: BoxDecoration(
                        color: Colors.red[50],
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(10),
                            topRight: Radius.circular(10))),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          margin: EdgeInsets.only(top: 5, left: 20),
                          child: Text(
                            'New Message',
                            style: GoogleFonts.roboto(
                                color: Colors.grey[600],
                                fontSize: 16,
                                fontWeight: FontWeight.w700),
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(top: 5, right: 20),
                          child: InkWell(
                            onTap: () {
                              setState(() {
                                Navigator.of(context).pop();
                              });
                            },
                            child: Icon(
                              Icons.close_outlined,
                              size: 25,
                              color: Colors.red[400],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  //TItle Email
                  Container(
                    margin: EdgeInsets.only(left: 30, right: 30),
                    child: TextFormField(
                      validator: (value) {
                        if (value!.isEmpty) {
                          return 'Recipient is required!';
                        } else {
                          return null;
                        }
                      },
                      controller: _userName..text = "",
                      decoration: InputDecoration(
                          contentPadding: const EdgeInsets.symmetric(
                              vertical: 18, horizontal: 14),
                          prefixIcon: Padding(
                            padding: const EdgeInsets.symmetric(
                                vertical: 12, horizontal: 0),
                            child: Text(
                              "To",
                              style:
                                  TextStyle(color: Colors.black, fontSize: 17),
                            ),
                          )),
                    ),
                  ),
                  //Select from List of Users
                  Container(
                    margin: EdgeInsets.only(left: 50, top: 30),
                    child: Text(
                      'Select a user whom you will send a message',
                      style: GoogleFonts.roboto(
                          color: Colors.black54,
                          fontWeight: FontWeight.w500,
                          fontSize: 16),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(left: 30, right: 30, top: 30),
                    width: double.infinity,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Text(
                          'Full Name',
                          style: GoogleFonts.roboto(
                              color: Colors.red,
                              fontWeight: FontWeight.w700,
                              fontSize: 14),
                        ),
                        Text(
                          'User Name',
                          style: GoogleFonts.roboto(
                              color: Colors.red,
                              fontWeight: FontWeight.w700,
                              fontSize: 14),
                        ),
                        Text(
                          'User Type',
                          style: GoogleFonts.roboto(
                              color: Colors.red,
                              fontWeight: FontWeight.w700,
                              fontSize: 14),
                        ),
                        Text(
                          'Action',
                          style: GoogleFonts.roboto(
                              color: Colors.red,
                              fontWeight: FontWeight.w700,
                              fontSize: 14),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    width: double.infinity,
                    height: 220,
                    margin: EdgeInsets.only(left: 30, right: 30, top: 10),
                    decoration: BoxDecoration(
                        color: Color(0xffe5eef8),
                        borderRadius: BorderRadius.circular(10)),
                    child: FutureBuilder(
                      future: getUsersListData(),
                      builder: (context, AsyncSnapshot snapshot) {
                        if (snapshot.hasData) {
                          return ListView.builder(
                            itemCount: snapshot.data!.length,
                            itemBuilder: (context, index) {
                              DocumentSnapshot userData = snapshot.data[index];
                              return Stack(
                                children: [
                                  Container(
                                    margin: EdgeInsets.only(
                                      top: 10,
                                    ),
                                    child: ListTile(
                                      title: Container(
                                        decoration: BoxDecoration(
                                            color: Colors.white,
                                            borderRadius:
                                                BorderRadius.circular(10)),
                                        padding: EdgeInsets.all(10),
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceEvenly,
                                          children: [
                                            Container(
                                              margin: EdgeInsets.only(left: 10),
                                              width: 65,
                                              child: Text(
                                                userData["UserName"],
                                                style: GoogleFonts.roboto(
                                                    color: Colors.black87,
                                                    fontWeight: FontWeight.w400,
                                                    fontSize: 14),
                                              ),
                                            ),
                                            Container(
                                              width: 70,
                                              margin: EdgeInsets.only(left: 20),
                                              child: Text(
                                                userData["FullName"],
                                                style: GoogleFonts.roboto(
                                                    color: Colors.black87,
                                                    fontWeight: FontWeight.w400,
                                                    fontSize: 14),
                                              ),
                                            ),
                                            Container(
                                              width: 55,
                                              margin: EdgeInsets.only(left: 25),
                                              child: Text(
                                                userData["User Type"],
                                                style: GoogleFonts.roboto(
                                                    color: Colors.black87,
                                                    fontWeight: FontWeight.w400,
                                                    fontSize: 14),
                                              ),
                                            ),
                                            InkWell(
                                              onTap: () {
                                                getUserID = userData['UserID'];
                                                getUserName =
                                                    userData["UserName"];
                                                getFullName =
                                                    userData["FullName"];
                                                getUserType =
                                                    userData["User Type"];
                                                getNotifications =
                                                    userData["Notifications"];
                                                getMail = userData["Mail"];
                                                _userName.text = getUserName!;
                                              },
                                              child: Container(
                                                width: 25,
                                                margin:
                                                    EdgeInsets.only(left: 10),
                                                child: Icon(
                                                  Icons.email_outlined,
                                                  size: 30,
                                                  color: Colors.blue,
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              );
                            },
                          );
                        } else {
                          return Container(
                            alignment: Alignment.center,
                            child: CircularProgressIndicator(),
                          );
                        }
                      },
                    ),
                  ),
                  //Subject
                  Container(
                    margin: EdgeInsets.only(top: 30, left: 30, right: 30),
                    child: TextFormField(
                      validator: (value) {
                        if (value!.isEmpty) {
                          return 'Subject is required';
                        } else {
                          return null;
                        }
                      },
                      controller: _subject..text = "",
                      decoration: InputDecoration(
                        hintText: 'Subject',
                      ),
                    ),
                  ),
                  //Content
                  Container(
                    margin: EdgeInsets.only(top: 10, left: 30, right: 30),
                    child: TextFormField(
                      validator: (value) {
                        if (value!.isEmpty) {
                          return 'Cannot send an message without its content!';
                        } else {
                          return null;
                        }
                      },
                      decoration: InputDecoration(
                          filled: true,
                          fillColor: Colors.white,
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10.0),
                            borderSide: BorderSide.none,
                          ),
                          hintText: 'Write your message here...'),
                      controller: _content..text = "",
                      maxLines: 8,
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      Container(
                        margin: EdgeInsets.only(top: 20, right: 40, bottom: 20),
                        decoration: BoxDecoration(
                          color: Colors.indigo,
                          borderRadius: BorderRadius.circular(10),
                        ),
                        padding: EdgeInsets.all(10),
                        child: InkWell(
                          onTap: () {
                            final isValid = formKey.currentState!.validate();

                            if (isValid) {
                              if (getUserType == 'I am a Donor') {
                                //Notification Count
                                int x = int.parse(getNotifications!) + 1;
                                getNotifications = x.toString();

                                //Mail count
                                int y = int.parse(getMail!) + 1;
                                getMail = y.toString();

                                sendMessageDonor();
                                sendMessageAll();
                                sendMessage();
                                updateDonorNotification();
                                updateAllNotification();
                                updateDonorMail();
                                updateAllMail();
                                logAdminSendMessage();
                                showMessageSentToast();
                                Navigator.of(context).pop();
                                _userName.text = "";
                                _subject.text = "";
                                _content.text = "";
                              } else if (getUserType == 'I am a Patient') {
                                //Notification Count
                                int x = int.parse(getNotifications!) + 1;
                                getNotifications = x.toString();

                                //Mail count
                                int y = int.parse(getMail!) + 1;
                                getMail = y.toString();

                                sendMessagePatient();
                                sendMessage();
                                sendMessageAll();
                                updatePatientNotification();
                                updateAllNotification();
                                updatePatientMail();
                                updateAllMail();
                                logAdminSendMessage();
                                showMessageSentToast();
                                Navigator.of(context).pop();
                                _userName.text = "";
                                _subject.text = "";
                                _content.text = "";
                              }
                            }
                          },
                          child: Text(
                            'Send',
                            style: GoogleFonts.roboto(
                                color: Colors.white,
                                fontWeight: FontWeight.w400,
                                fontSize: 14),
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
        );
      });

  //Show Sent Success
  void showMessageSentToast() => toast.showToast(
      child: buildMessageSentToast(), gravity: ToastGravity.CENTER);

  Widget buildMessageSentToast() => Container(
        padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
        decoration: BoxDecoration(
          color: Colors.green[300],
          borderRadius: BorderRadius.circular(5),
        ),
        child: Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            Text(
              "Message Sent!",
              style: TextStyle(color: Colors.black87, fontSize: 16),
            ),
          ],
        ),
      );

  getUsersListData() async {
    final firestore = FirebaseFirestore.instance;
    QuerySnapshot user = await firestore
        .collection('Users')
        .doc('All')
        .collection('Lists')
        .get();
    return user.docs;
  }

  sendMessage() async {
    String formattedTime = DateFormat('kk:mm:s:a').format(now);
    String formattedDate = formatter.format(now);
    await FirebaseFirestore.instance
        .collection('Email')
        .doc(_subject.text)
        .set({
      'From': 'Offical Admin',
      'To': getUserName,
      'Subject': _subject.text,
      'Body': _content.text,
      'FullName': getFullName,
      'User Type': getUserType,
      'UserID': getUserID,
      'Date': formattedDate,
      'Time': formattedTime
    });
  }

  sendMessageDonor() async {
    String? uid = getUserID;
    String formattedTime = DateFormat('kk:mm:s:a').format(now);
    String formattedDate = formatter.format(now);
    await FirebaseFirestore.instance
        .collection('Users')
        .doc('Donor')
        .collection('Lists')
        .doc(uid)
        .collection('Messages')
        .doc(_subject.text)
        .set({
      'From': 'Offical Admin',
      'To': getUserName,
      'Subject': _subject.text,
      'Body': _content.text,
      'FullName': getFullName,
      'User Type': getUserType,
      'UserID': getUserID,
      'Date': formattedDate,
      'Time': formattedTime
    });
  }

  sendMessagePatient() async {
    String? uid = getUserID;
    String formattedTime = DateFormat('kk:mm:s:a').format(now);
    String formattedDate = formatter.format(now);
    await FirebaseFirestore.instance
        .collection('Users')
        .doc('Patient')
        .collection('Lists')
        .doc(uid)
        .collection('Messages')
        .doc(_subject.text)
        .set({
      'From': 'Offical Admin',
      'To': getUserName,
      'Subject': _subject.text,
      'Body': _content.text,
      'FullName': getFullName,
      'User Type': getUserType,
      'UserID': getUserID,
      'Date': formattedDate,
      'Time': formattedTime
    });
  }

  sendMessageAll() async {
    String? uid = getUserID;
    String formattedTime = DateFormat('kk:mm:s:a').format(now);
    String formattedDate = formatter.format(now);
    await FirebaseFirestore.instance
        .collection('Users')
        .doc('All')
        .collection('Lists')
        .doc(uid)
        .collection('Messages')
        .doc(_subject.text)
        .set({
      'From': 'Offical Admin',
      'To': getUserName,
      'Subject': _subject.text,
      'Body': _content.text,
      'FullName': getFullName,
      'User Type': getUserType,
      'UserID': getUserID,
      'Date': formattedDate,
      'Time': formattedTime
    });
  }

  updateDonorNotification() async {
    String? uid = getUserID;
    await FirebaseFirestore.instance
        .collection('Users')
        .doc('Donor')
        .collection('Lists')
        .doc(uid)
        .update({
      'Notifications': getNotifications,
    });
  }

  updatePatientNotification() async {
    String? uid = getUserID;
    await FirebaseFirestore.instance
        .collection('Users')
        .doc('Patient')
        .collection('Lists')
        .doc(uid)
        .update({
      'Notifications': getNotifications,
    });
  }

  updateAllNotification() async {
    String? uid = getUserID;
    await FirebaseFirestore.instance
        .collection('Users')
        .doc('All')
        .collection('Lists')
        .doc(uid)
        .update({
      'Notifications': getNotifications,
    });
  }

  updateDonorMail() async {
    String? uid = getUserID;
    await FirebaseFirestore.instance
        .collection('Users')
        .doc('Donor')
        .collection('Lists')
        .doc(uid)
        .update({
      'Mail': getMail,
    });
  }

  updatePatientMail() async {
    String? uid = getUserID;
    await FirebaseFirestore.instance
        .collection('Users')
        .doc('Patient')
        .collection('Lists')
        .doc(uid)
        .update({
      'Mail': getMail,
    });
  }

  updateAllMail() async {
    String? uid = getUserID;
    await FirebaseFirestore.instance
        .collection('Users')
        .doc('All')
        .collection('Lists')
        .doc(uid)
        .update({
      'Mail': getMail,
    });
  }

  //Logs in the database
  logClickedDashboard() async {
    String formattedTime = DateFormat('kk:mm:s:a').format(now);
    String formattedDate = formatter.format(now);
    await FirebaseFirestore.instance.collection("Logs").add({
      'Message': "Admin Navigate to the Dashboard Page",
      'User': "Official Admin",
      'Logged Date': formattedDate,
      'Logged Time': formattedTime,
    });
  }

  logClickedUsers() async {
    String formattedTime = DateFormat('kk:mm:s:a').format(now);
    String formattedDate = formatter.format(now);
    await FirebaseFirestore.instance.collection("Logs").add({
      'Message': "Admin Navigate to the Users Page",
      'User': "Official Admin",
      'Logged Date': formattedDate,
      'Logged Time': formattedTime,
    });
  }

  logClickedArchives() async {
    String formattedTime = DateFormat('kk:mm:s:a').format(now);
    String formattedDate = formatter.format(now);
    await FirebaseFirestore.instance.collection("Logs").add({
      'Message': "Admin Navigate to the Archives Page",
      'User': "Official Admin",
      'Logged Date': formattedDate,
      'Logged Time': formattedTime,
    });
  }

  logClickedDataBaseLogs() async {
    String formattedTime = DateFormat('kk:mm:s:a').format(now);
    String formattedDate = formatter.format(now);
    await FirebaseFirestore.instance.collection("Logs").add({
      'Message': "Admin Navigate to the Database Logs Page",
      'User': "Official Admin",
      'Logged Date': formattedDate,
      'Logged Time': formattedTime,
    });
  }

  logClickedPosts() async {
    String formattedTime = DateFormat('kk:mm:s:a').format(now);
    String formattedDate = formatter.format(now);
    await FirebaseFirestore.instance.collection("Logs").add({
      'Message': "Admin Navigate to the Posts Page",
      'User': "Official Admin",
      'Logged Date': formattedDate,
      'Logged Time': formattedTime,
    });
  }

  logClickedAnalytics() async {
    String formattedTime = DateFormat('kk:mm:s:a').format(now);
    String formattedDate = formatter.format(now);
    await FirebaseFirestore.instance.collection("Logs").add({
      'Message': "Admin Navigate to the Analytics Page",
      'User': "Official Admin",
      'Logged Date': formattedDate,
      'Logged Time': formattedTime,
    });
  }

  logClickedMatching() async {
    String formattedTime = DateFormat('kk:mm:s:a').format(now);
    String formattedDate = formatter.format(now);
    await FirebaseFirestore.instance.collection("Logs").add({
      'Message': "Admin Navigate to the Matching Page",
      'User': "Official Admin",
      'Logged Date': formattedDate,
      'Logged Time': formattedTime,
    });
  }

  logClickedMatchUsers() async {
    String formattedTime = DateFormat('kk:mm:s:a').format(now);
    String formattedDate = formatter.format(now);
    await FirebaseFirestore.instance.collection("Logs").add({
      'Message': "Admin Navigate to the Match Users Page",
      'User': "Official Admin",
      'Logged Date': formattedDate,
      'Logged Time': formattedTime,
    });
  }

  logClickedArchiveMatchUsers() async {
    String formattedTime = DateFormat('kk:mm:s:a').format(now);
    String formattedDate = formatter.format(now);
    await FirebaseFirestore.instance.collection("Logs").add({
      'Message': "Admin Navigate to the Match Users Page",
      'User': "Official Admin",
      'Logged Date': formattedDate,
      'Logged Time': formattedTime,
    });
  }

  logClickedMail() async {
    String formattedTime = DateFormat('kk:mm:s:a').format(now);
    String formattedDate = formatter.format(now);
    await FirebaseFirestore.instance.collection("Logs").add({
      'Message': "Admin Navigate to the Mail Page",
      'User': "Official Admin",
      'Logged Date': formattedDate,
      'Logged Time': formattedTime,
    });
  }

  logAdminLogout() async {
    String formattedTime = DateFormat('kk:mm:s:a').format(now);
    String formattedDate = formatter.format(now);
    await FirebaseFirestore.instance.collection("Logs").add({
      'Message': "Admin Log Out",
      'User': "Official Admin",
      'Logged Date': formattedDate,
      'Logged Time': formattedTime,
    });
  }

  logAdminSendMessage() async {
    String formattedTime = DateFormat('kk:mm:s:a').format(now);
    String formattedDate = formatter.format(now);
    await FirebaseFirestore.instance.collection("Logs").add({
      'Message': "Admin has sent an Email to " + getUserName!,
      'User': "Official Admin",
      'Logged Date': formattedDate,
      'Logged Time': formattedTime,
    });
  }
}
