import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class AdminLogs extends StatefulWidget {
  AdminLogs({Key? key}) : super(key: key);

  @override
  _AdminLogsState createState() => _AdminLogsState();
}

class _AdminLogsState extends State<AdminLogs> {
  String? usersListValue;

  final usersList = [
    'All Registered',
    'Donor (Registered)',
    'Patient (Registered)',
    'All Tagged',
    'Donor (Tagged)',
    'Patient (Tagged)'
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xffe5eef8),
      body: SingleChildScrollView(
        child: Wrap(
          spacing: 8.0, // gap between adjacent chips
          runSpacing: 4.0, // gap between lines
          direction: Axis.horizontal,
          children: <Widget>[
            Container(
              margin: EdgeInsets.only(top: 60, left: 100),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    'System Logs',
                    style: GoogleFonts.roboto(
                        color: Colors.black54,
                        fontSize: 40,
                        fontWeight: FontWeight.w400),
                  ),
                ],
              ),
            ),
            Container(
              height: MediaQuery.of(context).size.height * 0.655,
              width: MediaQuery.of(context).size.width * 0.755,
              margin: EdgeInsets.only(top: 20, left: 50, bottom: 100),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: Colors.white,
              ),
              child: Stack(
                children: [
                  FutureBuilder(
                    future: getLogsData(),
                    builder: (context, AsyncSnapshot snapshot) {
                      if (snapshot.hasData) {
                        return ListView.builder(
                          itemCount: snapshot.data!.length,
                          itemBuilder: (context, index) {
                            DocumentSnapshot userData = snapshot.data[index];

                            return Stack(
                              children: [
                                Container(
                                  decoration: BoxDecoration(
                                      color: Colors.red[100],
                                      borderRadius: BorderRadius.circular(10)),
                                  margin: EdgeInsets.only(top: 10),
                                  child: ListTile(
                                    title: Column(
                                      children: [
                                        Row(
                                          children: [
                                            //Date
                                            Row(
                                              children: [
                                                Text(
                                                  '[Day] ',
                                                  style: GoogleFonts.roboto(
                                                    color: Colors.purple[400],
                                                    fontWeight: FontWeight.w600,
                                                    fontSize: 16,
                                                  ),
                                                ),
                                                Text(
                                                  userData["Logged Date"],
                                                  style: GoogleFonts.roboto(
                                                      color: Colors.black87,
                                                      fontWeight:
                                                          FontWeight.w600,
                                                      fontSize: 16,
                                                      letterSpacing: 2),
                                                ),
                                              ],
                                            ),
                                            //Time
                                            Container(
                                              margin: EdgeInsets.only(left: 20),
                                              child: Row(
                                                children: [
                                                  Text(
                                                    '[Time] ',
                                                    style: GoogleFonts.roboto(
                                                      color: Colors.brown[400],
                                                      fontWeight:
                                                          FontWeight.w600,
                                                      fontSize: 16,
                                                    ),
                                                  ),
                                                  Text(
                                                    userData["Logged Time"],
                                                    style: GoogleFonts.roboto(
                                                        color: Colors.black87,
                                                        fontWeight:
                                                            FontWeight.w600,
                                                        fontSize: 16,
                                                        letterSpacing: 2),
                                                  ),
                                                ],
                                              ),
                                            ),
                                            Container(
                                              margin: EdgeInsets.only(left: 20),
                                              child: Row(
                                                children: [
                                                  Text(
                                                    '[Info Message] ',
                                                    style: GoogleFonts.roboto(
                                                      color: Colors.blue[400],
                                                      fontWeight:
                                                          FontWeight.w600,
                                                      fontSize: 16,
                                                    ),
                                                  ),
                                                  Text(
                                                    userData["Message"],
                                                    style: GoogleFonts.roboto(
                                                        color: Colors.black87,
                                                        fontWeight:
                                                            FontWeight.w600,
                                                        fontSize: 16,
                                                        letterSpacing: 2),
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ],
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            );
                          },
                        );
                      } else {
                        return Text('Users Data Not Available');
                      }
                    },
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  //Filter Users DropdownMenu (NOT USED ATM)
  DropdownMenuItem<String> buildUserListMenuItem(String usersList) =>
      DropdownMenuItem(
        value: usersList,
        child: Container(
          child: Text(
            '   ' + usersList,
            style: GoogleFonts.roboto(color: Colors.black, fontSize: 15),
          ),
        ),
      );

  //Display All System Logs
  getLogsData() async {
    final firestore = FirebaseFirestore.instance;
    QuerySnapshot user = await firestore.collection('Logs').get();
    return user.docs;
  }
}
