import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:intl/intl.dart';
import 'package:crypto/crypto.dart';
import 'dart:convert'; // for the utf8.encode method

class AdminArchivesMatchUsersPage extends StatefulWidget {
  AdminArchivesMatchUsersPage({Key? key}) : super(key: key);

  @override
  _AdminArchivesMatchUsersPageState createState() =>
      _AdminArchivesMatchUsersPageState();
}

class _AdminArchivesMatchUsersPageState
    extends State<AdminArchivesMatchUsersPage> {
  //Display User Table
  getUsersData() async {
    final firestore = FirebaseFirestore.instance;
    QuerySnapshot user =
        await firestore.collection('Archive Match Users').get();

    return user.docs;
  }

  String? usersListValue;

  final usersList = [
    'All Registered',
    'Donor (Registered)',
    'Patient (Registered)',
    'All Tagged',
    'Donor (Tagged)',
    'Patient (Tagged)'
  ];

  //Toast
  final toast = FToast();
  @override
  void initState() {
    super.initState();

    toast.init(context);
  }

  //For  Archived
  String? getUid;
  String? getFullName;
  String? getUserName;
  String? getEmail;
  String? getGender;
  String? getUserType;
  String? getAge;
  String? getBloodType;
  String? getKidneySize;
  String? getSignature;
  String? getMobileNumber;
  String? getRegisteredDate;
  String? getRegisteredTime;
  String? getViews;
  String? getComments;
  String? getLikes;
  String? getPinned;
  String? getDonated;
  String? getReceived;
  String? getPosts;
  String? getNotifications;
  String? getReplies;
  String? getMatch;
  String? getMail;
  String? getStatus;

  //Time and Date
  var now = new DateTime.now();
  var formatter = new DateFormat('dd-MM-yyyy');

  String? getCurrentMatch;
  String? getAllCurrentMatch;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xffe5eef8),
      body: SingleChildScrollView(
        child: Wrap(
          spacing: 8.0, // gap between adjacent chips
          runSpacing: 4.0, // gap between lines
          direction: Axis.horizontal,
          children: <Widget>[
            Container(
              margin: EdgeInsets.only(top: 60, left: 100),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Text(
                    'Archived Match Users',
                    style: GoogleFonts.roboto(
                        color: Colors.black54,
                        fontSize: 40,
                        fontWeight: FontWeight.w400),
                  ),
                ],
              ),
            ),
            Container(
              height: MediaQuery.of(context).size.height * 0.855,
              width: MediaQuery.of(context).size.width * 0.755,
              margin: EdgeInsets.only(top: 20, left: 50, bottom: 100),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: Colors.white,
              ),
              child: Stack(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Container(
                        margin: EdgeInsets.only(top: 20, left: 2),
                        child: Text(
                          'Identifier'.toUpperCase(),
                          style: GoogleFonts.roboto(
                              color: Colors.black45,
                              fontWeight: FontWeight.w500,
                              fontSize: 16),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(top: 20, left: 4),
                        child: Text(
                          'User Name'.toUpperCase(),
                          style: GoogleFonts.roboto(
                              color: Colors.black45,
                              fontWeight: FontWeight.w500,
                              fontSize: 16),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(top: 20, left: 8),
                        child: Text(
                          'Email'.toUpperCase(),
                          style: GoogleFonts.roboto(
                              color: Colors.black45,
                              fontWeight: FontWeight.w500,
                              fontSize: 16),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(top: 20, left: 58),
                        child: Text(
                          'Gender'.toUpperCase(),
                          style: GoogleFonts.roboto(
                              color: Colors.black45,
                              fontWeight: FontWeight.w500,
                              fontSize: 16),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(top: 20, left: 20),
                        child: Text(
                          'Age'.toUpperCase(),
                          style: GoogleFonts.roboto(
                              color: Colors.black45,
                              fontWeight: FontWeight.w500,
                              fontSize: 16),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(top: 20),
                        child: Text(
                          'Identifier\nType'.toUpperCase(),
                          textAlign: TextAlign.center,
                          style: GoogleFonts.roboto(
                              color: Colors.black45,
                              fontWeight: FontWeight.w500,
                              fontSize: 16),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(top: 20),
                        child: Text(
                          'Blood\nType'.toUpperCase(),
                          textAlign: TextAlign.center,
                          style: GoogleFonts.roboto(
                              color: Colors.black45,
                              fontWeight: FontWeight.w500,
                              fontSize: 16),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(top: 20),
                        child: Text(
                          'Kidney\nSize(cm)'.toUpperCase(),
                          textAlign: TextAlign.center,
                          style: GoogleFonts.roboto(
                              color: Colors.black45,
                              fontWeight: FontWeight.w500,
                              fontSize: 16),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(top: 20),
                        child: Text(
                          'Mobile Number'.toUpperCase(),
                          style: GoogleFonts.roboto(
                              color: Colors.black45,
                              fontWeight: FontWeight.w500,
                              fontSize: 16),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(top: 20),
                        child: Text(
                          'Status'.toUpperCase(),
                          style: GoogleFonts.roboto(
                              color: Colors.black45,
                              fontWeight: FontWeight.w500,
                              fontSize: 16),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(
                          top: 20,
                        ),
                        child: Text(
                          'Delete'.toUpperCase(),
                          style: GoogleFonts.roboto(
                              color: Colors.black45,
                              fontWeight: FontWeight.w500,
                              fontSize: 16),
                        ),
                      ),
                    ],
                  ),
                  Container(
                    margin: EdgeInsets.only(
                      top: 50,
                    ),
                    child: FutureBuilder(
                      future: getUsersData(),
                      builder: (context, AsyncSnapshot snapshot) {
                        if (snapshot.hasData) {
                          return ListView.builder(
                            itemCount: snapshot.data!.length,
                            itemBuilder: (context, index) {
                              DocumentSnapshot userData = snapshot.data[index];
                              var hashMobileNumber =
                                  utf8.encode(userData["Mobile Number"]);
                              var displayMobileNumber =
                                  sha256.convert(hashMobileNumber);
                              var hashEmailAddress =
                                  utf8.encode(userData["Email Address"]);
                              var displayEmailAddress =
                                  sha256.convert(hashEmailAddress);
                              return Stack(
                                children: [
                                  Container(
                                    margin: EdgeInsets.only(
                                      top: 20,
                                    ),
                                    child: ListTile(
                                      title: Row(
                                        children: [
                                          Container(
                                            margin: EdgeInsets.only(left: 38),
                                            width: 110,
                                            child: Text(
                                              userData["FullName"],
                                              style: GoogleFonts.roboto(
                                                  color: Colors.black87,
                                                  fontWeight: FontWeight.w400,
                                                  fontSize: 14),
                                            ),
                                          ),
                                          Container(
                                            margin: EdgeInsets.only(left: 26),
                                            width: 120,
                                            child: Text(
                                              userData["UserName"],
                                              style: GoogleFonts.roboto(
                                                  color: Colors.black87,
                                                  fontWeight: FontWeight.w400,
                                                  fontSize: 14),
                                            ),
                                          ),
                                          Container(
                                            margin: EdgeInsets.only(left: 26),
                                            width: 130,
                                            child: Text(
                                              displayEmailAddress.toString(),
                                              style: GoogleFonts.roboto(
                                                  color: Colors.black87,
                                                  fontWeight: FontWeight.w400,
                                                  fontSize: 14),
                                            ),
                                          ),
                                          Container(
                                            margin: EdgeInsets.only(left: 28),
                                            width: 105,
                                            child: Text(
                                              userData["Gender"],
                                              style: GoogleFonts.roboto(
                                                  color: Colors.black87,
                                                  fontWeight: FontWeight.w400,
                                                  fontSize: 14),
                                            ),
                                          ),
                                          Container(
                                            width: 20,
                                            margin: EdgeInsets.only(left: 30),
                                            child: Text(
                                              userData["Age"],
                                              style: GoogleFonts.roboto(
                                                  color: Colors.black87,
                                                  fontWeight: FontWeight.w400,
                                                  fontSize: 14),
                                            ),
                                          ),
                                          Container(
                                            margin: EdgeInsets.only(left: 60),
                                            width: 100,
                                            child: Text(
                                              userData["User Type"],
                                              style: GoogleFonts.roboto(
                                                  color: Colors.black87,
                                                  fontWeight: FontWeight.w400,
                                                  fontSize: 14),
                                            ),
                                          ),
                                          Container(
                                            margin: EdgeInsets.only(left: 50),
                                            width: 20,
                                            child: Text(
                                              userData["Blood Type"],
                                              style: GoogleFonts.roboto(
                                                  color: Colors.black87,
                                                  fontWeight: FontWeight.w400,
                                                  fontSize: 14),
                                            ),
                                          ),
                                          Container(
                                            margin: EdgeInsets.only(left: 80),
                                            width: 30,
                                            child: Text(
                                              userData["Kidney Size"],
                                              style: GoogleFonts.roboto(
                                                  color: Colors.black87,
                                                  fontWeight: FontWeight.w400,
                                                  fontSize: 14),
                                            ),
                                          ),
                                          Container(
                                            margin: EdgeInsets.only(left: 80),
                                            width: 100,
                                            child: Text(
                                              displayMobileNumber.toString(),
                                              style: GoogleFonts.roboto(
                                                  color: Colors.black87,
                                                  fontWeight: FontWeight.w400,
                                                  fontSize: 14),
                                            ),
                                          ),
                                          Container(
                                            width: 80,
                                            margin: EdgeInsets.only(left: 65),
                                            child: Text(
                                              userData["Status"],
                                              style: GoogleFonts.roboto(
                                                  color: Colors.black87,
                                                  fontWeight: FontWeight.w400,
                                                  fontSize: 14),
                                            ),
                                          ),
                                          Container(
                                            margin: EdgeInsets.only(left: 20),
                                            child: Row(
                                              children: [
                                                //Return to Users Page
                                                IconButton(
                                                  onPressed: () {
                                                    getUid = userData["UserID"];
                                                    getFullName =
                                                        userData["FullName"];
                                                    getUserName =
                                                        userData["UserName"];
                                                    getEmail = userData[
                                                        "Email Address"];
                                                    getGender =
                                                        userData["Gender"];
                                                    getAge = userData["Age"];
                                                    getUserType =
                                                        userData["User Type"];
                                                    getBloodType =
                                                        userData["Blood Type"];
                                                    getKidneySize =
                                                        userData["Kidney Size"];
                                                    getMobileNumber = userData[
                                                        "Mobile Number"];
                                                    getStatus =
                                                        userData["Status"];

                                                    setState(() {
                                                      if (getUserType ==
                                                              'I am a Donor' &&
                                                          getStatus ==
                                                              "Full Registration Completed") {
                                                        logAdminRestoredDonor();
                                                        //Move to List
                                                        returnDonor();
                                                        returnAll();
                                                        //Temporary remove from Archived table

                                                        returnDeleteAll();
                                                        returnDeleteMatchAll();
                                                        //Show Success Message
                                                        showReturnToast();
                                                      } else if (getUserType ==
                                                              'I am a Patient' &&
                                                          getStatus ==
                                                              "Full Registration Completed") {
                                                        logAdminRestoredPatient();
                                                        //Move to List
                                                        returnPatient();
                                                        returnAll();
                                                        //Temporary remove from Archived table

                                                        returnDeleteAll();
                                                        returnDeleteMatchAll();
                                                        //Show Success Message
                                                        showReturnToast();
                                                      } else if (getUserType ==
                                                              'I am a Donor' &&
                                                          getStatus ==
                                                              "Tagged by Admin (Doctor)") {
                                                        logAdminRestoredDonor();
                                                        //Move to List
                                                        returnDonor();
                                                        returnAll();
                                                        //Temporary remove from Archived table

                                                        returnDeleteAll();
                                                        returnDeleteMatchAll();
                                                        //Show Success Message
                                                        showReturnToast();
                                                      } else if (getUserType ==
                                                              'I am a Patient' &&
                                                          getStatus ==
                                                              "Tagged by Admin (Doctor)") {
                                                        logAdminRestoredPatient();
                                                        //Move to List
                                                        returnPatient();
                                                        returnAll();
                                                        //Temporary remove from Archived table

                                                        returnDeleteAll();
                                                        returnDeleteMatchAll();
                                                        //Show Success Message
                                                        showReturnToast();
                                                      } else if (getUserType ==
                                                              'I am a Donor' &&
                                                          getStatus ==
                                                              "Potential Kidney Match Done") {
                                                        getStatus =
                                                            "Returned to List for Re-match";
                                                        logAdminRestoredDonor();
                                                        //Move to List
                                                        returnDonor();
                                                        returnAll();
                                                        //Temporary remove from Archived table

                                                        returnDeleteAll();
                                                        returnDeleteMatchAll();
                                                        //Show Success Message
                                                        showReturnToast();
                                                      } else if (getUserType ==
                                                              'I am a Patient' &&
                                                          getStatus ==
                                                              "Potential Kidney Match Done") {
                                                        getStatus =
                                                            "Returned to List for Re-match";
                                                        logAdminRestoredPatient();
                                                        //Move to List
                                                        returnPatient();
                                                        returnAll();
                                                        //Temporary remove from Archived table

                                                        returnDeleteAll();
                                                        returnDeleteMatchAll();
                                                        //Show Success Message
                                                        showReturnToast();
                                                      }
                                                    });
                                                  },
                                                  icon: Icon(
                                                    Icons.restore_outlined,
                                                    size: 20,
                                                    color: Colors.blue,
                                                  ),
                                                ),
                                                //Delete Button
                                                IconButton(
                                                  onPressed: () {
                                                    getUid = userData["UserID"];
                                                    getFullName =
                                                        userData["FullName"];
                                                    getUserName =
                                                        userData["UserName"];
                                                    getEmail = userData[
                                                        "Email Address"];
                                                    getGender =
                                                        userData["Gender"];
                                                    getAge = userData["Age"];
                                                    getUserType =
                                                        userData["User Type"];
                                                    getBloodType =
                                                        userData["Blood Type"];
                                                    getKidneySize =
                                                        userData["Kidney Size"];
                                                    getMobileNumber = userData[
                                                        "Mobile Number"];
                                                    getStatus =
                                                        userData["Status"];

                                                    setState(() {
                                                      showCustomDialog(context);
                                                    });
                                                  },
                                                  icon: Icon(
                                                    Icons
                                                        .delete_forever_outlined,
                                                    size: 20,
                                                    color: Colors.grey,
                                                  ),
                                                )
                                              ],
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 18),
                                    child: Divider(),
                                  ),
                                ],
                              );
                            },
                          );
                        } else {
                          return Text('Users Data Not Available');
                        }
                      },
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  //Filter Users DropdownMenu
  DropdownMenuItem<String> buildUserListMenuItem(String usersList) =>
      DropdownMenuItem(
        value: usersList,
        child: Container(
          child: Text(
            '   ' + usersList,
            style: GoogleFonts.roboto(color: Colors.black, fontSize: 15),
          ),
        ),
      );

  //Return User Data
  returnDonor() async {
    String? uid = getUid;
    await FirebaseFirestore.instance
        .collection("Match Users")
        .doc('Donor')
        .collection('Lists')
        .doc(uid)
        .set({
      'UserID': getUid,
      'FullName': getFullName,
      'UserName': getUserName,
      'Email Address': getEmail,
      'Gender': getGender,
      'Age': getAge,
      'User Type': getUserType,
      'Blood Type': getBloodType,
      'Kidney Size': getKidneySize,
      'Mobile Number': getMobileNumber,
      'Status': getStatus,
    });
  }

  returnPatient() async {
    String? uid = getUid;
    await FirebaseFirestore.instance
        .collection("Match Users")
        .doc('Patient')
        .collection('Lists')
        .doc(uid)
        .set({
      'UserID': getUid,
      'FullName': getFullName,
      'UserName': getUserName,
      'Email Address': getEmail,
      'Gender': getGender,
      'Age': getAge,
      'User Type': getUserType,
      'Blood Type': getBloodType,
      'Kidney Size': getKidneySize,
      'Mobile Number': getMobileNumber,
      'Status': getStatus,
    });
  }

  //Both Donor and Patient
  returnAll() async {
    String? uid = getUid;
    await FirebaseFirestore.instance
        .collection("Match Users")
        .doc('All')
        .collection('Lists')
        .doc(uid)
        .set({
      'UserID': getUid,
      'FullName': getFullName,
      'UserName': getUserName,
      'Email Address': getEmail,
      'Gender': getGender,
      'Age': getAge,
      'User Type': getUserType,
      'Blood Type': getBloodType,
      'Kidney Size': getKidneySize,
      'Mobile Number': getMobileNumber,
      'Status': getStatus,
    });
  }

  //Delete Temporary
  returnDeleteAll() async {
    String? uid = getUid;
    await FirebaseFirestore.instance
        .collection("Archive Match Users")
        .doc(uid)
        .delete();
  }

  returnDeleteMatchAll() async {
    String? uid = getUid;
    await FirebaseFirestore.instance
        .collection("Kidney Match")
        .doc(uid)
        .delete();
  }

  //Delete Forever

  returnDeleteForeverAll() async {
    String? uid = getUid;
    await FirebaseFirestore.instance
        .collection("Archive Match Users")
        .doc(uid)
        .delete();
  }

  returnDeleteForeverAll2() async {
    String? uid = getUid;
    await FirebaseFirestore.instance
        .collection("Kidney Match")
        .doc(uid)
        .delete();
  }

  deleteMatch() async {
    String? uid = getCurrentMatch;
    await FirebaseFirestore.instance
        .collection("Kidney Match")
        .doc(uid)
        .delete();
  }

  deleteAllMatch() async {
    String? uid = getCurrentMatch;
    String? mid = getAllCurrentMatch;
    await FirebaseFirestore.instance
        .collection("Kidney Match")
        .doc(uid)
        .collection("Matches")
        .doc(mid)
        .delete();
  }

  deleteArchiveMatch() async {
    String? uid = getCurrentMatch;

    await FirebaseFirestore.instance
        .collection("Archive Match Users")
        .doc(uid)
        .delete();
  }

  deleteAllArchiveMatch() async {
    String? uid = getCurrentMatch;
    String? mid = getAllCurrentMatch;
    await FirebaseFirestore.instance
        .collection("Archive Match Users")
        .doc(uid)
        .collection("Matches")
        .doc(mid)
        .delete();
  }

  //Show Return Success
  void showReturnToast() =>
      toast.showToast(child: buildReturnToast(), gravity: ToastGravity.CENTER);

  Widget buildReturnToast() => Container(
        padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
        decoration: BoxDecoration(
          color: Colors.green[300],
          borderRadius: BorderRadius.circular(5),
        ),
        child: Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            Text(
              "Restored",
              style: TextStyle(color: Colors.black87, fontSize: 16),
            ),
          ],
        ),
      );

  //Show Delete Success
  void showDeleteToast() =>
      toast.showToast(child: buildDeleteToast(), gravity: ToastGravity.CENTER);

  Widget buildDeleteToast() => Container(
        padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
        decoration: BoxDecoration(
          color: Colors.green[300],
          borderRadius: BorderRadius.circular(5),
        ),
        child: Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            Text(
              "Deleted",
              style: TextStyle(color: Colors.black87, fontSize: 16),
            ),
          ],
        ),
      );

  //Alert Dialog Method
  void showCustomDialog(BuildContext context) => showDialog(
      context: context,
      builder: (BuildContext context) {
        return Dialog(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20.0)), //this right here
          child: Container(
            height: 200,
            width: 100,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  "Delete",
                  style: GoogleFonts.roboto(
                      color: Colors.black87,
                      fontWeight: FontWeight.bold,
                      fontSize: 18),
                ),
                Text(
                  "\nAre you sure you want to delete?",
                  style:
                      GoogleFonts.roboto(color: Colors.grey[500], fontSize: 14),
                ),
                Container(
                  margin: EdgeInsets.only(top: 40),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Container(
                        child: InkWell(
                          onTap: () {
                            Navigator.of(context).pop();
                          },
                          child: Container(
                            height: 35,
                            width: 100,
                            decoration: BoxDecoration(
                                color: Colors.red[50],
                                borderRadius: BorderRadius.circular(18)),
                            child: Center(
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Text(
                                  "Cancel",
                                  style: TextStyle(
                                      color: Colors.grey[600], fontSize: 17),
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                      Container(
                        child: InkWell(
                          onTap: () {
                            setState(() {
                              returnDeleteForeverAll();
                              returnDeleteForeverAll2();
                              //Show Delete Message
                              showDeleteToast();
                              Navigator.of(context).pop();
                              logAdminDeleteArchive();
                            });
                          },
                          child: Container(
                            height: 35,
                            width: 100,
                            decoration: BoxDecoration(
                                color: Colors.red[200],
                                borderRadius: BorderRadius.circular(18)),
                            child: Center(
                              child: Text(
                                "Confirm",
                                style: TextStyle(
                                    color: Colors.red[400], fontSize: 17),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        );
      });

  //Logs in the database

  logAdminRestoredDonor() async {
    String formattedTime = DateFormat('kk:mm:s:a').format(now);
    String formattedDate = formatter.format(now);
    await FirebaseFirestore.instance.collection("Logs").add({
      'Message': "Admin has restored donor " + getUserName!,
      'User': "Official Admin",
      'Logged Date': formattedDate,
      'Logged Time': formattedTime,
    });
  }

  logAdminRestoredPatient() async {
    String formattedTime = DateFormat('kk:mm:s:a').format(now);
    String formattedDate = formatter.format(now);
    await FirebaseFirestore.instance.collection("Logs").add({
      'Message': "Admin has restored patient " + getUserName!,
      'User': "Official Admin",
      'Logged Date': formattedDate,
      'Logged Time': formattedTime,
    });
  }

  logAdminRestoredTaggedDonor() async {
    String formattedTime = DateFormat('kk:mm:s:a').format(now);
    String formattedDate = formatter.format(now);
    await FirebaseFirestore.instance.collection("Logs").add({
      'Message': "Admin has restored tagged donor " + getUserName!,
      'User': "Official Admin",
      'Logged Date': formattedDate,
      'Logged Time': formattedTime,
    });
  }

  logAdminRestoredTaggedPatient() async {
    String formattedTime = DateFormat('kk:mm:s:a').format(now);
    String formattedDate = formatter.format(now);
    await FirebaseFirestore.instance.collection("Logs").add({
      'Message': "Admin has restored tagged patient " + getUserName!,
      'User': "Official Admin",
      'Logged Date': formattedDate,
      'Logged Time': formattedTime,
    });
  }

  logAdminDeleteArchive() async {
    String formattedTime = DateFormat('kk:mm:s:a').format(now);
    String formattedDate = formatter.format(now);
    await FirebaseFirestore.instance.collection("Logs").add({
      'Message': "Admin has deleted match user " + getUserName!,
      'User': "Official Admin",
      'Logged Date': formattedDate,
      'Logged Time': formattedTime,
    });
  }
}
