import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:intl/intl.dart';
import 'package:crypto/crypto.dart';
import 'dart:convert'; // for the utf8.encode method

class AdminArchivesPage extends StatefulWidget {
  AdminArchivesPage({Key? key}) : super(key: key);

  @override
  _AdminArchivesPageState createState() => _AdminArchivesPageState();
}

class _AdminArchivesPageState extends State<AdminArchivesPage> {
  //Display User Table
  getUsersData() async {
    final firestore = FirebaseFirestore.instance;
    if (usersListValue == 'Donor (Registered)') {
      QuerySnapshot user = await firestore
          .collection('Archived')
          .doc('Users')
          .collection('Registered')
          .doc('Donor')
          .collection('Lists')
          .get();
      logAdminFilteredDonorOnly();
      return user.docs;
    } else if (usersListValue == 'Patient (Registered)') {
      QuerySnapshot user = await firestore
          .collection('Archived')
          .doc('Users')
          .collection('Registered')
          .doc('Patient')
          .collection('Lists')
          .get();
      logAdminFilteredPatientOnly();
      return user.docs;
    } else if (usersListValue == 'All (Registered)') {
      QuerySnapshot user = await firestore
          .collection('Archived')
          .doc('Users')
          .collection('Registered')
          .doc('All')
          .collection('Lists')
          .get();
      logAdminFilteredAllRegistered();
      return user.docs;
    } else if (usersListValue == 'All Tagged') {
      QuerySnapshot user = await firestore
          .collection('Archived')
          .doc('Users')
          .collection('Tagged Users')
          .doc('All')
          .collection('Lists')
          .get();
      logAdminFilteredAllTagged();
      return user.docs;
    } else if (usersListValue == 'Donor (Tagged)') {
      QuerySnapshot user = await firestore
          .collection('Archived')
          .doc('Users')
          .collection('Tagged Users')
          .doc('Donor')
          .collection('Lists')
          .get();
      logAdminFilteredDonorTagged();
      return user.docs;
    } else if (usersListValue == 'Patient (Tagged)') {
      QuerySnapshot user = await firestore
          .collection('Archived')
          .doc('Users')
          .collection('Tagged Users')
          .doc('Patient')
          .collection('Lists')
          .get();
      logAdminFilteredPatientTagged();
      return user.docs;
    } else {
      QuerySnapshot user = await firestore
          .collection('Archived')
          .doc('Users')
          .collection('Registered')
          .doc('All')
          .collection('Lists')
          .get();

      return user.docs;
    }
  }

  String? usersListValue;

  final usersList = [
    'All Registered',
    'Donor (Registered)',
    'Patient (Registered)',
    'All Tagged',
    'Donor (Tagged)',
    'Patient (Tagged)'
  ];

  //Toast
  final toast = FToast();
  @override
  void initState() {
    super.initState();

    toast.init(context);
  }

  //For  Archived
  String? getUid;
  String? getFullName;
  String? getUserName;
  String? getEmail;
  String? getGender;
  String? getUserType;
  String? getAge;
  String? getBloodType;
  String? getKidneySize;
  String? getSignature;
  String? getMobileNumber;
  String? getRegisteredDate;
  String? getRegisteredTime;
  String? getViews;
  String? getComments;
  String? getLikes;
  String? getPinned;
  String? getDonated;
  String? getReceived;
  String? getPosts;
  String? getNotifications;
  String? getReplies;
  String? getMatch;
  String? getMail;
  String? getStatus;

  //Time and Date
  var now = new DateTime.now();
  var formatter = new DateFormat('dd-MM-yyyy');

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xffe5eef8),
      body: SingleChildScrollView(
        child: Wrap(
          spacing: 8.0, // gap between adjacent chips
          runSpacing: 4.0, // gap between lines
          direction: Axis.horizontal,
          children: <Widget>[
            Container(
              margin: EdgeInsets.only(top: 60, left: 100),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    'Archived Users',
                    style: GoogleFonts.roboto(
                        color: Colors.black54,
                        fontSize: 40,
                        fontWeight: FontWeight.w400),
                  ),
                  Container(
                    margin: EdgeInsets.only(right: 70),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      color: Colors.grey[300],
                    ),
                    width: 240,
                    child: DropdownButtonHideUnderline(
                      child: Stack(
                        children: [
                          Row(
                            children: [
                              Container(
                                height: 45,
                                width: 45,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.only(
                                    topLeft: Radius.circular(10),
                                    bottomLeft: Radius.circular(10),
                                  ),
                                  color: Colors.grey[400],
                                ),
                                child: Icon(Icons.filter_alt),
                              ),
                              Expanded(
                                child: DropdownButtonFormField<String>(
                                  decoration: InputDecoration(
                                      border: InputBorder.none,
                                      fillColor: Colors.red),
                                  hint: Container(
                                    margin: EdgeInsets.only(left: 20),
                                    child: Text(
                                      "Filter Users",
                                      style: GoogleFonts.roboto(
                                          color: Colors.black54,
                                          fontWeight: FontWeight.w500,
                                          fontSize: 16),
                                    ),
                                  ),
                                  value: usersListValue,
                                  isExpanded: true,
                                  items: usersList
                                      .map(buildUserListMenuItem)
                                      .toList(),
                                  onChanged: (value) => setState(
                                    () => this.usersListValue = value,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Container(
              height: MediaQuery.of(context).size.height * 0.855,
              width: MediaQuery.of(context).size.width * 0.755,
              margin: EdgeInsets.only(top: 20, left: 50, bottom: 100),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: Colors.white,
              ),
              child: Stack(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Container(
                        margin: EdgeInsets.only(top: 20, left: 2),
                        child: Text(
                          'Identifier'.toUpperCase(),
                          style: GoogleFonts.roboto(
                              color: Colors.black45,
                              fontWeight: FontWeight.w500,
                              fontSize: 16),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(top: 20, left: 4),
                        child: Text(
                          'User Name'.toUpperCase(),
                          style: GoogleFonts.roboto(
                              color: Colors.black45,
                              fontWeight: FontWeight.w500,
                              fontSize: 16),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(top: 20, left: 8),
                        child: Text(
                          'Email'.toUpperCase(),
                          style: GoogleFonts.roboto(
                              color: Colors.black45,
                              fontWeight: FontWeight.w500,
                              fontSize: 16),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(top: 20, left: 58),
                        child: Text(
                          'Gender'.toUpperCase(),
                          style: GoogleFonts.roboto(
                              color: Colors.black45,
                              fontWeight: FontWeight.w500,
                              fontSize: 16),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(top: 20, left: 20),
                        child: Text(
                          'Age'.toUpperCase(),
                          style: GoogleFonts.roboto(
                              color: Colors.black45,
                              fontWeight: FontWeight.w500,
                              fontSize: 16),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(top: 20),
                        child: Text(
                          'Identifier\nType'.toUpperCase(),
                          textAlign: TextAlign.center,
                          style: GoogleFonts.roboto(
                              color: Colors.black45,
                              fontWeight: FontWeight.w500,
                              fontSize: 16),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(top: 20),
                        child: Text(
                          'Blood\nType'.toUpperCase(),
                          textAlign: TextAlign.center,
                          style: GoogleFonts.roboto(
                              color: Colors.black45,
                              fontWeight: FontWeight.w500,
                              fontSize: 16),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(top: 20),
                        child: Text(
                          'Kidney\nSize(cm)'.toUpperCase(),
                          textAlign: TextAlign.center,
                          style: GoogleFonts.roboto(
                              color: Colors.black45,
                              fontWeight: FontWeight.w500,
                              fontSize: 16),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(top: 20),
                        child: Text(
                          'Mobile Number'.toUpperCase(),
                          style: GoogleFonts.roboto(
                              color: Colors.black45,
                              fontWeight: FontWeight.w500,
                              fontSize: 16),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(top: 20),
                        child: Text(
                          'Status'.toUpperCase(),
                          style: GoogleFonts.roboto(
                              color: Colors.black45,
                              fontWeight: FontWeight.w500,
                              fontSize: 16),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(
                          top: 20,
                        ),
                        child: Text(
                          'Delete'.toUpperCase(),
                          style: GoogleFonts.roboto(
                              color: Colors.black45,
                              fontWeight: FontWeight.w500,
                              fontSize: 16),
                        ),
                      ),
                    ],
                  ),
                  Container(
                    margin: EdgeInsets.only(
                      top: 50,
                    ),
                    child: FutureBuilder(
                      future: getUsersData(),
                      builder: (context, AsyncSnapshot snapshot) {
                        if (snapshot.hasData) {
                          return ListView.builder(
                            itemCount: snapshot.data!.length,
                            itemBuilder: (context, index) {
                              DocumentSnapshot userData = snapshot.data[index];
                              var hashMobileNumber =
                                  utf8.encode(userData["Mobile Number"]);
                              var displayMobileNumber =
                                  sha256.convert(hashMobileNumber);
                              var hashEmailAddress =
                                  utf8.encode(userData["Email Address"]);
                              var displayEmailAddress =
                                  sha256.convert(hashEmailAddress);
                              return Stack(
                                children: [
                                  Container(
                                    margin: EdgeInsets.only(
                                      top: 20,
                                    ),
                                    child: ListTile(
                                      title: Row(
                                        children: [
                                          Container(
                                            margin: EdgeInsets.only(left: 38),
                                            width: 110,
                                            child: Text(
                                              userData["FullName"],
                                              style: GoogleFonts.roboto(
                                                  color: Colors.black87,
                                                  fontWeight: FontWeight.w400,
                                                  fontSize: 14),
                                            ),
                                          ),
                                          Container(
                                            margin: EdgeInsets.only(left: 26),
                                            width: 120,
                                            child: Text(
                                              userData["UserName"],
                                              style: GoogleFonts.roboto(
                                                  color: Colors.black87,
                                                  fontWeight: FontWeight.w400,
                                                  fontSize: 14),
                                            ),
                                          ),
                                          Container(
                                            margin: EdgeInsets.only(left: 26),
                                            width: 130,
                                            child: Text(
                                              displayEmailAddress.toString(),
                                              style: GoogleFonts.roboto(
                                                  color: Colors.black87,
                                                  fontWeight: FontWeight.w400,
                                                  fontSize: 14),
                                            ),
                                          ),
                                          Container(
                                            margin: EdgeInsets.only(left: 28),
                                            width: 105,
                                            child: Text(
                                              userData["Gender"],
                                              style: GoogleFonts.roboto(
                                                  color: Colors.black87,
                                                  fontWeight: FontWeight.w400,
                                                  fontSize: 14),
                                            ),
                                          ),
                                          Container(
                                            width: 20,
                                            margin: EdgeInsets.only(left: 30),
                                            child: Text(
                                              userData["Age"],
                                              style: GoogleFonts.roboto(
                                                  color: Colors.black87,
                                                  fontWeight: FontWeight.w400,
                                                  fontSize: 14),
                                            ),
                                          ),
                                          Container(
                                            margin: EdgeInsets.only(left: 60),
                                            width: 100,
                                            child: Text(
                                              userData["User Type"],
                                              style: GoogleFonts.roboto(
                                                  color: Colors.black87,
                                                  fontWeight: FontWeight.w400,
                                                  fontSize: 14),
                                            ),
                                          ),
                                          Container(
                                            margin: EdgeInsets.only(left: 50),
                                            width: 20,
                                            child: Text(
                                              userData["Blood Type"],
                                              style: GoogleFonts.roboto(
                                                  color: Colors.black87,
                                                  fontWeight: FontWeight.w400,
                                                  fontSize: 14),
                                            ),
                                          ),
                                          Container(
                                            margin: EdgeInsets.only(left: 80),
                                            width: 30,
                                            child: Text(
                                              userData["Kidney Size"],
                                              style: GoogleFonts.roboto(
                                                  color: Colors.black87,
                                                  fontWeight: FontWeight.w400,
                                                  fontSize: 14),
                                            ),
                                          ),
                                          Container(
                                            margin: EdgeInsets.only(left: 80),
                                            width: 100,
                                            child: Text(
                                              displayMobileNumber.toString(),
                                              style: GoogleFonts.roboto(
                                                  color: Colors.black87,
                                                  fontWeight: FontWeight.w400,
                                                  fontSize: 14),
                                            ),
                                          ),
                                          Container(
                                            width: 80,
                                            margin: EdgeInsets.only(left: 65),
                                            child: Text(
                                              userData["Status"],
                                              style: GoogleFonts.roboto(
                                                  color: Colors.black87,
                                                  fontWeight: FontWeight.w400,
                                                  fontSize: 14),
                                            ),
                                          ),
                                          Container(
                                            margin: EdgeInsets.only(left: 20),
                                            child: Row(
                                              children: [
                                                //Return to Users Page
                                                IconButton(
                                                  onPressed: () {
                                                    getUid = userData["UserID"];
                                                    getFullName =
                                                        userData["FullName"];
                                                    getUserName =
                                                        userData["UserName"];
                                                    getEmail = userData[
                                                        "Email Address"];
                                                    getGender =
                                                        userData["Gender"];
                                                    getAge = userData["Age"];
                                                    getUserType =
                                                        userData["User Type"];
                                                    getBloodType =
                                                        userData["Blood Type"];
                                                    getKidneySize =
                                                        userData["Kidney Size"];
                                                    getMobileNumber = userData[
                                                        "Mobile Number"];
                                                    getStatus =
                                                        userData["Status"];
                                                    getSignature =
                                                        userData["Signature"];
                                                    getRegisteredDate =
                                                        userData[
                                                            "Date Registered"];
                                                    getRegisteredTime =
                                                        userData[
                                                            "Time Registered"];
                                                    getViews =
                                                        userData["Views"];
                                                    getComments =
                                                        userData["Comments"];
                                                    getLikes =
                                                        userData["Likes"];
                                                    getPinned =
                                                        userData["Pinned"];
                                                    getDonated =
                                                        userData["Donated"];
                                                    getReceived =
                                                        userData["Received"];
                                                    getPosts =
                                                        userData["Posts"];
                                                    getNotifications = userData[
                                                        "Notifications"];
                                                    getReplies =
                                                        userData["Replies"];
                                                    getMatch =
                                                        userData["Match"];
                                                    getMail = userData["Mail"];

                                                    setState(() {
                                                      if (getUserType ==
                                                              'I am a Donor' &&
                                                          getStatus ==
                                                              "Full Registration Completed") {
                                                        logAdminRestoredDonor();
                                                        //Move to List
                                                        returnDonor();
                                                        returnAll();
                                                        //Temporary remove from Archived table
                                                        returnDeleteDonor();
                                                        returnDeleteAll();
                                                        //Show Success Message
                                                        showReturnToast();
                                                      } else if (getUserType ==
                                                              'I am a Patient' &&
                                                          getStatus ==
                                                              "Full Registration Completed") {
                                                        logAdminRestoredPatient();
                                                        //Move to List
                                                        returnPatient();
                                                        returnAll();
                                                        //Temporary remove from Archived table
                                                        returnDeletePatient();
                                                        returnDeleteAll();
                                                        //Show Success Message
                                                        showReturnToast();
                                                      } else if (getUserType ==
                                                              'I am a Donor' &&
                                                          getStatus ==
                                                              "Tagged by Admin (Doctor)") {
                                                        logAdminRestoredTaggedDonor();
                                                        //Move to List
                                                        returnTaggedDonor();
                                                        returnAllTagged();
                                                        //Temporary remove from Archived table
                                                        returnDeleteTaggedDonor();
                                                        returnDeleteAllTagged();
                                                        //Show Success Message
                                                        showReturnToast();
                                                      } else if (getUserType ==
                                                              'I am a Patient' &&
                                                          getStatus ==
                                                              "Tagged by Admin (Doctor)") {
                                                        logAdminRestoredTaggedPatient();
                                                        //Move to List
                                                        returnTaggedPatient();
                                                        returnAllTagged();
                                                        //Temporary remove from Archived table
                                                        returnDeleteTaggedPatient();
                                                        returnDeleteAllTagged();
                                                        //Show Success Message
                                                        showReturnToast();
                                                      }
                                                    });
                                                  },
                                                  icon: Icon(
                                                    Icons.restore_outlined,
                                                    size: 20,
                                                    color: Colors.blue,
                                                  ),
                                                ),
                                                //Delete Button
                                                IconButton(
                                                  onPressed: () {
                                                    getUid = userData["UserID"];
                                                    getFullName =
                                                        userData["FullName"];
                                                    getUserName =
                                                        userData["UserName"];
                                                    getEmail = userData[
                                                        "Email Address"];
                                                    getGender =
                                                        userData["Gender"];
                                                    getAge = userData["Age"];
                                                    getUserType =
                                                        userData["User Type"];
                                                    getBloodType =
                                                        userData["Blood Type"];
                                                    getKidneySize =
                                                        userData["Kidney Size"];
                                                    getMobileNumber = userData[
                                                        "Mobile Number"];
                                                    getStatus =
                                                        userData["Status"];
                                                    getSignature =
                                                        userData["Signature"];

                                                    setState(() {
                                                      showCustomDialog(context);
                                                    });
                                                  },
                                                  icon: Icon(
                                                    Icons
                                                        .delete_forever_outlined,
                                                    size: 20,
                                                    color: Colors.grey,
                                                  ),
                                                )
                                              ],
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 18),
                                    child: Divider(),
                                  ),
                                ],
                              );
                            },
                          );
                        } else {
                          return Text('Users Data Not Available');
                        }
                      },
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  //Filter Users DropdownMenu
  DropdownMenuItem<String> buildUserListMenuItem(String usersList) =>
      DropdownMenuItem(
        value: usersList,
        child: Container(
          child: Text(
            '   ' + usersList,
            style: GoogleFonts.roboto(color: Colors.black, fontSize: 15),
          ),
        ),
      );

  //Return User Data
  returnDonor() async {
    String? uid = getUid;
    await FirebaseFirestore.instance
        .collection("Users")
        .doc('Donor')
        .collection('Lists')
        .doc(uid)
        .set({
      'UserID': getUid,
      'FullName': getFullName,
      'UserName': getUserName,
      'Email Address': getEmail,
      'Gender': getGender,
      'Age': getAge,
      'User Type': getUserType,
      'Blood Type': getBloodType,
      'Kidney Size': getKidneySize,
      'Mobile Number': getMobileNumber,
      'Status': getStatus,
      'Signature': getSignature,
      'Date Registered': getRegisteredDate,
      'Time Registered': getRegisteredTime,
      'Views': getViews,
      'Comments': getComments,
      'Likes': getLikes,
      'Pinned': getPinned,
      'Donated': getDonated,
      'Received': getReceived,
      'Posts': getPosts,
      'Notifications': getNotifications,
      'Replies': getReplies,
      'Match': getMatch,
      'Mail': getMail
    });
  }

  returnPatient() async {
    String? uid = getUid;
    await FirebaseFirestore.instance
        .collection("Users")
        .doc('Patient')
        .collection('Lists')
        .doc(uid)
        .set({
      'UserID': getUid,
      'FullName': getFullName,
      'UserName': getUserName,
      'Email Address': getEmail,
      'Gender': getGender,
      'Age': getAge,
      'User Type': getUserType,
      'Blood Type': getBloodType,
      'Kidney Size': getKidneySize,
      'Mobile Number': getMobileNumber,
      'Status': getStatus,
      'Signature': getSignature,
      'Date Registered': getRegisteredDate,
      'Time Registered': getRegisteredTime,
      'Views': getViews,
      'Comments': getComments,
      'Likes': getLikes,
      'Pinned': getPinned,
      'Donated': getDonated,
      'Received': getReceived,
      'Posts': getPosts,
      'Notifications': getNotifications,
      'Replies': getReplies,
      'Match': getMatch,
      'Mail': getMail
    });
  }

  //Both Donor and Patient
  returnAll() async {
    String? uid = getUid;
    await FirebaseFirestore.instance
        .collection("Users")
        .doc('All')
        .collection('Lists')
        .doc(uid)
        .set({
      'UserID': getUid,
      'FullName': getFullName,
      'UserName': getUserName,
      'Email Address': getEmail,
      'Gender': getGender,
      'Age': getAge,
      'User Type': getUserType,
      'Blood Type': getBloodType,
      'Kidney Size': getKidneySize,
      'Mobile Number': getMobileNumber,
      'Status': getStatus,
      'Signature': getSignature,
      'Date Registered': getRegisteredDate,
      'Time Registered': getRegisteredTime,
      'Views': getViews,
      'Comments': getComments,
      'Likes': getLikes,
      'Pinned': getPinned,
      'Donated': getDonated,
      'Received': getReceived,
      'Posts': getPosts,
      'Notifications': getNotifications,
      'Replies': getReplies,
      'Match': getMatch,
      'Mail': getMail
    });
  }

  returnTaggedDonor() async {
    String? uid = getUserName;
    await FirebaseFirestore.instance
        .collection("Tagged Users")
        .doc('Donor')
        .collection('Lists')
        .doc(uid)
        .set({
      'UserID': getUserName,
      'FullName': getFullName,
      'UserName': getUserName,
      'Email Address': getEmail,
      'Gender': getGender,
      'Age': getAge,
      'User Type': getUserType,
      'Blood Type': getBloodType,
      'Kidney Size': getKidneySize,
      'Mobile Number': getMobileNumber,
      'Status': getStatus,
      'Signature': getSignature,
      'Date Registered': getRegisteredDate,
      'Time Registered': getRegisteredTime,
      'Views': getViews,
      'Comments': getComments,
      'Likes': getLikes,
      'Pinned': getPinned,
      'Donated': getDonated,
      'Received': getReceived,
      'Posts': getPosts,
      'Notifications': getNotifications,
      'Replies': getReplies,
      'Match': getMatch,
      'Mail': getMail
    });
  }

  returnTaggedPatient() async {
    String? uid = getUserName;
    await FirebaseFirestore.instance
        .collection("Tagged Users")
        .doc('Patient')
        .collection('Lists')
        .doc(uid)
        .set({
      'UserID': getUserName,
      'FullName': getFullName,
      'UserName': getUserName,
      'Email Address': getEmail,
      'Gender': getGender,
      'Age': getAge,
      'User Type': getUserType,
      'Blood Type': getBloodType,
      'Kidney Size': getKidneySize,
      'Mobile Number': getMobileNumber,
      'Status': getStatus,
      'Signature': getSignature,
      'Date Registered': getRegisteredDate,
      'Time Registered': getRegisteredTime,
      'Views': getViews,
      'Comments': getComments,
      'Likes': getLikes,
      'Pinned': getPinned,
      'Donated': getDonated,
      'Received': getReceived,
      'Posts': getPosts,
      'Notifications': getNotifications,
      'Replies': getReplies,
      'Match': getMatch,
      'Mail': getMail
    });
  }

  returnAllTagged() async {
    String? uid = getUserName;
    await FirebaseFirestore.instance
        .collection("Tagged Users")
        .doc('All Tagged')
        .collection('Lists')
        .doc(uid)
        .set({
      'UserID': getUserName,
      'FullName': getFullName,
      'UserName': getUserName,
      'Email Address': getEmail,
      'Gender': getGender,
      'Age': getAge,
      'User Type': getUserType,
      'Blood Type': getBloodType,
      'Kidney Size': getKidneySize,
      'Mobile Number': getMobileNumber,
      'Status': getStatus,
      'Signature': getSignature,
      'Date Registered': getRegisteredDate,
      'Time Registered': getRegisteredTime,
      'Views': getViews,
      'Comments': getComments,
      'Likes': getLikes,
      'Pinned': getPinned,
      'Donated': getDonated,
      'Received': getReceived,
      'Posts': getPosts,
      'Notifications': getNotifications,
      'Replies': getReplies,
      'Match': getMatch,
      'Mail': getMail
    });
  }

  //Delete Temporary
  returnDeleteDonor() async {
    String? uid = getUid;
    await FirebaseFirestore.instance
        .collection("Archived")
        .doc('Users')
        .collection('Registered')
        .doc('Donor')
        .collection('Lists')
        .doc(uid)
        .delete();
  }

  returnDeletePatient() async {
    String? uid = getUid;
    await FirebaseFirestore.instance
        .collection("Archived")
        .doc('Users')
        .collection('Registered')
        .doc('Patient')
        .collection('Lists')
        .doc(uid)
        .delete();
  }

  //Both Donor and Patient
  returnDeleteAll() async {
    String? uid = getUid;
    await FirebaseFirestore.instance
        .collection("Archived")
        .doc('Users')
        .collection('Registered')
        .doc('All')
        .collection('Lists')
        .doc(uid)
        .delete();
  }

  returnDeleteTaggedDonor() async {
    String? uid = getUserName;
    await FirebaseFirestore.instance
        .collection("Archived")
        .doc('Users')
        .collection('Tagged Users')
        .doc('Donor')
        .collection('Lists')
        .doc(uid)
        .delete();
  }

  returnDeleteTaggedPatient() async {
    String? uid = getUserName;
    await FirebaseFirestore.instance
        .collection("Archived")
        .doc('Users')
        .collection('Tagged Users')
        .doc('Patient')
        .collection('Lists')
        .doc(uid)
        .delete();
  }

  returnDeleteAllTagged() async {
    String? uid = getUserName;
    await FirebaseFirestore.instance
        .collection("Archived")
        .doc('Users')
        .collection('Tagged Users')
        .doc('All')
        .collection('Lists')
        .doc(uid)
        .delete();
  }

  //Delete Forever
  returnDeleteForeverDonor() async {
    String? uid = getUid;
    await FirebaseFirestore.instance
        .collection("Archived")
        .doc('Users')
        .collection('Registered')
        .doc('Donor')
        .collection('Lists')
        .doc(uid)
        .delete();
  }

  returnDeleteForeverPatient() async {
    String? uid = getUid;
    await FirebaseFirestore.instance
        .collection("Archived")
        .doc('Users')
        .collection('Registered')
        .doc('Patient')
        .collection('Lists')
        .doc(uid)
        .delete();
  }

  //Both Donor and Patient
  returnDeleteForeverAll() async {
    String? uid = getUid;
    await FirebaseFirestore.instance
        .collection("Archived")
        .doc('Users')
        .collection('Registered')
        .doc('All')
        .collection('Lists')
        .doc(uid)
        .delete();
  }

  returnDeleteForeverTaggedDonor() async {
    String? uid = getUserName;
    await FirebaseFirestore.instance
        .collection("Archived")
        .doc('Users')
        .collection('Tagged Users')
        .doc('Donor')
        .collection('Lists')
        .doc(uid)
        .delete();
  }

  returnDeleteForeverTaggedPatient() async {
    String? uid = getUserName;
    await FirebaseFirestore.instance
        .collection("Archived")
        .doc('Users')
        .collection('Tagged Users')
        .doc('Patient')
        .collection('Lists')
        .doc(uid)
        .delete();
  }

  returnDeleteForeverAllTagged() async {
    String? uid = getUserName;
    await FirebaseFirestore.instance
        .collection("Archived")
        .doc('Users')
        .collection('Tagged Users')
        .doc('All')
        .collection('Lists')
        .doc(uid)
        .delete();
  }

  returnDeleteMatchUsers() async {
    String? uid = getUid;
    await FirebaseFirestore.instance
        .collection("Match Users")
        .doc(uid)
        .delete();
  }

  returnDeleteArchiveMatchUsers() async {
    String? uid = getUid;
    await FirebaseFirestore.instance
        .collection("Archive Match Users")
        .doc(uid)
        .delete();
  }

  //Show Return Success
  void showReturnToast() =>
      toast.showToast(child: buildReturnToast(), gravity: ToastGravity.CENTER);

  Widget buildReturnToast() => Container(
        padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
        decoration: BoxDecoration(
          color: Colors.green[300],
          borderRadius: BorderRadius.circular(5),
        ),
        child: Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            Text(
              "Restored",
              style: TextStyle(color: Colors.black87, fontSize: 16),
            ),
          ],
        ),
      );

  //Show Delete Success
  void showDeleteToast() =>
      toast.showToast(child: buildDeleteToast(), gravity: ToastGravity.CENTER);

  Widget buildDeleteToast() => Container(
        padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
        decoration: BoxDecoration(
          color: Colors.green[300],
          borderRadius: BorderRadius.circular(5),
        ),
        child: Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            Text(
              "Deleted",
              style: TextStyle(color: Colors.black87, fontSize: 16),
            ),
          ],
        ),
      );

  //Alert Dialog Method
  void showCustomDialog(BuildContext context) => showDialog(
      context: context,
      builder: (BuildContext context) {
        return Dialog(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20.0)), //this right here
          child: Container(
            height: 200,
            width: 100,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  "Delete",
                  style: GoogleFonts.roboto(
                      color: Colors.black87,
                      fontWeight: FontWeight.bold,
                      fontSize: 18),
                ),
                Text(
                  "\nAre you sure you want to delete?",
                  style:
                      GoogleFonts.roboto(color: Colors.grey[500], fontSize: 14),
                ),
                Container(
                  margin: EdgeInsets.only(top: 40),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Container(
                        child: InkWell(
                          onTap: () {
                            Navigator.of(context).pop();
                          },
                          child: Container(
                            height: 35,
                            width: 100,
                            decoration: BoxDecoration(
                                color: Colors.red[50],
                                borderRadius: BorderRadius.circular(18)),
                            child: Center(
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Text(
                                  "Cancel",
                                  style: TextStyle(
                                      color: Colors.grey[600], fontSize: 17),
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                      Container(
                        child: InkWell(
                          onTap: () {
                            setState(() {
                              if (getUserType == 'I am a Donor' &&
                                  getStatus == "Full Registration Completed") {
                                //Delete
                                returnDeleteForeverDonor();
                                returnDeleteAll();
                                //returnDeleteMatchUsers();
                                //returnDeleteArchiveMatchUsers();
                                //Show Delete Message
                                showDeleteToast();
                                Navigator.of(context).pop();
                                logAdminDeleteDonor();
                              } else if (getUserType == 'I am a Patient' &&
                                  getStatus == "Full Registration Completed") {
                                //Delete
                                returnDeleteForeverPatient();
                                returnDeleteForeverAll();
                                //returnDeleteMatchUsers();
                                //returnDeleteArchiveMatchUsers();
                                //Show Delete Message
                                showDeleteToast();
                                Navigator.of(context).pop();
                                logAdminDeletePatient();
                              } else if (getUserType == 'I am a Donor' &&
                                  getStatus == "Tagged by Admin (Doctor)") {
                                //Delete
                                returnDeleteForeverTaggedDonor();
                                returnDeleteForeverAllTagged();
                                //Show Delete Message
                                showDeleteToast();
                                Navigator.of(context).pop();
                                logAdminDeletePatient();
                              } else if (getUserType == 'I am a Patient' &&
                                  getStatus == "Tagged by Admin (Doctor)") {
                                //Delete
                                returnDeleteForeverTaggedPatient();
                                returnDeleteForeverAllTagged();
                                //Show Delete Message
                                showDeleteToast();
                                Navigator.of(context).pop();
                                logAdminDeletePatient();
                              }
                            });
                          },
                          child: Container(
                            height: 35,
                            width: 100,
                            decoration: BoxDecoration(
                                color: Colors.red[200],
                                borderRadius: BorderRadius.circular(18)),
                            child: Center(
                              child: Text(
                                "Confirm",
                                style: TextStyle(
                                    color: Colors.red[400], fontSize: 17),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        );
      });

  //Logs in the database

  logAdminRestoredDonor() async {
    String formattedTime = DateFormat('kk:mm:s:a').format(now);
    String formattedDate = formatter.format(now);
    await FirebaseFirestore.instance.collection("Logs").add({
      'Message': "Admin has restored donor " + getUserName!,
      'User': "Official Admin",
      'Logged Date': formattedDate,
      'Logged Time': formattedTime,
    });
  }

  logAdminRestoredPatient() async {
    String formattedTime = DateFormat('kk:mm:s:a').format(now);
    String formattedDate = formatter.format(now);
    await FirebaseFirestore.instance.collection("Logs").add({
      'Message': "Admin has restored patient " + getUserName!,
      'User': "Official Admin",
      'Logged Date': formattedDate,
      'Logged Time': formattedTime,
    });
  }

  logAdminRestoredTaggedDonor() async {
    String formattedTime = DateFormat('kk:mm:s:a').format(now);
    String formattedDate = formatter.format(now);
    await FirebaseFirestore.instance.collection("Logs").add({
      'Message': "Admin has restored tagged donor " + getUserName!,
      'User': "Official Admin",
      'Logged Date': formattedDate,
      'Logged Time': formattedTime,
    });
  }

  logAdminRestoredTaggedPatient() async {
    String formattedTime = DateFormat('kk:mm:s:a').format(now);
    String formattedDate = formatter.format(now);
    await FirebaseFirestore.instance.collection("Logs").add({
      'Message': "Admin has restored tagged patient " + getUserName!,
      'User': "Official Admin",
      'Logged Date': formattedDate,
      'Logged Time': formattedTime,
    });
  }

  logAdminDeleteDonor() async {
    String formattedTime = DateFormat('kk:mm:s:a').format(now);
    String formattedDate = formatter.format(now);
    await FirebaseFirestore.instance.collection("Logs").add({
      'Message': "Admin has deleted donor " + getUserName!,
      'User': "Official Admin",
      'Logged Date': formattedDate,
      'Logged Time': formattedTime,
    });
  }

  logAdminDeletePatient() async {
    String formattedTime = DateFormat('kk:mm:s:a').format(now);
    String formattedDate = formatter.format(now);
    await FirebaseFirestore.instance.collection("Logs").add({
      'Message': "Admin has deleted patient " + getUserName!,
      'User': "Official Admin",
      'Logged Date': formattedDate,
      'Logged Time': formattedTime,
    });
  }

  logAdminDeleteTaggedDonor() async {
    String formattedTime = DateFormat('kk:mm:s:a').format(now);
    String formattedDate = formatter.format(now);
    await FirebaseFirestore.instance.collection("Logs").add({
      'Message': "Admin has deleted tagged donor " + getUserName!,
      'User': "Official Admin",
      'Logged Date': formattedDate,
      'Logged Time': formattedTime,
    });
  }

  logAdminDeleteTaggedPatient() async {
    String formattedTime = DateFormat('kk:mm:s:a').format(now);
    String formattedDate = formatter.format(now);
    await FirebaseFirestore.instance.collection("Logs").add({
      'Message': "Admin has deleted tagged patient " + getUserName!,
      'User': "Official Admin",
      'Logged Date': formattedDate,
      'Logged Time': formattedTime,
    });
  }

  logAdminFilteredAllRegistered() async {
    String formattedTime = DateFormat('kk:mm:s:a').format(now);
    String formattedDate = formatter.format(now);
    await FirebaseFirestore.instance.collection("Logs").add({
      'Message': "Admin set archived users view to all registered",
      'User': "Official Admin",
      'Logged Date': formattedDate,
      'Logged Time': formattedTime,
    });
  }

  logAdminFilteredDonorOnly() async {
    String formattedTime = DateFormat('kk:mm:s:a').format(now);
    String formattedDate = formatter.format(now);
    await FirebaseFirestore.instance.collection("Logs").add({
      'Message': "Admin set archived users view to Donor Only",
      'User': "Official Admin",
      'Logged Date': formattedDate,
      'Logged Time': formattedTime,
    });
  }

  logAdminFilteredPatientOnly() async {
    String formattedTime = DateFormat('kk:mm:s:a').format(now);
    String formattedDate = formatter.format(now);
    await FirebaseFirestore.instance.collection("Logs").add({
      'Message': "Admin set archived users view to Patient Only",
      'User': "Official Admin",
      'Logged Date': formattedDate,
      'Logged Time': formattedTime,
    });
  }

  logAdminFilteredAllTagged() async {
    String formattedTime = DateFormat('kk:mm:s:a').format(now);
    String formattedDate = formatter.format(now);
    await FirebaseFirestore.instance.collection("Logs").add({
      'Message': "Admin set archived users view to all tagged",
      'User': "Official Admin",
      'Logged Date': formattedDate,
      'Logged Time': formattedTime,
    });
  }

  logAdminFilteredDonorTagged() async {
    String formattedTime = DateFormat('kk:mm:s:a').format(now);
    String formattedDate = formatter.format(now);
    await FirebaseFirestore.instance.collection("Logs").add({
      'Message': "Admin set archived users view to Donor tagged Only",
      'User': "Official Admin",
      'Logged Date': formattedDate,
      'Logged Time': formattedTime,
    });
  }

  logAdminFilteredPatientTagged() async {
    String formattedTime = DateFormat('kk:mm:s:a').format(now);
    String formattedDate = formatter.format(now);
    await FirebaseFirestore.instance.collection("Logs").add({
      'Message': "Admin set archived users view to Patient tagged Only",
      'User': "Official Admin",
      'Logged Date': formattedDate,
      'Logged Time': formattedTime,
    });
  }
}
