import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:organ_donor_app/pages/AboutPage.dart';

class AccountTermsofService extends StatefulWidget {
  AccountTermsofService({Key? key}) : super(key: key);

  @override
  _AccountTermsofServiceState createState() => _AccountTermsofServiceState();
}

class _AccountTermsofServiceState extends State<AccountTermsofService> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xffe5eef8),
      body: Stack(
        children: [
          //Middle Content
          SingleChildScrollView(
            child: Container(
              margin: EdgeInsets.only(top: 90, bottom: 40),
              child: Center(
                child: Column(
                  children: [
                    Container(
                      margin: EdgeInsets.only(
                        left: 20,
                        right: 20,
                      ),
                      child: SelectableText.rich(
                        TextSpan(
                          children: [
                            TextSpan(
                              text: "Welcome to Organ Donor App!",
                              style: GoogleFonts.roboto(
                                  color: Colors.black87,
                                  fontSize: 14,
                                  height: 1.7,
                                  fontWeight: FontWeight.w700),
                            ),
                            TextSpan(
                              children: [
                                TextSpan(
                                  text:
                                      "\n\nOrgan donor application assists you to find a match for kidney transplantation and enables you to connect the patient and donor. These Terms of Service guide your access and connection to the Organ Donor Application. These Terms and Services govern your use of Organ Donor Application, Features, and the other services.",
                                  style: GoogleFonts.roboto(
                                      color: Colors.black87,
                                      fontSize: 14,
                                      height: 1.7),
                                ),
                                TextSpan(
                                  text:
                                      "\n\nWe will not transfer personal information to the other health sector and public alliance, and we never disclose anything that directly identifies you (such as your name, email address, mobile number, or other contact information) unless you give us express consent. Our application's first concern is the protection of our users. We also utilize your personal information to help us assess if you are a good match based on the characteristics of the donor and patient. By using our service, you accept that we may access your information and follow the application's rules.",
                                  style: GoogleFonts.roboto(
                                      color: Colors.black87,
                                      fontSize: 14,
                                      height: 1.7),
                                ),
                                TextSpan(
                                  text: "\n\nRead our Privacy Policy",
                                  style: GoogleFonts.roboto(
                                      color: Colors.black87,
                                      fontSize: 14,
                                      height: 1.7,
                                      fontWeight: FontWeight.w700),
                                ),
                                TextSpan(
                                  text:
                                      "\n\nKindly read our Privacy Policy, which includes our statement, for more details on our data policies. By accessing or using the Organ Donor Application, you acknowledge that we can gather and process your data in line with the Privacy Policy.",
                                  style: GoogleFonts.roboto(
                                      color: Colors.black87,
                                      fontSize: 14,
                                      height: 1.7),
                                ),
                                TextSpan(
                                  text: "\n\nThe service we provide",
                                  style: GoogleFonts.roboto(
                                      color: Colors.black87,
                                      fontSize: 14,
                                      height: 1.7,
                                      fontWeight: FontWeight.w700),
                                ),
                                TextSpan(
                                  text:
                                      "\n\nOur aim is to provide patients and donors with the opportunity to find a perfect match. We provide the following products and services to assist further our objective.",
                                  style: GoogleFonts.roboto(
                                      color: Colors.black87,
                                      fontSize: 14,
                                      height: 1.7),
                                ),
                                TextSpan(
                                  text:
                                      "\n\nOffering you a customized experience",
                                  style: GoogleFonts.roboto(
                                      color: Colors.black87,
                                      fontSize: 14,
                                      height: 1.7,
                                      fontWeight: FontWeight.w700),
                                ),
                                TextSpan(
                                  text:
                                      "\n\nYour experience on Organ Donor Application is different from other health applications: from the donor post, comments, and pinned, as well as other content you see in the donor and patient profiles. Other features of our application include SMS authentication, One Time passwords, a Landing page for terms and conditions, User Profile, and Analytics for matching donors and patients.",
                                  style: GoogleFonts.roboto(
                                      color: Colors.black87,
                                      fontSize: 14,
                                      height: 1.7),
                                ),
                                TextSpan(
                                  text:
                                      "\n\nUtilize and develop modern technology to offer and functional services for kidney donors and patients",
                                  style: GoogleFonts.roboto(
                                      color: Colors.black87,
                                      fontSize: 14,
                                      height: 1.7,
                                      fontWeight: FontWeight.w700),
                                ),
                                TextSpan(
                                  text:
                                      "\n\nWe employ and develop innovative technologies, such as analytics for matching the characteristics of the patient and donor, posted information data, user profiles, and security policies, to ensure that individuals may use our products safely, independent of the information provided to us.",
                                  style: GoogleFonts.roboto(
                                      color: Colors.black87,
                                      fontSize: 14,
                                      height: 1.7),
                                ),
                                TextSpan(
                                  text:
                                      "\n\nResearch methods to improve our services",
                                  style: GoogleFonts.roboto(
                                      color: Colors.black87,
                                      fontSize: 14,
                                      height: 1.7,
                                      fontWeight: FontWeight.w700),
                                ),
                                TextSpan(
                                  text:
                                      "\n\nWe conduct research in order to create, evaluate, and enhance our apps. This involves evaluating information about our end-users and learning how they interact with our system. Our Terms and Conditions describe how we utilize information to prove this study in terms of developing our services.",
                                  style: GoogleFonts.roboto(
                                      color: Colors.black87,
                                      fontSize: 14,
                                      height: 1.7),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ),
                    //End Line
                    Container(
                      alignment: Alignment.topRight,
                      margin: EdgeInsets.only(left: 15, right: 15, bottom: 40),
                      child: SelectableText.rich(
                        TextSpan(
                          children: [
                            TextSpan(
                              text: "\n\nDevelopers:",
                              style: GoogleFonts.roboto(
                                color: Colors.black87,
                                fontSize: 14,
                                height: 1.7,
                                fontWeight: FontWeight.w700,
                              ),
                            ),
                            TextSpan(
                              text: "\nEspiritu, Justin Lloyd:",
                              style: GoogleFonts.roboto(
                                color: Colors.black87,
                                fontSize: 14,
                                height: 1.7,
                              ),
                            ),
                            TextSpan(
                              text: "\nMayor, Aldrin:",
                              style: GoogleFonts.roboto(
                                color: Colors.black87,
                                fontSize: 14,
                                height: 1.7,
                              ),
                            ),
                            TextSpan(
                              text: "\nRivera, Debbie Cheyenne:",
                              style: GoogleFonts.roboto(
                                color: Colors.black87,
                                fontSize: 14,
                                height: 1.7,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
          //Header
          Container(
            decoration: BoxDecoration(color: Color(0xffe5eef8)),
            height: 80,
            margin: EdgeInsets.only(top: 0, left: 0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                InkWell(
                  onTap: () {
                    Navigator.pushAndRemoveUntil(
                        context,
                        MaterialPageRoute(builder: (builder) => AboutPage()),
                        (route) => false);
                  },
                  child: Padding(
                    padding: const EdgeInsets.only(top: 20, left: 20),
                    child: Icon(Icons.arrow_back_ios_outlined, size: 25.0),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 20, left: 0),
                  child: Text(
                    "Organ Donor App - About Us",
                    style: GoogleFonts.roboto(
                        color: Colors.black87,
                        fontWeight: FontWeight.w400,
                        fontSize: 18),
                  ),
                ),
                Container(width: 32.0, height: 0.0),
              ],
            ),
          ),
          //Bottom Button
          Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              Container(
                decoration: BoxDecoration(
                  boxShadow: [BoxShadow(color: Colors.grey.withOpacity(0.5))],
                  color: Color(0xffe5eef8),
                ),
                padding: EdgeInsets.only(top: 10, bottom: 10),
                child: Column(
                  children: [
                    Container(
                      height: 30,
                      width: double.infinity,
                      padding: EdgeInsets.only(
                        left: 135,
                        right: 135,
                        top: 3,
                        bottom: 3,
                      ),
                      child: InkWell(
                        onTap: () {
                          Navigator.pushAndRemoveUntil(
                              context,
                              MaterialPageRoute(
                                  builder: (builder) => AboutPage()),
                              (route) => false);
                        },
                        child: Container(
                          decoration: BoxDecoration(
                              color: Colors.red[100],
                              borderRadius: BorderRadius.circular(18)),
                          child: Center(
                            child: Text(
                              "Back",
                              style: TextStyle(
                                  color: Colors.red[400], fontSize: 17),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
