import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:organ_donor_app/Service/Auth_Service.dart';
import 'package:flutter/gestures.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:organ_donor_app/pages/Dashboard.dart';
import 'package:intl/intl.dart';

// ignore: must_be_immutable
class CompleteRegistration extends StatefulWidget {
  String? mobileNumber;

  CompleteRegistration({
    required this.mobileNumber,
  });

  @override
  _CompleteRegistrationState createState() => _CompleteRegistrationState();
}

class _CompleteRegistrationState extends State<CompleteRegistration> {
  String? mobileNumber;

  _CompleteRegistrationState({this.mobileNumber});
  final firestoreInstance = FirebaseFirestore.instance;

//FireStore
  int initialPatientCount = 0;
  int finalPatientCount = 0;

  int initialDonorCount = 0;
  int finalDonorCount = 0;

//Display Number Of Patients
  getPatientDocument() async {
    await FirebaseFirestore.instance
        .collection('Users')
        .doc('Patient')
        .collection('Lists')
        .get()
        .then((myDocuments) {
      print("${myDocuments.docs.length}");
      int patientCount = myDocuments.docs.length;
      initialPatientCount = patientCount;
    });
  }

//Display Number Of Patients
  getDonorDocument() async {
    await FirebaseFirestore.instance
        .collection('Users')
        .doc('Donor')
        .collection('Lists')
        .get()
        .then((myDocuments) {
      print("${myDocuments.docs.length}");
      int donorCount = myDocuments.docs.length;
      initialDonorCount = donorCount;
    });
  }

//holds Patient Document
  patient() async {
    if (fullName!.isEmpty) {
      User? user = FirebaseAuth.instance.currentUser;
      String formattedTime = DateFormat('kk:mm:s:a').format(now);
      String formattedDate = formatter.format(now);

      await FirebaseFirestore.instance
          .collection('Users')
          .doc('Patient')
          .collection('Lists')
          .doc(user!.uid)
          .set({
        'UserID': user.uid,
        'FullName': '[UnknownPatient]',
        'UserName': userName,
        'Gender': genderValue,
        'Email Address': email,
        'Mobile Number': widget.mobileNumber,
        'User Type': usersValue,
        'Signature': '',
        'Age': '',
        'Blood Type': '',
        'Kidney Size': '',
        'Status': patientStatus,
        'Date Registered': formattedDate,
        'Time Registered': formattedTime,
        'Views': '0',
        'Comments': '0',
        'Likes': '0',
        'Pinned': '0',
        'Donated': '0',
        'Received': '0',
        'Posts': '0',
        'Notifications': '0',
        'Replies': '0',
        'Match': '0',
        'Mail': '0'
      });
    } else {
      User? user = FirebaseAuth.instance.currentUser;
      String formattedTime = DateFormat('kk:mm:s:a').format(now);
      String formattedDate = formatter.format(now);

      await FirebaseFirestore.instance
          .collection('Users')
          .doc('Patient')
          .collection('Lists')
          .doc(user!.uid)
          .set({
        'UserID': user.uid,
        'FullName': fullName,
        'UserName': userName,
        'Gender': genderValue,
        'Email Address': email,
        'Mobile Number': widget.mobileNumber,
        'User Type': usersValue,
        'Signature': '',
        'Age': '',
        'Blood Type': '',
        'Kidney Size': '',
        'Status': patientStatus,
        'Date Registered': formattedDate,
        'Time Registered': formattedTime,
        'Views': '0',
        'Comments': '0',
        'Likes': '0',
        'Pinned': '0',
        'Donated': '0',
        'Received': '0',
        'Posts': '0',
        'Notifications': '0',
        'Replies': '0',
        'Match': '0',
        'Mail': '0'
      });
    }
  }

  //holds Patient Document
  donor() async {
    if (fullName!.isEmpty) {
      User? user = FirebaseAuth.instance.currentUser;
      String formattedTime = DateFormat('kk:mm:s:a').format(now);
      String formattedDate = formatter.format(now);

      await FirebaseFirestore.instance
          .collection('Users')
          .doc('Donor')
          .collection('Lists')
          .doc(user!.uid)
          .set({
        'UserID': user.uid,
        'FullName': '[UnknownDonor]',
        'UserName': userName,
        'Gender': genderValue,
        'Email Address': email,
        'Mobile Number': widget.mobileNumber,
        'User Type': usersValue,
        'Signature': '',
        'Age': '',
        'Blood Type': '',
        'Kidney Size': '',
        'Status': donorStatus,
        'Date Registered': formattedDate,
        'Time Registered': formattedTime,
        'Views': '0',
        'Comments': '0',
        'Likes': '0',
        'Pinned': '0',
        'Donated': '0',
        'Received': '0',
        'Posts': '0',
        'Notifications': '0',
        'Replies': '0',
        'Match': '0',
        'Mail': '0'
      });
    } else {
      User? user = FirebaseAuth.instance.currentUser;
      String formattedTime = DateFormat('kk:mm:s:a').format(now);
      String formattedDate = formatter.format(now);

      await FirebaseFirestore.instance
          .collection('Users')
          .doc('Donor')
          .collection('Lists')
          .doc(user!.uid)
          .set({
        'UserID': user.uid,
        'FullName': fullName,
        'UserName': userName,
        'Gender': genderValue,
        'Email Address': email,
        'Mobile Number': widget.mobileNumber,
        'User Type': usersValue,
        'Signature': '',
        'Age': '',
        'Blood Type': '',
        'Kidney Size': '',
        'Status': donorStatus,
        'Date Registered': formattedDate,
        'Time Registered': formattedTime,
        'Views': '0',
        'Comments': '0',
        'Likes': '0',
        'Pinned': '0',
        'Donated': '0',
        'Received': '0',
        'Posts': '0',
        'Notifications': '0',
        'Replies': '0',
        'Match': '0',
        'Mail': '0'
      });
    }
  }

//All Donor Document
  donorAll() async {
    if (fullName!.isEmpty) {
      User? user = FirebaseAuth.instance.currentUser;
      String formattedTime = DateFormat('kk:mm:s:a').format(now);
      String formattedDate = formatter.format(now);

      await FirebaseFirestore.instance
          .collection('Users')
          .doc('All')
          .collection('Lists')
          .doc(user!.uid)
          .set({
        'UserID': user.uid,
        'FullName': '[UnknownDonor]',
        'UserName': userName,
        'Gender': genderValue,
        'Email Address': email,
        'Mobile Number': widget.mobileNumber,
        'User Type': usersValue,
        'Signature': '',
        'Age': '',
        'Blood Type': '',
        'Kidney Size': '',
        'Status': donorStatus,
        'Date Registered': formattedDate,
        'Time Registered': formattedTime,
        'Views': '0',
        'Comments': '0',
        'Likes': '0',
        'Pinned': '0',
        'Donated': '0',
        'Received': '0',
        'Posts': '0',
        'Notifications': '0',
        'Replies': '0',
        'Match': '0',
        'Mail': '0'
      });
    } else {
      User? user = FirebaseAuth.instance.currentUser;
      String formattedTime = DateFormat('kk:mm:s:a').format(now);
      String formattedDate = formatter.format(now);

      await FirebaseFirestore.instance
          .collection('Users')
          .doc('All')
          .collection('Lists')
          .doc(user!.uid)
          .set({
        'UserID': user.uid,
        'FullName': fullName,
        'UserName': userName,
        'Gender': genderValue,
        'Email Address': email,
        'Mobile Number': widget.mobileNumber,
        'User Type': usersValue,
        'Signature': '',
        'Age': '',
        'Blood Type': '',
        'Kidney Size': '',
        'Status': donorStatus,
        'Date Registered': formattedDate,
        'Time Registered': formattedTime,
        'Views': '0',
        'Comments': '0',
        'Likes': '0',
        'Pinned': '0',
        'Donated': '0',
        'Received': '0',
        'Posts': '0',
        'Notifications': '0',
        'Replies': '0',
        'Match': '0',
        'Mail': '0'
      });
    }
  }

  //All Patient Document
  patientAll() async {
    if (fullName!.isEmpty) {
      User? user = FirebaseAuth.instance.currentUser;
      String formattedTime = DateFormat('kk:mm:s:a').format(now);
      String formattedDate = formatter.format(now);
      await FirebaseFirestore.instance
          .collection('Users')
          .doc('All')
          .collection('Lists')
          .doc(user!.uid)
          .set({
        'UserID': user.uid,
        'FullName': '[UnknownPatient]',
        'UserName': userName,
        'Gender': genderValue,
        'Email Address': email,
        'Mobile Number': widget.mobileNumber,
        'User Type': usersValue,
        'Signature': '',
        'Age': '',
        'Blood Type': '',
        'Kidney Size': '',
        'Status': patientStatus,
        'Date Registered': formattedDate,
        'Time Registered': formattedTime,
        'Views': '0',
        'Comments': '0',
        'Likes': '0',
        'Pinned': '0',
        'Donated': '0',
        'Received': '0',
        'Posts': '0',
        'Notifications': '0',
        'Replies': '0',
        'Match': '0',
        'Mail': '0'
      });
    } else {
      User? user = FirebaseAuth.instance.currentUser;
      String formattedTime = DateFormat('kk:mm:s:a').format(now);
      String formattedDate = formatter.format(now);
      await FirebaseFirestore.instance
          .collection('Users')
          .doc('All')
          .collection('Lists')
          .doc(user!.uid)
          .set({
        'UserID': user.uid,
        'FullName': fullName,
        'UserName': userName,
        'Gender': genderValue,
        'Email Address': email,
        'Mobile Number': widget.mobileNumber,
        'User Type': usersValue,
        'Signature': '',
        'Age': '',
        'Blood Type': '',
        'Kidney Size': '',
        'Status': patientStatus,
        'Date Registered': formattedDate,
        'Time Registered': formattedTime,
        'Views': '0',
        'Comments': '0',
        'Likes': '0',
        'Pinned': '0',
        'Donated': '0',
        'Received': '0',
        'Posts': '0',
        'Notifications': '0',
        'Replies': '0',
        'Match': '0',
        'Mail': '0'
      });
    }
  }

  final users = ['I am a Donor', 'I am a Patient'];
  final gender = ['Male', 'Female', 'Prefer not to say', 'Other'];

//
  String? fullName;
  String? userName;
  String? genderValue;
  String? email;
  String? age;
  String? finalValue;
  String? usersValue;
  String? donorStatus;
  String? patientStatus;
  final formKey = GlobalKey<FormState>();

  String? userNamed;

  //Time and Date
  var now = new DateTime.now();
  var formatter = new DateFormat('dd-MM-yyyy');

  AuthClass authClass = AuthClass();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: Stack(
        children: [
          Container(
            decoration: BoxDecoration(
              image: DecorationImage(
                  image: AssetImage('assets/5.png'), fit: BoxFit.cover),
            ),
          ),
          SingleChildScrollView(
            child: Container(
              margin: EdgeInsets.only(top: 50),
              child: Center(
                child: Form(
                  key: formKey,
                  child: Column(
                    children: [
                      SizedBox(height: 15),
                      Text(
                        "Complete Registration",
                        textAlign: TextAlign.center,
                        style: GoogleFonts.roboto(
                            color: Colors.black,
                            fontWeight: FontWeight.bold,
                            fontSize: 35),
                      ),
                      SizedBox(height: 15),
                      Text(
                        "You are just one step away from completing your \ndetails before continue with logging in",
                        textAlign: TextAlign.center,
                        style: GoogleFonts.roboto(
                            color: Colors.grey[600], fontSize: 13),
                      ),
                      SizedBox(height: 15),
                      Container(
                        margin: EdgeInsets.only(left: 50, right: 50),
                        child: TextFormField(
                          inputFormatters: [
                            LengthLimitingTextInputFormatter(30)
                          ],
                          validator: (value) {
                            return null;
                          },
                          onSaved: (value) => fullName = value,
                          decoration: InputDecoration(
                            border: OutlineInputBorder(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(7))),
                            labelText: 'Full Name',
                            hintText: 'Enter Your Full Name (Optional)',
                          ),
                        ),
                      ),
                      SizedBox(height: 15),
                      Container(
                        margin: EdgeInsets.only(left: 50, right: 50),
                        child: TextFormField(
                          validator: (value) {
                            if (value!.isEmpty) {
                              return 'This is Required';
                            } else {
                              return null;
                            }
                          },
                          onSaved: (value) => userName = value,
                          decoration: InputDecoration(
                            border: OutlineInputBorder(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(7))),
                            labelText: 'User Name',
                            hintText: "",
                            contentPadding: const EdgeInsets.symmetric(
                                vertical: 18, horizontal: 14),
                          ),
                        ),
                      ),
                      SizedBox(height: 15),
                      Container(
                        margin: EdgeInsets.only(left: 50, right: 50),
                        child: DropdownButtonHideUnderline(
                          child: DropdownButtonFormField<String>(
                            decoration: InputDecoration(
                                border: OutlineInputBorder(
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(7)))),
                            validator: (value) {
                              if (value == null) {
                                return 'Please Select an Option';
                              }
                            },
                            hint: Text("Gender"),
                            value: genderValue,
                            isExpanded: true,
                            items: gender.map(buildGenderMenuItem).toList(),
                            onChanged: (value) =>
                                setState(() => this.genderValue = value),
                          ),
                        ),
                      ),
                      SizedBox(height: 15),
                      Container(
                        margin: EdgeInsets.only(left: 50, right: 50),
                        child: DropdownButtonHideUnderline(
                          child: DropdownButtonFormField<String>(
                            decoration: InputDecoration(
                                border: OutlineInputBorder(
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(7)))),
                            validator: (value) {
                              if (value == null) {
                                return 'Please Specify your Option';
                              }
                            },
                            hint: Text("Specify? (Donor or Patient)"),
                            value: usersValue,
                            isExpanded: true,
                            items: users.map(buildUserMenuItem).toList(),
                            onChanged: (value) =>
                                setState(() => this.usersValue = value),
                          ),
                        ),
                      ),
                      SizedBox(height: 15),
                      Container(
                        margin: EdgeInsets.only(left: 50, right: 50),
                        child: TextFormField(
                          enabled: false,
                          keyboardType: TextInputType.number,
                          inputFormatters: [
                            LengthLimitingTextInputFormatter(11)
                          ],
                          decoration: InputDecoration(
                              border: OutlineInputBorder(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(7))),
                              contentPadding: const EdgeInsets.symmetric(
                                  vertical: 18, horizontal: 14),
                              prefixIcon: Padding(
                                padding: const EdgeInsets.symmetric(
                                    vertical: 12, horizontal: 15),
                                child: Text(
                                  "(+63)" + "\t\t" + widget.mobileNumber!,
                                  style: TextStyle(
                                      color: Colors.black, fontSize: 17),
                                ),
                              )),
                        ),
                      ),
                      SizedBox(height: 15),
                      Container(
                        margin: EdgeInsets.only(left: 50, right: 50),
                        child: TextFormField(
                          inputFormatters: [
                            LengthLimitingTextInputFormatter(30)
                          ],
                          validator: (value) {
                            final pattern =
                                r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
                            final regExp = RegExp(pattern);

                            if (value!.isNotEmpty) {
                              if (!regExp.hasMatch(value)) {
                                return 'Enter a valid email';
                              } else {
                                return null;
                              }
                            }
                          },
                          onSaved: (value) => email = value,
                          decoration: InputDecoration(
                            border: OutlineInputBorder(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(7))),
                            labelText: 'Email Address',
                            hintText: 'Enter Your Address (Optional)',
                          ),
                        ),
                      ),
                      SizedBox(height: 15),
                      Container(
                        margin: EdgeInsets.only(left: 50, right: 50),
                        child: RichText(
                          textAlign: TextAlign.justify,
                          text: TextSpan(
                            children: [
                              TextSpan(
                                text:
                                    "We strongly recommend providing and email address for a more secure way of recovering your account.",
                                style: GoogleFonts.roboto(
                                    color: Colors.grey,
                                    fontSize: 14,
                                    height: 1.5),
                              ),
                            ],
                          ),
                        ),
                      ),
                      SizedBox(height: 25),
                      SizedBox(
                        height: 55,
                        width: 300,
                        child: ElevatedButton(
                          child: Text('                Proceed               '),
                          onPressed: () {
                            final isValid = formKey.currentState!.validate();

                            if (isValid) {
                              formKey.currentState!.save();
                              checkRegisterUserName(userName!, context);
                            }
                          },
                          style: ButtonStyle(
                              backgroundColor:
                                  MaterialStateProperty.all(Colors.red),
                              padding:
                                  MaterialStateProperty.all(EdgeInsets.all(15)),
                              shape: MaterialStateProperty.all<
                                      RoundedRectangleBorder>(
                                  RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(18.0),
                              )),
                              textStyle: MaterialStateProperty.all(
                                  TextStyle(fontSize: 20))),
                        ),
                      ),
                      SizedBox(height: 25),
                      SizedBox(
                        height: 55,
                        width: 300,
                        child: RichText(
                          textAlign: TextAlign.center,
                          text: TextSpan(
                            text:
                                'By Proceeding you confirm that you agree with our',
                            style: GoogleFonts.roboto(
                                color: Colors.grey[600], fontSize: 13),
                            children: <TextSpan>[
                              TextSpan(
                                text: ' Term and Conditions',
                                style: GoogleFonts.roboto(
                                    color: Colors.lightBlue[400], fontSize: 13),
                                recognizer: TapGestureRecognizer()
                                  ..onTap = () {},
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  DropdownMenuItem<String> buildUserMenuItem(String users) => DropdownMenuItem(
        value: users,
        child: Text(
          users,
          style: GoogleFonts.roboto(color: Colors.black, fontSize: 15),
        ),
      );

  DropdownMenuItem<String> buildGenderMenuItem(String gender) =>
      DropdownMenuItem(
        value: gender,
        child: Text(
          gender,
          style: GoogleFonts.roboto(color: Colors.black, fontSize: 15),
        ),
      );

  //CheckUserName
  Future<void> checkRegisterUserName(
      String userName, BuildContext context) async {
    // For Registered Donor
    final CollectionReference checkIfDonorVerified = FirebaseFirestore.instance
        .collection("Users")
        .doc("Donor")
        .collection("Lists");

    QuerySnapshot _queryDonor =
        await checkIfDonorVerified.where('UserName', isEqualTo: userName).get();

    // For Archived Donor
    final CollectionReference checkIfArchivedDonorVerified = FirebaseFirestore
        .instance
        .collection("Archived")
        .doc("Users")
        .collection("Registered")
        .doc("Donor")
        .collection("Lists");

    QuerySnapshot _queryArchivedDonor = await checkIfArchivedDonorVerified
        .where('UserName', isEqualTo: userName)
        .get();

    // For Registered Patient
    final CollectionReference checkIfPatientVerified = FirebaseFirestore
        .instance
        .collection("Users")
        .doc("Patient")
        .collection("Lists");

    QuerySnapshot _queryPatient = await checkIfPatientVerified
        .where('UserName', isEqualTo: userName)
        .get();

    // For Archived Patient
    final CollectionReference checkIfArchivedPatientVerified = FirebaseFirestore
        .instance
        .collection("Archived")
        .doc("Users")
        .collection("Registered")
        .doc("Donor")
        .collection("Lists");

    QuerySnapshot _queryArchivedPatient = await checkIfArchivedPatientVerified
        .where('UserName', isEqualTo: userName)
        .get();

    if ((_queryDonor.docs.length > 0) ||
        (_queryPatient.docs.length > 0) ||
        (_queryArchivedDonor.docs.length > 0) ||
        (_queryArchivedPatient.docs.length > 0)) {
      showSnackBar(context);
    } else {
      if (usersValue == 'I am a Patient') {
        patientStatus = "Full Registration Completed";
        patient();
        patientAll();
      } else if (usersValue == 'I am a Donor') {
        donorStatus = "Full Registration Completed";
        donor();
        donorAll();
      }
      Navigator.pushAndRemoveUntil(
          context,
          MaterialPageRoute(builder: (builder) => Dashboard()),
          (route) => false);
    }
  }

  void showSnackBar(BuildContext context) {
    ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(content: Text("This user name is already taken.")));
  }
}
