import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:organ_donor_app/pages/ProfileSettings.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:fluttertoast/fluttertoast.dart';

class InformationManagementPage extends StatefulWidget {
  InformationManagementPage({Key? key}) : super(key: key);

  @override
  _InformationManagementPageState createState() =>
      _InformationManagementPageState();
}

class _InformationManagementPageState extends State<InformationManagementPage> {
  String? donorFullName;
  String? donorUserID;
  String? donorUserType;
  String? donorUserName;

  String? patientUserName;
  String? patientUserID;
  String? patientFullName;
  String? patientUserType;
  String? usersValue;
  String? currentUserID;

  TextEditingController _email = TextEditingController();

  //Toast
  final toast = FToast();
  @override
  void initState() {
    super.initState();

    toast.init(context);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xffe5eef8),
      body: Stack(
        children: [
          Container(
            margin: EdgeInsets.only(top: 40, left: 20),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                InkWell(
                  onTap: () {
                    Navigator.pushAndRemoveUntil(
                        context,
                        MaterialPageRoute(
                            builder: (builder) => ProfileSettings()),
                        (route) => false);
                  },
                  child: Icon(Icons.arrow_back_ios_outlined, size: 25.0),
                ),
                Text(
                  "Information Management",
                  style: GoogleFonts.roboto(
                      color: Colors.black87,
                      fontWeight: FontWeight.bold,
                      fontSize: 18),
                ),
                Container(width: 32.0, height: 0.0),
              ],
            ),
          ),
          //Content
          Container(
            margin: EdgeInsets.only(top: 100, left: 15, right: 15),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  margin: EdgeInsets.only(left: 10),
                  child: Text(
                    'Personal Information Management',
                    style: GoogleFonts.roboto(
                        color: Colors.grey[600], fontSize: 14),
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(top: 10),
                  height: 200,
                  width: double.maxFinite,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    boxShadow: [
                      BoxShadow(
                        color: Colors.grey.withOpacity(0.5),
                      ),
                    ],
                    color: Colors.white,
                  ),
                  padding: EdgeInsets.only(top: 10, bottom: 10),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        margin: EdgeInsets.only(top: 10, left: 20, right: 20),
                        child: Text(
                          'Please enter your email address so that the admin can contact you regarding other concerns with organ donation.',
                          style: GoogleFonts.roboto(
                              color: Colors.grey[600], fontSize: 14),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(top: 20, left: 15, right: 15),
                        child: InkWell(
                          onTap: () {},
                          child: TextFormField(
                            decoration: InputDecoration(
                                filled: true,
                                fillColor: Color(0xffe5eef8),
                                border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(10.0),
                                  borderSide: BorderSide.none,
                                ),
                                hintText: 'Please enter your email'),
                            controller: _email..text = "",
                          ),
                        ),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          Container(
                            margin: EdgeInsets.only(top: 20, right: 20),
                            child: InkWell(
                              onTap: () {
                                if (usersValue == 'I am a Donor') {
                                  setState(() {
                                    updateDonorEmail();
                                    updateAllEmail();
                                    showUpdateEmailToast();
                                    _email.text = "";
                                  });
                                } else {
                                  setState(() {
                                    updatePatientEmail();
                                    updateAllEmail();
                                    showUpdateEmailToast();
                                    _email.text = "";
                                  });
                                }
                              },
                              child: Text(
                                'Submit',
                                style: GoogleFonts.roboto(
                                    color: Colors.red[400],
                                    fontSize: 14,
                                    fontWeight: FontWeight.w600),
                              ),
                            ),
                          ),
                        ],
                      )
                    ],
                  ),
                ),
              ],
            ),
          ),
          //check if donor or patient
          FutureBuilder(
            future: getUserData(),
            builder: (context, snapshot) {
              if (snapshot.connectionState != ConnectionState.done)
                return Text("");
              if (donorUserID != null) {
                currentUserID = donorUserID;
                usersValue = donorUserType;

                return Text('');
              }
              if (patientUserID != null) {
                currentUserID = patientUserID;
                usersValue = patientUserType;

                return Text('');
              } else {
                return Text(
                  "null",
                  style: GoogleFonts.roboto(
                      color: Colors.black54,
                      fontSize: 18,
                      fontWeight: FontWeight.bold),
                );
              }
            },
          ),
        ],
      ),
    );
  }

  //Show Update Success
  void showUpdateEmailToast() => toast.showToast(
      child: buildUpdateEmailToast(), gravity: ToastGravity.CENTER);

  Widget buildUpdateEmailToast() => Container(
        padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
        decoration: BoxDecoration(
          color: Colors.green[300],
          borderRadius: BorderRadius.circular(5),
        ),
        child: Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            Text(
              "Email Updated",
              style: TextStyle(color: Colors.black87, fontSize: 16),
            ),
          ],
        ),
      );

  //Get User Information
  getUserData() async {
    final donor = FirebaseAuth.instance.currentUser;
    if (donor != null) {
      await FirebaseFirestore.instance
          .collection('Users')
          .doc("Donor")
          .collection("Lists")
          .doc(donor.uid)
          .get()
          .then((value) {
        donorFullName = value.data()!['FullName'];
        donorUserID = value.data()!['UserID'];
        donorUserName = value.data()!['UserName'];
        donorUserType = value.data()!['User Type'];
      }).catchError((e) {
        print(e);
      });
    }

    final patient = FirebaseAuth.instance.currentUser;
    if (patient != null) {
      await FirebaseFirestore.instance
          .collection('Users')
          .doc("Patient")
          .collection("Lists")
          .doc(patient.uid)
          .get()
          .then((value) {
        patientFullName = value.data()!['FullName'];
        patientUserID = value.data()!['UserID'];
        patientUserName = value.data()!['UserName'];
        patientUserType = value.data()!['User Type'];
      }).catchError((e) {
        print(e);
      });
    }
  }

//Update Email of User
  updateDonorEmail() async {
    String? uid = currentUserID;
    await FirebaseFirestore.instance
        .collection("Users")
        .doc('Donor')
        .collection('Lists')
        .doc(uid)
        .update({
      'Email Address': _email.text,
    });
  }

  updatePatientEmail() async {
    String? uid = currentUserID;
    await FirebaseFirestore.instance
        .collection("Users")
        .doc('Patient')
        .collection('Lists')
        .doc(uid)
        .update({
      'Email Address': _email.text,
    });
  }

  updateAllEmail() async {
    String? uid = currentUserID;
    await FirebaseFirestore.instance
        .collection("Users")
        .doc('All')
        .collection('Lists')
        .doc(uid)
        .update({
      'Email Address': _email.text,
    });
  }
}
