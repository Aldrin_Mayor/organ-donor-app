import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:intl/intl.dart';

class AdminMatchingPage extends StatefulWidget {
  AdminMatchingPage({Key? key}) : super(key: key);

  @override
  _AdminMatchingPageState createState() => _AdminMatchingPageState();
}

class _AdminMatchingPageState extends State<AdminMatchingPage> {
  //Donor
  String? getDonorUserID;
  String? getDonorFullName;
  String? getDonorUserName;
  String? getDonorUserType;
  String? getDonorGender;
  String? getDonorAge;
  String? getDonorBloodType;
  String? getDonorKidneySize;

  TextEditingController _donorUserID = TextEditingController();
  TextEditingController _donorFullName = TextEditingController();
  TextEditingController _donorUserName = TextEditingController();
  TextEditingController _donorUserType = TextEditingController();
  TextEditingController _donorGender = TextEditingController();
  TextEditingController _donorAge = TextEditingController();
  TextEditingController _donorBloodType = TextEditingController();
  TextEditingController _donorKidneySize = TextEditingController();

  //Patient
  String? getPatientUserID;
  String? getPatientFullName;
  String? getPatientUserName;
  String? getPatientUserType;
  String? getPatientGender;
  String? getPatientAge;
  String? getPatientBloodType;
  String? getPatientKidneySize;

  TextEditingController _patientUserID = TextEditingController();
  TextEditingController _patientFullName = TextEditingController();
  TextEditingController _patientUserName = TextEditingController();
  TextEditingController _patientUserType = TextEditingController();
  TextEditingController _patientGender = TextEditingController();
  TextEditingController _patientAge = TextEditingController();
  TextEditingController _patientBloodType = TextEditingController();
  TextEditingController _patientKidneySize = TextEditingController();

  //For Matching
  double? agePerecentage;
  double? bloodTypePercentage;
  double? kidneySizePercentage;
  double? matchResultPercentage;

  TextEditingController _agePercentage = TextEditingController();
  TextEditingController _bloodTypePercentage = TextEditingController();
  TextEditingController _kidneySizePercentage = TextEditingController();
  TextEditingController _matchResultPercentage = TextEditingController();
  TextEditingController _matchResultPercentageMessage = TextEditingController();

  bool isButtonEnabled = false;

  final toast = FToast();
  @override
  void initState() {
    super.initState();

    toast.init(context);
  }

  //Time and Date
  var now = new DateTime.now();
  var formatter = new DateFormat('dd-MM-yyyy');

  String? getDonorID;
  String? getDUserName;
  String? getPatientID;
  String? getPUserName;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xffe5eef8),
      body: SingleChildScrollView(
        child: Wrap(
          spacing: 8.0, // gap between adjacent chips
          runSpacing: 4.0, // gap between lines
          direction: Axis.horizontal,
          children: <Widget>[
            Container(
              margin: EdgeInsets.only(top: 60, left: 100),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    'Match a Donor and Patient',
                    style: GoogleFonts.roboto(
                        color: Colors.black54,
                        fontSize: 40,
                        fontWeight: FontWeight.w400),
                  ),
                  Container(
                    margin: EdgeInsets.only(right: 200),
                    height: 40,
                    width: 170,
                    decoration: BoxDecoration(
                      color: Colors.red,
                      borderRadius: BorderRadius.circular(20),
                    ),
                    child: InkWell(
                      onTap: () {
                        showMatchCustomDialog(context);
                      },
                      child: Row(
                        children: [
                          Container(
                            margin: EdgeInsets.only(left: 10),
                            child: Icon(
                              Icons.search_outlined,
                              color: Colors.red[100],
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.only(left: 10),
                            child: Text(
                              'Find match'.toUpperCase(),
                              style: GoogleFonts.roboto(
                                  color: Colors.white, fontSize: 16),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Container(
              height: MediaQuery.of(context).size.height * 0.7,
              width: MediaQuery.of(context).size.width * 0.755,
              margin: EdgeInsets.only(top: 20, left: 50),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: Colors.white,
              ),
              child: Stack(
                children: [
                  //Contents
                  FutureBuilder(
                    future: getAllMatchData(),
                    builder: (context, AsyncSnapshot snapshot) {
                      if (snapshot.hasData) {
                        return ListView.builder(
                          itemCount: snapshot.data!.length,
                          itemBuilder: (context, index) {
                            DocumentSnapshot userData = snapshot.data[index];

                            return Stack(
                              children: [
                                Container(
                                  color: Colors.red[50],
                                  margin: EdgeInsets.only(
                                      top: 20, left: 20, right: 20),
                                  child: ListTile(
                                    title: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        //Time and Date
                                        Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: [
                                            Container(
                                              child: Row(
                                                children: [
                                                  Text(
                                                    userData["Date Matched"],
                                                    style: GoogleFonts.roboto(
                                                        color: Colors.grey,
                                                        fontSize: 14),
                                                  ),
                                                  Container(
                                                    margin: EdgeInsets.only(
                                                        left: 10),
                                                    child: Text(
                                                      userData["Time Matched"],
                                                      style: GoogleFonts.roboto(
                                                          color: Colors.grey,
                                                          fontSize: 14),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                            InkWell(
                                                onTap: () {
                                                  getDonorID =
                                                      userData["Donor UserID"];
                                                  getPatientID = userData[
                                                      "Patient UserID"];
                                                  getDUserName =
                                                      userData["Donor"];
                                                  getPUserName =
                                                      userData["Patient"];
                                                  showCustomDialog(context);
                                                },
                                                child: Icon(Icons
                                                    .delete_forever_outlined)),
                                          ],
                                        ),
                                        //Content
                                        Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: [
                                            //Donor Profile
                                            Container(
                                              margin: EdgeInsets.only(
                                                  top: 80, left: 100),
                                              child: Row(
                                                children: [
                                                  Image.asset(
                                                    'assets/admin_donor.png',
                                                    height: 100,
                                                  ),
                                                  Container(
                                                    margin: EdgeInsets.only(
                                                        left: 10),
                                                    child: Text(
                                                      userData["Donor"],
                                                      style: GoogleFonts.roboto(
                                                          color: Colors.black87,
                                                          fontSize: 20),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                            //Percentages
                                            Column(
                                              children: [
                                                Container(
                                                  height: 50,
                                                  child: SelectableText.rich(
                                                    TextSpan(
                                                      children: [
                                                        TextSpan(
                                                          text: "Match Rate: ",
                                                          style: GoogleFonts
                                                              .roboto(
                                                                  color: Colors
                                                                      .grey,
                                                                  fontSize: 40,
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .w700),
                                                        ),
                                                        TextSpan(
                                                          text: userData[
                                                              "Match Rate"],
                                                          style: GoogleFonts
                                                              .roboto(
                                                                  color: Colors
                                                                      .red[400],
                                                                  fontSize: 40,
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .w700),
                                                        ),
                                                      ],
                                                    ),
                                                  ),
                                                ),
                                                Container(
                                                  margin:
                                                      EdgeInsets.only(top: 10),
                                                  child: Column(
                                                    children: [
                                                      Text(
                                                        'Summary'.toUpperCase(),
                                                        style:
                                                            GoogleFonts.roboto(
                                                                color: Colors
                                                                    .indigo,
                                                                fontSize: 20,
                                                                fontWeight:
                                                                    FontWeight
                                                                        .w600,
                                                                letterSpacing:
                                                                    2),
                                                      ),
                                                      //Age Percentage
                                                      Container(
                                                        margin: EdgeInsets.only(
                                                            top: 20),
                                                        child: Row(
                                                          children: [
                                                            Text(
                                                              'Age % : ',
                                                              style: GoogleFonts.roboto(
                                                                  color: Colors
                                                                          .blue[
                                                                      400],
                                                                  fontSize: 16,
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .w400,
                                                                  letterSpacing:
                                                                      2),
                                                            ),
                                                            Text(
                                                              userData[
                                                                  "Age Percentage"],
                                                              style: GoogleFonts.roboto(
                                                                  color: Colors
                                                                          .blue[
                                                                      400],
                                                                  fontSize: 16,
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .w400,
                                                                  letterSpacing:
                                                                      2),
                                                            )
                                                          ],
                                                        ),
                                                      ),
                                                      //Blood Type Percentage
                                                      Container(
                                                        margin: EdgeInsets.only(
                                                            top: 10),
                                                        child: Row(
                                                          children: [
                                                            Text(
                                                              'Blood Type % : ',
                                                              style: GoogleFonts.roboto(
                                                                  color: Colors
                                                                          .blue[
                                                                      400],
                                                                  fontSize: 16,
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .w400,
                                                                  letterSpacing:
                                                                      2),
                                                            ),
                                                            Text(
                                                              userData[
                                                                  "Blood Type Percentage"],
                                                              style: GoogleFonts.roboto(
                                                                  color: Colors
                                                                          .blue[
                                                                      400],
                                                                  fontSize: 16,
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .w400,
                                                                  letterSpacing:
                                                                      2),
                                                            )
                                                          ],
                                                        ),
                                                      ),
                                                      //Kidney Size Percentage
                                                      Container(
                                                        margin: EdgeInsets.only(
                                                            top: 10),
                                                        child: Row(
                                                          children: [
                                                            Text(
                                                              'Kidney Size % : ',
                                                              style: GoogleFonts.roboto(
                                                                  color: Colors
                                                                          .blue[
                                                                      400],
                                                                  fontSize: 16,
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .w400,
                                                                  letterSpacing:
                                                                      2),
                                                            ),
                                                            Text(
                                                              userData[
                                                                  "Kidney Size Percentage"],
                                                              style: GoogleFonts.roboto(
                                                                  color: Colors
                                                                          .blue[
                                                                      400],
                                                                  fontSize: 16,
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .w400,
                                                                  letterSpacing:
                                                                      2),
                                                            )
                                                          ],
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                              ],
                                            ),

                                            //Patient Profile
                                            Container(
                                              margin: EdgeInsets.only(
                                                  top: 80, right: 100),
                                              child: Row(
                                                children: [
                                                  Container(
                                                    margin: EdgeInsets.only(
                                                        right: 10),
                                                    child: Text(
                                                      userData["Patient"],
                                                      style: GoogleFonts.roboto(
                                                          color: Colors.black87,
                                                          fontSize: 20),
                                                    ),
                                                  ),
                                                  Image.asset(
                                                    'assets/admin_patient.png',
                                                    height: 100,
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ],
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            );
                          },
                        );
                      } else {
                        return Text('Users Data Not Available');
                      }
                    },
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  //Generate Kidney Match
  void showMatchCustomDialog(BuildContext context) => showDialog(
      context: context,
      builder: (BuildContext context) {
        return Dialog(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10.0)), //this right here
          child: Container(
            height: MediaQuery.of(context).size.height * 0.999,
            width: MediaQuery.of(context).size.width * 1,
            child: Stack(
              children: [
                //Close Icon
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    Container(
                      margin: EdgeInsets.only(top: 10, right: 20),
                      child: InkWell(
                        onTap: () {
                          setState(() {
                            Navigator.of(context).pop();
                            isButtonEnabled = false;
                            //Donor clear Fields
                            _donorFullName.text = "";
                            _donorUserName.text = "";
                            _donorGender.text = "";
                            _donorAge.text = "";
                            _donorBloodType.text = "";
                            _donorKidneySize.text = "";
                            //Patient clear Fields
                            _patientFullName.text = "";
                            _patientUserName.text = "";
                            _patientGender.text = "";
                            _patientAge.text = "";
                            _patientBloodType.text = "";
                            _patientKidneySize.text = "";

                            _agePercentage.text = "";
                            _bloodTypePercentage.text = "";
                            _kidneySizePercentage.text = "";
                            _matchResultPercentage.text = "";
                            _matchResultPercentageMessage.text = "";
                          });
                        },
                        child: Icon(
                          Icons.close_outlined,
                          size: 25,
                          color: Colors.red[100],
                        ),
                      ),
                    ),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container(
                      margin: EdgeInsets.only(top: 40, left: 40),
                      child: Text(
                        'Generate Kidney Match'.toUpperCase(),
                        style: GoogleFonts.roboto(
                            color: Colors.black87,
                            fontSize: 40,
                            fontWeight: FontWeight.w700,
                            letterSpacing: 2),
                      ),
                    ),
                  ],
                ),

                //Donor and Patient Registered
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    //Donor Registered
                    Column(
                      children: [
                        Container(
                          margin: EdgeInsets.only(
                            top: 100,
                          ),
                          child: Text(
                            'Donor Registered',
                            style: GoogleFonts.roboto(
                                color: Colors.black87,
                                fontSize: 20,
                                fontWeight: FontWeight.w600,
                                letterSpacing: 1),
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(left: 20),
                          width: 450,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: [
                              Text(
                                'Full Name',
                                style: GoogleFonts.roboto(
                                    color: Colors.red,
                                    fontWeight: FontWeight.w700,
                                    fontSize: 14),
                              ),
                              Text(
                                'User Name',
                                style: GoogleFonts.roboto(
                                    color: Colors.red,
                                    fontWeight: FontWeight.w700,
                                    fontSize: 14),
                              ),
                              Text(
                                'Gender',
                                style: GoogleFonts.roboto(
                                    color: Colors.red,
                                    fontWeight: FontWeight.w700,
                                    fontSize: 14),
                              ),
                              Text(
                                'Age',
                                style: GoogleFonts.roboto(
                                    color: Colors.red,
                                    fontWeight: FontWeight.w700,
                                    fontSize: 14),
                              ),
                              Text(
                                '\nBlood\nType',
                                style: GoogleFonts.roboto(
                                    color: Colors.red,
                                    fontWeight: FontWeight.w700,
                                    fontSize: 14),
                              ),
                              Text(
                                '\nKidney\nSize',
                                style: GoogleFonts.roboto(
                                    color: Colors.red,
                                    fontWeight: FontWeight.w700,
                                    fontSize: 14),
                              ),
                            ],
                          ),
                        ),
                        Container(
                          width: 460,
                          height: 300,
                          margin: EdgeInsets.only(left: 40),
                          decoration: BoxDecoration(
                              color: Color(0xffe5eef8),
                              borderRadius: BorderRadius.circular(10)),
                          child: FutureBuilder(
                            future: getDonorRegisteredData(),
                            builder: (context, AsyncSnapshot snapshot) {
                              if (snapshot.hasData) {
                                return ListView.builder(
                                  itemCount: snapshot.data!.length,
                                  itemBuilder: (context, index) {
                                    DocumentSnapshot userData =
                                        snapshot.data[index];
                                    return Stack(
                                      children: [
                                        Container(
                                          margin: EdgeInsets.only(
                                            top: 10,
                                          ),
                                          child: ListTile(
                                            title: Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.spaceEvenly,
                                              children: [
                                                Container(
                                                  margin:
                                                      EdgeInsets.only(left: 10),
                                                  width: 65,
                                                  child: Text(
                                                    userData["FullName"],
                                                    style: GoogleFonts.roboto(
                                                        color: Colors.black87,
                                                        fontWeight:
                                                            FontWeight.w400,
                                                        fontSize: 14),
                                                  ),
                                                ),
                                                Container(
                                                  width: 70,
                                                  margin:
                                                      EdgeInsets.only(left: 20),
                                                  child: Text(
                                                    userData["UserName"],
                                                    style: GoogleFonts.roboto(
                                                        color: Colors.black87,
                                                        fontWeight:
                                                            FontWeight.w400,
                                                        fontSize: 14),
                                                  ),
                                                ),
                                                Container(
                                                  width: 55,
                                                  margin:
                                                      EdgeInsets.only(left: 25),
                                                  child: Text(
                                                    userData["Gender"],
                                                    style: GoogleFonts.roboto(
                                                        color: Colors.black87,
                                                        fontWeight:
                                                            FontWeight.w400,
                                                        fontSize: 14),
                                                  ),
                                                ),
                                                Container(
                                                  width: 20,
                                                  margin:
                                                      EdgeInsets.only(left: 10),
                                                  child: Text(
                                                    userData["Age"],
                                                    style: GoogleFonts.roboto(
                                                        color: Colors.black87,
                                                        fontWeight:
                                                            FontWeight.w400,
                                                        fontSize: 14),
                                                  ),
                                                ),
                                                Container(
                                                  width: 25,
                                                  margin:
                                                      EdgeInsets.only(left: 35),
                                                  child: Text(
                                                    userData["Blood Type"],
                                                    style: GoogleFonts.roboto(
                                                        color: Colors.black87,
                                                        fontWeight:
                                                            FontWeight.w400,
                                                        fontSize: 14),
                                                  ),
                                                ),
                                                Container(
                                                  width: 25,
                                                  margin:
                                                      EdgeInsets.only(left: 30),
                                                  child: Text(
                                                    userData["Kidney Size"],
                                                    style: GoogleFonts.roboto(
                                                        color: Colors.black87,
                                                        fontWeight:
                                                            FontWeight.w400,
                                                        fontSize: 14),
                                                  ),
                                                ),
                                                InkWell(
                                                  onTap: () {
                                                    getDonorFullName =
                                                        userData["FullName"];
                                                    getDonorUserName =
                                                        userData["UserName"];
                                                    getDonorGender =
                                                        userData["Gender"];
                                                    getDonorUserType =
                                                        userData["User Type"];
                                                    getDonorAge =
                                                        userData["Age"];
                                                    getDonorBloodType =
                                                        userData["Blood Type"];
                                                    getDonorKidneySize =
                                                        userData["Kidney Size"];
                                                    getDonorUserID =
                                                        userData["UserID"];

                                                    _donorUserID.text =
                                                        getDonorUserID!;
                                                    _donorFullName.text =
                                                        getDonorFullName!;
                                                    _donorUserName.text =
                                                        getDonorUserName!;
                                                    _donorUserType.text =
                                                        getDonorUserType!;
                                                    _donorGender.text =
                                                        getDonorGender!;
                                                    _donorAge.text =
                                                        getDonorAge!;
                                                    _donorBloodType.text =
                                                        getDonorBloodType!;
                                                    _donorKidneySize.text =
                                                        getDonorKidneySize!;
                                                  },
                                                  child: Container(
                                                    width: 25,
                                                    margin: EdgeInsets.only(
                                                        left: 10),
                                                    child: Icon(
                                                      Icons
                                                          .arrow_right_outlined,
                                                      size: 30,
                                                      color: Colors.blue,
                                                    ),
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.symmetric(
                                              horizontal: 18),
                                          child: Divider(),
                                        ),
                                      ],
                                    );
                                  },
                                );
                              } else {
                                return Container(
                                  alignment: Alignment.center,
                                  child: CircularProgressIndicator(),
                                );
                              }
                            },
                          ),
                        ),
                      ],
                    ),
                    //Patient Registered
                    Column(
                      children: [
                        Container(
                          margin: EdgeInsets.only(
                            top: 100,
                          ),
                          child: Text(
                            'Patient Registered',
                            style: GoogleFonts.roboto(
                                color: Colors.black87,
                                fontSize: 20,
                                fontWeight: FontWeight.w600,
                                letterSpacing: 1),
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(left: 20),
                          width: 450,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: [
                              Text(
                                'Full Name',
                                style: GoogleFonts.roboto(
                                    color: Colors.red,
                                    fontWeight: FontWeight.w700,
                                    fontSize: 14),
                              ),
                              Text(
                                'User Name',
                                style: GoogleFonts.roboto(
                                    color: Colors.red,
                                    fontWeight: FontWeight.w700,
                                    fontSize: 14),
                              ),
                              Text(
                                'Gender',
                                style: GoogleFonts.roboto(
                                    color: Colors.red,
                                    fontWeight: FontWeight.w700,
                                    fontSize: 14),
                              ),
                              Text(
                                'Age',
                                style: GoogleFonts.roboto(
                                    color: Colors.red,
                                    fontWeight: FontWeight.w700,
                                    fontSize: 14),
                              ),
                              Text(
                                '\nBlood\nType',
                                style: GoogleFonts.roboto(
                                    color: Colors.red,
                                    fontWeight: FontWeight.w700,
                                    fontSize: 14),
                              ),
                              Text(
                                '\nKidney\nSize',
                                style: GoogleFonts.roboto(
                                    color: Colors.red,
                                    fontWeight: FontWeight.w700,
                                    fontSize: 14),
                              ),
                            ],
                          ),
                        ),
                        Container(
                          width: 460,
                          height: 300,
                          margin: EdgeInsets.only(right: 40),
                          decoration: BoxDecoration(
                              color: Color(0xffe5eef8),
                              borderRadius: BorderRadius.circular(10)),
                          child: FutureBuilder(
                            future: getPatientRegisteredData(),
                            builder: (context, AsyncSnapshot snapshot) {
                              if (snapshot.hasData) {
                                return ListView.builder(
                                  itemCount: snapshot.data!.length,
                                  itemBuilder: (context, index) {
                                    DocumentSnapshot userData =
                                        snapshot.data[index];
                                    return Stack(
                                      children: [
                                        Container(
                                          margin: EdgeInsets.only(
                                            top: 10,
                                          ),
                                          child: ListTile(
                                            title: Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.spaceEvenly,
                                              children: [
                                                InkWell(
                                                  onTap: () {
                                                    getPatientFullName =
                                                        userData["FullName"];
                                                    getPatientUserName =
                                                        userData["UserName"];
                                                    getPatientUserType =
                                                        userData["User Type"];
                                                    getPatientGender =
                                                        userData["Gender"];
                                                    getPatientAge =
                                                        userData["Age"];
                                                    getPatientBloodType =
                                                        userData["Blood Type"];
                                                    getPatientKidneySize =
                                                        userData["Kidney Size"];
                                                    getPatientUserID =
                                                        userData["UserID"];

                                                    _patientUserID.text =
                                                        getPatientUserID!;
                                                    _patientFullName.text =
                                                        getPatientFullName!;
                                                    _patientUserType.text =
                                                        getPatientUserType!;
                                                    _patientUserName.text =
                                                        getPatientUserName!;
                                                    _patientGender.text =
                                                        getPatientGender!;
                                                    _patientAge.text =
                                                        getPatientAge!;
                                                    _patientBloodType.text =
                                                        getPatientBloodType!;
                                                    _patientKidneySize.text =
                                                        getPatientKidneySize!;
                                                  },
                                                  child: Container(
                                                    width: 25,
                                                    margin: EdgeInsets.only(
                                                        left: 10),
                                                    child: Icon(
                                                      Icons.arrow_left_outlined,
                                                      size: 30,
                                                      color: Colors.blue,
                                                    ),
                                                  ),
                                                ),
                                                Container(
                                                  margin:
                                                      EdgeInsets.only(left: 10),
                                                  width: 65,
                                                  child: Text(
                                                    userData["FullName"],
                                                    style: GoogleFonts.roboto(
                                                        color: Colors.black87,
                                                        fontWeight:
                                                            FontWeight.w400,
                                                        fontSize: 14),
                                                  ),
                                                ),
                                                Container(
                                                  width: 70,
                                                  margin:
                                                      EdgeInsets.only(left: 20),
                                                  child: Text(
                                                    userData["UserName"],
                                                    style: GoogleFonts.roboto(
                                                        color: Colors.black87,
                                                        fontWeight:
                                                            FontWeight.w400,
                                                        fontSize: 14),
                                                  ),
                                                ),
                                                Container(
                                                  width: 55,
                                                  margin:
                                                      EdgeInsets.only(left: 25),
                                                  child: Text(
                                                    userData["Gender"],
                                                    style: GoogleFonts.roboto(
                                                        color: Colors.black87,
                                                        fontWeight:
                                                            FontWeight.w400,
                                                        fontSize: 14),
                                                  ),
                                                ),
                                                Container(
                                                  width: 20,
                                                  margin:
                                                      EdgeInsets.only(left: 10),
                                                  child: Text(
                                                    userData["Age"],
                                                    style: GoogleFonts.roboto(
                                                        color: Colors.black87,
                                                        fontWeight:
                                                            FontWeight.w400,
                                                        fontSize: 14),
                                                  ),
                                                ),
                                                Container(
                                                  width: 25,
                                                  margin:
                                                      EdgeInsets.only(left: 35),
                                                  child: Text(
                                                    userData["Blood Type"],
                                                    style: GoogleFonts.roboto(
                                                        color: Colors.black87,
                                                        fontWeight:
                                                            FontWeight.w400,
                                                        fontSize: 14),
                                                  ),
                                                ),
                                                Container(
                                                  width: 25,
                                                  margin:
                                                      EdgeInsets.only(left: 30),
                                                  child: Text(
                                                    userData["Kidney Size"],
                                                    style: GoogleFonts.roboto(
                                                        color: Colors.black87,
                                                        fontWeight:
                                                            FontWeight.w400,
                                                        fontSize: 14),
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.symmetric(
                                              horizontal: 18),
                                          child: Divider(),
                                        ),
                                      ],
                                    );
                                  },
                                );
                              } else {
                                return Container(
                                  alignment: Alignment.center,
                                  child: CircularProgressIndicator(),
                                );
                              }
                            },
                          ),
                        ),
                      ],
                    ),
                  ],
                ),

                //Donor and Patient Tagged
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    //Donor Tagged
                    Column(
                      children: [
                        Container(
                          margin: EdgeInsets.only(
                            top: 500,
                          ),
                          child: Text(
                            'Donor Tagged',
                            style: GoogleFonts.roboto(
                                color: Colors.black87,
                                fontSize: 20,
                                fontWeight: FontWeight.w600,
                                letterSpacing: 1),
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(left: 20),
                          width: 450,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: [
                              Text(
                                'Full Name',
                                style: GoogleFonts.roboto(
                                    color: Colors.red,
                                    fontWeight: FontWeight.w700,
                                    fontSize: 14),
                              ),
                              Text(
                                'User Name',
                                style: GoogleFonts.roboto(
                                    color: Colors.red,
                                    fontWeight: FontWeight.w700,
                                    fontSize: 14),
                              ),
                              Text(
                                'Gender',
                                style: GoogleFonts.roboto(
                                    color: Colors.red,
                                    fontWeight: FontWeight.w700,
                                    fontSize: 14),
                              ),
                              Text(
                                'Age',
                                style: GoogleFonts.roboto(
                                    color: Colors.red,
                                    fontWeight: FontWeight.w700,
                                    fontSize: 14),
                              ),
                              Text(
                                '\nBlood\nType',
                                style: GoogleFonts.roboto(
                                    color: Colors.red,
                                    fontWeight: FontWeight.w700,
                                    fontSize: 14),
                              ),
                              Text(
                                '\nKidney\nSize',
                                style: GoogleFonts.roboto(
                                    color: Colors.red,
                                    fontWeight: FontWeight.w700,
                                    fontSize: 14),
                              ),
                            ],
                          ),
                        ),
                        Container(
                          width: 460,
                          height: 300,
                          margin: EdgeInsets.only(left: 40),
                          decoration: BoxDecoration(
                              color: Color(0xffe5eef8),
                              borderRadius: BorderRadius.circular(10)),
                          child: FutureBuilder(
                            future: getDonorTaggedData(),
                            builder: (context, AsyncSnapshot snapshot) {
                              if (snapshot.hasData) {
                                return ListView.builder(
                                  itemCount: snapshot.data!.length,
                                  itemBuilder: (context, index) {
                                    DocumentSnapshot userData =
                                        snapshot.data[index];
                                    return Stack(
                                      children: [
                                        Container(
                                          margin: EdgeInsets.only(
                                            top: 10,
                                          ),
                                          child: ListTile(
                                            title: Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.spaceEvenly,
                                              children: [
                                                Container(
                                                  margin:
                                                      EdgeInsets.only(left: 10),
                                                  width: 65,
                                                  child: Text(
                                                    userData["FullName"],
                                                    style: GoogleFonts.roboto(
                                                        color: Colors.black87,
                                                        fontWeight:
                                                            FontWeight.w400,
                                                        fontSize: 14),
                                                  ),
                                                ),
                                                Container(
                                                  width: 70,
                                                  margin:
                                                      EdgeInsets.only(left: 20),
                                                  child: Text(
                                                    userData["UserName"],
                                                    style: GoogleFonts.roboto(
                                                        color: Colors.black87,
                                                        fontWeight:
                                                            FontWeight.w400,
                                                        fontSize: 14),
                                                  ),
                                                ),
                                                Container(
                                                  width: 55,
                                                  margin:
                                                      EdgeInsets.only(left: 25),
                                                  child: Text(
                                                    userData["Gender"],
                                                    style: GoogleFonts.roboto(
                                                        color: Colors.black87,
                                                        fontWeight:
                                                            FontWeight.w400,
                                                        fontSize: 14),
                                                  ),
                                                ),
                                                Container(
                                                  width: 20,
                                                  margin:
                                                      EdgeInsets.only(left: 10),
                                                  child: Text(
                                                    userData["Age"],
                                                    style: GoogleFonts.roboto(
                                                        color: Colors.black87,
                                                        fontWeight:
                                                            FontWeight.w400,
                                                        fontSize: 14),
                                                  ),
                                                ),
                                                Container(
                                                  width: 25,
                                                  margin:
                                                      EdgeInsets.only(left: 35),
                                                  child: Text(
                                                    userData["Blood Type"],
                                                    style: GoogleFonts.roboto(
                                                        color: Colors.black87,
                                                        fontWeight:
                                                            FontWeight.w400,
                                                        fontSize: 14),
                                                  ),
                                                ),
                                                Container(
                                                  width: 25,
                                                  margin:
                                                      EdgeInsets.only(left: 30),
                                                  child: Text(
                                                    userData["Kidney Size"],
                                                    style: GoogleFonts.roboto(
                                                        color: Colors.black87,
                                                        fontWeight:
                                                            FontWeight.w400,
                                                        fontSize: 14),
                                                  ),
                                                ),
                                                InkWell(
                                                  onTap: () {
                                                    getDonorFullName =
                                                        userData["FullName"];
                                                    getDonorUserName =
                                                        userData["UserName"];
                                                    getDonorUserType =
                                                        userData["User Type"];
                                                    getDonorGender =
                                                        userData["Gender"];
                                                    getDonorAge =
                                                        userData["Age"];
                                                    getDonorBloodType =
                                                        userData["Blood Type"];
                                                    getDonorKidneySize =
                                                        userData["Kidney Size"];
                                                    getDonorUserID =
                                                        userData["UserID"];

                                                    _donorUserID.text =
                                                        getDonorUserID!;
                                                    _donorFullName.text =
                                                        getDonorFullName!;
                                                    _donorUserName.text =
                                                        getDonorUserName!;
                                                    _donorUserType.text =
                                                        getDonorUserType!;
                                                    _donorGender.text =
                                                        getDonorGender!;
                                                    _donorAge.text =
                                                        getDonorAge!;
                                                    _donorBloodType.text =
                                                        getDonorBloodType!;
                                                    _donorKidneySize.text =
                                                        getDonorKidneySize!;
                                                  },
                                                  child: Container(
                                                    width: 25,
                                                    margin: EdgeInsets.only(
                                                        left: 10),
                                                    child: Icon(
                                                      Icons
                                                          .arrow_right_outlined,
                                                      size: 30,
                                                      color: Colors.blue,
                                                    ),
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.symmetric(
                                              horizontal: 18),
                                          child: Divider(),
                                        ),
                                      ],
                                    );
                                  },
                                );
                              } else {
                                return Container(
                                  alignment: Alignment.center,
                                  child: CircularProgressIndicator(),
                                );
                              }
                            },
                          ),
                        ),
                      ],
                    ),
                    //Patient Tagged
                    Column(
                      children: [
                        Container(
                          margin: EdgeInsets.only(
                            top: 500,
                          ),
                          child: Text(
                            'Patient Tagged',
                            style: GoogleFonts.roboto(
                                color: Colors.black87,
                                fontSize: 20,
                                fontWeight: FontWeight.w600,
                                letterSpacing: 1),
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(left: 20),
                          width: 450,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: [
                              Text(
                                'Full Name',
                                style: GoogleFonts.roboto(
                                    color: Colors.red,
                                    fontWeight: FontWeight.w700,
                                    fontSize: 14),
                              ),
                              Text(
                                'User Name',
                                style: GoogleFonts.roboto(
                                    color: Colors.red,
                                    fontWeight: FontWeight.w700,
                                    fontSize: 14),
                              ),
                              Text(
                                'Gender',
                                style: GoogleFonts.roboto(
                                    color: Colors.red,
                                    fontWeight: FontWeight.w700,
                                    fontSize: 14),
                              ),
                              Text(
                                'Age',
                                style: GoogleFonts.roboto(
                                    color: Colors.red,
                                    fontWeight: FontWeight.w700,
                                    fontSize: 14),
                              ),
                              Text(
                                '\nBlood\nType',
                                style: GoogleFonts.roboto(
                                    color: Colors.red,
                                    fontWeight: FontWeight.w700,
                                    fontSize: 14),
                              ),
                              Text(
                                '\nKidney\nSize',
                                style: GoogleFonts.roboto(
                                    color: Colors.red,
                                    fontWeight: FontWeight.w700,
                                    fontSize: 14),
                              ),
                            ],
                          ),
                        ),
                        Container(
                          width: 460,
                          height: 300,
                          margin: EdgeInsets.only(right: 40),
                          decoration: BoxDecoration(
                              color: Color(0xffe5eef8),
                              borderRadius: BorderRadius.circular(10)),
                          child: FutureBuilder(
                            future: getPatientTaggedData(),
                            builder: (context, AsyncSnapshot snapshot) {
                              if (snapshot.hasData) {
                                return ListView.builder(
                                  itemCount: snapshot.data!.length,
                                  itemBuilder: (context, index) {
                                    DocumentSnapshot userData =
                                        snapshot.data[index];
                                    return Stack(
                                      children: [
                                        Container(
                                          margin: EdgeInsets.only(
                                            top: 10,
                                          ),
                                          child: ListTile(
                                            title: Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.spaceEvenly,
                                              children: [
                                                InkWell(
                                                  onTap: () {
                                                    getPatientFullName =
                                                        userData["FullName"];
                                                    getPatientUserName =
                                                        userData["UserName"];
                                                    getPatientUserType =
                                                        userData["User Type"];
                                                    getPatientGender =
                                                        userData["Gender"];
                                                    getPatientAge =
                                                        userData["Age"];
                                                    getPatientBloodType =
                                                        userData["Blood Type"];
                                                    getPatientKidneySize =
                                                        userData["Kidney Size"];
                                                    getPatientUserID =
                                                        userData["UserID"];

                                                    _patientUserID.text =
                                                        getPatientUserID!;
                                                    _patientFullName.text =
                                                        getPatientFullName!;
                                                    _patientUserName.text =
                                                        getPatientUserName!;
                                                    _patientUserType.text =
                                                        getPatientUserType!;
                                                    _patientGender.text =
                                                        getPatientGender!;
                                                    _patientAge.text =
                                                        getPatientAge!;
                                                    _patientBloodType.text =
                                                        getPatientBloodType!;
                                                    _patientKidneySize.text =
                                                        getPatientKidneySize!;
                                                  },
                                                  child: Container(
                                                    width: 25,
                                                    margin: EdgeInsets.only(
                                                        left: 10),
                                                    child: Icon(
                                                      Icons.arrow_left_outlined,
                                                      size: 30,
                                                      color: Colors.blue,
                                                    ),
                                                  ),
                                                ),
                                                Container(
                                                  margin:
                                                      EdgeInsets.only(left: 10),
                                                  width: 65,
                                                  child: Text(
                                                    userData["FullName"],
                                                    style: GoogleFonts.roboto(
                                                        color: Colors.black87,
                                                        fontWeight:
                                                            FontWeight.w400,
                                                        fontSize: 14),
                                                  ),
                                                ),
                                                Container(
                                                  width: 70,
                                                  margin:
                                                      EdgeInsets.only(left: 20),
                                                  child: Text(
                                                    userData["UserName"],
                                                    style: GoogleFonts.roboto(
                                                        color: Colors.black87,
                                                        fontWeight:
                                                            FontWeight.w400,
                                                        fontSize: 14),
                                                  ),
                                                ),
                                                Container(
                                                  width: 55,
                                                  margin:
                                                      EdgeInsets.only(left: 25),
                                                  child: Text(
                                                    userData["Gender"],
                                                    style: GoogleFonts.roboto(
                                                        color: Colors.black87,
                                                        fontWeight:
                                                            FontWeight.w400,
                                                        fontSize: 14),
                                                  ),
                                                ),
                                                Container(
                                                  width: 20,
                                                  margin:
                                                      EdgeInsets.only(left: 10),
                                                  child: Text(
                                                    userData["Age"],
                                                    style: GoogleFonts.roboto(
                                                        color: Colors.black87,
                                                        fontWeight:
                                                            FontWeight.w400,
                                                        fontSize: 14),
                                                  ),
                                                ),
                                                Container(
                                                  width: 25,
                                                  margin:
                                                      EdgeInsets.only(left: 35),
                                                  child: Text(
                                                    userData["Blood Type"],
                                                    style: GoogleFonts.roboto(
                                                        color: Colors.black87,
                                                        fontWeight:
                                                            FontWeight.w400,
                                                        fontSize: 14),
                                                  ),
                                                ),
                                                Container(
                                                  width: 25,
                                                  margin:
                                                      EdgeInsets.only(left: 30),
                                                  child: Text(
                                                    userData["Kidney Size"],
                                                    style: GoogleFonts.roboto(
                                                        color: Colors.black87,
                                                        fontWeight:
                                                            FontWeight.w400,
                                                        fontSize: 14),
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.symmetric(
                                              horizontal: 18),
                                          child: Divider(),
                                        ),
                                      ],
                                    );
                                  },
                                );
                              } else {
                                return Container(
                                  alignment: Alignment.center,
                                  child: CircularProgressIndicator(),
                                );
                              }
                            },
                          ),
                        ),
                      ],
                    ),
                  ],
                ),

                //Donor and Patient match
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    //Donor Match Card
                    Container(
                      margin: EdgeInsets.only(top: 150, left: 500),
                      width: 200,
                      height: 500,
                      decoration: BoxDecoration(
                          color: Colors.red[100],
                          borderRadius: BorderRadius.circular(10)),
                      child: Column(
                        children: [
                          Container(
                            margin: EdgeInsets.only(top: 20),
                            child: Image.asset(
                              'assets/user.png',
                              height: 50,
                            ),
                          ),
                          Container(
                            width: 120,
                            child: Column(
                              children: [
                                TextFormField(
                                  controller: _donorFullName
                                    ..text = _donorFullName.text,
                                  style: GoogleFonts.roboto(
                                      color: Colors.red[400],
                                      fontWeight: FontWeight.w600,
                                      fontSize: 14),
                                  key: Key(
                                    getDonorFullName.toString(),
                                  ),
                                ),
                                Text(
                                  'Full Name: ',
                                  style: GoogleFonts.roboto(
                                      color: Colors.grey[600],
                                      fontWeight: FontWeight.bold,
                                      fontSize: 14),
                                ),
                              ],
                            ),
                          ),
                          Container(
                            width: 120,
                            child: Column(
                              children: [
                                TextFormField(
                                  controller: _donorUserName
                                    ..text = _donorUserName.text,
                                  style: GoogleFonts.roboto(
                                      color: Colors.red[400],
                                      fontWeight: FontWeight.w600,
                                      fontSize: 14),
                                  key: Key(
                                    getDonorUserName.toString(),
                                  ),
                                ),
                                Text(
                                  'User Name: ',
                                  style: GoogleFonts.roboto(
                                      color: Colors.grey[600],
                                      fontWeight: FontWeight.bold,
                                      fontSize: 14),
                                ),
                              ],
                            ),
                          ),
                          Container(
                            width: 120,
                            child: Column(
                              children: [
                                TextFormField(
                                  controller: _donorGender
                                    ..text = _donorGender.text,
                                  style: GoogleFonts.roboto(
                                      color: Colors.red[400],
                                      fontWeight: FontWeight.w600,
                                      fontSize: 14),
                                  key: Key(
                                    getDonorGender.toString(),
                                  ),
                                ),
                                Text(
                                  'Gender: ',
                                  style: GoogleFonts.roboto(
                                      color: Colors.grey[600],
                                      fontWeight: FontWeight.bold,
                                      fontSize: 14),
                                ),
                              ],
                            ),
                          ),
                          Container(
                            width: 120,
                            child: Column(
                              children: [
                                TextFormField(
                                  controller: _donorAge..text = _donorAge.text,
                                  style: GoogleFonts.roboto(
                                      color: Colors.red[400],
                                      fontWeight: FontWeight.w600,
                                      fontSize: 14),
                                  key: Key(
                                    getDonorAge.toString(),
                                  ),
                                ),
                                Text(
                                  'Age: ',
                                  style: GoogleFonts.roboto(
                                      color: Colors.grey[600],
                                      fontWeight: FontWeight.bold,
                                      fontSize: 14),
                                ),
                              ],
                            ),
                          ),
                          Container(
                            width: 120,
                            child: Column(
                              children: [
                                TextFormField(
                                  controller: _donorBloodType
                                    ..text = _donorBloodType.text,
                                  style: GoogleFonts.roboto(
                                      color: Colors.red[400],
                                      fontWeight: FontWeight.w600,
                                      fontSize: 14),
                                  key: Key(
                                    getDonorBloodType.toString(),
                                  ),
                                ),
                                Text(
                                  'Blood Type: ',
                                  style: GoogleFonts.roboto(
                                      color: Colors.grey[600],
                                      fontWeight: FontWeight.bold,
                                      fontSize: 14),
                                ),
                              ],
                            ),
                          ),
                          Container(
                            width: 120,
                            child: Column(
                              children: [
                                TextFormField(
                                  controller: _donorKidneySize
                                    ..text = _donorKidneySize.text,
                                  style: GoogleFonts.roboto(
                                      color: Colors.red[400],
                                      fontWeight: FontWeight.w600,
                                      fontSize: 14),
                                  key: Key(
                                    getDonorKidneySize.toString(),
                                  ),
                                ),
                                Text(
                                  'Kidney Size: ',
                                  style: GoogleFonts.roboto(
                                      color: Colors.grey[600],
                                      fontWeight: FontWeight.bold,
                                      fontSize: 14),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),

                    Column(
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            //Calculate Button
                            Container(
                              margin: EdgeInsets.only(top: 150),
                              height: 50,
                              width: 150,
                              child: ElevatedButton(
                                style: ElevatedButton.styleFrom(
                                  primary: Colors.red[50],
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(30),
                                  ),
                                ),
                                onPressed: () {
                                  //Kidney Percentage
                                  double dAge = double.parse(_donorAge.text);
                                  double pAge = double.parse(_patientAge.text);
                                  if (dAge > pAge) {
                                    setState(() {
                                      agePerecentage = (pAge / dAge) * 100;
                                      _agePercentage.text =
                                          agePerecentage!.toStringAsFixed(2);
                                    });
                                  } else {
                                    setState(() {
                                      agePerecentage = (dAge / pAge) * 100;
                                      _agePercentage.text =
                                          agePerecentage!.toStringAsFixed(2);
                                    });
                                  }

                                  //Blood Type
                                  String? dBT = _donorBloodType.text;
                                  String? pBT = _patientBloodType.text;
                                  //Compatible to A +
                                  if (dBT == "A+" && pBT == "A+") {
                                    //Blood Type A+
                                    bloodTypePercentage = 100.00;
                                    _bloodTypePercentage.text =
                                        bloodTypePercentage!.toStringAsFixed(2);
                                  } else if (dBT == "A+" && pBT == "A-") {
                                    bloodTypePercentage = 100.00;
                                    _bloodTypePercentage.text =
                                        bloodTypePercentage!.toStringAsFixed(2);
                                  } else if (dBT == "A+" && pBT == "AB-") {
                                    bloodTypePercentage = 100.00;
                                    _bloodTypePercentage.text =
                                        bloodTypePercentage!.toStringAsFixed(2);
                                  } else if (dBT == "A+" && pBT == "AB+") {
                                    bloodTypePercentage = 100.00;
                                    _bloodTypePercentage.text =
                                        bloodTypePercentage!.toStringAsFixed(2);
                                    //Not Compatible to A+
                                  } else if (dBT == "A+" && pBT == "B+") {
                                    bloodTypePercentage = 00.00;
                                    _bloodTypePercentage.text =
                                        bloodTypePercentage!.toStringAsFixed(2);
                                  } else if (dBT == "A+" && pBT == "B-") {
                                    bloodTypePercentage = 00.00;
                                    _bloodTypePercentage.text =
                                        bloodTypePercentage!.toStringAsFixed(2);
                                  } else if (dBT == "A+" && pBT == "O+") {
                                    bloodTypePercentage = 00.00;
                                    _bloodTypePercentage.text =
                                        bloodTypePercentage!.toStringAsFixed(2);
                                  } else if (dBT == "A+" && pBT == "O-") {
                                    bloodTypePercentage = 00.00;
                                    _bloodTypePercentage.text =
                                        bloodTypePercentage!.toStringAsFixed(2);
                                    //Compatible to A-
                                  } else if (dBT == "A-" && pBT == "A+") {
                                    //Blood Type A-
                                    bloodTypePercentage = 100.00;
                                    _bloodTypePercentage.text =
                                        bloodTypePercentage!.toStringAsFixed(2);
                                  } else if (dBT == "A-" && pBT == "A-") {
                                    bloodTypePercentage = 100.00;
                                    _bloodTypePercentage.text =
                                        bloodTypePercentage!.toStringAsFixed(2);
                                  } else if (dBT == "A-" && pBT == "AB-") {
                                    bloodTypePercentage = 100.00;
                                    _bloodTypePercentage.text =
                                        bloodTypePercentage!.toStringAsFixed(2);
                                  } else if (dBT == "A-" && pBT == "AB+") {
                                    bloodTypePercentage = 100.00;
                                    _bloodTypePercentage.text =
                                        bloodTypePercentage!.toStringAsFixed(2);
                                    //Not Compatible to A-
                                  } else if (dBT == "A-" && pBT == "B+") {
                                    bloodTypePercentage = 00.00;
                                    _bloodTypePercentage.text =
                                        bloodTypePercentage!.toStringAsFixed(2);
                                  } else if (dBT == "A-" && pBT == "B-") {
                                    bloodTypePercentage = 00.00;
                                    _bloodTypePercentage.text =
                                        bloodTypePercentage!.toStringAsFixed(2);
                                  } else if (dBT == "A-" && pBT == "O+") {
                                    bloodTypePercentage = 00.00;
                                    _bloodTypePercentage.text =
                                        bloodTypePercentage!.toStringAsFixed(2);
                                  } else if (dBT == "A-" && pBT == "O-") {
                                    bloodTypePercentage = 00.00;
                                    _bloodTypePercentage.text =
                                        bloodTypePercentage!.toStringAsFixed(2);
                                    //Compatible to B+
                                  } else if (dBT == "B+" && pBT == "B+") {
                                    //Blood Type B+
                                    bloodTypePercentage = 100.00;
                                    _bloodTypePercentage.text =
                                        bloodTypePercentage!.toStringAsFixed(2);
                                  } else if (dBT == "B+" && pBT == "B-") {
                                    bloodTypePercentage = 100.00;
                                    _bloodTypePercentage.text =
                                        bloodTypePercentage!.toStringAsFixed(2);
                                  } else if (dBT == "B+" && pBT == "AB-") {
                                    bloodTypePercentage = 100.00;
                                    _bloodTypePercentage.text =
                                        bloodTypePercentage!.toStringAsFixed(2);
                                  } else if (dBT == "B+" && pBT == "AB+") {
                                    bloodTypePercentage = 100.00;
                                    _bloodTypePercentage.text =
                                        bloodTypePercentage!.toStringAsFixed(2);
                                    //Not Compatible to B+
                                  } else if (dBT == "B+" && pBT == "A+") {
                                    bloodTypePercentage = 00.00;
                                    _bloodTypePercentage.text =
                                        bloodTypePercentage!.toStringAsFixed(2);
                                  } else if (dBT == "B+" && pBT == "A-") {
                                    bloodTypePercentage = 00.00;
                                    _bloodTypePercentage.text =
                                        bloodTypePercentage!.toStringAsFixed(2);
                                  } else if (dBT == "B+" && pBT == "O+") {
                                    bloodTypePercentage = 00.00;
                                    _bloodTypePercentage.text =
                                        bloodTypePercentage!.toStringAsFixed(2);
                                  } else if (dBT == "B+" && pBT == "O-") {
                                    bloodTypePercentage = 00.00;
                                    _bloodTypePercentage.text =
                                        bloodTypePercentage!.toStringAsFixed(2);
                                    //Compatible to B-
                                  } else if (dBT == "B-" && pBT == "B+") {
                                    //Blood Type B+
                                    bloodTypePercentage = 100.00;
                                    _bloodTypePercentage.text =
                                        bloodTypePercentage!.toStringAsFixed(2);
                                  } else if (dBT == "B-" && pBT == "B-") {
                                    bloodTypePercentage = 100.00;
                                    _bloodTypePercentage.text =
                                        bloodTypePercentage!.toStringAsFixed(2);
                                  } else if (dBT == "B-" && pBT == "AB-") {
                                    bloodTypePercentage = 100.00;
                                    _bloodTypePercentage.text =
                                        bloodTypePercentage!.toStringAsFixed(2);
                                  } else if (dBT == "B-" && pBT == "AB+") {
                                    bloodTypePercentage = 100.00;
                                    _bloodTypePercentage.text =
                                        bloodTypePercentage!.toStringAsFixed(2);
                                    //Not Compatible to B-
                                  } else if (dBT == "B-" && pBT == "A+") {
                                    bloodTypePercentage = 00.00;
                                    _bloodTypePercentage.text =
                                        bloodTypePercentage!.toStringAsFixed(2);
                                  } else if (dBT == "B-" && pBT == "A-") {
                                    bloodTypePercentage = 00.00;
                                    _bloodTypePercentage.text =
                                        bloodTypePercentage!.toStringAsFixed(2);
                                  } else if (dBT == "B-" && pBT == "O+") {
                                    bloodTypePercentage = 00.00;
                                    _bloodTypePercentage.text =
                                        bloodTypePercentage!.toStringAsFixed(2);
                                  } else if (dBT == "B-" && pBT == "O-") {
                                    bloodTypePercentage = 00.00;
                                    _bloodTypePercentage.text =
                                        bloodTypePercentage!.toStringAsFixed(2);
                                    //Compatible to AB+
                                  } else if (dBT == "AB+" && pBT == "AB+") {
                                    //Blood Type AB
                                    bloodTypePercentage = 100.00;
                                    _bloodTypePercentage.text =
                                        bloodTypePercentage!.toStringAsFixed(2);
                                  } else if (dBT == "AB+" && pBT == "AB-") {
                                    bloodTypePercentage = 100.00;
                                    _bloodTypePercentage.text =
                                        bloodTypePercentage!.toStringAsFixed(2);
                                    //Not Compatible to AB
                                  } else if (dBT == "AB+" && pBT == "B-") {
                                    bloodTypePercentage = 00.00;
                                    _bloodTypePercentage.text =
                                        bloodTypePercentage!.toStringAsFixed(2);
                                  } else if (dBT == "AB+" && pBT == "B+") {
                                    bloodTypePercentage = 00.00;
                                    _bloodTypePercentage.text =
                                        bloodTypePercentage!.toStringAsFixed(2);
                                  } else if (dBT == "AB+" && pBT == "A+") {
                                    bloodTypePercentage = 00.00;
                                    _bloodTypePercentage.text =
                                        bloodTypePercentage!.toStringAsFixed(2);
                                  } else if (dBT == "AB+" && pBT == "A-") {
                                    bloodTypePercentage = 00.00;
                                    _bloodTypePercentage.text =
                                        bloodTypePercentage!.toStringAsFixed(2);
                                  } else if (dBT == "AB+" && pBT == "O+") {
                                    bloodTypePercentage = 00.00;
                                    _bloodTypePercentage.text =
                                        bloodTypePercentage!.toStringAsFixed(2);
                                  } else if (dBT == "AB+" && pBT == "O-") {
                                    bloodTypePercentage = 00.00;
                                    _bloodTypePercentage.text =
                                        bloodTypePercentage!.toStringAsFixed(2);
                                    //Compatible to Blood Type AB-
                                  } else if (dBT == "AB-" && pBT == "AB+") {
                                    //Blood Type AB
                                    bloodTypePercentage = 100.00;
                                    _bloodTypePercentage.text =
                                        bloodTypePercentage!.toStringAsFixed(2);
                                  } else if (dBT == "AB-" && pBT == "AB-") {
                                    bloodTypePercentage = 100.00;
                                    _bloodTypePercentage.text =
                                        bloodTypePercentage!.toStringAsFixed(2);
                                    //Not Compatible to AB
                                  } else if (dBT == "AB-" && pBT == "B-") {
                                    bloodTypePercentage = 00.00;
                                    _bloodTypePercentage.text =
                                        bloodTypePercentage!.toStringAsFixed(2);
                                  } else if (dBT == "AB-" && pBT == "B+") {
                                    bloodTypePercentage = 00.00;
                                    _bloodTypePercentage.text =
                                        bloodTypePercentage!.toStringAsFixed(2);
                                  } else if (dBT == "AB-" && pBT == "A+") {
                                    bloodTypePercentage = 00.00;
                                    _bloodTypePercentage.text =
                                        bloodTypePercentage!.toStringAsFixed(2);
                                  } else if (dBT == "AB-" && pBT == "A-") {
                                    bloodTypePercentage = 00.00;
                                    _bloodTypePercentage.text =
                                        bloodTypePercentage!.toStringAsFixed(2);
                                  } else if (dBT == "AB-" && pBT == "O+") {
                                    bloodTypePercentage = 00.00;
                                    _bloodTypePercentage.text =
                                        bloodTypePercentage!.toStringAsFixed(2);
                                  } else if (dBT == "AB-" && pBT == "O-") {
                                    bloodTypePercentage = 00.00;
                                    _bloodTypePercentage.text =
                                        bloodTypePercentage!.toStringAsFixed(2);
                                    //Compatible to Blood Type O+
                                  } else if (dBT == "O+" && pBT == "A+") {
                                    //Blood Type O+
                                    bloodTypePercentage = 100.00;
                                    _bloodTypePercentage.text =
                                        bloodTypePercentage!.toStringAsFixed(2);
                                  } else if (dBT == "O+" && pBT == "A-") {
                                    bloodTypePercentage = 100.00;
                                    _bloodTypePercentage.text =
                                        bloodTypePercentage!.toStringAsFixed(2);
                                  } else if (dBT == "O+" && pBT == "B+") {
                                    bloodTypePercentage = 100.00;
                                    _bloodTypePercentage.text =
                                        bloodTypePercentage!.toStringAsFixed(2);
                                  } else if (dBT == "O+" && pBT == "B-") {
                                    bloodTypePercentage = 100.00;
                                    _bloodTypePercentage.text =
                                        bloodTypePercentage!.toStringAsFixed(2);
                                  } else if (dBT == "O+" && pBT == "AB+") {
                                    bloodTypePercentage = 100.00;
                                    _bloodTypePercentage.text =
                                        bloodTypePercentage!.toStringAsFixed(2);
                                  } else if (dBT == "O+" && pBT == "AB-") {
                                    bloodTypePercentage = 100.00;
                                    _bloodTypePercentage.text =
                                        bloodTypePercentage!.toStringAsFixed(2);
                                  } else if (dBT == "O+" && pBT == "O+") {
                                    bloodTypePercentage = 100.00;
                                    _bloodTypePercentage.text =
                                        bloodTypePercentage!.toStringAsFixed(2);
                                  } else if (dBT == "O+" && pBT == "O-") {
                                    bloodTypePercentage = 100.00;
                                    _bloodTypePercentage.text =
                                        bloodTypePercentage!.toStringAsFixed(2);
                                    //Blood type O-
                                  } else if (dBT == "O-" && pBT == "A+") {
                                    //Blood Type O-
                                    bloodTypePercentage = 100.00;
                                    _bloodTypePercentage.text =
                                        bloodTypePercentage!.toStringAsFixed(2);
                                  } else if (dBT == "O-" && pBT == "A-") {
                                    bloodTypePercentage = 100.00;
                                    _bloodTypePercentage.text =
                                        bloodTypePercentage!.toStringAsFixed(2);
                                  } else if (dBT == "O-" && pBT == "B+") {
                                    bloodTypePercentage = 100.00;
                                    _bloodTypePercentage.text =
                                        bloodTypePercentage!.toStringAsFixed(2);
                                  } else if (dBT == "O-" && pBT == "B-") {
                                    bloodTypePercentage = 100.00;
                                    _bloodTypePercentage.text =
                                        bloodTypePercentage!.toStringAsFixed(2);
                                  } else if (dBT == "O-" && pBT == "AB+") {
                                    bloodTypePercentage = 100.00;
                                    _bloodTypePercentage.text =
                                        bloodTypePercentage!.toStringAsFixed(2);
                                  } else if (dBT == "O-" && pBT == "AB-") {
                                    bloodTypePercentage = 100.00;
                                    _bloodTypePercentage.text =
                                        bloodTypePercentage!.toStringAsFixed(2);
                                  } else if (dBT == "O-" && pBT == "O+") {
                                    bloodTypePercentage = 100.00;
                                    _bloodTypePercentage.text =
                                        bloodTypePercentage!.toStringAsFixed(2);
                                  } else if (dBT == "O-" && pBT == "O-") {
                                    bloodTypePercentage = 100.00;
                                    _bloodTypePercentage.text =
                                        bloodTypePercentage!.toStringAsFixed(2);
                                  }

                                  //Kidney Size
                                  double dKS =
                                      double.parse(_donorKidneySize.text);
                                  double pKS =
                                      double.parse(_patientKidneySize.text);

                                  if (dKS > pKS) {
                                    kidneySizePercentage = (pKS / dKS) * 100;
                                    _kidneySizePercentage.text =
                                        kidneySizePercentage!
                                            .toStringAsFixed(2);
                                  } else {
                                    kidneySizePercentage = (dKS / pKS) * 100;
                                    _kidneySizePercentage.text =
                                        kidneySizePercentage!
                                            .toStringAsFixed(2);
                                  }

                                  //Match Percent

                                  double ageP =
                                      double.parse(_agePercentage.text);
                                  double bloodTypeP =
                                      double.parse(_bloodTypePercentage.text);
                                  double kidneySizeP =
                                      double.parse(_kidneySizePercentage.text);

                                  if (_bloodTypePercentage.text == "0.00") {
                                    matchResultPercentage =
                                        (ageP + bloodTypeP + kidneySizeP) / 3;
                                    _matchResultPercentage.text =
                                        matchResultPercentage!
                                                .toStringAsFixed(2) +
                                            " %";
                                    _matchResultPercentageMessage
                                        .text = _donorUserName
                                            .text +
                                        " blood type is not compatible with " +
                                        _patientUserName.text +
                                        "'s" +
                                        " blood type. Please find other patient.";
                                  } else {
                                    matchResultPercentage =
                                        (ageP + bloodTypeP + kidneySizeP) / 3;
                                    _matchResultPercentage.text =
                                        matchResultPercentage!
                                                .toStringAsFixed(2) +
                                            " %";
                                    _matchResultPercentageMessage.text =
                                        "The results shows " +
                                            _donorUserName.text +
                                            " has " +
                                            _matchResultPercentage.text +
                                            " % kidney match rate with " +
                                            _patientUserName.text +
                                            " and does not have any incompatibility.";
                                    setState(() {
                                      isButtonEnabled = true;
                                      Navigator.of(context).pop();
                                      showMatchCustomDialog(context);
                                    });
                                  }
                                  showMatchToast();
                                  logAdminFindMatch();
                                },
                                child: Text('Calculate',
                                    style: GoogleFonts.roboto(
                                        color: Colors.red[400],
                                        fontSize: 18,
                                        fontWeight: FontWeight.w700,
                                        letterSpacing: 2)),
                              ),
                            ),
                            //Divider/space
                            Container(
                              width: 50,
                            ),
                            //Post Button
                            Container(
                              margin: EdgeInsets.only(top: 150),
                              height: 50,
                              width: 150,
                              child: ElevatedButton(
                                style: ElevatedButton.styleFrom(
                                  primary: Colors.red[50],
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(30),
                                  ),
                                ),
                                onPressed: isButtonEnabled
                                    ? () {
                                        if (_donorUserType.text ==
                                            "I am a Donor") {
                                          postMatch();
                                          postMatchSpecificDonor();
                                          postMatchSpecificPatient();
                                          postMatchSpecificAllDonor();
                                          postMatchSpecificAllPatient();
                                        } else if (_donorUserType.text ==
                                            "Donor") {
                                          postMatch();
                                          postMatchSpecificPatient();
                                          postMatchSpecificAllPatient();
                                        }
                                        showPostResultToast();
                                        logAdminPostedResults();
                                        setState(() {
                                          Navigator.of(context).pop();
                                          isButtonEnabled = false;
                                          //Donor clear Fields
                                          _donorFullName.text = "";
                                          _donorUserName.text = "";
                                          _donorGender.text = "";
                                          _donorAge.text = "";
                                          _donorBloodType.text = "";
                                          _donorKidneySize.text = "";
                                          //Patient clear Fields
                                          _patientFullName.text = "";
                                          _patientUserName.text = "";
                                          _patientGender.text = "";
                                          _patientAge.text = "";
                                          _patientBloodType.text = "";
                                          _patientKidneySize.text = "";

                                          _agePercentage.text = "";
                                          _bloodTypePercentage.text = "";
                                          _kidneySizePercentage.text = "";
                                          _matchResultPercentage.text = "";
                                        });
                                      }
                                    : null,
                                child: Text('Post Results',
                                    style: GoogleFonts.roboto(
                                        color: Colors.red[400],
                                        fontSize: 18,
                                        fontWeight: FontWeight.w700,
                                        letterSpacing: 2)),
                              ),
                            ),
                          ],
                        ),

                        //Match Percentage
                        Container(
                          margin: EdgeInsets.only(top: 100),
                          child: Text(
                            'Match Rate',
                            style: GoogleFonts.roboto(
                                color: Colors.black87,
                                fontWeight: FontWeight.w600,
                                fontSize: 16,
                                letterSpacing: 2),
                          ),
                        ),
                        Container(
                          width: 100,
                          child: TextFormField(
                            textAlign: TextAlign.center,
                            decoration: InputDecoration(
                              filled: true,
                              border: OutlineInputBorder(
                                borderSide: BorderSide.none,
                              ),
                            ),
                            style: GoogleFonts.roboto(
                                color: Colors.red[400],
                                fontWeight: FontWeight.w600,
                                fontSize: 16),
                            controller: _matchResultPercentage
                              ..text = _matchResultPercentage.text,
                          ),
                        ),

                        //Match Message
                        Container(
                          margin: EdgeInsets.only(top: 20),
                          child: Text(
                            'Message',
                            style: GoogleFonts.roboto(
                                color: Colors.black87,
                                fontWeight: FontWeight.w600,
                                fontSize: 16,
                                letterSpacing: 2),
                          ),
                        ),
                        Container(
                          width: 300,
                          child: TextFormField(
                            textAlign: TextAlign.center,
                            decoration: InputDecoration(
                              filled: true,
                              border: OutlineInputBorder(
                                borderSide: BorderSide.none,
                              ),
                            ),
                            style: GoogleFonts.roboto(
                                color: Colors.red[400],
                                fontWeight: FontWeight.w600,
                                fontSize: 16),
                            controller: _matchResultPercentageMessage
                              ..text = _matchResultPercentageMessage.text,
                            maxLines: 3,
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(top: 20),
                          child: Text(
                            'Summary of Results:'.toUpperCase(),
                            style: GoogleFonts.roboto(
                                color: Colors.black87,
                                fontWeight: FontWeight.w600,
                                fontSize: 16,
                                letterSpacing: 2),
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(top: 20),
                          child: Text(
                            'Age % Match',
                            style: GoogleFonts.roboto(
                                color: Colors.black87,
                                fontWeight: FontWeight.w600,
                                fontSize: 16,
                                letterSpacing: 2),
                          ),
                        ),
                        //Kidney Age Percentage
                        Container(
                          width: 100,
                          child: TextFormField(
                            textAlign: TextAlign.center,
                            decoration: InputDecoration(
                              filled: true,
                              border: OutlineInputBorder(
                                borderSide: BorderSide.none,
                              ),
                            ),
                            style: GoogleFonts.roboto(
                                color: Colors.red[400],
                                fontWeight: FontWeight.w600,
                                fontSize: 16),
                            controller: _agePercentage
                              ..text = _agePercentage.text,
                          ),
                        ),

                        Container(
                          margin: EdgeInsets.only(top: 10),
                          child: Text(
                            'Blood Type % Match',
                            style: GoogleFonts.roboto(
                                color: Colors.black87,
                                fontWeight: FontWeight.w600,
                                fontSize: 16,
                                letterSpacing: 2),
                          ),
                        ),
                        //Blood Type Percentage
                        Container(
                          width: 100,
                          child: TextFormField(
                            textAlign: TextAlign.center,
                            decoration: InputDecoration(
                              filled: true,
                              border: OutlineInputBorder(
                                borderSide: BorderSide.none,
                              ),
                            ),
                            style: GoogleFonts.roboto(
                                color: Colors.red[400],
                                fontWeight: FontWeight.w600,
                                fontSize: 16),
                            controller: _bloodTypePercentage
                              ..text = _bloodTypePercentage.text,
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(top: 10),
                          child: Text(
                            'Kidney Size % Match',
                            style: GoogleFonts.roboto(
                                color: Colors.black87,
                                fontWeight: FontWeight.w600,
                                fontSize: 16,
                                letterSpacing: 2),
                          ),
                        ),
                        //Kidney Size Percentage
                        Container(
                          width: 200,
                          child: TextFormField(
                            textAlign: TextAlign.center,
                            decoration: InputDecoration(
                              filled: true,
                              border: OutlineInputBorder(
                                borderSide: BorderSide.none,
                              ),
                            ),
                            style: GoogleFonts.roboto(
                                color: Colors.red[400],
                                fontWeight: FontWeight.w600,
                                fontSize: 16),
                            controller: _kidneySizePercentage
                              ..text = _kidneySizePercentage.text,
                          ),
                        ),
                      ],
                    ),

                    //Patient Match Card
                    Container(
                      margin: EdgeInsets.only(top: 150, right: 500),
                      width: 200,
                      height: 500,
                      decoration: BoxDecoration(
                          color: Colors.red[100],
                          borderRadius: BorderRadius.circular(10)),
                      child: Column(
                        children: [
                          Container(
                            margin: EdgeInsets.only(top: 20),
                            child: Image.asset(
                              'assets/user.png',
                              height: 50,
                            ),
                          ),
                          Container(
                            width: 120,
                            child: Column(
                              children: [
                                TextFormField(
                                  controller: _patientFullName
                                    ..text = _patientFullName.text,
                                  style: GoogleFonts.roboto(
                                      color: Colors.red[400],
                                      fontWeight: FontWeight.w600,
                                      fontSize: 14),
                                  key: Key(
                                    getDonorFullName.toString(),
                                  ),
                                ),
                                Text(
                                  'Full Name: ',
                                  style: GoogleFonts.roboto(
                                      color: Colors.grey[600],
                                      fontWeight: FontWeight.bold,
                                      fontSize: 14),
                                ),
                              ],
                            ),
                          ),
                          Container(
                            width: 120,
                            child: Column(
                              children: [
                                TextFormField(
                                  controller: _patientUserName
                                    ..text = _patientUserName.text,
                                  style: GoogleFonts.roboto(
                                      color: Colors.red[400],
                                      fontWeight: FontWeight.w600,
                                      fontSize: 14),
                                  key: Key(
                                    getPatientUserName.toString(),
                                  ),
                                ),
                                Text(
                                  'User Name: ',
                                  style: GoogleFonts.roboto(
                                      color: Colors.grey[600],
                                      fontWeight: FontWeight.bold,
                                      fontSize: 14),
                                ),
                              ],
                            ),
                          ),
                          Container(
                            width: 120,
                            child: Column(
                              children: [
                                TextFormField(
                                  controller: _patientGender
                                    ..text = _patientGender.text,
                                  style: GoogleFonts.roboto(
                                      color: Colors.red[400],
                                      fontWeight: FontWeight.w600,
                                      fontSize: 14),
                                  key: Key(
                                    getPatientGender.toString(),
                                  ),
                                ),
                                Text(
                                  'Gender: ',
                                  style: GoogleFonts.roboto(
                                      color: Colors.grey[600],
                                      fontWeight: FontWeight.bold,
                                      fontSize: 14),
                                ),
                              ],
                            ),
                          ),
                          Container(
                            width: 120,
                            child: Column(
                              children: [
                                TextFormField(
                                  controller: _patientAge
                                    ..text = _patientAge.text,
                                  style: GoogleFonts.roboto(
                                      color: Colors.red[400],
                                      fontWeight: FontWeight.w600,
                                      fontSize: 14),
                                  key: Key(
                                    getPatientAge.toString(),
                                  ),
                                ),
                                Text(
                                  'Age: ',
                                  style: GoogleFonts.roboto(
                                      color: Colors.grey[600],
                                      fontWeight: FontWeight.bold,
                                      fontSize: 14),
                                ),
                              ],
                            ),
                          ),
                          Container(
                            width: 120,
                            child: Column(
                              children: [
                                TextFormField(
                                  controller: _patientBloodType
                                    ..text = _patientBloodType.text,
                                  style: GoogleFonts.roboto(
                                      color: Colors.red[400],
                                      fontWeight: FontWeight.w600,
                                      fontSize: 14),
                                  key: Key(
                                    getPatientBloodType.toString(),
                                  ),
                                ),
                                Text(
                                  'Blood Type: ',
                                  style: GoogleFonts.roboto(
                                      color: Colors.grey[600],
                                      fontWeight: FontWeight.bold,
                                      fontSize: 14),
                                ),
                              ],
                            ),
                          ),
                          Container(
                            width: 120,
                            child: Column(
                              children: [
                                TextFormField(
                                  controller: _patientKidneySize
                                    ..text = _patientKidneySize.text,
                                  style: GoogleFonts.roboto(
                                      color: Colors.red[400],
                                      fontWeight: FontWeight.w600,
                                      fontSize: 14),
                                  key: Key(
                                    getPatientKidneySize.toString(),
                                  ),
                                ),
                                Text(
                                  'Kidney Size: ',
                                  style: GoogleFonts.roboto(
                                      color: Colors.grey[600],
                                      fontWeight: FontWeight.bold,
                                      fontSize: 14),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        );
      });

  //Alert Dialog For Delete
  void showCustomDialog(BuildContext context) => showDialog(
      context: context,
      builder: (BuildContext context) {
        return Dialog(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20.0)), //this right here
          child: Container(
            height: 200,
            width: 100,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  "Delete Match",
                  style: GoogleFonts.roboto(
                      color: Colors.black87,
                      fontWeight: FontWeight.bold,
                      fontSize: 18),
                ),
                Text(
                  "\nAre you sure you want to delete?",
                  style:
                      GoogleFonts.roboto(color: Colors.grey[500], fontSize: 14),
                ),
                Container(
                  margin: EdgeInsets.only(top: 40),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Container(
                        child: InkWell(
                          onTap: () {
                            Navigator.of(context).pop();
                          },
                          child: Container(
                            height: 35,
                            width: 100,
                            decoration: BoxDecoration(
                                color: Colors.red[50],
                                borderRadius: BorderRadius.circular(18)),
                            child: Center(
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Text(
                                  "Cancel",
                                  style: TextStyle(
                                      color: Colors.grey[600], fontSize: 17),
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                      Container(
                        child: InkWell(
                          onTap: () {
                            setState(() {
                              deleteMatch();
                              deleteSpecificDonorMatch();
                              deleteAllDonorMatch();
                              deleteSpecificPatientMatch();
                              deleteAllPatientMatch();
                              showDeleteToast();
                              Navigator.of(context).pop();
                            });
                          },
                          child: Container(
                            height: 35,
                            width: 100,
                            decoration: BoxDecoration(
                                color: Colors.red[200],
                                borderRadius: BorderRadius.circular(18)),
                            child: Center(
                              child: Text(
                                "Confirm",
                                style: TextStyle(
                                    color: Colors.red[400], fontSize: 17),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        );
      });

  //Show Delete Succes
  void showDeleteToast() =>
      toast.showToast(child: buildDeleteToast(), gravity: ToastGravity.CENTER);

  Widget buildDeleteToast() => Container(
        padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
        decoration: BoxDecoration(
          color: Colors.green[300],
          borderRadius: BorderRadius.circular(5),
        ),
        child: Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            Text(
              "Match removed!",
              style: TextStyle(color: Colors.black87, fontSize: 16),
            ),
          ],
        ),
      );

  //Show Match Success
  void showMatchToast() =>
      toast.showToast(child: buildMatchToast(), gravity: ToastGravity.CENTER);

  Widget buildMatchToast() => Container(
        padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
        decoration: BoxDecoration(
          color: Colors.green[300],
          borderRadius: BorderRadius.circular(5),
        ),
        child: Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            Text(
              "Success",
              style: TextStyle(color: Colors.black87, fontSize: 16),
            ),
          ],
        ),
      );

  //Show Post Result Success
  void showPostResultToast() => toast.showToast(
      child: buildPostResultToast(), gravity: ToastGravity.CENTER);

  Widget buildPostResultToast() => Container(
        padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
        decoration: BoxDecoration(
          color: Colors.green[300],
          borderRadius: BorderRadius.circular(5),
        ),
        child: Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            Text(
              "Results Posted",
              style: TextStyle(color: Colors.black87, fontSize: 16),
            ),
          ],
        ),
      );
  //Display Donor Registered Table
  getDonorRegisteredData() async {
    final firestore = FirebaseFirestore.instance;
    QuerySnapshot user = await firestore
        .collection('Users')
        .doc('Donor')
        .collection('Lists')
        .get();
    return user.docs;
  }

  getDonorTaggedData() async {
    final firestore = FirebaseFirestore.instance;
    QuerySnapshot user = await firestore
        .collection('Tagged Users')
        .doc('Donor')
        .collection('Lists')
        .get();
    return user.docs;
  }

  getPatientRegisteredData() async {
    final firestore = FirebaseFirestore.instance;
    QuerySnapshot user = await firestore
        .collection('Users')
        .doc('Patient')
        .collection('Lists')
        .get();
    return user.docs;
  }

  getPatientTaggedData() async {
    final firestore = FirebaseFirestore.instance;
    QuerySnapshot user = await firestore
        .collection('Tagged Users')
        .doc('Patient')
        .collection('Lists')
        .get();
    return user.docs;
  }

  postMatch() async {
    String? uid = _donorUserName.text + " and " + _patientUserName.text;
    String formattedTime = DateFormat('kk:mm:s:a').format(now);
    String formattedDate = formatter.format(now);
    await FirebaseFirestore.instance
        .collection("Kidney Match")
        .doc('All')
        .collection('Lists')
        .doc(uid)
        .set({
      "Donor": _donorUserName.text,
      "Donor UserID": _donorUserID.text,
      "Patient": _patientUserName.text,
      "Patient UserID": _patientUserID.text,
      "Match Rate": _matchResultPercentage.text,
      "Date Matched": formattedDate,
      "Time Matched": formattedTime,
      "Age Percentage": _agePercentage.text,
      "Blood Type Percentage": _bloodTypePercentage.text,
      "Kidney Size Percentage": _kidneySizePercentage.text,
    });
  }

  postMatchSpecificDonor() async {
    String? uid = _donorUserID.text;
    String? pid = _patientUserID.text;
    String formattedTime = DateFormat('kk:mm:s:a').format(now);
    String formattedDate = formatter.format(now);
    await FirebaseFirestore.instance
        .collection("Users")
        .doc('Donor')
        .collection('Lists')
        .doc(uid)
        .collection('Matches')
        .doc(pid)
        .set({
      "User Name": _patientUserName.text,
      "Match Rate": _matchResultPercentage.text,
      "Date Matched": formattedDate,
      "Time Matched": formattedTime,
      "Age Percentage": _agePercentage.text,
      "Blood Type Percentage": _bloodTypePercentage.text,
      "Kidney Size Percentage": _kidneySizePercentage.text,
    });
  }

  postMatchSpecificPatient() async {
    String? uid = _patientUserID.text;
    String? pid = _donorUserID.text;
    String formattedTime = DateFormat('kk:mm:s:a').format(now);
    String formattedDate = formatter.format(now);
    await FirebaseFirestore.instance
        .collection("Users")
        .doc('Patient')
        .collection('Lists')
        .doc(uid)
        .collection('Matches')
        .doc(pid)
        .set({
      "User Name": _donorUserName.text,
      "Match Rate": _matchResultPercentage.text,
      "Date Matched": formattedDate,
      "Time Matched": formattedTime,
      "Age Percentage": _agePercentage.text,
      "Blood Type Percentage": _bloodTypePercentage.text,
      "Kidney Size Percentage": _kidneySizePercentage.text,
    });
  }

  postMatchSpecificAllDonor() async {
    String? uid = _donorUserID.text;
    String? pid = _patientUserID.text;
    String formattedTime = DateFormat('kk:mm:s:a').format(now);
    String formattedDate = formatter.format(now);
    await FirebaseFirestore.instance
        .collection("Users")
        .doc('All')
        .collection('Lists')
        .doc(uid)
        .collection('Matches')
        .doc(pid)
        .set({
      "User Name": _patientUserName.text,
      "Match Rate": _matchResultPercentage.text,
      "Date Matched": formattedDate,
      "Time Matched": formattedTime,
      "Age Percentage": _agePercentage.text,
      "Blood Type Percentage": _bloodTypePercentage.text,
      "Kidney Size Percentage": _kidneySizePercentage.text,
    });
  }

  postMatchSpecificAllPatient() async {
    String? uid = _patientUserID.text;
    String? pid = _donorUserID.text;
    String formattedTime = DateFormat('kk:mm:s:a').format(now);
    String formattedDate = formatter.format(now);
    await FirebaseFirestore.instance
        .collection("Users")
        .doc('All')
        .collection('Lists')
        .doc(uid)
        .collection('Matches')
        .doc(pid)
        .set({
      "User Name": _donorUserName.text,
      "Match Rate": _matchResultPercentage.text,
      "Date Matched": formattedDate,
      "Time Matched": formattedTime,
      "Age Percentage": _agePercentage.text,
      "Blood Type Percentage": _bloodTypePercentage.text,
      "Kidney Size Percentage": _kidneySizePercentage.text,
    });
  }

  //Display All Matches Made
  getAllMatchData() async {
    final firestore = FirebaseFirestore.instance;
    QuerySnapshot user = await firestore
        .collection('Kidney Match')
        .doc('All')
        .collection('Lists')
        .get();
    return user.docs;
  }

  deleteMatch() async {
    String? donorUserName = getDUserName;
    String? patientUserName = getPUserName;
    await FirebaseFirestore.instance
        .collection("Kidney Match")
        .doc("All")
        .collection("Lists")
        .doc(donorUserName! + " and " + patientUserName!)
        .delete();
  }

  deleteSpecificDonorMatch() async {
    String? donorUserID = getDonorID;
    String? patientUserID = getPatientID;
    await FirebaseFirestore.instance
        .collection("Users")
        .doc("Donor")
        .collection("Lists")
        .doc(donorUserID)
        .collection("Matches")
        .doc(patientUserID)
        .delete();
  }

  deleteSpecificPatientMatch() async {
    String? patientUserID = getPatientID;
    String? donorUserID = getDonorID;
    await FirebaseFirestore.instance
        .collection("Users")
        .doc("Patient")
        .collection("Lists")
        .doc(patientUserID)
        .collection("Matches")
        .doc(donorUserID)
        .delete();
  }

  deleteAllDonorMatch() async {
    String? donorUserID = getDonorID;
    String? patientUserID = getPatientID;
    await FirebaseFirestore.instance
        .collection("Users")
        .doc("All")
        .collection("Lists")
        .doc(donorUserID)
        .collection("Matches")
        .doc(patientUserID)
        .delete();
  }

  deleteAllPatientMatch() async {
    String? patientUserID = getPatientID;
    String? donorUserID = getDonorID;
    await FirebaseFirestore.instance
        .collection("Users")
        .doc("All")
        .collection("Lists")
        .doc(patientUserID)
        .collection("Matches")
        .doc(donorUserID)
        .delete();
  }

  logAdminFindMatch() async {
    String formattedTime = DateFormat('kk:mm:s:a').format(now);
    String formattedDate = formatter.format(now);
    await FirebaseFirestore.instance.collection("Logs").add({
      'Message': "Admin Match Success!",
      'User': "Official Admin",
      'Logged Date': formattedDate,
      'Logged Time': formattedTime,
    });
  }

  logAdminPostedResults() async {
    String formattedTime = DateFormat('kk:mm:s:a').format(now);
    String formattedDate = formatter.format(now);
    await FirebaseFirestore.instance.collection("Logs").add({
      'Message': "Admin Posted the Results",
      'User': "Official Admin",
      'Logged Date': formattedDate,
      'Logged Time': formattedTime,
    });
  }
}
