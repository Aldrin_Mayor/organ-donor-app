import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:organ_donor_app/pages/AboutPage.dart';

class AccountPrivacyPolicy extends StatefulWidget {
  AccountPrivacyPolicy({Key? key}) : super(key: key);

  @override
  _AccountPrivacyPolicyState createState() => _AccountPrivacyPolicyState();
}

class _AccountPrivacyPolicyState extends State<AccountPrivacyPolicy> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xffe5eef8),
      body: Stack(
        children: [
          //Middle Content
          SingleChildScrollView(
            child: Container(
              margin: EdgeInsets.only(top: 90, bottom: 40),
              child: Center(
                child: Column(
                  children: [
                    Container(
                      margin: EdgeInsets.only(
                        left: 20,
                        right: 20,
                      ),
                      child: SelectableText.rich(
                        TextSpan(
                          children: [
                            TextSpan(
                              text:
                                  "This Privacy Policy describes Our policies and procedures on the collection, use and disclosure of Your information when You use the Service and tells You about Your privacy rights and how the law protects You.",
                              style: GoogleFonts.roboto(
                                color: Colors.black87,
                                fontSize: 14,
                                height: 1.7,
                              ),
                            ),
                            TextSpan(
                              children: [
                                TextSpan(
                                  text:
                                      "\n\nWe use Your Personal data to provide and improve the Service. By using the Service, you agree to the collection and use of information in accordance with this Privacy Policy. ",
                                  style: GoogleFonts.roboto(
                                      color: Colors.black87,
                                      fontSize: 14,
                                      height: 1.7),
                                ),
                              ],
                            ),
                            TextSpan(
                              text: "\n\n\nInterpretation and Definitions",
                              style: GoogleFonts.roboto(
                                color: Colors.black87,
                                fontSize: 14,
                                height: 1.7,
                                fontWeight: FontWeight.w700,
                              ),
                            ),
                            TextSpan(
                              text: "\nInterpretation",
                              style: GoogleFonts.roboto(
                                color: Colors.black87,
                                fontSize: 14,
                                height: 1.7,
                                fontWeight: FontWeight.w700,
                              ),
                            ),
                            TextSpan(
                              children: [
                                TextSpan(
                                  text:
                                      "\n\nThe words of which the initial letter is capitalized have meanings defined under the following conditions. The following definitions shall have the same meaning regardless of whether they appear in singular or in plural.",
                                  style: GoogleFonts.roboto(
                                      color: Colors.black87,
                                      fontSize: 14,
                                      height: 1.7),
                                ),
                              ],
                            ),
                            TextSpan(
                              text: "\n\nDefinitions",
                              style: GoogleFonts.roboto(
                                color: Colors.black87,
                                fontSize: 14,
                                height: 1.7,
                                fontWeight: FontWeight.w700,
                              ),
                            ),
                            TextSpan(
                              text: "\nPurposes of this Privacy Policy",
                              style: GoogleFonts.roboto(
                                color: Colors.black87,
                                fontSize: 14,
                                height: 1.7,
                                fontWeight: FontWeight.w700,
                              ),
                            ),
                            TextSpan(
                              children: [
                                TextSpan(
                                  text:
                                      "\n\n● Account means a unique account created for You to access our Service or parts of our Service.",
                                  style: GoogleFonts.roboto(
                                      color: Colors.black87,
                                      fontSize: 14,
                                      height: 1.7),
                                ),
                              ],
                            ),
                            TextSpan(
                              children: [
                                TextSpan(
                                  text:
                                      '\n\n●	Affiliate means an entity that controls, is controlled by or is under common control with a party, where "control" means ownership of 50% or more of the shares, equity interest or other securities entitled to vote for election of directors or other managing authority.',
                                  style: GoogleFonts.roboto(
                                      color: Colors.black87,
                                      fontSize: 14,
                                      height: 1.7),
                                ),
                              ],
                            ),
                            TextSpan(
                              children: [
                                TextSpan(
                                  text:
                                      "\n\n●	Application means the software program provided by the Company downloaded by You on any electronic device, named Organ Donor App",
                                  style: GoogleFonts.roboto(
                                      color: Colors.black87,
                                      fontSize: 14,
                                      height: 1.7),
                                ),
                              ],
                            ),
                            TextSpan(
                              children: [
                                TextSpan(
                                  text:
                                      '\n\n●	Company (referred to as either "the Company", "We", "Us" or "Our" in this Agreement) refers to Organ Donor App.',
                                  style: GoogleFonts.roboto(
                                      color: Colors.black87,
                                      fontSize: 14,
                                      height: 1.7),
                                ),
                              ],
                            ),
                            TextSpan(
                              children: [
                                TextSpan(
                                  text: "\n\n●	Country refers to: Philippines",
                                  style: GoogleFonts.roboto(
                                      color: Colors.black87,
                                      fontSize: 14,
                                      height: 1.7),
                                ),
                              ],
                            ),
                            TextSpan(
                              children: [
                                TextSpan(
                                  text:
                                      "\n\n●	Device means any device that can access the Service such as a computer, a cellphone or a digital tablet.",
                                  style: GoogleFonts.roboto(
                                      color: Colors.black87,
                                      fontSize: 14,
                                      height: 1.7),
                                ),
                              ],
                            ),
                            TextSpan(
                              children: [
                                TextSpan(
                                  text:
                                      "\n\n●	Personal Data is any information that relates to an identified or identifiable individual.",
                                  style: GoogleFonts.roboto(
                                      color: Colors.black87,
                                      fontSize: 14,
                                      height: 1.7),
                                ),
                              ],
                            ),
                            TextSpan(
                              children: [
                                TextSpan(
                                  text:
                                      "\n\n●	Service refers to the Application.",
                                  style: GoogleFonts.roboto(
                                      color: Colors.black87,
                                      fontSize: 14,
                                      height: 1.7),
                                ),
                              ],
                            ),
                            TextSpan(
                              children: [
                                TextSpan(
                                  text:
                                      "\n\n●	Service Provider means any natural or legal person who processes the data on behalf of the Company. It refers to third-party companies or individuals employed by the Company to facilitate the Service, to provide the Service on behalf of the Company, to perform services related to the Service or to assist the Company in analyzing how the Service is used.",
                                  style: GoogleFonts.roboto(
                                      color: Colors.black87,
                                      fontSize: 14,
                                      height: 1.7),
                                ),
                              ],
                            ),
                            TextSpan(
                              children: [
                                TextSpan(
                                  text:
                                      "\n\n●	Usage Data refers to data collected automatically, either generated using the Service or from the Service infrastructure itself (for example, the duration of a page visit).",
                                  style: GoogleFonts.roboto(
                                      color: Colors.black87,
                                      fontSize: 14,
                                      height: 1.7),
                                ),
                              ],
                            ),
                            TextSpan(
                              children: [
                                TextSpan(
                                  text:
                                      "\n\n●	You mean the individual accessing or using the Service, or the company, or other legal entity on behalf of which such individual is accessing or using the Service, as applicable.",
                                  style: GoogleFonts.roboto(
                                      color: Colors.black87,
                                      fontSize: 14,
                                      height: 1.7),
                                ),
                              ],
                            ),
                            TextSpan(
                              text:
                                  "\n\nCollecting and Using Your Personal Data",
                              style: GoogleFonts.roboto(
                                color: Colors.black87,
                                fontSize: 14,
                                height: 1.7,
                                fontWeight: FontWeight.w700,
                              ),
                            ),
                            TextSpan(
                              text: "\n\nTypes of Data Collected",
                              style: GoogleFonts.roboto(
                                color: Colors.black87,
                                fontSize: 14,
                                height: 1.7,
                                fontWeight: FontWeight.w700,
                              ),
                            ),
                            TextSpan(
                              text: "\n\nPersonal Data",
                              style: GoogleFonts.roboto(
                                color: Colors.black87,
                                fontSize: 14,
                                height: 1.7,
                                fontWeight: FontWeight.w700,
                              ),
                            ),
                            TextSpan(
                              children: [
                                TextSpan(
                                  text:
                                      "\n\nWhile using Our Service, we may ask You to provide Us with certain personally identifiable information that can be used to contact or identify You. Personally identifiable information may include, but is not limited to:",
                                  style: GoogleFonts.roboto(
                                      color: Colors.black87,
                                      fontSize: 14,
                                      height: 1.7),
                                ),
                              ],
                            ),
                            TextSpan(
                              children: [
                                TextSpan(
                                  text:
                                      "\n\n\t\t\t\t\t●	Full Name (Optional)\n\t\t\t\t\t●	Email (Optional)\n\t\t\t\t\t●	Mobile Number (Required)\n\t\t\t\t\t●	Age (Required)\n\t\t\t\t\t● Gender (Required)\n\t\t\t\t\t● Blood Type (Required)",
                                  style: GoogleFonts.roboto(
                                      color: Colors.black87,
                                      fontSize: 14,
                                      height: 1.7),
                                ),
                              ],
                            ),
                            TextSpan(
                              text: "\n\n\nUsage Data",
                              style: GoogleFonts.roboto(
                                color: Colors.black87,
                                fontSize: 14,
                                height: 1.7,
                                fontWeight: FontWeight.w700,
                              ),
                            ),
                            TextSpan(
                              children: [
                                TextSpan(
                                  text:
                                      "\n\nUsage Data is collected automatically when using the Service.",
                                  style: GoogleFonts.roboto(
                                      color: Colors.black87,
                                      fontSize: 14,
                                      height: 1.7),
                                ),
                              ],
                            ),
                            TextSpan(
                              children: [
                                TextSpan(
                                  text:
                                      "\n\nUsage Data may include information such as Your Device's Internet Protocol address (e.g., IP address), browser type, browser version, the pages of our Service that You visit, the time and date of Your visit, the time spent on those pages, unique device identifiers and other diagnostic data.",
                                  style: GoogleFonts.roboto(
                                      color: Colors.black87,
                                      fontSize: 14,
                                      height: 1.7),
                                ),
                              ],
                            ),
                            TextSpan(
                              children: [
                                TextSpan(
                                  text:
                                      "\n\nWhen You access the Service by or through a mobile device, we may collect certain information automatically, including, but not limited to, the type of mobile device You use, your mobile device unique ID, the IP address of Your mobile device, your mobile operating system, the type of mobile Internet browser You use, unique device identifiers and other diagnostic data.",
                                  style: GoogleFonts.roboto(
                                      color: Colors.black87,
                                      fontSize: 14,
                                      height: 1.7),
                                ),
                              ],
                            ),
                            TextSpan(
                              children: [
                                TextSpan(
                                  text:
                                      "\n\nWe may also collect information that Your browser sends whenever You visit our Service or when You access the Service by or through a mobile device.",
                                  style: GoogleFonts.roboto(
                                      color: Colors.black87,
                                      fontSize: 14,
                                      height: 1.7),
                                ),
                              ],
                            ),
                            TextSpan(
                              children: [
                                TextSpan(
                                  text:
                                      "\n\nInformation Collected while Using the Application",
                                  style: GoogleFonts.roboto(
                                      color: Colors.black87,
                                      fontSize: 14,
                                      height: 1.7),
                                ),
                              ],
                            ),
                            TextSpan(
                              children: [
                                TextSpan(
                                  text:
                                      "\n\nWhile using Our Application, to provide features of Our Application, we may collect, with Your prior permission:",
                                  style: GoogleFonts.roboto(
                                      color: Colors.black87,
                                      fontSize: 14,
                                      height: 1.7),
                                ),
                              ],
                            ),
                            TextSpan(
                              text: "\n\n\nRetention Of Your Personal Data",
                              style: GoogleFonts.roboto(
                                color: Colors.black87,
                                fontSize: 14,
                                height: 1.7,
                                fontWeight: FontWeight.w700,
                              ),
                            ),
                            TextSpan(
                              children: [
                                TextSpan(
                                  text:
                                      "\n\nOrgan Donor App will retain Your Personal Data only for as long as is necessary for the purposes set out in this Privacy Policy. We will retain and use Your Personal Data to the extent necessary to comply with our legal obligations (for example, if we are required to retain your data to comply with applicable laws), resolve disputes, and enforce our legal agreements and policies.",
                                  style: GoogleFonts.roboto(
                                      color: Colors.black87,
                                      fontSize: 14,
                                      height: 1.7),
                                ),
                              ],
                            ),
                            TextSpan(
                              children: [
                                TextSpan(
                                  text:
                                      "\n\nOrgan Donor App will also retain Usage Data for internal analysis purposes. Usage Data is generally retained for a shorter period, except when this data is used to strengthen the security or to improve the functionality of Our Service, or We are legally obligated to retain this data for longer time periods.",
                                  style: GoogleFonts.roboto(
                                      color: Colors.black87,
                                      fontSize: 14,
                                      height: 1.7),
                                ),
                              ],
                            ),
                            TextSpan(
                              children: [
                                TextSpan(
                                  text: "\n\n\nTransfer of Your Personal Data",
                                  style: GoogleFonts.roboto(
                                      color: Colors.black87,
                                      fontSize: 14,
                                      height: 1.7,
                                      fontWeight: FontWeight.w700),
                                ),
                              ],
                            ),
                            TextSpan(
                              children: [
                                TextSpan(
                                  text:
                                      "\n\nYour information, including Personal Data, is processed at the Organ Donor App' Admin database and in any other places where the parties involved in the processing are located. It means that this information may be transferred to — and maintained on — computers located outside of Your state, province, country, or other governmental jurisdiction where the data protection laws may differ than those from Your jurisdiction.",
                                  style: GoogleFonts.roboto(
                                      color: Colors.black87,
                                      fontSize: 14,
                                      height: 1.7),
                                ),
                              ],
                            ),
                            TextSpan(
                              children: [
                                TextSpan(
                                  text:
                                      "\n\nYour consent to this Privacy Policy followed by Your submission of such information represents Your agreement to that transfer.",
                                  style: GoogleFonts.roboto(
                                      color: Colors.black87,
                                      fontSize: 14,
                                      height: 1.7),
                                ),
                              ],
                            ),
                            TextSpan(
                              text:
                                  "\n\nThe Organ Donor App will take all steps reasonably necessary to ensure that Your data is treated securely and in accordance with this Privacy Policy and no transfer of Your Personal Data will take place to an organization or a country unless there are adequate controls in place including the security of Your data and other personal information.",
                              style: GoogleFonts.roboto(
                                color: Colors.black87,
                                fontSize: 14,
                                height: 1.7,
                              ),
                            ),
                            TextSpan(
                              children: [
                                TextSpan(
                                  text:
                                      "\n\n\nDisclosure of Your Personal Data",
                                  style: GoogleFonts.roboto(
                                      color: Colors.black87,
                                      fontSize: 14,
                                      height: 1.7,
                                      fontWeight: FontWeight.w700),
                                ),
                              ],
                            ),
                            TextSpan(
                              children: [
                                TextSpan(
                                  text: "\n\nLaw Enforcement",
                                  style: GoogleFonts.roboto(
                                      color: Colors.black87,
                                      fontSize: 14,
                                      height: 1.7,
                                      fontWeight: FontWeight.w700),
                                ),
                              ],
                            ),
                            TextSpan(
                              children: [
                                TextSpan(
                                  text:
                                      "\n\nUnder certain circumstances, the Organ Donor App may be required to disclose Your Personal Data if required to do so by law or in response to valid requests by public authorities (e.g. a court or a government agency).",
                                  style: GoogleFonts.roboto(
                                      color: Colors.black87,
                                      fontSize: 14,
                                      height: 1.7),
                                ),
                              ],
                            ),
                            TextSpan(
                              children: [
                                TextSpan(
                                  text: "\n\nOther Legal Requirements",
                                  style: GoogleFonts.roboto(
                                      color: Colors.black87,
                                      fontSize: 14,
                                      height: 1.7,
                                      fontWeight: FontWeight.w700),
                                ),
                              ],
                            ),
                            TextSpan(
                              children: [
                                TextSpan(
                                  text: "\n\n●	Comply with a legal obligation",
                                  style: GoogleFonts.roboto(
                                      color: Colors.black87,
                                      fontSize: 14,
                                      height: 1.7),
                                ),
                              ],
                            ),
                            TextSpan(
                              children: [
                                TextSpan(
                                  text:
                                      "\n●	Protect and defend the rights or property of the Organ Donor App",
                                  style: GoogleFonts.roboto(
                                      color: Colors.black87,
                                      fontSize: 14,
                                      height: 1.7),
                                ),
                              ],
                            ),
                            TextSpan(
                              children: [
                                TextSpan(
                                  text:
                                      "\n●	Prevent or investigate possible wrongdoing in connection with the Service",
                                  style: GoogleFonts.roboto(
                                      color: Colors.black87,
                                      fontSize: 14,
                                      height: 1.7),
                                ),
                              ],
                            ),
                            TextSpan(
                              children: [
                                TextSpan(
                                  text:
                                      "\n●	Protect the personal safety of Users of the Service or the public",
                                  style: GoogleFonts.roboto(
                                      color: Colors.black87,
                                      fontSize: 14,
                                      height: 1.7),
                                ),
                              ],
                            ),
                            TextSpan(
                              children: [
                                TextSpan(
                                  text: "\n●	Protect against legal liability",
                                  style: GoogleFonts.roboto(
                                      color: Colors.black87,
                                      fontSize: 14,
                                      height: 1.7),
                                ),
                              ],
                            ),
                            TextSpan(
                              children: [
                                TextSpan(
                                  text:
                                      "\n\n● The Organ Donor App may disclose Your Personal Data in the good faith belief that such action is necessary to:",
                                  style: GoogleFonts.roboto(
                                      color: Colors.black87,
                                      fontSize: 14,
                                      height: 1.7),
                                ),
                              ],
                            ),
                            TextSpan(
                              text: "\n\n\nSecurity of Your Personal Data",
                              style: GoogleFonts.roboto(
                                color: Colors.black87,
                                fontSize: 14,
                                height: 1.7,
                                fontWeight: FontWeight.w700,
                              ),
                            ),
                            TextSpan(
                              children: [
                                TextSpan(
                                  text:
                                      "\n\nThe security of Your Personal Data is important to Us but remember that no method of transmission over the Internet, or method of electronic storage is 100% secure. While We strive to use commercially acceptable means to protect Your Personal Data, we cannot guarantee its absolute security.",
                                  style: GoogleFonts.roboto(
                                      color: Colors.black87,
                                      fontSize: 14,
                                      height: 1.7),
                                ),
                              ],
                            ),
                            TextSpan(
                              text: "\n\n\nChanges to this Privacy Policy",
                              style: GoogleFonts.roboto(
                                color: Colors.black87,
                                fontSize: 14,
                                height: 1.7,
                                fontWeight: FontWeight.w700,
                              ),
                            ),
                            TextSpan(
                              children: [
                                TextSpan(
                                  text:
                                      "\n\nWe may update Our Privacy Policy from time to time. We will notify You of any changes by posting the new Privacy Policy on this page.",
                                  style: GoogleFonts.roboto(
                                      color: Colors.black87,
                                      fontSize: 14,
                                      height: 1.7),
                                ),
                              ],
                            ),
                            TextSpan(
                              children: [
                                TextSpan(
                                  text:
                                      '\n\nWe will let You know via email and/or a prominent notice on Our Service, prior to the change becoming effective and update the "Last updated" date at the top of this Privacy Policy.',
                                  style: GoogleFonts.roboto(
                                      color: Colors.black87,
                                      fontSize: 14,
                                      height: 1.7),
                                ),
                              ],
                            ),
                            TextSpan(
                              children: [
                                TextSpan(
                                  text:
                                      '\n\nYou are advised to review this Privacy Policy periodically for any changes. Changes to this Privacy Policy are effective when they are posted on this page.',
                                  style: GoogleFonts.roboto(
                                      color: Colors.black87,
                                      fontSize: 14,
                                      height: 1.7),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ),
                    //End Line
                    Container(
                      alignment: Alignment.topRight,
                      margin: EdgeInsets.only(left: 15, right: 15, bottom: 40),
                      child: SelectableText.rich(
                        TextSpan(
                          children: [
                            TextSpan(
                              text: "\n\nDevelopers:",
                              style: GoogleFonts.roboto(
                                color: Colors.black87,
                                fontSize: 14,
                                height: 1.7,
                                fontWeight: FontWeight.w700,
                              ),
                            ),
                            TextSpan(
                              text: "\nEspiritu, Justin Lloyd:",
                              style: GoogleFonts.roboto(
                                color: Colors.black87,
                                fontSize: 14,
                                height: 1.7,
                              ),
                            ),
                            TextSpan(
                              text: "\nMayor, Aldrin:",
                              style: GoogleFonts.roboto(
                                color: Colors.black87,
                                fontSize: 14,
                                height: 1.7,
                              ),
                            ),
                            TextSpan(
                              text: "\nRivera, Debbie Cheyenne:",
                              style: GoogleFonts.roboto(
                                color: Colors.black87,
                                fontSize: 14,
                                height: 1.7,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
          //Header
          Container(
            decoration: BoxDecoration(color: Color(0xffe5eef8)),
            height: 80,
            margin: EdgeInsets.only(top: 0, left: 0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                InkWell(
                  onTap: () {
                    Navigator.pushAndRemoveUntil(
                        context,
                        MaterialPageRoute(builder: (builder) => AboutPage()),
                        (route) => false);
                  },
                  child: Padding(
                    padding: const EdgeInsets.only(top: 20, left: 20),
                    child: Icon(Icons.arrow_back_ios_outlined, size: 25.0),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 20),
                  child: Text(
                    "Organ Donor App - About Us",
                    style: GoogleFonts.roboto(
                        color: Colors.black87,
                        fontWeight: FontWeight.w400,
                        fontSize: 18),
                  ),
                ),
                Container(width: 32.0, height: 0.0),
              ],
            ),
          ),
          //Bottom Button
          Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              Container(
                decoration: BoxDecoration(
                  boxShadow: [BoxShadow(color: Colors.grey.withOpacity(0.5))],
                  color: Color(0xffe5eef8),
                ),
                padding: EdgeInsets.only(top: 10, bottom: 10),
                child: Column(
                  children: [
                    Container(
                      height: 30,
                      width: double.infinity,
                      padding: EdgeInsets.only(
                        left: 135,
                        right: 135,
                        top: 3,
                        bottom: 3,
                      ),
                      child: InkWell(
                        onTap: () {
                          Navigator.pushAndRemoveUntil(
                              context,
                              MaterialPageRoute(
                                  builder: (builder) => AboutPage()),
                              (route) => false);
                        },
                        child: Container(
                          decoration: BoxDecoration(
                              color: Colors.red[100],
                              borderRadius: BorderRadius.circular(18)),
                          child: Center(
                            child: Text(
                              "Back",
                              style: TextStyle(
                                  color: Colors.red[400], fontSize: 17),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
