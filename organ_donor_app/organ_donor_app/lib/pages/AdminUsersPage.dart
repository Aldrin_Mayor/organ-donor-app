import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:intl/intl.dart';
import 'package:crypto/crypto.dart';
import 'dart:convert'; // for the utf8.encode method

class AdminUsersPage extends StatefulWidget {
  AdminUsersPage({Key? key}) : super(key: key);

  @override
  _AdminUsersPageState createState() => _AdminUsersPageState();
}

class _AdminUsersPageState extends State<AdminUsersPage> {
  //holds Patient Document

  patientTagged() async {
    String formattedTime = DateFormat('kk:mm:s:a').format(now);
    String formattedDate = formatter.format(now);
    await FirebaseFirestore.instance
        .collection('Tagged Users')
        .doc('Patient')
        .collection('Lists')
        .doc(userName)
        .set({
      'UserID': userName,
      'FullName': fullName,
      'UserName': userName,
      'Gender': genderValue,
      'Email Address': email,
      'Mobile Number': mobileNumber,
      'User Type': usersValue,
      'Age': ageValue,
      'Blood Type': bloodType,
      'Kidney Size': kidneySize,
      'Status': patientStatus,
      'Signature': 'null',
      'Date Registered': formattedDate,
      'Time Registered': formattedTime,
      'Views': 'null',
      'Comments': 'null',
      'Likes': 'null',
      'Pinned': 'null',
      'Donated': 'null',
      'Received': 'null',
      'Posts': 'null',
      'Notifications': 'null',
      'Replies': 'null',
      'Match': 'null',
      'Mail': 'null'
    });
  }

//holds Donor Document
  donorTagged() async {
    String formattedTime = DateFormat('kk:mm:s:a').format(now);
    String formattedDate = formatter.format(now);
    await FirebaseFirestore.instance
        .collection('Tagged Users')
        .doc('Donor')
        .collection('Lists')
        .doc(userName)
        .set({
      'UserID': userName,
      'FullName': fullName,
      'UserName': userName,
      'Gender': genderValue,
      'Email Address': email,
      'Mobile Number': mobileNumber,
      'User Type': usersValue,
      'Age': ageValue,
      'Blood Type': bloodType,
      'Kidney Size': kidneySize,
      'Status': donorStatus,
      'Signature': 'null',
      'Date Registered': formattedDate,
      'Time Registered': formattedTime,
      'Views': 'null',
      'Comments': 'null',
      'Likes': 'null',
      'Pinned': 'null',
      'Donated': 'null',
      'Received': 'null',
      'Posts': 'null',
      'Notifications': 'null',
      'Replies': 'null',
      'Match': 'null',
      'Mail': 'null'
    });
  }

  allDonorTagged() async {
    String formattedTime = DateFormat('kk:mm:s:a').format(now);
    String formattedDate = formatter.format(now);
    await FirebaseFirestore.instance
        .collection('Tagged Users')
        .doc('All Tagged')
        .collection('Lists')
        .doc(userName)
        .set({
      'UserID': userName,
      'FullName': fullName,
      'UserName': userName,
      'Gender': genderValue,
      'Email Address': email,
      'Mobile Number': mobileNumber,
      'User Type': usersValue,
      'Age': ageValue,
      'Blood Type': bloodType,
      'Kidney Size': kidneySize,
      'Status': donorStatus,
      'Signature': 'null',
      'Date Registered': formattedDate,
      'Time Registered': formattedTime,
      'Views': 'null',
      'Comments': 'null',
      'Likes': 'null',
      'Pinned': 'null',
      'Donated': 'null',
      'Received': 'null',
      'Posts': 'null',
      'Notifications': 'null',
      'Replies': 'null',
      'Match': 'null',
      'Mail': 'null'
    });
  }

  allPatientTagged() async {
    String formattedTime = DateFormat('kk:mm:s:a').format(now);
    String formattedDate = formatter.format(now);
    await FirebaseFirestore.instance
        .collection('Tagged Users')
        .doc('All Tagged')
        .collection('Lists')
        .doc(userName)
        .set({
      'UserID': userName,
      'FullName': fullName,
      'UserName': userName,
      'Gender': genderValue,
      'Email Address': email,
      'Mobile Number': mobileNumber,
      'User Type': usersValue,
      'Age': ageValue,
      'Blood Type': bloodType,
      'Kidney Size': kidneySize,
      'Status': patientStatus,
      'Signature': 'null',
      'Date Registered': formattedDate,
      'Time Registered': formattedTime,
      'Views': 'null',
      'Comments': 'null',
      'Likes': 'null',
      'Pinned': 'null',
      'Donated': 'null',
      'Received': 'null',
      'Posts': 'null',
      'Notifications': 'null',
      'Replies': 'null',
      'Match': 'null',
      'Mail': 'null'
    });
  }

  String? usersListValue;

  final usersList = [
    'All Registered',
    'Donor (Registered)',
    'Patient (Registered)',
    'All Tagged',
    'Donor (Tagged)',
    'Patient (Tagged)'
  ];

  final users = ['Donor', 'Patient'];
  final gender = ['Male', 'Female', 'Prefer not to say', 'Other'];
  final blood = ['A+', 'A-', 'B+', 'B-', 'AB+', 'AB-', 'O+', 'O-'];

  //Local Variables
  String? fullName;
  String? userName;
  String? genderValue;
  String? mobileNumber;
  String? email;
  String? age;
  String? finalValue;
  String? usersValue;
  String? donorStatus;
  String? patientStatus;
  String? bloodType;
  String? ageValue;
  String? kidneySize;
  final formKey = GlobalKey<FormState>();

  //Set State Refresh
  final toast = FToast();
  @override
  void initState() {
    super.initState();

    toast.init(context);
  }

  //Controller for Edit
  TextEditingController _fullName = TextEditingController();
  TextEditingController _userName = TextEditingController();
  TextEditingController _email = TextEditingController();
  TextEditingController _gender = TextEditingController();
  TextEditingController _userType = TextEditingController();
  TextEditingController _age = TextEditingController();
  TextEditingController _bloodType = TextEditingController();
  TextEditingController _kidneySize = TextEditingController();
  TextEditingController _mobileNumber = TextEditingController();

  //For Edit and Archived
  String? getUid;
  String? getFullName;
  String? getUserName;
  String? getEmail;
  String? getGender;
  String? getUserType;
  String? getAge;
  String? getBloodType;
  String? getKidneySize;
  String? getMobileNumber;
  String? getSignature;
  String? getRegisteredDate;
  String? getRegisteredTime;
  String? getViews;
  String? getComments;
  String? getLikes;
  String? getPinned;
  String? getDonated;
  String? getReceived;
  String? getPosts;
  String? getNotifications;
  String? getReplies;
  String? getMatch;
  String? getMail;
  String? getStatus; // For Archive

  bool isButtonEnabled = false;
  bool isFieldEnabled = false;

  //Time and Date
  var now = new DateTime.now();
  var formatter = new DateFormat('dd-MM-yyyy');

  //For verification to view hashed mobile Number and Email
  String? viewHashedPassword = "M@sterP@55w0rd";
  String? decryptMobileNumber = "";
  String? decryptEmail;
  TextEditingController _password = TextEditingController();
  TextEditingController _viewMobileNumberPassword = TextEditingController();
  TextEditingController _viewEmailAddressPassword = TextEditingController();
  bool isHiddenPassword = true;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xffe5eef8),
      body: SingleChildScrollView(
        child: Wrap(
          spacing: 8.0, // gap between adjacent chips
          runSpacing: 4.0, // gap between lines
          direction: Axis.horizontal,
          children: <Widget>[
            Container(
              margin: EdgeInsets.only(top: 60, left: 100),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    'Users',
                    style: GoogleFonts.roboto(
                        color: Colors.black54,
                        fontSize: 40,
                        fontWeight: FontWeight.w400),
                  ),
                  Container(
                    margin: EdgeInsets.only(left: 800),
                    height: 40,
                    width: 130,
                    decoration: BoxDecoration(
                      color: Colors.red,
                      borderRadius: BorderRadius.circular(20),
                    ),
                    child: Row(
                      children: [
                        Container(
                          margin: EdgeInsets.only(left: 20),
                          child: Icon(
                            Icons.add,
                            color: Colors.red[100],
                          ),
                        ),
                        InkWell(
                          onTap: () {
                            showAddCustomDialog(context);
                          },
                          child: Container(
                            margin: EdgeInsets.only(left: 10),
                            child: Text(
                              'Tag'.toUpperCase(),
                              style: GoogleFonts.roboto(
                                  color: Colors.white, fontSize: 20),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(right: 70),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      color: Colors.grey[300],
                    ),
                    width: 240,
                    child: DropdownButtonHideUnderline(
                      child: Stack(
                        children: [
                          Row(
                            children: [
                              Container(
                                height: 45,
                                width: 45,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.only(
                                    topLeft: Radius.circular(10),
                                    bottomLeft: Radius.circular(10),
                                  ),
                                  color: Colors.grey[400],
                                ),
                                child: Icon(Icons.filter_alt),
                              ),
                              Expanded(
                                child: DropdownButtonFormField<String>(
                                  decoration: InputDecoration(
                                      border: InputBorder.none,
                                      fillColor: Colors.red),
                                  hint: Container(
                                    margin: EdgeInsets.only(left: 20),
                                    child: Text(
                                      "Filter Users",
                                      style: GoogleFonts.roboto(
                                          color: Colors.black54,
                                          fontWeight: FontWeight.w500,
                                          fontSize: 16),
                                    ),
                                  ),
                                  value: usersListValue,
                                  isExpanded: true,
                                  items: usersList
                                      .map(buildUserListMenuItem)
                                      .toList(),
                                  onChanged: (value) => setState(
                                    () => this.usersListValue = value,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Container(
              height: MediaQuery.of(context).size.height * 0.855,
              width: MediaQuery.of(context).size.width * 0.755,
              margin: EdgeInsets.only(top: 20, left: 50, bottom: 100),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: Colors.white,
              ),
              child: Stack(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Container(
                        margin: EdgeInsets.only(top: 20, left: 2),
                        child: Text(
                          'Identifier'.toUpperCase(),
                          style: GoogleFonts.roboto(
                              color: Colors.black45,
                              fontWeight: FontWeight.w500,
                              fontSize: 16),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(top: 20, left: 4),
                        child: Text(
                          'User Name'.toUpperCase(),
                          style: GoogleFonts.roboto(
                              color: Colors.black45,
                              fontWeight: FontWeight.w500,
                              fontSize: 16),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(top: 20, left: 8),
                        child: Text(
                          'Email'.toUpperCase(),
                          style: GoogleFonts.roboto(
                              color: Colors.black45,
                              fontWeight: FontWeight.w500,
                              fontSize: 16),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(top: 20, left: 58),
                        child: Text(
                          'Gender'.toUpperCase(),
                          style: GoogleFonts.roboto(
                              color: Colors.black45,
                              fontWeight: FontWeight.w500,
                              fontSize: 16),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(top: 20, left: 20),
                        child: Text(
                          'Age'.toUpperCase(),
                          style: GoogleFonts.roboto(
                              color: Colors.black45,
                              fontWeight: FontWeight.w500,
                              fontSize: 16),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(top: 20),
                        child: Text(
                          'Identifier\nType'.toUpperCase(),
                          textAlign: TextAlign.center,
                          style: GoogleFonts.roboto(
                              color: Colors.black45,
                              fontWeight: FontWeight.w500,
                              fontSize: 16),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(top: 20),
                        child: Text(
                          'Blood\nType'.toUpperCase(),
                          textAlign: TextAlign.center,
                          style: GoogleFonts.roboto(
                              color: Colors.black45,
                              fontWeight: FontWeight.w500,
                              fontSize: 16),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(top: 20),
                        child: Text(
                          'Kidney\nSize(cm)'.toUpperCase(),
                          textAlign: TextAlign.center,
                          style: GoogleFonts.roboto(
                              color: Colors.black45,
                              fontWeight: FontWeight.w500,
                              fontSize: 16),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(top: 20),
                        child: Text(
                          'Mobile Number'.toUpperCase(),
                          style: GoogleFonts.roboto(
                              color: Colors.black45,
                              fontWeight: FontWeight.w500,
                              fontSize: 16),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(top: 20),
                        child: Text(
                          'Status'.toUpperCase(),
                          style: GoogleFonts.roboto(
                              color: Colors.black45,
                              fontWeight: FontWeight.w500,
                              fontSize: 16),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(
                          top: 20,
                        ),
                        child: Text(
                          'Delete'.toUpperCase(),
                          style: GoogleFonts.roboto(
                              color: Colors.black45,
                              fontWeight: FontWeight.w500,
                              fontSize: 16),
                        ),
                      ),
                    ],
                  ),
                  Container(
                    margin: EdgeInsets.only(
                      top: 50,
                    ),
                    child: FutureBuilder(
                      future: getUsersData(),
                      builder: (context, AsyncSnapshot snapshot) {
                        if (snapshot.hasData) {
                          return ListView.builder(
                            itemCount: snapshot.data!.length,
                            itemBuilder: (context, index) {
                              DocumentSnapshot userData = snapshot.data[index];
                              var hashMobileNumber =
                                  utf8.encode(userData["Mobile Number"]);
                              var displayMobileNumber =
                                  sha256.convert(hashMobileNumber);
                              var hashEmailAddress =
                                  utf8.encode(userData["Email Address"]);
                              var displayEmailAddress =
                                  sha256.convert(hashEmailAddress);
                              return Stack(
                                children: [
                                  Container(
                                    margin: EdgeInsets.only(
                                      top: 20,
                                    ),
                                    child: ListTile(
                                      title: Row(
                                        children: [
                                          Container(
                                            margin: EdgeInsets.only(left: 38),
                                            width: 110,
                                            child: Text(
                                              userData["FullName"],
                                              style: GoogleFonts.roboto(
                                                  color: Colors.black87,
                                                  fontWeight: FontWeight.w400,
                                                  fontSize: 14),
                                            ),
                                          ),
                                          Container(
                                            margin: EdgeInsets.only(left: 26),
                                            width: 120,
                                            child: Text(
                                              userData["UserName"],
                                              style: GoogleFonts.roboto(
                                                  color: Colors.black87,
                                                  fontWeight: FontWeight.w400,
                                                  fontSize: 14),
                                            ),
                                          ),
                                          Container(
                                            margin: EdgeInsets.only(left: 20),
                                            width: 100,
                                            child: Text(
                                              displayEmailAddress.toString(),
                                              style: GoogleFonts.roboto(
                                                  color: Colors.black87,
                                                  fontWeight: FontWeight.w400,
                                                  fontSize: 14),
                                            ),
                                          ),
                                          InkWell(
                                            onTap: () {
                                              getEmail =
                                                  userData["Email Address"];
                                              showHashedEmailAddressCustomDialog(
                                                  context);
                                            },
                                            child: Container(
                                              margin: EdgeInsets.only(left: 10),
                                              child: Icon(
                                                  Icons.remove_red_eye_rounded),
                                            ),
                                          ),
                                          Container(
                                            margin: EdgeInsets.only(left: 28),
                                            width: 105,
                                            child: Text(
                                              userData["Gender"],
                                              style: GoogleFonts.roboto(
                                                  color: Colors.black87,
                                                  fontWeight: FontWeight.w400,
                                                  fontSize: 14),
                                            ),
                                          ),
                                          Container(
                                            width: 20,
                                            margin: EdgeInsets.only(left: 30),
                                            child: Text(
                                              userData["Age"],
                                              style: GoogleFonts.roboto(
                                                  color: Colors.black87,
                                                  fontWeight: FontWeight.w400,
                                                  fontSize: 14),
                                            ),
                                          ),
                                          Container(
                                            margin: EdgeInsets.only(left: 60),
                                            width: 100,
                                            child: Text(
                                              userData["User Type"],
                                              style: GoogleFonts.roboto(
                                                  color: Colors.black87,
                                                  fontWeight: FontWeight.w400,
                                                  fontSize: 14),
                                            ),
                                          ),
                                          Container(
                                            margin: EdgeInsets.only(left: 50),
                                            width: 20,
                                            child: Text(
                                              userData["Blood Type"],
                                              style: GoogleFonts.roboto(
                                                  color: Colors.black87,
                                                  fontWeight: FontWeight.w400,
                                                  fontSize: 14),
                                            ),
                                          ),
                                          Container(
                                            margin: EdgeInsets.only(left: 80),
                                            width: 30,
                                            child: Text(
                                              userData["Kidney Size"],
                                              style: GoogleFonts.roboto(
                                                  color: Colors.black87,
                                                  fontWeight: FontWeight.w400,
                                                  fontSize: 14),
                                            ),
                                          ),
                                          Container(
                                            margin: EdgeInsets.only(left: 70),
                                            width: 100,
                                            child: Text(
                                              displayMobileNumber.toString(),
                                              style: GoogleFonts.roboto(
                                                  color: Colors.black87,
                                                  fontWeight: FontWeight.w400,
                                                  fontSize: 14),
                                            ),
                                          ),
                                          InkWell(
                                            onTap: () {
                                              getMobileNumber =
                                                  userData["Mobile Number"];
                                              showHashedMobileNumberCustomDialog(
                                                  context);
                                            },
                                            child: Container(
                                              margin: EdgeInsets.only(left: 10),
                                              child: Icon(
                                                  Icons.remove_red_eye_rounded),
                                            ),
                                          ),
                                          Container(
                                            width: 80,
                                            margin: EdgeInsets.only(left: 45),
                                            child: Text(
                                              userData["Status"],
                                              style: GoogleFonts.roboto(
                                                  color: Colors.black87,
                                                  fontWeight: FontWeight.w400,
                                                  fontSize: 14),
                                            ),
                                          ),
                                          Container(
                                            margin: EdgeInsets.only(left: 20),
                                            child: Row(
                                              children: [
                                                IconButton(
                                                  onPressed: () {
                                                    getUid = userData["UserID"];
                                                    getFullName =
                                                        userData["FullName"];
                                                    getUserName =
                                                        userData["UserName"];
                                                    getEmail = userData[
                                                        "Email Address"];
                                                    getGender =
                                                        userData["Gender"];
                                                    getAge = userData["Age"];
                                                    getUserType =
                                                        userData["User Type"];
                                                    getBloodType =
                                                        userData["Blood Type"];
                                                    getKidneySize =
                                                        userData["Kidney Size"];
                                                    getMobileNumber = userData[
                                                        "Mobile Number"];
                                                    getStatus = "Archived";
                                                    getSignature =
                                                        userData["Signature"];
                                                    getRegisteredDate =
                                                        userData[
                                                            "Date Registered"];
                                                    getRegisteredTime =
                                                        userData[
                                                            "Time Registered"];
                                                    getViews =
                                                        userData["Views"];
                                                    getComments =
                                                        userData["Comments"];
                                                    getLikes =
                                                        userData["Likes"];
                                                    getPinned =
                                                        userData["Pinned"];
                                                    getDonated =
                                                        userData["Donated"];
                                                    getReceived =
                                                        userData["Received"];
                                                    getPosts =
                                                        userData["Posts"];
                                                    getNotifications = userData[
                                                        "Notifications"];
                                                    getReplies =
                                                        userData["Replies"];
                                                    getMatch =
                                                        userData["Match"];
                                                    getMail = userData["Mail"];

                                                    setState(() {
                                                      if (getUserType ==
                                                          'I am a Donor') {
                                                        //Move to archive
                                                        archiveDonor();
                                                        archiveAll();
                                                        //Temporary remove from Users table
                                                        archiveDeleteDonor();
                                                        archiveDeleteAll();
                                                        //Show Success Message
                                                        showArchivedToast();
                                                        logAdminArchivedDonor();
                                                      } else if (getUserType ==
                                                          'I am a Patient') {
                                                        //Move to archive
                                                        archivePatient();
                                                        archiveAll();
                                                        //Temporary remove from Users table
                                                        archiveDeletePatient();
                                                        archiveDeleteAll();
                                                        //Show Success Message
                                                        showArchivedToast();
                                                        logAdminArchivedPatient();
                                                      } else if (getUserType ==
                                                          'Donor') {
                                                        //Move to archive
                                                        archiveTaggedDonor();
                                                        archiveAllTagged();
                                                        //Temporary remove from Users table
                                                        archiveDeleteTaggedDonor();
                                                        archiveDeleteAllTagged();
                                                        //Show Success Message
                                                        showArchivedToast();
                                                        logAdminArchivedTaggedDonor();
                                                      } else if (getUserType ==
                                                          'Patient') {
                                                        //Move to archive
                                                        archiveTaggedPatient();
                                                        archiveAllTagged();
                                                        //Temporary remove from Users table
                                                        archiveDeleteTaggedPatient();
                                                        archiveDeleteAllTagged();
                                                        //Show Success Message
                                                        showArchivedToast();
                                                        logAdminArchivedTaggedDonor();
                                                      }
                                                    });
                                                  },
                                                  icon: Icon(
                                                    Icons.archive_outlined,
                                                    size: 20,
                                                    color: Colors.blue,
                                                  ),
                                                ),
                                                IconButton(
                                                  onPressed: () {
                                                    getUid = userData["UserID"];
                                                    getFullName =
                                                        userData["FullName"];
                                                    getUserName =
                                                        userData["UserName"];
                                                    getEmail = userData[
                                                        "Email Address"];
                                                    getGender =
                                                        userData["Gender"];
                                                    getAge = userData["Age"];
                                                    getUserType =
                                                        userData["User Type"];
                                                    getBloodType =
                                                        userData["Blood Type"];
                                                    getKidneySize =
                                                        userData["Kidney Size"];
                                                    getMobileNumber = userData[
                                                        "Mobile Number"];
                                                    getSignature =
                                                        userData["Signature"];
                                                    setState(() {
                                                      showEditustomDialog(
                                                          context);
                                                    });
                                                  },
                                                  icon: Icon(
                                                    Icons.edit,
                                                    size: 20,
                                                    color: Colors.grey,
                                                  ),
                                                )
                                              ],
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 18),
                                    child: Divider(),
                                  ),
                                ],
                              );
                            },
                          );
                        } else {
                          return Container(
                            alignment: Alignment.center,
                            child: CircularProgressIndicator(),
                          );
                        }
                      },
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  //Display User Table
  getUsersData() async {
    final firestore = FirebaseFirestore.instance;
    if (usersListValue == 'Donor (Registered)') {
      QuerySnapshot user = await firestore
          .collection('Users')
          .doc('Donor')
          .collection('Lists')
          .get();
      logAdminFilteredDonorOnly();
      return user.docs;
    } else if (usersListValue == 'Patient (Registered)') {
      QuerySnapshot user = await firestore
          .collection('Users')
          .doc('Patient')
          .collection('Lists')
          .get();
      logAdminFilteredPatientOnly();
      return user.docs;
    } else if (usersListValue == 'All (Registered)') {
      QuerySnapshot user = await firestore
          .collection('Users')
          .doc('All')
          .collection('Lists')
          .get();
      logAdminFilteredAllRegistered();
      return user.docs;
    } else if (usersListValue == 'All Tagged') {
      QuerySnapshot user = await firestore
          .collection('Tagged Users')
          .doc('All Tagged')
          .collection('Lists')
          .get();
      logAdminFilteredAllTagged();
      return user.docs;
    } else if (usersListValue == 'Donor (Tagged)') {
      QuerySnapshot user = await firestore
          .collection('Tagged Users')
          .doc('Donor')
          .collection('Lists')
          .get();
      logAdminFilteredDonorTagged();
      return user.docs;
    } else if (usersListValue == 'Patient (Tagged)') {
      QuerySnapshot user = await firestore
          .collection('Tagged Users')
          .doc('Patient')
          .collection('Lists')
          .get();
      logAdminFilteredPatientOnly();
      return user.docs;
    } else {
      QuerySnapshot user = await firestore
          .collection('Users')
          .doc('All')
          .collection('Lists')
          .get();
      return user.docs;
    }
  }

  //Filter Users DropdownMenu
  DropdownMenuItem<String> buildUserListMenuItem(String usersList) =>
      DropdownMenuItem(
        value: usersList,
        child: Container(
          child: Text(
            '   ' + usersList,
            style: GoogleFonts.roboto(color: Colors.black, fontSize: 15),
          ),
        ),
      );

  //User Type DropdownMenu
  DropdownMenuItem<String> buildUserMenuItem(String users) => DropdownMenuItem(
        value: users,
        child: Text(
          users,
          style: GoogleFonts.roboto(color: Colors.black, fontSize: 15),
        ),
      );

  //Gender DropdwownMenu
  DropdownMenuItem<String> buildGenderMenuItem(String gender) =>
      DropdownMenuItem(
        value: gender,
        child: Text(
          gender,
          style: GoogleFonts.roboto(color: Colors.black, fontSize: 15),
        ),
      );

//BloodType DropdwownMenu
  DropdownMenuItem<String> buildBloodTypeMenuItem(String blood) =>
      DropdownMenuItem(
        value: blood,
        child: Text(
          blood,
          style: GoogleFonts.roboto(color: Colors.black, fontSize: 15),
        ),
      );

//Show Patient Tagged Success Toast Message
  void showPatientTaggedSuccessToast() => toast.showToast(
      child: buildPatientTaggedSuccessToast(), gravity: ToastGravity.CENTER);

  Widget buildPatientTaggedSuccessToast() => Container(
        padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
        decoration: BoxDecoration(
          color: Colors.green[300],
          borderRadius: BorderRadius.circular(5),
        ),
        child: Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            Text(
              "Patient Tagged Success!",
              style: TextStyle(color: Colors.black87, fontSize: 16),
            ),
          ],
        ),
      );

//Show Donor Tagged Success Toast Message
  void showDonorTaggedSuccessToast() => toast.showToast(
      child: buildDonorTaggedSuccessToast(), gravity: ToastGravity.CENTER);

  Widget buildDonorTaggedSuccessToast() => Container(
        padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
        decoration: BoxDecoration(
          color: Colors.green[300],
          borderRadius: BorderRadius.circular(5),
        ),
        child: Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            Text(
              "Donor Tagged Success!",
              style: TextStyle(color: Colors.black87, fontSize: 16),
            ),
          ],
        ),
      );

//Show Edit Enabled
  void showEditEnabledToast() => toast.showToast(
      child: buildEditEnabledToast(), gravity: ToastGravity.CENTER);

  Widget buildEditEnabledToast() => Container(
        padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
        decoration: BoxDecoration(
          color: Colors.green[300],
          borderRadius: BorderRadius.circular(5),
        ),
        child: Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            Text(
              "Edit Mode",
              style: TextStyle(color: Colors.black87, fontSize: 16),
            ),
          ],
        ),
      );

//Show Edit Success
  void showEditSuccessToast() => toast.showToast(
      child: buildEditSuccessToast(), gravity: ToastGravity.CENTER);

  Widget buildEditSuccessToast() => Container(
        padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
        decoration: BoxDecoration(
          color: Colors.green[300],
          borderRadius: BorderRadius.circular(5),
        ),
        child: Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            Text(
              "Information Changed Success!",
              style: TextStyle(color: Colors.black87, fontSize: 16),
            ),
          ],
        ),
      );

//Show Edit Cancel
  void showEditCancelToast() => toast.showToast(
      child: buildEditCancelToast(), gravity: ToastGravity.CENTER);

  Widget buildEditCancelToast() => Container(
        padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
        decoration: BoxDecoration(
          color: Colors.green[300],
          borderRadius: BorderRadius.circular(5),
        ),
        child: Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            Text(
              "Edit Canceled!",
              style: TextStyle(color: Colors.black87, fontSize: 16),
            ),
          ],
        ),
      );

  //Show Archived Success
  void showArchivedToast() => toast.showToast(
      child: buildArchivedToast(), gravity: ToastGravity.CENTER);

  Widget buildArchivedToast() => Container(
        padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
        decoration: BoxDecoration(
          color: Colors.green[300],
          borderRadius: BorderRadius.circular(5),
        ),
        child: Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            Text(
              "Moved to archived",
              style: TextStyle(color: Colors.black87, fontSize: 16),
            ),
          ],
        ),
      );

  //Add Donor and Patient Alert Dialog Method
  void showAddCustomDialog(BuildContext context) => showDialog(
      context: context,
      builder: (BuildContext context) {
        return Dialog(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10.0)), //this right here
          child: Container(
            height: MediaQuery.of(context).size.height * 0.9,
            width: MediaQuery.of(context).size.width * 0.4,
            child: Form(
              key: formKey,
              child: Column(
                children: [
                  //Close Icon
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      Container(
                        margin: EdgeInsets.only(top: 10, right: 20),
                        child: InkWell(
                          onTap: () {
                            setState(() {
                              Navigator.of(context).pop();
                            });
                          },
                          child: Icon(
                            Icons.close_outlined,
                            size: 25,
                            color: Colors.red[100],
                          ),
                        ),
                      ),
                    ],
                  ),
                  //Header
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Container(
                        margin: EdgeInsets.only(top: 10, left: 40),
                        child: Text(
                          'Registration Tag'.toUpperCase(),
                          style: GoogleFonts.roboto(
                              color: Colors.black87,
                              fontSize: 40,
                              fontWeight: FontWeight.w700,
                              letterSpacing: 2),
                        ),
                      ),
                    ],
                  ),
                  //FullName and UserName Header
                  Row(
                    children: [
                      Container(
                        margin: EdgeInsets.only(top: 30, left: 40),
                        child: Text(
                          'Full Name (Optional)',
                          style: GoogleFonts.roboto(
                              color: Colors.black87,
                              fontSize: 18,
                              fontWeight: FontWeight.w500,
                              letterSpacing: 2),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(top: 30, left: 150),
                        child: Text(
                          'User Name',
                          style: GoogleFonts.roboto(
                              color: Colors.black87,
                              fontSize: 18,
                              fontWeight: FontWeight.w500,
                              letterSpacing: 2),
                        ),
                      ),
                    ],
                  ),
                  //FullName and UserName TextFields
                  Row(
                    children: [
                      //Fullname
                      Container(
                        width: MediaQuery.of(context).size.width * 0.150,
                        margin: EdgeInsets.only(top: 10, left: 40),
                        child: TextFormField(
                          inputFormatters: [
                            LengthLimitingTextInputFormatter(30)
                          ],
                          validator: (value) {
                            return null;
                          },
                          onSaved: (value) => fullName = value,
                          decoration: InputDecoration(
                            border: OutlineInputBorder(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(7))),
                            hintText: 'Enter Full Name ',
                          ),
                        ),
                      ),
                      //UseName
                      Container(
                        width: MediaQuery.of(context).size.width * 0.150,
                        margin: EdgeInsets.only(top: 10, left: 70),
                        child: TextFormField(
                          validator: (value) {
                            if (value!.isEmpty) {
                              return 'Attention! This is Required';
                            } else {
                              return null;
                            }
                          },
                          onSaved: (value) => userName = value,
                          decoration: InputDecoration(
                            border: OutlineInputBorder(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(7))),
                            hintText: "Enter User Name",
                            contentPadding: const EdgeInsets.symmetric(
                                vertical: 18, horizontal: 14),
                          ),
                        ),
                      ),
                    ],
                  ),
                  //Email and Mobile Number Header
                  Row(
                    children: [
                      Container(
                        margin: EdgeInsets.only(top: 30, left: 40),
                        child: Text(
                          'Email (Optional)',
                          style: GoogleFonts.roboto(
                              color: Colors.black87,
                              fontSize: 18,
                              fontWeight: FontWeight.w500,
                              letterSpacing: 2),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(top: 30, left: 195),
                        child: Text(
                          'Mobile Number',
                          style: GoogleFonts.roboto(
                              color: Colors.black87,
                              fontSize: 18,
                              fontWeight: FontWeight.w500,
                              letterSpacing: 2),
                        ),
                      ),
                    ],
                  ),

                  //Email and Mobile Number TextFields
                  Row(
                    children: [
                      //Email
                      Container(
                        width: MediaQuery.of(context).size.width * 0.150,
                        margin: EdgeInsets.only(top: 10, left: 40),
                        child: TextFormField(
                          inputFormatters: [
                            LengthLimitingTextInputFormatter(30)
                          ],
                          validator: (value) {
                            final pattern =
                                r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
                            final regExp = RegExp(pattern);

                            if (value!.isNotEmpty) {
                              if (!regExp.hasMatch(value)) {
                                return 'Enter a valid email';
                              } else {
                                return null;
                              }
                            }
                          },
                          onSaved: (value) => email = value,
                          decoration: InputDecoration(
                            border: OutlineInputBorder(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(7))),
                            hintText: 'Enter Email Address',
                          ),
                        ),
                      ),
                      //Mobile Number
                      Container(
                        width: MediaQuery.of(context).size.width * 0.150,
                        margin: EdgeInsets.only(top: 10, left: 70),
                        child: TextFormField(
                          keyboardType: TextInputType.number,
                          inputFormatters: [
                            LengthLimitingTextInputFormatter(11)
                          ],
                          validator: (value) {
                            if (value!.isEmpty) {
                              return 'Attention! This is Required';
                            }
                            if (value.length <= 10) {
                              return 'Enter a Valid Mobile Number';
                            } else {
                              return null;
                            }
                          },
                          onSaved: (value) => mobileNumber = value,
                          decoration: InputDecoration(
                              border: OutlineInputBorder(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(7))),
                              hintText: 'Enter Mobile Number',
                              contentPadding: const EdgeInsets.symmetric(
                                  vertical: 18, horizontal: 14),
                              prefixIcon: Padding(
                                padding: const EdgeInsets.symmetric(
                                    vertical: 12, horizontal: 15),
                                child: Text(
                                  "(+63)",
                                  style: TextStyle(
                                      color: Colors.black, fontSize: 17),
                                ),
                              )),
                        ),
                      ),
                    ],
                  ),
                  //Age and Kidney Size Header
                  Row(
                    children: [
                      Container(
                        margin: EdgeInsets.only(top: 30, left: 40),
                        child: Text(
                          'Age',
                          style: GoogleFonts.roboto(
                              color: Colors.black87,
                              fontSize: 18,
                              fontWeight: FontWeight.w500,
                              letterSpacing: 2),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(top: 30, left: 320),
                        child: Text(
                          'Kidney Size',
                          style: GoogleFonts.roboto(
                              color: Colors.black87,
                              fontSize: 18,
                              fontWeight: FontWeight.w500,
                              letterSpacing: 2),
                        ),
                      ),
                    ],
                  ),

                  //Age and kidney Size TextFields
                  Row(
                    children: [
                      //Age
                      Container(
                        width: MediaQuery.of(context).size.width * 0.150,
                        margin: EdgeInsets.only(top: 10, left: 40),
                        child: TextFormField(
                          validator: (value) {
                            if (value!.isEmpty) {
                              return 'Attention! This is Required';
                            } else {
                              return null;
                            }
                          },
                          onSaved: (value) => ageValue = value,
                          decoration: InputDecoration(
                            border: OutlineInputBorder(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(7))),
                            hintText: "Determines Age of Kidney",
                            contentPadding: const EdgeInsets.symmetric(
                                vertical: 18, horizontal: 14),
                          ),
                        ),
                      ),
                      //Kidney Size
                      Container(
                        width: MediaQuery.of(context).size.width * 0.150,
                        margin: EdgeInsets.only(top: 10, left: 70),
                        child: TextFormField(
                          validator: (value) {
                            if (value!.isEmpty) {
                              return 'Attention! This is Required';
                            } else {
                              return null;
                            }
                          },
                          onSaved: (value) => kidneySize = value,
                          decoration: InputDecoration(
                            border: OutlineInputBorder(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(7))),
                            hintText: "(cm)",
                            contentPadding: const EdgeInsets.symmetric(
                                vertical: 18, horizontal: 14),
                          ),
                        ),
                      ),
                    ],
                  ),

                  //Blood Type and User Type Header
                  Row(
                    children: [
                      Container(
                        margin: EdgeInsets.only(top: 30, left: 40),
                        child: Text(
                          'Blood Type',
                          style: GoogleFonts.roboto(
                              color: Colors.black87,
                              fontSize: 18,
                              fontWeight: FontWeight.w500,
                              letterSpacing: 2),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(top: 30, left: 250),
                        child: Text(
                          'User type',
                          style: GoogleFonts.roboto(
                              color: Colors.black87,
                              fontSize: 18,
                              fontWeight: FontWeight.w500,
                              letterSpacing: 2),
                        ),
                      ),
                    ],
                  ),

                  //Blood Type and User Type Selections
                  Row(
                    children: [
                      //Blood Type
                      Container(
                        width: MediaQuery.of(context).size.width * 0.150,
                        margin: EdgeInsets.only(top: 10, left: 40),
                        child: DropdownButtonHideUnderline(
                          child: DropdownButtonFormField<String>(
                            decoration: InputDecoration(
                                border: OutlineInputBorder(
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(7)))),
                            validator: (value) {
                              if (value == null) {
                                return 'Please Select an Option';
                              }
                            },
                            hint: Text("Select Type of Blood"),
                            value: bloodType,
                            isExpanded: true,
                            items: blood.map(buildBloodTypeMenuItem).toList(),
                            onChanged: (value) =>
                                setState(() => this.bloodType = value),
                          ),
                        ),
                      ),
                      //User Type
                      Container(
                        width: MediaQuery.of(context).size.width * 0.150,
                        margin: EdgeInsets.only(top: 10, left: 70),
                        child: DropdownButtonHideUnderline(
                          child: DropdownButtonFormField<String>(
                            decoration: InputDecoration(
                                border: OutlineInputBorder(
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(7)))),
                            validator: (value) {
                              if (value == null) {
                                return 'Please Specify your Option';
                              }
                            },
                            hint: Text("Specify? (Donor or Patient)"),
                            value: usersValue,
                            isExpanded: true,
                            items: users.map(buildUserMenuItem).toList(),
                            onChanged: (value) =>
                                setState(() => this.usersValue = value),
                          ),
                        ),
                      ),
                    ],
                  ),

                  //Gender
                  Row(
                    children: [
                      Container(
                        margin: EdgeInsets.only(top: 30, left: 40),
                        child: Text(
                          'Gender',
                          style: GoogleFonts.roboto(
                              color: Colors.black87,
                              fontSize: 18,
                              fontWeight: FontWeight.w500,
                              letterSpacing: 2),
                        ),
                      ),
                    ],
                  ),

                  Row(
                    children: [
                      //Gender Selection
                      Container(
                        width: MediaQuery.of(context).size.width * 0.150,
                        margin: EdgeInsets.only(top: 10, left: 40),
                        child: DropdownButtonHideUnderline(
                          child: DropdownButtonFormField<String>(
                            decoration: InputDecoration(
                                border: OutlineInputBorder(
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(7)))),
                            validator: (value) {
                              if (value == null) {
                                return 'Please Select an Option';
                              }
                            },
                            hint: Text("Gender"),
                            value: genderValue,
                            isExpanded: true,
                            items: gender.map(buildGenderMenuItem).toList(),
                            onChanged: (value) =>
                                setState(() => this.genderValue = value),
                          ),
                        ),
                      ),
                    ],
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 50),
                    decoration: BoxDecoration(
                        color: Colors.red[100],
                        borderRadius: BorderRadius.circular(30)),
                    height: 50,
                    width: 400,
                    child: InkWell(
                      onTap: () {
                        final isValid = formKey.currentState!.validate();

                        if (isValid) {
                          formKey.currentState!.save();

                          if (usersValue == 'Patient') {
                            setState(() {
                              patientStatus = "Tagged by Admin (Doctor)";
                              patientTagged();
                              allPatientTagged();
                              Navigator.of(context).pop();
                              showPatientTaggedSuccessToast();
                              logAdminTaggedPatient();
                            });
                          } else if (usersValue == 'Donor') {
                            setState(() {
                              donorStatus = "Tagged by Admin (Doctor)";
                              donorTagged();
                              allDonorTagged();
                              Navigator.of(context).pop();
                              showDonorTaggedSuccessToast();
                              logAdminTaggedDonor();
                            });
                          }
                        }
                      },
                      child: Container(
                        alignment: Alignment.center,
                        child: Text(
                          'Register',
                          style: GoogleFonts.roboto(
                              color: Colors.red[400],
                              fontSize: 18,
                              fontWeight: FontWeight.w500,
                              letterSpacing: 2),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      });

  //Edit Donor and Patient Alert Dialog Method
  void showEditustomDialog(BuildContext context) => showDialog(
      context: context,
      builder: (BuildContext context) {
        return Dialog(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10.0)), //this right here
          child: Container(
            height: MediaQuery.of(context).size.height * 0.835,
            width: MediaQuery.of(context).size.width * 0.4,
            child: Column(
              children: [
                //Close Icon
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    Container(
                      margin: EdgeInsets.only(top: 10, right: 20),
                      child: InkWell(
                        onTap: () {
                          setState(() {
                            isButtonEnabled = false;
                            isFieldEnabled = false;
                            Navigator.of(context).pop();
                          });
                        },
                        child: Icon(
                          Icons.close_outlined,
                          size: 25,
                          color: Colors.red[100],
                        ),
                      ),
                    ),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Container(
                      margin: EdgeInsets.only(left: 40),
                      child: Text(
                        'Edit Information'.toUpperCase(),
                        style: GoogleFonts.roboto(
                            color: Colors.black87,
                            fontSize: 40,
                            fontWeight: FontWeight.w700,
                            letterSpacing: 2),
                      ),
                    ),
                    //Edit Icon
                    Container(
                      margin: EdgeInsets.only(top: 40, left: 20),
                      child: InkWell(
                        onTap: () {
                          setState(() {
                            isButtonEnabled = true;
                            isFieldEnabled = true;
                            showEditEnabledToast();
                            Navigator.of(context).pop();
                            showEditustomDialog(context);
                          });
                        },
                        child: Icon(
                          Icons.edit_outlined,
                          size: 25,
                          color: Colors.red[200],
                        ),
                      ),
                    ),
                  ],
                ),
                //FullName and UserName Header
                Row(
                  children: [
                    Container(
                      margin: EdgeInsets.only(top: 30, left: 40),
                      child: Text(
                        'Full Name (Optional)',
                        style: GoogleFonts.roboto(
                            color: Colors.black87,
                            fontSize: 18,
                            fontWeight: FontWeight.w500,
                            letterSpacing: 2),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 30, left: 150),
                      child: Text(
                        'User Name',
                        style: GoogleFonts.roboto(
                            color: Colors.black87,
                            fontSize: 18,
                            fontWeight: FontWeight.w500,
                            letterSpacing: 2),
                      ),
                    ),
                  ],
                ),
                //FullName and UserName TextFields
                Row(
                  children: [
                    //Fullname
                    Container(
                      width: MediaQuery.of(context).size.width * 0.150,
                      margin: EdgeInsets.only(top: 10, left: 40),
                      child: TextFormField(
                        inputFormatters: [LengthLimitingTextInputFormatter(30)],
                        validator: (value) {
                          return null;
                        },
                        controller: _fullName..text = getFullName!,
                        enabled: isFieldEnabled,
                        decoration: InputDecoration(
                          border: OutlineInputBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(7))),
                          hintText: 'Enter Full Name ',
                        ),
                      ),
                    ),
                    //UseName
                    Container(
                      width: MediaQuery.of(context).size.width * 0.150,
                      margin: EdgeInsets.only(top: 10, left: 70),
                      child: TextFormField(
                        validator: (value) {
                          if (value!.isEmpty) {
                            return 'Attention! This is Required';
                          } else {
                            return null;
                          }
                        },
                        controller: _userName..text = getUserName!,
                        enabled: isFieldEnabled,
                        decoration: InputDecoration(
                          border: OutlineInputBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(7))),
                          hintText: "Enter User Name",
                          contentPadding: const EdgeInsets.symmetric(
                              vertical: 18, horizontal: 14),
                        ),
                      ),
                    ),
                  ],
                ),

                //Email and Mobile Number Header
                Row(
                  children: [
                    Container(
                      margin: EdgeInsets.only(top: 30, left: 40),
                      child: Text(
                        'Email (Optional)',
                        style: GoogleFonts.roboto(
                            color: Colors.black87,
                            fontSize: 18,
                            fontWeight: FontWeight.w500,
                            letterSpacing: 2),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 30, left: 195),
                      child: Text(
                        'Mobile Number',
                        style: GoogleFonts.roboto(
                            color: Colors.black87,
                            fontSize: 18,
                            fontWeight: FontWeight.w500,
                            letterSpacing: 2),
                      ),
                    ),
                  ],
                ),

                //Email and Mobile Number TextFields
                Row(
                  children: [
                    //Email
                    Container(
                      width: MediaQuery.of(context).size.width * 0.150,
                      margin: EdgeInsets.only(top: 10, left: 40),
                      child: TextFormField(
                        inputFormatters: [LengthLimitingTextInputFormatter(30)],
                        validator: (value) {
                          final pattern =
                              r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
                          final regExp = RegExp(pattern);

                          if (value!.isNotEmpty) {
                            if (!regExp.hasMatch(value)) {
                              return 'Enter a valid email';
                            } else {
                              return null;
                            }
                          }
                        },
                        controller: _email..text = getEmail!,
                        enabled: isFieldEnabled,
                        decoration: InputDecoration(
                          border: OutlineInputBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(7))),
                          hintText: 'Enter Email Address',
                        ),
                      ),
                    ),
                    //Mobile Number
                    Container(
                      width: MediaQuery.of(context).size.width * 0.150,
                      margin: EdgeInsets.only(top: 10, left: 70),
                      child: TextFormField(
                        keyboardType: TextInputType.number,
                        inputFormatters: [LengthLimitingTextInputFormatter(11)],
                        validator: (value) {
                          if (value!.isEmpty) {
                            return 'Attention! This is Required';
                          }
                          if (value.length <= 10) {
                            return 'Enter a Valid Mobile Number';
                          } else {
                            return null;
                          }
                        },
                        controller: _mobileNumber..text = getMobileNumber!,
                        enabled: isFieldEnabled,
                        decoration: InputDecoration(
                            border: OutlineInputBorder(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(7))),
                            hintText: 'Enter Mobile Number',
                            contentPadding: const EdgeInsets.symmetric(
                                vertical: 18, horizontal: 14),
                            prefixIcon: Padding(
                              padding: const EdgeInsets.symmetric(
                                  vertical: 12, horizontal: 15),
                              child: Text(
                                "(+63)",
                                style: TextStyle(
                                    color: Colors.black, fontSize: 17),
                              ),
                            )),
                      ),
                    ),
                  ],
                ),

                //Age and Kidney Size Header
                Row(
                  children: [
                    Container(
                      margin: EdgeInsets.only(top: 30, left: 40),
                      child: Text(
                        'Age',
                        style: GoogleFonts.roboto(
                            color: Colors.black87,
                            fontSize: 18,
                            fontWeight: FontWeight.w500,
                            letterSpacing: 2),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 30, left: 320),
                      child: Text(
                        'Kidney Size',
                        style: GoogleFonts.roboto(
                            color: Colors.black87,
                            fontSize: 18,
                            fontWeight: FontWeight.w500,
                            letterSpacing: 2),
                      ),
                    ),
                  ],
                ),

                //Age and kidney Size TextFields
                Row(
                  children: [
                    //Age
                    Container(
                      width: MediaQuery.of(context).size.width * 0.150,
                      margin: EdgeInsets.only(top: 10, left: 40),
                      child: TextFormField(
                        validator: (value) {
                          if (value!.isEmpty) {
                            return 'Attention! This is Required';
                          } else {
                            return null;
                          }
                        },
                        controller: _age..text = getAge!,
                        enabled: isFieldEnabled,
                        decoration: InputDecoration(
                          border: OutlineInputBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(7))),
                          hintText: "Determines Age of Kidney",
                          contentPadding: const EdgeInsets.symmetric(
                              vertical: 18, horizontal: 14),
                        ),
                      ),
                    ),
                    //Kidney Size
                    Container(
                      width: MediaQuery.of(context).size.width * 0.150,
                      margin: EdgeInsets.only(top: 10, left: 70),
                      child: TextFormField(
                        validator: (value) {
                          if (value!.isEmpty) {
                            return 'Attention! This is Required';
                          } else {
                            return null;
                          }
                        },
                        controller: _kidneySize..text = getKidneySize!,
                        enabled: isFieldEnabled,
                        decoration: InputDecoration(
                          border: OutlineInputBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(7))),
                          hintText: "(cm)",
                          contentPadding: const EdgeInsets.symmetric(
                              vertical: 18, horizontal: 14),
                        ),
                      ),
                    ),
                  ],
                ),

                //Blood Type and User Type Header
                Row(
                  children: [
                    Container(
                      margin: EdgeInsets.only(top: 30, left: 40),
                      child: Text(
                        'Blood Type',
                        style: GoogleFonts.roboto(
                            color: Colors.black87,
                            fontSize: 18,
                            fontWeight: FontWeight.w500,
                            letterSpacing: 2),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 30, left: 250),
                      child: Text(
                        'User type',
                        style: GoogleFonts.roboto(
                            color: Colors.black87,
                            fontSize: 18,
                            fontWeight: FontWeight.w500,
                            letterSpacing: 2),
                      ),
                    ),
                  ],
                ),

                //Blood Type and User Type Selections
                Row(
                  children: [
                    //Blood Type
                    Container(
                      width: MediaQuery.of(context).size.width * 0.150,
                      margin: EdgeInsets.only(top: 10, left: 40),
                      child: InkWell(
                        onTap: isButtonEnabled
                            ? () {
                                showModalBottomSheet(
                                  context: context,
                                  builder: (context) => buildBloodTypeSheet(),
                                );
                              }
                            : null,
                        child: TextFormField(
                          enabled: false,
                          validator: (value) {
                            if (value!.isEmpty) {
                              return 'Attention! This is Required';
                            } else {
                              return null;
                            }
                          },
                          controller: _bloodType..text = getBloodType!,
                          decoration: InputDecoration(
                            border: OutlineInputBorder(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(7))),
                            hintText: "",
                            contentPadding: const EdgeInsets.symmetric(
                                vertical: 18, horizontal: 14),
                          ),
                        ),
                      ),
                    ),
                    //user Type
                    Container(
                      width: MediaQuery.of(context).size.width * 0.150,
                      margin: EdgeInsets.only(top: 10, left: 70),
                      child: InkWell(
                        onTap: isButtonEnabled ? () {} : null,
                        child: TextFormField(
                          enabled: false,
                          validator: (value) {
                            if (value!.isEmpty) {
                              return 'Attention! This is Required';
                            } else {
                              return null;
                            }
                          },
                          controller: _userType..text = getUserType!,
                          decoration: InputDecoration(
                            border: OutlineInputBorder(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(7))),
                            hintText: "Specify? Donor or Patient",
                            contentPadding: const EdgeInsets.symmetric(
                                vertical: 18, horizontal: 14),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),

                //Gender
                Row(
                  children: [
                    Container(
                      margin: EdgeInsets.only(top: 30, left: 40),
                      child: Text(
                        'Gender',
                        style: GoogleFonts.roboto(
                            color: Colors.black87,
                            fontSize: 18,
                            fontWeight: FontWeight.w500,
                            letterSpacing: 2),
                      ),
                    ),
                  ],
                ),

                Row(
                  children: [
                    //Gender Selection
                    Container(
                      width: MediaQuery.of(context).size.width * 0.150,
                      margin: EdgeInsets.only(top: 10, left: 40),
                      child: InkWell(
                        onTap: isButtonEnabled
                            ? () {
                                showModalBottomSheet(
                                  context: context,
                                  builder: (context) => buildGenderSheet(),
                                );
                              }
                            : null,
                        child: TextFormField(
                          enabled: false,
                          validator: (value) {
                            if (value!.isEmpty) {
                              return 'Attention! This is Required';
                            } else {
                              return null;
                            }
                          },
                          controller: _gender..text = getGender!,
                          decoration: InputDecoration(
                            border: OutlineInputBorder(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(7))),
                            hintText: "Select Gender  ",
                            contentPadding: const EdgeInsets.symmetric(
                                vertical: 18, horizontal: 14),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    //Cancel
                    Container(
                      margin: EdgeInsets.only(top: 50),
                      height: 50,
                      width: 200,
                      child: ElevatedButton(
                        style: ElevatedButton.styleFrom(
                          primary: Colors.red[50],
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(30),
                          ),
                        ),
                        onPressed: isButtonEnabled
                            ? () {
                                setState(() {
                                  isFieldEnabled = false;
                                  isButtonEnabled = false;
                                  Navigator.of(context).pop();
                                  showEditustomDialog(context);
                                  showEditCancelToast();
                                });
                              }
                            : null,
                        child: Text('Cancel Edit',
                            style: GoogleFonts.roboto(
                                color: Colors.grey,
                                fontSize: 18,
                                fontWeight: FontWeight.w700,
                                letterSpacing: 2)),
                      ),
                    ),
                    //Save
                    Container(
                      margin: EdgeInsets.only(top: 50),
                      height: 50,
                      width: 200,
                      child: ElevatedButton(
                        style: ElevatedButton.styleFrom(
                          primary: Colors.red[100],
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(30),
                          ),
                        ),
                        onPressed: isButtonEnabled
                            ? () {
                                //For Registered Users
                                if (getUserType == 'I am a Donor') {
                                  setState(() {
                                    saveDonorData();
                                    saveUserDataToAll();
                                    showEditSuccessToast();
                                    Navigator.of(context).pop();
                                    isFieldEnabled = false;
                                    isButtonEnabled = false;
                                    logAdminEditDonor();
                                  });
                                } else if (getUserType == 'I am a Patient') {
                                  setState(() {
                                    savePatientData();
                                    saveUserDataToAll();
                                    showEditSuccessToast();
                                    Navigator.of(context).pop();
                                    isFieldEnabled = false;
                                    isButtonEnabled = false;
                                    logAdminEditPatient();
                                  });
                                }
                                //For Tagged Users
                                else if (getUserType == 'Donor') {
                                  setState(() {
                                    saveTaggedDonor();
                                    saveTaggedUserToAll();
                                    showEditSuccessToast();
                                    Navigator.of(context).pop();
                                    isFieldEnabled = false;
                                    isButtonEnabled = false;
                                    logAdminEditTaggedDonor();
                                  });
                                } else if (getUserType == 'Patient') {
                                  setState(() {
                                    saveTaggedPatient();
                                    saveTaggedUserToAll();
                                    showEditSuccessToast();
                                    Navigator.of(context).pop();
                                    isFieldEnabled = false;
                                    isButtonEnabled = false;
                                    logAdminEditTaggedPatient();
                                  });
                                }
                              }
                            : null,
                        child: Text('Save',
                            style: GoogleFonts.roboto(
                                color: Colors.red[400],
                                fontSize: 18,
                                fontWeight: FontWeight.w700,
                                letterSpacing: 2)),
                      ),
                    ),
                  ],
                )
              ],
            ),
          ),
        );
      });

  //Mobile Number Hashed Dialog
  void showHashedMobileNumberCustomDialog(BuildContext context) => showDialog(
      context: context,
      builder: (BuildContext context) {
        return Dialog(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10.0)), //this right here
          child: Container(
            height: MediaQuery.of(context).size.height * 0.255,
            width: MediaQuery.of(context).size.width * 0.2,
            child: Form(
              key: formKey,
              child: Column(
                children: [
                  //Close Icon
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      Container(
                        margin: EdgeInsets.only(top: 10, right: 20),
                        child: InkWell(
                          onTap: () {
                            setState(() {
                              Navigator.of(context).pop();
                              _viewMobileNumberPassword.text = "";
                            });
                          },
                          child: Icon(
                            Icons.close_outlined,
                            size: 25,
                            color: Colors.red[100],
                          ),
                        ),
                      ),
                    ],
                  ),
                  //Header
                  Container(
                    child: Text('Password is required to see the content',
                        style: GoogleFonts.roboto(
                            color: Colors.black87,
                            fontSize: 14,
                            fontWeight: FontWeight.w700,
                            letterSpacing: 1)),
                  ),
                  Container(
                    margin: EdgeInsets.only(left: 60, right: 60, top: 20),
                    child: TextFormField(
                      obscureText: isHiddenPassword,
                      validator: (value) {
                        if (value!.isEmpty) {
                          return 'Password is Required!';
                        } else if (_password.text != viewHashedPassword) {
                          return 'Wrong Password!';
                        }
                      },
                      controller: _password..text = "",
                      decoration: InputDecoration(
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.all(Radius.circular(7))),
                        hintText: "Password",
                        contentPadding: const EdgeInsets.symmetric(
                            vertical: 18, horizontal: 14),
                      ),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 20),
                    decoration: BoxDecoration(
                        color: Colors.red[50],
                        borderRadius: BorderRadius.circular(10)),
                    padding: EdgeInsets.all(10),
                    child: InkWell(
                      onTap: () {
                        final isValid = formKey.currentState!.validate();

                        if (isValid) {
                          if (_password.text == viewHashedPassword) {
                            setState(() {
                              _viewMobileNumberPassword.text =
                                  "Mobile Number: " + getMobileNumber!;
                              _password.text = "";
                            });
                          } else {
                            setState(() {
                              _password.text = "";
                            });
                          }
                        }
                      },
                      child: Text('Enter',
                          style: GoogleFonts.roboto(
                            color: Colors.red[400],
                            fontSize: 12,
                            fontWeight: FontWeight.w300,
                          )),
                    ),
                  ),

                  Container(
                    margin: EdgeInsets.only(left: 50, right: 50),
                    child: TextFormField(
                      decoration: InputDecoration(
                        filled: true,
                        fillColor: Colors.white,
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10.0),
                          borderSide: BorderSide.none,
                        ),
                      ),
                      enabled: false,
                      textAlign: TextAlign.center,
                      controller: _viewMobileNumberPassword
                        ..text = _viewMobileNumberPassword.text,
                    ),
                  )
                ],
              ),
            ),
          ),
        );
      });

  //Email Address Hashed Dialog
  void showHashedEmailAddressCustomDialog(BuildContext context) => showDialog(
      context: context,
      builder: (BuildContext context) {
        return Dialog(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10.0)), //this right here
          child: Container(
            height: MediaQuery.of(context).size.height * 0.255,
            width: MediaQuery.of(context).size.width * 0.2,
            child: Form(
              key: formKey,
              child: Column(
                children: [
                  //Close Icon
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      Container(
                        margin: EdgeInsets.only(top: 10, right: 20),
                        child: InkWell(
                          onTap: () {
                            setState(() {
                              Navigator.of(context).pop();
                              _viewEmailAddressPassword.text = "";
                            });
                          },
                          child: Icon(
                            Icons.close_outlined,
                            size: 25,
                            color: Colors.red[100],
                          ),
                        ),
                      ),
                    ],
                  ),
                  //Header
                  Container(
                    child: Text('Password is required to see the content',
                        style: GoogleFonts.roboto(
                            color: Colors.black87,
                            fontSize: 14,
                            fontWeight: FontWeight.w700,
                            letterSpacing: 1)),
                  ),
                  Container(
                    margin: EdgeInsets.only(left: 60, right: 60, top: 20),
                    child: TextFormField(
                      obscureText: isHiddenPassword,
                      validator: (value) {
                        if (value!.isEmpty) {
                          return 'Password is Required!';
                        } else if (_password.text != viewHashedPassword) {
                          return 'Wrong Password!';
                        }
                      },
                      controller: _password..text = "",
                      decoration: InputDecoration(
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.all(Radius.circular(7))),
                        hintText: "Password",
                        contentPadding: const EdgeInsets.symmetric(
                            vertical: 18, horizontal: 14),
                      ),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 20),
                    decoration: BoxDecoration(
                        color: Colors.red[50],
                        borderRadius: BorderRadius.circular(10)),
                    padding: EdgeInsets.all(10),
                    child: InkWell(
                      onTap: () {
                        final isValid = formKey.currentState!.validate();

                        if (isValid) {
                          if (_password.text == viewHashedPassword) {
                            setState(() {
                              _viewEmailAddressPassword.text =
                                  "Email Address: " + getEmail!;
                              _password.text = "";
                            });
                          } else {
                            setState(() {
                              _password.text = "";
                            });
                          }
                        }
                      },
                      child: Text('Enter',
                          style: GoogleFonts.roboto(
                            color: Colors.red[400],
                            fontSize: 12,
                            fontWeight: FontWeight.w300,
                          )),
                    ),
                  ),

                  Container(
                    margin: EdgeInsets.only(left: 50, right: 50),
                    child: TextFormField(
                      decoration: InputDecoration(
                        filled: true,
                        fillColor: Colors.white,
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10.0),
                          borderSide: BorderSide.none,
                        ),
                      ),
                      enabled: false,
                      textAlign: TextAlign.center,
                      controller: _viewEmailAddressPassword
                        ..text = _viewEmailAddressPassword.text,
                      maxLines: 2,
                    ),
                  )
                ],
              ),
            ),
          ),
        );
      });

  //Edit Update Data
  saveUserDataToAll() async {
    String? uid = getUid;
    await FirebaseFirestore.instance
        .collection("Users")
        .doc("All")
        .collection("Lists")
        .doc(uid)
        .update({
      'FullName': _fullName.text,
      'UserName': _userName.text,
      'Email Address': _email.text,
      'Gender': _gender.text,
      'Age': _age.text,
      'User Type': _userType.text,
      'Blood Type': _bloodType.text,
      'Kidney Size': _kidneySize.text,
      'Mobile Number': _mobileNumber.text,
    });
  }

  saveDonorData() async {
    String? uid = getUid;
    await FirebaseFirestore.instance
        .collection("Users")
        .doc('Donor')
        .collection('Lists')
        .doc(uid)
        .update({
      'FullName': _fullName.text,
      'UserName': _userName.text,
      'Email Address': _email.text,
      'Gender': _gender.text,
      'Age': _age.text,
      'User Type': _userType.text,
      'Blood Type': _bloodType.text,
      'Kidney Size': _kidneySize.text,
      'Mobile Number': _mobileNumber.text,
    });
  }

  savePatientData() async {
    String? uid = getUid;
    await FirebaseFirestore.instance
        .collection("Users")
        .doc('Patient')
        .collection('Lists')
        .doc(uid)
        .update({
      'FullName': _fullName.text,
      'UserName': _userName.text,
      'Email Address': _email.text,
      'Gender': _gender.text,
      'Age': _age.text,
      'User Type': _userType.text,
      'Blood Type': _bloodType.text,
      'Kidney Size': _kidneySize.text,
      'Mobile Number': _mobileNumber.text,
    });
  }

  saveTaggedUserToAll() async {
    String? uid = getUserName;
    await FirebaseFirestore.instance
        .collection("Tagged Users")
        .doc('All Tagged')
        .collection('Lists')
        .doc(uid)
        .update({
      'FullName': _fullName.text,
      'UserName': _userName.text,
      'Email Address': _email.text,
      'Gender': _gender.text,
      'Age': _age.text,
      'User Type': _userType.text,
      'Blood Type': _bloodType.text,
      'Kidney Size': _kidneySize.text,
      'Mobile Number': _mobileNumber.text,
    });
  }

  saveTaggedDonor() async {
    String? uid = getUserName;
    await FirebaseFirestore.instance
        .collection("Tagged Users")
        .doc('Donor')
        .collection('Lists')
        .doc(uid)
        .update({
      'FullName': _fullName.text,
      'UserName': _userName.text,
      'Email Address': _email.text,
      'Gender': _gender.text,
      'Age': _age.text,
      'User Type': _userType.text,
      'Blood Type': _bloodType.text,
      'Kidney Size': _kidneySize.text,
      'Mobile Number': _mobileNumber.text,
    });
  }

  saveTaggedPatient() async {
    String? uid = getUserName;
    await FirebaseFirestore.instance
        .collection("Tagged Users")
        .doc('Patient')
        .collection('Lists')
        .doc(uid)
        .update({
      'FullName': _fullName.text,
      'UserName': _userName.text,
      'Email Address': _email.text,
      'Gender': _gender.text,
      'Age': _age.text,
      'User Type': _userType.text,
      'Blood Type': _bloodType.text,
      'Kidney Size': _kidneySize.text,
      'Mobile Number': _mobileNumber.text,
    });
  }

  //Archive User Data
  archiveDonor() async {
    String? uid = getUid;
    await FirebaseFirestore.instance
        .collection("Archived")
        .doc('Users')
        .collection('Registered')
        .doc('Donor')
        .collection('Lists')
        .doc(uid)
        .set({
      'UserID': getUid,
      'FullName': getFullName,
      'UserName': getUserName,
      'Email Address': getEmail,
      'Gender': getGender,
      'Age': getAge,
      'User Type': getUserType,
      'Blood Type': getBloodType,
      'Kidney Size': getKidneySize,
      'Mobile Number': getMobileNumber,
      'Status': getStatus,
      'Signature': getSignature,
      'Date Registered': getRegisteredDate,
      'Time Registered': getRegisteredTime,
      'Views': getViews,
      'Comments': getComments,
      'Likes': getLikes,
      'Pinned': getPinned,
      'Donated': getDonated,
      'Received': getReceived,
      'Posts': getPosts,
      'Notifications': getNotifications,
      'Replies': getReplies,
      'Match': getMatch,
      'Mail': getMail
    });
  }

  archivePatient() async {
    String? uid = getUid;
    await FirebaseFirestore.instance
        .collection("Archived")
        .doc('Users')
        .collection('Registered')
        .doc('Patient')
        .collection('Lists')
        .doc(uid)
        .set({
      'UserID': getUid,
      'FullName': getFullName,
      'UserName': getUserName,
      'Email Address': getEmail,
      'Gender': getGender,
      'Age': getAge,
      'User Type': getUserType,
      'Blood Type': getBloodType,
      'Kidney Size': getKidneySize,
      'Mobile Number': getMobileNumber,
      'Status': getStatus,
      'Signature': getSignature,
      'Date Registered': getRegisteredDate,
      'Time Registered': getRegisteredTime,
      'Views': getViews,
      'Comments': getComments,
      'Likes': getLikes,
      'Pinned': getPinned,
      'Donated': getDonated,
      'Received': getReceived,
      'Posts': getPosts,
      'Notifications': getNotifications,
      'Replies': getReplies,
      'Match': getMatch,
      'Mail': getMail
    });
  }

  //Both Donor and Patient
  archiveAll() async {
    String? uid = getUid;
    await FirebaseFirestore.instance
        .collection("Archived")
        .doc('Users')
        .collection('Registered')
        .doc('All')
        .collection('Lists')
        .doc(uid)
        .set({
      'UserID': getUid,
      'FullName': getFullName,
      'UserName': getUserName,
      'Email Address': getEmail,
      'Gender': getGender,
      'Age': getAge,
      'User Type': getUserType,
      'Blood Type': getBloodType,
      'Kidney Size': getKidneySize,
      'Mobile Number': getMobileNumber,
      'Status': getStatus,
      'Signature': getSignature,
      'Date Registered': getRegisteredDate,
      'Time Registered': getRegisteredTime,
      'Views': getViews,
      'Comments': getComments,
      'Likes': getLikes,
      'Pinned': getPinned,
      'Donated': getDonated,
      'Received': getReceived,
      'Posts': getPosts,
      'Notifications': getNotifications,
      'Replies': getReplies,
      'Match': getMatch,
      'Mail': getMail
    });
  }

  archiveTaggedDonor() async {
    String? uid = getUserName;
    await FirebaseFirestore.instance
        .collection("Archived")
        .doc('Users')
        .collection('Tagged Users')
        .doc('Donor')
        .collection('Lists')
        .doc(uid)
        .set({
      'UserID': getUserName,
      'FullName': getFullName,
      'UserName': getUserName,
      'Email Address': getEmail,
      'Gender': getGender,
      'Age': getAge,
      'User Type': getUserType,
      'Blood Type': getBloodType,
      'Kidney Size': getKidneySize,
      'Mobile Number': getMobileNumber,
      'Status': getStatus,
      'Signature': getSignature,
      'Date Registered': getRegisteredDate,
      'Time Registered': getRegisteredTime,
      'Views': getViews,
      'Comments': getComments,
      'Likes': getLikes,
      'Pinned': getPinned,
      'Donated': getDonated,
      'Received': getReceived,
      'Posts': getPosts,
      'Notifications': getNotifications,
      'Replies': getReplies,
      'Match': getMatch,
      'Mail': getMail
    });
  }

  archiveTaggedPatient() async {
    String? uid = getUserName;
    await FirebaseFirestore.instance
        .collection("Archived")
        .doc('Users')
        .collection('Tagged Users')
        .doc('Patient')
        .collection('Lists')
        .doc(uid)
        .set({
      'UserID': getUserName,
      'FullName': getFullName,
      'UserName': getUserName,
      'Email Address': getEmail,
      'Gender': getGender,
      'Age': getAge,
      'User Type': getUserType,
      'Blood Type': getBloodType,
      'Kidney Size': getKidneySize,
      'Mobile Number': getMobileNumber,
      'Status': getStatus,
      'Signature': getSignature,
      'Date Registered': getRegisteredDate,
      'Time Registered': getRegisteredTime,
      'Views': getViews,
      'Comments': getComments,
      'Likes': getLikes,
      'Pinned': getPinned,
      'Donated': getDonated,
      'Received': getReceived,
      'Posts': getPosts,
      'Notifications': getNotifications,
      'Replies': getReplies,
      'Match': getMatch,
      'Mail': getMail
    });
  }

  archiveAllTagged() async {
    String? uid = getUserName;
    await FirebaseFirestore.instance
        .collection("Archived")
        .doc('Users')
        .collection('Tagged Users')
        .doc('All')
        .collection('Lists')
        .doc(uid)
        .set({
      'UserID': getUserName,
      'FullName': getFullName,
      'UserName': getUserName,
      'Email Address': getEmail,
      'Gender': getGender,
      'Age': getAge,
      'User Type': getUserType,
      'Blood Type': getBloodType,
      'Kidney Size': getKidneySize,
      'Mobile Number': getMobileNumber,
      'Status': getStatus,
      'Signature': getSignature,
      'Date Registered': getRegisteredDate,
      'Time Registered': getRegisteredTime,
      'Views': getViews,
      'Comments': getComments,
      'Likes': getLikes,
      'Pinned': getPinned,
      'Donated': getDonated,
      'Received': getReceived,
      'Posts': getPosts,
      'Notifications': getNotifications,
      'Replies': getReplies,
      'Match': getMatch,
      'Mail': getMail
    });
  }

  //Delete Temporary
  archiveDeleteDonor() async {
    String? uid = getUid;
    await FirebaseFirestore.instance
        .collection("Users")
        .doc('Donor')
        .collection('Lists')
        .doc(uid)
        .delete();
  }

  archiveDeletePatient() async {
    String? uid = getUid;
    await FirebaseFirestore.instance
        .collection("Users")
        .doc('Patient')
        .collection('Lists')
        .doc(uid)
        .delete();
  }

  //Both Donor and Patient
  archiveDeleteAll() async {
    String? uid = getUid;
    await FirebaseFirestore.instance
        .collection("Users")
        .doc('All')
        .collection('Lists')
        .doc(uid)
        .delete();
  }

  archiveDeleteTaggedDonor() async {
    String? uid = getUserName;
    await FirebaseFirestore.instance
        .collection("Tagged Users")
        .doc('Donor')
        .collection('Lists')
        .doc(uid)
        .delete();
  }

  archiveDeleteTaggedPatient() async {
    String? uid = getUserName;
    await FirebaseFirestore.instance
        .collection("Tagged Users")
        .doc('Patient')
        .collection('Lists')
        .doc(uid)
        .delete();
  }

  archiveDeleteAllTagged() async {
    String? uid = getUserName;
    await FirebaseFirestore.instance
        .collection('Tagged Users')
        .doc('All Tagged')
        .collection('Lists')
        .doc(uid)
        .delete();
  }

  // Bottom Sheet For Gender
  Widget buildGenderSheet() => Container(
        height: 250,
        color: Color(0xffe5eef8).withOpacity(0.9),
        child: Container(
          margin: EdgeInsets.symmetric(horizontal: 600),
          color: Colors.red[100],
          child: ListView(
            children: [
              //Header Gender
              ListTile(
                title: Text(
                  'Gender',
                  style: GoogleFonts.roboto(
                      color: Colors.black87,
                      fontSize: 15,
                      fontWeight: FontWeight.w500),
                ),
              ),
              //Selection
              ListTile(
                title: Text(
                  'Male',
                  style:
                      GoogleFonts.roboto(color: Colors.black87, fontSize: 15),
                ),
                onTap: () {
                  if (getUserType == "I am a Donor") {
                    _gender..text = "Male";
                    Navigator.of(context).pop();
                  } else if (getUserType == "I am a Patient") {
                    _gender..text = "Male";
                    Navigator.of(context).pop();
                  } else if (getUserType == "Donor") {
                    _gender..text = "Male";
                    Navigator.of(context).pop();
                  } else if (getUserType == "Donor") {
                    _gender..text = "Male";
                    Navigator.of(context).pop();
                  }
                },
              ),

              ListTile(
                title: Text(
                  'Female',
                  style:
                      GoogleFonts.roboto(color: Colors.black87, fontSize: 15),
                ),
                onTap: () {
                  if (getUserType == "I am a Donor") {
                    _gender..text = "Female";
                    Navigator.of(context).pop();
                  } else if (getUserType == "I am a Patient") {
                    _gender..text = "Female";
                    Navigator.of(context).pop();
                  } else if (getUserType == "Donor") {
                    _gender..text = "Female";
                    Navigator.of(context).pop();
                  } else if (getUserType == "Donor") {
                    _gender..text = "Female";
                    Navigator.of(context).pop();
                  }
                },
              ),
              ListTile(
                title: Text(
                  'Other',
                  style:
                      GoogleFonts.roboto(color: Colors.black87, fontSize: 15),
                ),
                onTap: () {
                  if (getUserType == "I am a Donor") {
                    _gender..text = "Other";
                    Navigator.of(context).pop();
                  } else if (getUserType == "I am a Patient") {
                    _gender..text = "Other";
                    Navigator.of(context).pop();
                  } else if (getUserType == "Donor") {
                    _gender..text = "Other";
                    Navigator.of(context).pop();
                  } else if (getUserType == "Donor") {
                    _gender..text = "Other";
                    Navigator.of(context).pop();
                  }
                },
              ),
              ListTile(
                title: Text(
                  'Prefer not to say',
                  style:
                      GoogleFonts.roboto(color: Colors.black87, fontSize: 15),
                ),
                onTap: () {
                  if (getUserType == "I am a Donor") {
                    _gender..text = "Prefer not to say";
                    Navigator.of(context).pop();
                  } else if (getUserType == "I am a Patient") {
                    _gender..text = "Prefer not to say";
                    Navigator.of(context).pop();
                  } else if (getUserType == "Donor") {
                    _gender..text = "Prefer not to say";
                    Navigator.of(context).pop();
                  } else if (getUserType == "Donor") {
                    _gender..text = "Prefer not to say";
                    Navigator.of(context).pop();
                  }
                },
              ),
            ],
          ),
        ),
      );

// Bottom Sheet For Blood Type
  Widget buildBloodTypeSheet() => Container(
        height: 300,
        color: Color(0xffe5eef8).withOpacity(0.9),
        child: Container(
          margin: EdgeInsets.symmetric(horizontal: 600),
          color: Colors.red[100],
          child: ListView(
            children: [
              //Header Blood type
              ListTile(
                title: Text(
                  'Choose your blood type',
                  style: GoogleFonts.roboto(
                      color: Colors.black87,
                      fontSize: 15,
                      fontWeight: FontWeight.w500),
                ),
              ),
              //Selection
              ListTile(
                title: Text(
                  'A+',
                  style:
                      GoogleFonts.roboto(color: Colors.black87, fontSize: 15),
                ),
                onTap: () {
                  if (getUserType == "I am a Donor") {
                    _bloodType.text = 'A+';
                    Navigator.of(context).pop();
                  } else if (getUserType == "I am a Patient") {
                    _bloodType.text = 'A+';
                    Navigator.of(context).pop();
                  } else if (getUserType == "Donor") {
                    _bloodType.text = 'A+';
                    Navigator.of(context).pop();
                  } else if (getUserType == "Patient") {
                    _bloodType.text = 'A+';
                    Navigator.of(context).pop();
                  }
                },
              ),
              ListTile(
                title: Text(
                  'A-',
                  style:
                      GoogleFonts.roboto(color: Colors.black87, fontSize: 15),
                ),
                onTap: () {
                  if (getUserType == "I am a Donor") {
                    _bloodType.text = 'A-';
                    Navigator.of(context).pop();
                  } else if (getUserType == "I am a Patient") {
                    _bloodType.text = 'A-';
                    Navigator.of(context).pop();
                  } else if (getUserType == "Donor") {
                    _bloodType.text = 'A-';
                    Navigator.of(context).pop();
                  } else if (getUserType == "Patient") {
                    _bloodType.text = 'A-';
                    Navigator.of(context).pop();
                  }
                },
              ),
              ListTile(
                title: Text(
                  'B+',
                  style:
                      GoogleFonts.roboto(color: Colors.black87, fontSize: 15),
                ),
                onTap: () {
                  if (getUserType == "I am a Donor") {
                    _bloodType.text = 'B+';
                    Navigator.of(context).pop();
                  } else if (getUserType == "I am a Patient") {
                    _bloodType.text = 'B+';
                    Navigator.of(context).pop();
                  } else if (getUserType == "Donor") {
                    _bloodType.text = 'B+';
                    Navigator.of(context).pop();
                  } else if (getUserType == "Patient") {
                    _bloodType.text = 'B+';
                    Navigator.of(context).pop();
                  }
                },
              ),
              ListTile(
                title: Text(
                  'B-',
                  style:
                      GoogleFonts.roboto(color: Colors.black87, fontSize: 15),
                ),
                onTap: () {
                  if (getUserType == "I am a Donor") {
                    _bloodType.text = 'B-';
                    Navigator.of(context).pop();
                  } else if (getUserType == "I am a Patient") {
                    _bloodType.text = 'B-';
                    Navigator.of(context).pop();
                  } else if (getUserType == "Donor") {
                    _bloodType.text = 'B-';
                    Navigator.of(context).pop();
                  } else if (getUserType == "Patient") {
                    _bloodType.text = 'B-';
                    Navigator.of(context).pop();
                  }
                },
              ),
              ListTile(
                title: Text(
                  'AB+',
                  style:
                      GoogleFonts.roboto(color: Colors.black87, fontSize: 15),
                ),
                onTap: () {
                  if (getUserType == "I am a Donor") {
                    _bloodType.text = 'AB+';
                    Navigator.of(context).pop();
                  } else if (getUserType == "I am a Patient") {
                    _bloodType.text = 'AB+';
                    Navigator.of(context).pop();
                  } else if (getUserType == "Donor") {
                    _bloodType.text = 'AB+';
                    Navigator.of(context).pop();
                  } else if (getUserType == "Patient") {
                    _bloodType.text = 'AB+';
                    Navigator.of(context).pop();
                  }
                },
              ),
              ListTile(
                title: Text(
                  'AB-',
                  style:
                      GoogleFonts.roboto(color: Colors.black87, fontSize: 15),
                ),
                onTap: () {
                  if (getUserType == "I am a Donor") {
                    _bloodType.text = 'AB-';
                    Navigator.of(context).pop();
                  } else if (getUserType == "I am a Patient") {
                    _bloodType.text = 'AB-';
                    Navigator.of(context).pop();
                  } else if (getUserType == "Donor") {
                    _bloodType.text = 'AB-';
                    Navigator.of(context).pop();
                  } else if (getUserType == "Patient") {
                    _bloodType.text = 'AB-';
                    Navigator.of(context).pop();
                  }
                },
              ),
              ListTile(
                title: Text(
                  'O+',
                  style:
                      GoogleFonts.roboto(color: Colors.black87, fontSize: 15),
                ),
                onTap: () {
                  if (getUserType == "I am a Donor") {
                    _bloodType.text = 'O+';
                    Navigator.of(context).pop();
                  } else if (getUserType == "I am a Patient") {
                    _bloodType.text = 'O+';
                    Navigator.of(context).pop();
                  } else if (getUserType == "Donor") {
                    _bloodType.text = 'O+';
                    Navigator.of(context).pop();
                  } else if (getUserType == "Patient") {
                    _bloodType.text = 'O+';
                    Navigator.of(context).pop();
                  }
                },
              ),
              ListTile(
                title: Text(
                  'O-',
                  style:
                      GoogleFonts.roboto(color: Colors.black87, fontSize: 15),
                ),
                onTap: () {
                  if (getUserType == "I am a Donor") {
                    _bloodType.text = 'O-';
                    Navigator.of(context).pop();
                  } else if (getUserType == "I am a Patient") {
                    _bloodType.text = 'O-';
                    Navigator.of(context).pop();
                  } else if (getUserType == "Donor") {
                    _bloodType.text = 'O-';
                    Navigator.of(context).pop();
                  } else if (getUserType == "Patient") {
                    _bloodType.text = 'O-';
                    Navigator.of(context).pop();
                  }
                },
              ),
            ],
          ),
        ),
      );

  //Logs in the database
  logAdminTaggedDonor() async {
    String formattedTime = DateFormat('kk:mm:s:a').format(now);
    String formattedDate = formatter.format(now);
    await FirebaseFirestore.instance.collection("Logs").add({
      'Message': "Admin Tagged Donor Successfull!",
      'User': "Official Admin",
      'Logged Date': formattedDate,
      'Logged Time': formattedTime,
    });
  }

  logAdminTaggedPatient() async {
    String formattedTime = DateFormat('kk:mm:s:a').format(now);
    String formattedDate = formatter.format(now);
    await FirebaseFirestore.instance.collection("Logs").add({
      'Message': "Admin Tagged Patient Successfull!",
      'User': "Official Admin",
      'Logged Date': formattedDate,
      'Logged Time': formattedTime,
    });
  }

  logAdminArchivedDonor() async {
    String formattedTime = DateFormat('kk:mm:s:a').format(now);
    String formattedDate = formatter.format(now);
    await FirebaseFirestore.instance.collection("Logs").add({
      'Message': "Admin has archive donor " + getUserName!,
      'User': "Official Admin",
      'Logged Date': formattedDate,
      'Logged Time': formattedTime,
    });
  }

  logAdminArchivedPatient() async {
    String formattedTime = DateFormat('kk:mm:s:a').format(now);
    String formattedDate = formatter.format(now);
    await FirebaseFirestore.instance.collection("Logs").add({
      'Message': "Admin has archive patient " + getUserName!,
      'User': "Official Admin",
      'Logged Date': formattedDate,
      'Logged Time': formattedTime,
    });
  }

  logAdminArchivedTaggedDonor() async {
    String formattedTime = DateFormat('kk:mm:s:a').format(now);
    String formattedDate = formatter.format(now);
    await FirebaseFirestore.instance.collection("Logs").add({
      'Message': "Admin has archive tagged donor " + getUserName!,
      'User': "Official Admin",
      'Logged Date': formattedDate,
      'Logged Time': formattedTime,
    });
  }

  logAdminArchivedTaggedPatient() async {
    String formattedTime = DateFormat('kk:mm:s:a').format(now);
    String formattedDate = formatter.format(now);
    await FirebaseFirestore.instance.collection("Logs").add({
      'Message': "Admin has archive tagged patient " + getUserName!,
      'User': "Official Admin",
      'Logged Date': formattedDate,
      'Logged Time': formattedTime,
    });
  }

  logAdminEditDonor() async {
    String formattedTime = DateFormat('kk:mm:s:a').format(now);
    String formattedDate = formatter.format(now);
    await FirebaseFirestore.instance.collection("Logs").add({
      'Message': "Admin had edit donor " + getUserName!,
      'User': "Official Admin",
      'Logged Date': formattedDate,
      'Logged Time': formattedTime,
    });
  }

  logAdminEditPatient() async {
    String formattedTime = DateFormat('kk:mm:s:a').format(now);
    String formattedDate = formatter.format(now);
    await FirebaseFirestore.instance.collection("Logs").add({
      'Message': "Admin had edit patient " + getUserName!,
      'User': "Official Admin",
      'Logged Date': formattedDate,
      'Logged Time': formattedTime,
    });
  }

  logAdminEditTaggedDonor() async {
    String formattedTime = DateFormat('kk:mm:s:a').format(now);
    String formattedDate = formatter.format(now);
    await FirebaseFirestore.instance.collection("Logs").add({
      'Message': "Admin had edit tagged donor " + getUserName!,
      'User': "Official Admin",
      'Logged Date': formattedDate,
      'Logged Time': formattedTime,
    });
  }

  logAdminEditTaggedPatient() async {
    String formattedTime = DateFormat('kk:mm:s:a').format(now);
    String formattedDate = formatter.format(now);
    await FirebaseFirestore.instance.collection("Logs").add({
      'Message': "Admin had edit tagged patient " + getUserName!,
      'User': "Official Admin",
      'Logged Date': formattedDate,
      'Logged Time': formattedTime,
    });
  }

  logAdminFilteredAllRegistered() async {
    String formattedTime = DateFormat('kk:mm:s:a').format(now);
    String formattedDate = formatter.format(now);
    await FirebaseFirestore.instance.collection("Logs").add({
      'Message': "Admin set users view to all registered",
      'User': "Official Admin",
      'Logged Date': formattedDate,
      'Logged Time': formattedTime,
    });
  }

  logAdminFilteredDonorOnly() async {
    String formattedTime = DateFormat('kk:mm:s:a').format(now);
    String formattedDate = formatter.format(now);
    await FirebaseFirestore.instance.collection("Logs").add({
      'Message': "Admin set users view to Donor Only",
      'User': "Official Admin",
      'Logged Date': formattedDate,
      'Logged Time': formattedTime,
    });
  }

  logAdminFilteredPatientOnly() async {
    String formattedTime = DateFormat('kk:mm:s:a').format(now);
    String formattedDate = formatter.format(now);
    await FirebaseFirestore.instance.collection("Logs").add({
      'Message': "Admin set users view to Patient Only",
      'User': "Official Admin",
      'Logged Date': formattedDate,
      'Logged Time': formattedTime,
    });
  }

  logAdminFilteredAllTagged() async {
    String formattedTime = DateFormat('kk:mm:s:a').format(now);
    String formattedDate = formatter.format(now);
    await FirebaseFirestore.instance.collection("Logs").add({
      'Message': "Admin set users view to all tagged",
      'User': "Official Admin",
      'Logged Date': formattedDate,
      'Logged Time': formattedTime,
    });
  }

  logAdminFilteredDonorTagged() async {
    String formattedTime = DateFormat('kk:mm:s:a').format(now);
    String formattedDate = formatter.format(now);
    await FirebaseFirestore.instance.collection("Logs").add({
      'Message': "Admin set users view to Donor tagged Only",
      'User': "Official Admin",
      'Logged Date': formattedDate,
      'Logged Time': formattedTime,
    });
  }

  logAdminFilteredPatientTagged() async {
    String formattedTime = DateFormat('kk:mm:s:a').format(now);
    String formattedDate = formatter.format(now);
    await FirebaseFirestore.instance.collection("Logs").add({
      'Message': "Admin set users view to Patient tagged Only",
      'User': "Official Admin",
      'Logged Date': formattedDate,
      'Logged Time': formattedTime,
    });
  }
}
