import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:organ_donor_app/pages/NotificationsMailPage.dart';
import 'package:organ_donor_app/pages/api/notification_api.dart';

class NotificationsPage extends StatefulWidget {
  NotificationsPage({Key? key}) : super(key: key);

  @override
  _NotificationsPageState createState() => _NotificationsPageState();
}

class _NotificationsPageState extends State<NotificationsPage> {
  String? donorFullName;
  String? donorUserType;
  String? donorUserName;
  String? donorNotifications;
  String? donorReplies;
  String? donorLikes;

  String? patientFullName;
  String? patientUserType;
  String? patientUserName;
  String? patientNotifications;
  String? patientReplies;
  String? patientLikes;

  String? currentUserName;
  String? currentNotifications;
  String? currentReplies;
  String? currentLikes;
  String? currentMail;

  String? usersValue;

  @override
  void initState() {
    super.initState();

    NotificationApi.init();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xffe5eef8),
      body: Stack(
        children: [
          Container(
            margin: EdgeInsets.only(top: 40, right: 15),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Container(width: 32.0, height: 0.0),
                Text(
                  "Messages",
                  style: GoogleFonts.roboto(
                      color: Colors.black87,
                      fontWeight: FontWeight.bold,
                      fontSize: 18),
                ),
              ],
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 100, left: 15, right: 15),
            height: 250,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
              boxShadow: [
                BoxShadow(
                  color: Colors.grey.withOpacity(0.5),
                ),
              ],
              color: Colors.white,
            ),
            padding: EdgeInsets.only(top: 10, bottom: 10),
            child: Column(
              children: [
                // System
                Container(
                  color: Colors.white,
                  child: Row(
                    children: [
                      Container(
                        margin: EdgeInsets.only(left: 10),
                        padding: EdgeInsets.all(5),
                        decoration: BoxDecoration(
                            shape: BoxShape.circle, color: Colors.blue[50]),
                        child: Transform.rotate(
                          angle: -38,
                          child: Icon(
                            Icons.notifications,
                            size: 30.0,
                            color: Colors.blue[300],
                          ),
                        ),
                      ),
                      //get notification count from firebase
                      FutureBuilder(
                        future: getUserData(),
                        builder: (context, snapshot) {
                          if (snapshot.connectionState != ConnectionState.done)
                            return Text("");

                          if (currentNotifications == "0") {
                            return Container(
                              height: 50,
                              width: 240,
                              margin: EdgeInsets.only(top: 20, left: 10),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    'System',
                                    style: GoogleFonts.roboto(
                                        color: Colors.black87,
                                        fontSize: 16,
                                        fontWeight: FontWeight.w600),
                                  ),
                                  Text(
                                    'No Notifications',
                                    style: GoogleFonts.roboto(
                                        color: Colors.black45, fontSize: 14),
                                  ),
                                ],
                              ),
                            );
                          } else {
                            return Container(
                              height: 50,
                              width: 240,
                              margin: EdgeInsets.only(top: 20, left: 10),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    'System',
                                    style: GoogleFonts.roboto(
                                        color: Colors.black87,
                                        fontSize: 16,
                                        fontWeight: FontWeight.w600),
                                  ),
                                  Text(
                                    '$currentNotifications notification/s',
                                    style: GoogleFonts.roboto(
                                        color: Colors.black45, fontSize: 14),
                                  ),
                                ],
                              ),
                            );
                          }
                        },
                      ),
                    ],
                  ),
                ),
                //Mail
                Container(
                  color: Colors.white,
                  child: Row(
                    children: [
                      Container(
                        margin: EdgeInsets.only(left: 10),
                        padding: EdgeInsets.all(5),
                        decoration: BoxDecoration(
                            shape: BoxShape.circle, color: Colors.green[50]),
                        child: Transform.rotate(
                          angle: -38,
                          child: Icon(
                            Icons.email_outlined,
                            size: 30.0,
                            color: Colors.green[300],
                          ),
                        ),
                      ),
                      //get notification count from firebase
                      InkWell(
                        onTap: () {
                          Navigator.of(context).push(MaterialPageRoute(
                              builder: (context) => NotificationsMailPage()));
                        },
                        child: FutureBuilder(
                          future: getUserData(),
                          builder: (context, snapshot) {
                            if (snapshot.connectionState !=
                                ConnectionState.done) return Text("");

                            if (currentMail == "0") {
                              return Container(
                                height: 50,
                                width: 240,
                                margin: EdgeInsets.only(top: 20, left: 10),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      'Mail',
                                      style: GoogleFonts.roboto(
                                          color: Colors.black87,
                                          fontSize: 16,
                                          fontWeight: FontWeight.w600),
                                    ),
                                    Text(
                                      'No Messages',
                                      style: GoogleFonts.roboto(
                                          color: Colors.black45, fontSize: 14),
                                    ),
                                  ],
                                ),
                              );
                            } else {
                              return Container(
                                height: 50,
                                width: 240,
                                margin: EdgeInsets.only(top: 20, left: 10),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      'Mail',
                                      style: GoogleFonts.roboto(
                                          color: Colors.black87,
                                          fontSize: 16,
                                          fontWeight: FontWeight.w600),
                                    ),
                                    Text(
                                      '$currentMail message/s',
                                      style: GoogleFonts.roboto(
                                          color: Colors.black45, fontSize: 14),
                                    ),
                                  ],
                                ),
                              );
                            }
                          },
                        ),
                      ),
                    ],
                  ),
                ),
                //Likes
                Container(
                  color: Colors.white,
                  child: Row(
                    children: [
                      Container(
                        margin: EdgeInsets.only(left: 10),
                        padding: EdgeInsets.all(10),
                        decoration: BoxDecoration(
                            shape: BoxShape.circle, color: Colors.red[50]),
                        child: Transform.rotate(
                          angle: 0,
                          child: Icon(
                            Icons.thumb_up_alt_outlined,
                            size: 20.0,
                            color: Colors.red[300],
                          ),
                        ),
                      ),
                      //get likes count from firebase
                      FutureBuilder(
                        future: getUserData(),
                        builder: (context, snapshot) {
                          if (snapshot.connectionState != ConnectionState.done)
                            return Text("");
                          if (currentLikes == "0") {
                            return Container(
                              height: 50,
                              width: 240,
                              margin: EdgeInsets.only(top: 20, left: 10),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    'Likes',
                                    style: GoogleFonts.roboto(
                                        color: Colors.black87,
                                        fontSize: 16,
                                        fontWeight: FontWeight.w600),
                                  ),
                                  Text(
                                    'No likes received yet',
                                    style: GoogleFonts.roboto(
                                        color: Colors.black45, fontSize: 14),
                                  ),
                                ],
                              ),
                            );
                          } else {
                            return Container(
                              height: 50,
                              width: 240,
                              margin: EdgeInsets.only(top: 20, left: 10),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    'Likes',
                                    style: GoogleFonts.roboto(
                                        color: Colors.black87,
                                        fontSize: 16,
                                        fontWeight: FontWeight.w600),
                                  ),
                                  Text(
                                    '$currentLikes comment/s received',
                                    style: GoogleFonts.roboto(
                                        color: Colors.black45, fontSize: 14),
                                  ),
                                ],
                              ),
                            );
                          }
                        },
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  getUserData() async {
    final currentUser = FirebaseAuth.instance.currentUser;
    if (currentUser != null) {
      await FirebaseFirestore.instance
          .collection('Users')
          .doc("All")
          .collection("Lists")
          .doc(currentUser.uid)
          .get()
          .then((value) {
        currentUserName = value.data()!['UserName'];
        currentNotifications = value.data()!['Notifications'];
        currentReplies = value.data()!['Replies'];
        currentLikes = value.data()!['Likes'];
        currentMail = value.data()!['Mail'];
      }).catchError((e) {
        print(e);
      });
    }
  }
}
