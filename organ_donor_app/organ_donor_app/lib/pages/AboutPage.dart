import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:organ_donor_app/pages/AccountPrivacyPolicy.dart';
import 'package:organ_donor_app/pages/AccountTermsOfService.dart';
import 'package:organ_donor_app/pages/ContactUsPage.dart';
import 'package:organ_donor_app/pages/ForumsPrivacyPolicy.dart';
import 'package:organ_donor_app/pages/ForumsTermOfService.dart';
import 'package:organ_donor_app/pages/ProfileSettings.dart';
import 'package:organ_donor_app/widget/info_widget.dart';
import 'package:package_info/package_info.dart';

class AboutPage extends StatefulWidget {
  AboutPage({Key? key}) : super(key: key);

  @override
  _AboutPageState createState() => _AboutPageState();
}

class _AboutPageState extends State<AboutPage> {
  Map<String, dynamic> map = {};
  @override
  void initState() {
    super.initState();

    init();
  }

  Future init() async {
    final appVersion = await PackageInfoApi.getAppVersion();

    if (!mounted) return;

    setState(() => map = {
          'Organ Donor App Version': appVersion,
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xffe5eef8),
      body: Stack(
        children: [
          InfoWidget(
            map: map,
          ),
          Container(
            margin: EdgeInsets.only(top: 40, left: 20),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                InkWell(
                  onTap: () {
                    Navigator.pushAndRemoveUntil(
                        context,
                        MaterialPageRoute(
                            builder: (builder) => ProfileSettings()),
                        (route) => false);
                  },
                  child: Icon(Icons.arrow_back_ios_outlined, size: 25.0),
                ),
                Text(
                  "About Organ Donor App",
                  style: GoogleFonts.roboto(
                      color: Colors.black87,
                      fontWeight: FontWeight.bold,
                      fontSize: 18),
                ),
                Container(width: 32.0, height: 0.0),
              ],
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                margin: EdgeInsets.only(top: 80),
                width: 200,
                height: 200,
                child: Transform.rotate(
                  angle: 0,
                  child: Image(image: AssetImage('assets/logo.png')),
                ),
              ),
            ],
          ),
          Container(
            margin: EdgeInsets.only(top: 280),
            child: ListView(
              children: [
                //Contact Us
                Container(
                  margin: EdgeInsets.all(20),
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(10)),
                  child: ListTile(
                    trailing: Icon(
                      Icons.arrow_forward_ios_outlined,
                      color: Colors.grey,
                    ),
                    title: Text(
                      'Contact Us',
                      style: GoogleFonts.roboto(
                          color: Colors.black87, fontSize: 15),
                    ),
                    onTap: () {
                      Navigator.pushAndRemoveUntil(
                          context,
                          MaterialPageRoute(
                              builder: (builder) => ContactUsPage()),
                          (route) => false);
                    },
                  ),
                ),

                //Privacy Policy
                Container(
                  margin: EdgeInsets.all(20),
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(10)),
                  child: Column(
                    children: [
                      ListTile(
                        trailing: Icon(
                          Icons.arrow_forward_ios_outlined,
                          color: Colors.grey,
                        ),
                        title: Text(
                          'Forums Privacy Policy',
                          style: GoogleFonts.roboto(
                              color: Colors.black87, fontSize: 15),
                        ),
                        onTap: () {
                          Navigator.pushAndRemoveUntil(
                              context,
                              MaterialPageRoute(
                                  builder: (builder) => ForumsPrivacyPolicy()),
                              (route) => false);
                        },
                      ),
                      //Terms of Service
                      ListTile(
                        trailing: Icon(
                          Icons.arrow_forward_ios_outlined,
                          color: Colors.grey,
                        ),
                        title: Text(
                          'Forums Term of Service',
                          style: GoogleFonts.roboto(
                              color: Colors.black87, fontSize: 15),
                        ),
                        onTap: () {
                          Navigator.pushAndRemoveUntil(
                              context,
                              MaterialPageRoute(
                                  builder: (builder) => ForumsTermOfService()),
                              (route) => false);
                        },
                      ),
                      //Account Privacy Policy
                      ListTile(
                        trailing: Icon(
                          Icons.arrow_forward_ios_outlined,
                          color: Colors.grey,
                        ),
                        title: Text(
                          'Organ Donor App Account Privacy Policy',
                          style: GoogleFonts.roboto(
                              color: Colors.black87, fontSize: 15),
                        ),
                        onTap: () {
                          Navigator.pushAndRemoveUntil(
                              context,
                              MaterialPageRoute(
                                  builder: (builder) => AccountPrivacyPolicy()),
                              (route) => false);
                        },
                      ),

                      //Terms of Service
                      ListTile(
                        trailing: Icon(
                          Icons.arrow_forward_ios_outlined,
                          color: Colors.grey,
                        ),
                        title: Text(
                          'Organ Donor App Terms of Service',
                          style: GoogleFonts.roboto(
                              color: Colors.black87, fontSize: 15),
                        ),
                        onTap: () {
                          Navigator.pushAndRemoveUntil(
                              context,
                              MaterialPageRoute(
                                  builder: (builder) =>
                                      AccountTermsofService()),
                              (route) => false);
                        },
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

class PackageInfoApi {
  static Future<String?> getPackageName() async {
    final packageInfo = await PackageInfo.fromPlatform();

    return packageInfo.packageName;
  }

  static Future<String?> getAppVersion() async {
    final packageInfo = await PackageInfo.fromPlatform();
    return '${packageInfo.version}';
  }
}
