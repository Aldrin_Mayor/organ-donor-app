import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/gestures.dart';
import 'package:organ_donor_app/Service/Auth_Service.dart';
import 'package:organ_donor_app/pages/SignUpPage.dart';
import 'package:google_fonts/google_fonts.dart';

class SignInPage extends StatefulWidget {
  SignInPage({Key? key}) : super(key: key);

  @override
  _SignInPageState createState() => _SignInPageState();
}

class _SignInPageState extends State<SignInPage> {
  bool circular = false;

  String? mobileNumber;

  int start = 60;
  bool wait = false;
  String buttonName = "Send";
  AuthClass authClass = AuthClass();
  String verificationIdFinal = "";
  String smsCode = "";
  final formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: Stack(
        children: [
          Container(
            decoration: BoxDecoration(
              image: DecorationImage(
                  image: AssetImage('assets/5.png'), fit: BoxFit.cover),
            ),
          ),
          SingleChildScrollView(
            child: Container(
              margin: EdgeInsets.only(top: 50),
              child: Center(
                child: Form(
                  key: formKey,
                  child: Column(
                    children: <Widget>[
                      Container(
                        width: 300,
                        height: 300,
                        child: Transform.rotate(
                          angle: 38,
                          child: Image(image: AssetImage('assets/logo.png')),
                        ),
                      ),
                      SizedBox(height: 15),
                      Container(
                        margin: EdgeInsets.only(left: 50, right: 50),
                        child: TextFormField(
                          keyboardType: TextInputType.number,
                          inputFormatters: [
                            LengthLimitingTextInputFormatter(10)
                          ],
                          validator: (value) {
                            if (value!.isEmpty) {
                              return 'This is Required';
                            }
                            if (value.length <= 9) {
                              return 'Enter a Valid Mobile Number';
                            } else {
                              return null;
                            }
                          },
                          onSaved: (value) => mobileNumber = "0" + value!,
                          decoration: InputDecoration(
                              border: OutlineInputBorder(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(7))),
                              labelText: 'Mobile Number',
                              hintText: 'Enter Your Mobile Number',
                              contentPadding: const EdgeInsets.symmetric(
                                  vertical: 18, horizontal: 14),
                              prefixIcon: Padding(
                                padding: const EdgeInsets.symmetric(
                                    vertical: 12, horizontal: 15),
                                child: Text(
                                  "(+63)",
                                  style: TextStyle(
                                      color: Colors.black, fontSize: 17),
                                ),
                              )),
                        ),
                      ),
                      SizedBox(height: 25),
                      InkWell(
                        onTap: () {
                          final isValid = formKey.currentState!.validate();

                          if (isValid) {
                            formKey.currentState!.save();
                            authClass.checkLoginPhoneNumber(
                                mobileNumber!, context);
                          }
                        },
                        child: Container(
                          height: 60,
                          width: MediaQuery.of(context).size.width - 60,
                          margin: EdgeInsets.only(left: 50, right: 50),
                          decoration: BoxDecoration(
                              color: Colors.red,
                              borderRadius: BorderRadius.circular(10)),
                          child: Center(
                            child: circular
                                ? CircularProgressIndicator()
                                : Text(
                                    "Login",
                                    style: GoogleFonts.roboto(
                                        color: Colors.white,
                                        fontWeight: FontWeight.w500,
                                        fontSize: 20),
                                  ),
                          ),
                        ),
                      ),
                      SizedBox(height: 25),
                      Container(
                          child: Center(
                              child: RichText(
                        text: TextSpan(
                            text: 'Not yet Registered?',
                            style: TextStyle(color: Colors.black, fontSize: 15),
                            children: <TextSpan>[
                              TextSpan(
                                  text: ' Click here',
                                  style: TextStyle(
                                      color: Colors.blueAccent, fontSize: 15),
                                  recognizer: TapGestureRecognizer()
                                    ..onTap = () {
                                      Navigator.of(context).push(
                                          MaterialPageRoute(
                                              builder: (context) =>
                                                  SignUpPage()));
                                    })
                            ]),
                      )))
                    ],
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
