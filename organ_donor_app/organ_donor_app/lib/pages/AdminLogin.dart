import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:organ_donor_app/pages/AdminDashboard.dart';
import 'package:intl/intl.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class AdminLogin extends StatefulWidget {
  AdminLogin({Key? key}) : super(key: key);

  @override
  _AdminLoginState createState() => _AdminLoginState();
}

class _AdminLoginState extends State<AdminLogin> {
  bool isHiddenPassword = true;

  TextEditingController userName = new TextEditingController();
  TextEditingController password = new TextEditingController();

  String? adminUserName = 'admin';
  String? adminPassword = '1234';
  final formKey = GlobalKey<FormState>();

  //Time and Date
  var now = new DateTime.now();
  var formatter = new DateFormat('dd-MM-yyyy');

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFFB3E5FC),
      body: Container(
        child: Form(
          key: formKey,
          child: SingleChildScrollView(
            child: Wrap(
              spacing: 8.0, // gap between adjacent chips
              runSpacing: 4.0, // gap between lines
              direction: Axis.horizontal,
              children: <Widget>[
                Container(
                  margin: EdgeInsets.only(top: 200, left: 100),
                  child: Text(
                    'Admin Login\nOrgan Donor\nApplication',
                    style: GoogleFonts.roboto(
                        color: Colors.black87,
                        fontSize: 100,
                        fontWeight: FontWeight.w700),
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(top: 150, left: 50),
                  child: Image.asset('assets/admin_login.png'),
                ),
                Container(
                  margin: EdgeInsets.only(top: 200, left: 50),
                  width: 500,
                  height: 450,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(30),
                      color: Colors.red[200]),
                  child: Column(
                    children: [
                      //Admin UserName
                      Container(
                        margin: EdgeInsets.only(top: 50),
                        child: Row(
                          children: [
                            Text("\t\t\t"),
                            Icon(Icons.person),
                            Text("\t\t\t"),
                            Text(
                              "User Name",
                              style: GoogleFonts.roboto(
                                  color: Colors.blue[900],
                                  fontWeight: FontWeight.bold,
                                  fontSize: 28),
                            ),
                          ],
                        ),
                      ),
                      TextFormField(
                        controller: userName,
                        decoration: InputDecoration(
                          border: UnderlineInputBorder(),
                          labelText: '',
                          contentPadding: EdgeInsets.only(left: 60),
                        ),
                        validator: (value) {
                          if (value!.isEmpty) {
                            return 'User Name is Required!';
                          } else if (userName.text != adminUserName) {
                            return 'Wrong User Name!';
                          }
                        },
                      ),
                      //Admin Password
                      Container(
                        margin: EdgeInsets.only(top: 50),
                        child: Row(
                          children: [
                            Text("\t\t\t"),
                            Icon(Icons.lock),
                            Text("\t\t\t"),
                            Text(
                              "Password",
                              style: GoogleFonts.roboto(
                                  color: Colors.blue[900],
                                  fontWeight: FontWeight.bold,
                                  fontSize: 28),
                            ),
                          ],
                        ),
                      ),
                      TextFormField(
                        controller: password,
                        obscureText: isHiddenPassword,
                        decoration: InputDecoration(
                          border: UnderlineInputBorder(),
                          labelText: '',
                          contentPadding: EdgeInsets.only(left: 60),
                          suffixIcon: InkWell(
                            onTap: togglePasswordView,
                            child: Icon(
                              Icons.visibility,
                            ),
                          ),
                        ),
                        validator: (value) {
                          if (value!.isEmpty) {
                            return 'Password is Required!';
                          } else if (password.text != adminPassword) {
                            return 'Wrong Password!';
                          }
                        },
                      ),
                      //Login Button
                      Container(
                        margin: EdgeInsets.only(top: 50),
                        child: ElevatedButton(
                          child: Text(
                            '                Login               ',
                            style: TextStyle(color: Colors.white, fontSize: 28),
                          ),
                          onPressed: () {
                            final isValid = formKey.currentState!.validate();

                            if (isValid) {
                              loginAdmin();
                              //logs
                              logAdminLogin();
                            }
                          },
                          style: ButtonStyle(
                              backgroundColor:
                                  MaterialStateProperty.all(Colors.red),
                              padding:
                                  MaterialStateProperty.all(EdgeInsets.all(15)),
                              shape: MaterialStateProperty.all<
                                      RoundedRectangleBorder>(
                                  RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(18.0),
                              )),
                              textStyle: MaterialStateProperty.all(
                                  TextStyle(fontSize: 20))),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  void togglePasswordView() {
    //First Way
    if (isHiddenPassword == true) {
      isHiddenPassword = false;
    } else {
      isHiddenPassword = true;
    }
    //Second Way
    setState(() {
      //  isHiddenPassword = !isHiddenPassword;
    });
  }

  void loginAdmin() {
    if (userName.text == adminUserName || password.text == adminPassword) {
      Navigator.of(context)
          .push(MaterialPageRoute(builder: (context) => AdminDashboard()));
    }
  }

  //Logs in the database
  logAdminLogin() async {
    String formattedTime = DateFormat('kk:mm:s:a').format(now);
    String formattedDate = formatter.format(now);
    await FirebaseFirestore.instance.collection("Logs").add({
      'Message': "Admin Login Successfull!",
      'User': "Official Admin",
      'Logged Date': formattedDate,
      'Logged Time': formattedTime,
    });
  }
}
