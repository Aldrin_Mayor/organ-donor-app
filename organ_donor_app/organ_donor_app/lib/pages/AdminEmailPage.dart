import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:fluttertoast/fluttertoast.dart';

class AdminEmailPage extends StatefulWidget {
  AdminEmailPage({Key? key}) : super(key: key);

  @override
  _AdminEmailPageState createState() => _AdminEmailPageState();
}

class _AdminEmailPageState extends State<AdminEmailPage> {
  //Set State Refresh
  final toast = FToast();
  @override
  void initState() {
    super.initState();

    toast.init(context);
  }

  String? getUserID;
  String? getSubject;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xffe5eef8),
      body: SingleChildScrollView(
        child: Wrap(
          spacing: 8.0, // gap between adjacent chips
          runSpacing: 4.0, // gap between lines
          direction: Axis.horizontal,
          children: <Widget>[
            Container(
              margin: EdgeInsets.only(top: 60, left: 100),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    'Sent',
                    style: GoogleFonts.roboto(
                        color: Colors.black54,
                        fontSize: 40,
                        fontWeight: FontWeight.w400),
                  ),
                ],
              ),
            ),
            Container(
              height: MediaQuery.of(context).size.height * 0.7,
              width: MediaQuery.of(context).size.width * 0.755,
              margin: EdgeInsets.only(top: 20, left: 50, bottom: 100),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: Colors.white,
              ),
              child: Stack(
                children: [
                  //Contents
                  FutureBuilder(
                    future: getEmailsData(),
                    builder: (context, AsyncSnapshot snapshot) {
                      if (snapshot.hasData) {
                        return ListView.builder(
                          itemCount: snapshot.data!.length,
                          itemBuilder: (context, index) {
                            DocumentSnapshot userData = snapshot.data[index];
                            return Stack(
                              children: [
                                Container(
                                  decoration: BoxDecoration(
                                      color: Colors.blue[50],
                                      borderRadius: BorderRadius.circular(5)),
                                  margin: EdgeInsets.only(
                                      top: 10, left: 10, right: 10),
                                  child: ListTile(
                                    title: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      children: [
                                        Container(
                                          margin: EdgeInsets.only(left: 50),
                                          child: Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceBetween,
                                            children: [
                                              Row(
                                                children: [
                                                  Text(
                                                    'To: ',
                                                    style: GoogleFonts.roboto(
                                                        color: Colors.black87,
                                                        fontWeight:
                                                            FontWeight.w300,
                                                        fontSize: 16),
                                                  ),
                                                  Container(
                                                    child: Text(
                                                      userData["To"],
                                                      style: GoogleFonts.roboto(
                                                          color: Colors.black87,
                                                          fontWeight:
                                                              FontWeight.w300,
                                                          fontSize: 16),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                              //Subject and Body of eMail
                                              Container(
                                                width: MediaQuery.of(context)
                                                        .size
                                                        .width *
                                                    0.4,
                                                margin: EdgeInsets.only(
                                                  top: 50,
                                                ),
                                                child: Column(
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.start,
                                                  children: [
                                                    Container(
                                                      child: Text(
                                                        userData["Subject"],
                                                        style:
                                                            GoogleFonts.roboto(
                                                                color: Colors
                                                                    .black87,
                                                                fontWeight:
                                                                    FontWeight
                                                                        .w600,
                                                                fontSize: 16),
                                                      ),
                                                    ),
                                                    Container(
                                                      margin: EdgeInsets.only(
                                                          top: 10, bottom: 30),
                                                      child: Text(
                                                        userData["Body"],
                                                        style:
                                                            GoogleFonts.roboto(
                                                                color: Colors
                                                                    .black87,
                                                                fontWeight:
                                                                    FontWeight
                                                                        .w300,
                                                                fontSize: 16),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                              Row(
                                                children: [
                                                  Container(
                                                    margin: EdgeInsets.only(
                                                        right: 20),
                                                    child: Text(
                                                      userData["Date"],
                                                      style: GoogleFonts.roboto(
                                                          color: Colors.grey,
                                                          fontWeight:
                                                              FontWeight.w400,
                                                          fontSize: 14),
                                                    ),
                                                  ),
                                                  Container(
                                                    margin: EdgeInsets.only(
                                                        right: 20),
                                                    child: Text(
                                                      userData["Time"],
                                                      style: GoogleFonts.roboto(
                                                          color: Colors.grey,
                                                          fontWeight:
                                                              FontWeight.w400,
                                                          fontSize: 14),
                                                    ),
                                                  ),
                                                  Container(
                                                    margin: EdgeInsets.only(
                                                        right: 50),
                                                    child: InkWell(
                                                      onTap: () {
                                                        getUserID =
                                                            userData["UserID"];
                                                        getSubject =
                                                            userData["Subject"];
                                                        showCustomDialog(
                                                            context);
                                                      },
                                                      child: Icon(
                                                        Icons
                                                            .delete_forever_outlined,
                                                        color: Colors.grey,
                                                      ),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            );
                          },
                        );
                      } else {
                        return Container(
                          alignment: Alignment.center,
                          child: CircularProgressIndicator(),
                        );
                      }
                    },
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  //Alert Dialog For Delete
  void showCustomDialog(BuildContext context) => showDialog(
      context: context,
      builder: (BuildContext context) {
        return Dialog(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20.0)), //this right here
          child: Container(
            height: 200,
            width: 100,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  "Delete Sent",
                  style: GoogleFonts.roboto(
                      color: Colors.black87,
                      fontWeight: FontWeight.bold,
                      fontSize: 18),
                ),
                Text(
                  "\nAre you sure you want to delete?",
                  style:
                      GoogleFonts.roboto(color: Colors.grey[500], fontSize: 14),
                ),
                Container(
                  margin: EdgeInsets.only(top: 40),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Container(
                        child: InkWell(
                          onTap: () {
                            Navigator.of(context).pop();
                          },
                          child: Container(
                            height: 35,
                            width: 100,
                            decoration: BoxDecoration(
                                color: Colors.red[50],
                                borderRadius: BorderRadius.circular(18)),
                            child: Center(
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Text(
                                  "Cancel",
                                  style: TextStyle(
                                      color: Colors.grey[600], fontSize: 17),
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                      Container(
                        child: InkWell(
                          onTap: () {
                            setState(() {
                              deleteEmailAdmin();
                              showMessageDeleteToast();
                              Navigator.of(context).pop();
                            });
                          },
                          child: Container(
                            height: 35,
                            width: 100,
                            decoration: BoxDecoration(
                                color: Colors.red[200],
                                borderRadius: BorderRadius.circular(18)),
                            child: Center(
                              child: Text(
                                "Confirm",
                                style: TextStyle(
                                    color: Colors.red[400], fontSize: 17),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        );
      });

  //Show Delete Success
  void showMessageDeleteToast() => toast.showToast(
      child: buildMessageDeleteToast(), gravity: ToastGravity.CENTER);

  Widget buildMessageDeleteToast() => Container(
        padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
        decoration: BoxDecoration(
          color: Colors.green[300],
          borderRadius: BorderRadius.circular(5),
        ),
        child: Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            Text(
              "Message Deleted!",
              style: TextStyle(color: Colors.black87, fontSize: 16),
            ),
          ],
        ),
      );

  //Display Feedback Table
  getEmailsData() async {
    final firestore = FirebaseFirestore.instance;
    QuerySnapshot user = await firestore.collection('Email').get();
    return user.docs;
  }

  deleteEmailAdmin() async {
    String? uid = getSubject;
    await FirebaseFirestore.instance.collection("Email").doc(uid).delete();
  }
}
