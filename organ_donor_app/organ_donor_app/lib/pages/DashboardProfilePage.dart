import 'package:curved_navigation_bar/curved_navigation_bar.dart';
import 'package:flutter/material.dart';
import 'package:organ_donor_app/pages/FeedBackPage.dart';
import 'package:organ_donor_app/pages/HomePage.dart';
import 'package:organ_donor_app/pages/MakePosts.dart';
import 'package:organ_donor_app/pages/NotificationsPage.dart';
import 'package:organ_donor_app/pages/ProfilePage.dart';
import 'package:google_fonts/google_fonts.dart';

class DashboardProfilePage extends StatefulWidget {
  DashboardProfilePage({Key? key}) : super(key: key);

  @override
  _DashboardProfilePageState createState() => _DashboardProfilePageState();
}

class _DashboardProfilePageState extends State<DashboardProfilePage> {
  int index = 4;

  final screens = [
    HomePage(),
    FeedBackPage(),
    HomePage(),
    NotificationsPage(),
    ProfilePage(),
  ];
  @override
  Widget build(BuildContext context) {
    //Bottom Navigation Bar

    final items = <Widget>[
      Icon(Icons.home_outlined, size: 30),
      Icon(Icons.feedback_outlined, size: 30),
      InkWell(
          onTap: () {
            showPosts();
          },
          child: Icon(Icons.post_add_outlined, size: 50)),
      Icon(Icons.notifications_outlined, size: 30),
      Icon(Icons.settings_outlined, size: 30),
    ];
    return Scaffold(
      extendBody: true,
      resizeToAvoidBottomInset: false,
      body: screens[index],
      bottomNavigationBar: Theme(
        data: Theme.of(context).copyWith(
          iconTheme: IconThemeData(color: Colors.red),
        ),
        child: CurvedNavigationBar(
          buttonBackgroundColor: Colors.red.shade50,
          backgroundColor: Colors.red.shade400,
          animationCurve: Curves.easeInOut,
          animationDuration: Duration(milliseconds: 300),
          height: 60,
          index: index,
          items: items,
          onTap: (index) => setState(() => this.index = index),
        ),
      ),
    );
  }

  void showPosts() {
    showModalBottomSheet(
      context: context,
      builder: (context) => buildSheet(),
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(top: Radius.circular(20))),
    );
  }

  // Bottom Sheet For Posts
  Widget buildSheet() => Container(
        height: 250,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            InkWell(
              onTap: () {
                Navigator.of(context).pop();
              },
              child: Container(
                margin: EdgeInsets.only(top: 20, left: 10),
                child: Icon(
                  Icons.close_outlined,
                  color: Colors.grey,
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: 40, left: 10),
              child: Text(
                'Post to Organ Donor App',
                style: GoogleFonts.roboto(
                    color: Colors.black87,
                    fontSize: 15,
                    fontWeight: FontWeight.w500),
              ),
            ),
            //Create Post
            InkWell(
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => MakePosts()),
                );
              },
              child: Container(
                height: 70,
                margin: EdgeInsets.only(top: 30, left: 20, right: 20),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(10)),
                    color: Color(0xffe5eef8)),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Row(
                      children: [
                        Container(
                          padding: EdgeInsets.all(5),
                          margin: EdgeInsets.only(left: 30),
                          decoration: BoxDecoration(
                              shape: BoxShape.circle, color: Colors.green[100]),
                          child: Icon(
                            Icons.note_add_rounded,
                            size: 40,
                            color: Colors.teal[400],
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(left: 20),
                          child: Text(
                            'Post',
                            style: GoogleFonts.roboto(
                                color: Colors.black87,
                                fontSize: 15,
                                fontWeight: FontWeight.w500),
                          ),
                        ),
                      ],
                    ),
                    Container(
                      margin: EdgeInsets.only(right: 20),
                      child: Icon(
                        Icons.arrow_forward_ios,
                        size: 15,
                        color: Colors.grey,
                      ),
                    )
                  ],
                ),
              ),
            ),
            //Create Poll
          ],
        ),
      );
}
