import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:organ_donor_app/Service/Auth_Service.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'dart:ui';
import 'package:organ_donor_app/pages/CommentsPage.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:organ_donor_app/pages/EditPosts.dart';
import 'package:organ_donor_app/pages/SignInPage.dart';
import 'package:organ_donor_app/pages/api/notification_api.dart';

class HomePage extends StatefulWidget {
  HomePage({Key? key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  String? donorUserID;
  String? donorCountPosts;
  String? donorCountNotifications;
  String? patientUserID;
  String? patientCountPosts;
  String? patientCountNotifications;
  String? currentUserID;

  AuthClass authClass = AuthClass();

  String? getUid;

  // For Passing the data
  String? getFullName;
  String? getUserName;
  String? getDate;
  String? getTime;
  String? getUserID;
  String? getPostTitle;
  String? getPostContent;
  String? getUserType;
  String? getPostViews;
  String? getPostComments;
  String? getPostLikes;
  String? getNotification;

  //Toast
  final toast = FToast();
  @override
  void initState() {
    super.initState();
    toast.init(context);

    //Notifications
    NotificationApi.init();
    //listenNotifications();
  }

  String? countPost = "0";
  String? countNotification;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Color(0xffe5eef8),
        appBar: PreferredSize(
            preferredSize: Size.fromHeight(200),
            child: AppBar(
              centerTitle: true,
              flexibleSpace: ClipRRect(
                borderRadius: BorderRadius.only(
                  bottomRight: Radius.circular(50),
                  bottomLeft: Radius.circular(50),
                ),
                child: Container(
                  decoration: BoxDecoration(
                      image: DecorationImage(
                          image: AssetImage('assets/kidneybackground.jpg'),
                          fit: BoxFit.fill)),
                ),
              ),
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.only(
                  bottomRight: Radius.circular(50),
                  bottomLeft: Radius.circular(50),
                ),
              ),
            )),
        body: Stack(
          children: [
            //Contents
            Container(
              decoration: BoxDecoration(
                color: Color(0xffe5eef8),
              ),
              child: Stack(
                children: [
                  //Contents
                  Container(
                    color: Color(0xffe5eef8),
                    margin: EdgeInsets.only(
                        top: 60, left: 15, right: 15, bottom: 10),
                    child: FutureBuilder(
                      future: getPostsData(),
                      builder: (context, AsyncSnapshot snapshot) {
                        if (snapshot.hasData) {
                          return ListView.builder(
                            itemCount: snapshot.data!.length,
                            itemBuilder: (context, index) {
                              DocumentSnapshot userData = snapshot.data[index];
                              getUid = userData["UserID"];

                              return Stack(
                                children: [
                                  Container(
                                    decoration: BoxDecoration(
                                        color: Colors.white,
                                        borderRadius:
                                            BorderRadius.circular(10)),
                                    margin: EdgeInsets.only(top: 20),
                                    child: ListTile(
                                      title: Column(
                                        children: [
                                          Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceBetween,
                                            children: [
                                              Column(
                                                children: [
                                                  Row(
                                                    children: [
                                                      Container(
                                                        margin: EdgeInsets.only(
                                                            top: 10),
                                                        child: Image.asset(
                                                          'assets/user.png',
                                                          height: 30,
                                                        ),
                                                      ),
                                                      Container(
                                                        margin: EdgeInsets.only(
                                                            top: 20, left: 10),
                                                        child: Column(
                                                          crossAxisAlignment:
                                                              CrossAxisAlignment
                                                                  .start,
                                                          children: [
                                                            //UserName
                                                            Container(
                                                              child: Row(
                                                                children: [
                                                                  Text(
                                                                    userData[
                                                                        "UserName"],
                                                                    style: GoogleFonts.roboto(
                                                                        color: Colors
                                                                            .black87,
                                                                        fontWeight:
                                                                            FontWeight
                                                                                .w600,
                                                                        fontSize:
                                                                            14),
                                                                  ),
                                                                  //Verified Logo
                                                                  Container(
                                                                    margin: EdgeInsets
                                                                        .only(
                                                                            left:
                                                                                5),
                                                                    child: Icon(
                                                                        Icons
                                                                            .verified_outlined,
                                                                        size:
                                                                            20,
                                                                        color: Colors
                                                                            .green),
                                                                  ),
                                                                ],
                                                              ),
                                                            ),
                                                            Container(
                                                              margin: EdgeInsets
                                                                  .only(top: 5),
                                                              child: Row(
                                                                children: [
                                                                  Text(
                                                                    userData[
                                                                        "Date"],
                                                                    style: GoogleFonts.roboto(
                                                                        color: Colors
                                                                            .black54,
                                                                        fontWeight:
                                                                            FontWeight
                                                                                .w600,
                                                                        fontSize:
                                                                            12),
                                                                  ),
                                                                  Container(
                                                                    margin: EdgeInsets
                                                                        .only(
                                                                            left:
                                                                                2),
                                                                    child: Text(
                                                                      userData[
                                                                          "Time"],
                                                                      style: GoogleFonts.roboto(
                                                                          color: Colors
                                                                              .black54,
                                                                          fontWeight: FontWeight
                                                                              .w600,
                                                                          fontSize:
                                                                              12),
                                                                    ),
                                                                  ),
                                                                ],
                                                              ),
                                                            ),
                                                          ],
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                ],
                                              ),

                                              //Setting Action
                                              InkWell(
                                                onTap: () {
                                                  getUserID =
                                                      userData["UserID"];
                                                  getUserName =
                                                      userData["UserName"];
                                                  getFullName =
                                                      userData["FullName"];
                                                  getUserType =
                                                      userData["User Type"];
                                                  getDate = userData["Date"];
                                                  getTime = userData["Time"];
                                                  getPostTitle =
                                                      userData["Post Title"];
                                                  getPostContent =
                                                      userData["Post Content"];
                                                  if (currentUserID ==
                                                      getUserID) {
                                                    showModalBottomSheet(
                                                      context: context,
                                                      builder: (context) =>
                                                          buildCurrentUserSheet(),
                                                    );
                                                  } else {
                                                    showModalBottomSheet(
                                                      context: context,
                                                      builder: (context) =>
                                                          buildNotCurrentUserSheet(),
                                                    );
                                                  }
                                                },
                                                child: Icon(
                                                    Icons.more_vert_outlined,
                                                    size: 20,
                                                    color: Colors.grey),
                                              ),
                                            ],
                                          ),
                                          Row(
                                            children: [
                                              Container(
                                                width: MediaQuery.of(context)
                                                        .size
                                                        .width *
                                                    0.82,
                                                margin: EdgeInsets.only(
                                                    top: 20, left: 0),
                                                child: Text(
                                                  userData["Post Title"],
                                                  style: GoogleFonts.roboto(
                                                      color: Colors.black87,
                                                      fontWeight:
                                                          FontWeight.w600,
                                                      fontSize: 16),
                                                ),
                                              ),
                                            ],
                                          ),
                                          Row(
                                            children: [
                                              Container(
                                                width: MediaQuery.of(context)
                                                        .size
                                                        .width *
                                                    0.82,
                                                margin: EdgeInsets.only(
                                                  top: 20,
                                                  left: 0,
                                                ),
                                                child: Text(
                                                  userData["Post Content"],
                                                  style: GoogleFonts.roboto(
                                                      color: Colors.black87,
                                                      fontWeight:
                                                          FontWeight.w400,
                                                      fontSize: 14),
                                                ),
                                              ),
                                            ],
                                          ),
                                          //views, comment and Like
                                          Container(
                                            margin: EdgeInsets.only(
                                                top: 20, bottom: 10),
                                            child: Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.spaceEvenly,
                                              children: [
                                                //Views
                                                InkWell(
                                                  onTap: () {},
                                                  child: Row(
                                                    children: [
                                                      //Views Icon
                                                      Icon(
                                                        Icons
                                                            .remove_red_eye_rounded,
                                                        color: Colors.grey,
                                                      ),
                                                      //Views Count
                                                      Container(
                                                        margin: EdgeInsets.only(
                                                            left: 10),
                                                        child: Text(
                                                            userData["Views"],
                                                            style: GoogleFonts
                                                                .roboto(
                                                                    color: Colors
                                                                        .grey,
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .w400,
                                                                    fontSize:
                                                                        14)),
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                                //Comments
                                                InkWell(
                                                  onTap: () {
                                                    getUserID =
                                                        userData["UserID"];
                                                    getUserName =
                                                        userData["UserName"];
                                                    getFullName =
                                                        userData["FullName"];
                                                    getUserType =
                                                        userData["User Type"];
                                                    getDate = userData["Date"];
                                                    getTime = userData["Time"];
                                                    getPostTitle =
                                                        userData["Post Title"];
                                                    getPostContent = userData[
                                                        "Post Content"];
                                                    getPostViews =
                                                        userData["Views"];
                                                    getPostComments =
                                                        userData["Comments"];
                                                    getPostLikes =
                                                        userData["Likes"];

                                                    // For counting Views to Posts
                                                    if (getUserType ==
                                                        'I am a Donor') {
                                                      setState(() {
                                                        int x = int.parse(
                                                                getPostViews!) +
                                                            1;
                                                        getPostViews =
                                                            x.toString();

                                                        viewsAddDonor();
                                                        viewsAddAll();
                                                        viewsAddSpecificDonor();
                                                        viewsAddSpecificAll();
                                                      });
                                                    } else if (getUserType ==
                                                        'I am a Patient') {
                                                      setState(() {
                                                        int x = int.parse(
                                                                getPostViews!) +
                                                            1;
                                                        getPostViews =
                                                            x.toString();

                                                        viewsAddPatient();
                                                        viewsAddAll();
                                                        viewsAddSpecificPatient();
                                                        viewsAddSpecificAll();
                                                      });
                                                    } else {
                                                      setState(() {
                                                        int x = int.parse(
                                                                getPostViews!) +
                                                            1;
                                                        getPostViews =
                                                            x.toString();

                                                        viewsAddAdmin();
                                                        viewsAddAll();
                                                      });
                                                    }
                                                    Navigator.of(context).push(
                                                        MaterialPageRoute(
                                                            builder: (context) =>
                                                                CommentsPage(
                                                                  getUserName:
                                                                      getUserName,
                                                                  getFullName:
                                                                      getFullName,
                                                                  getUserID:
                                                                      getUserID,
                                                                  getUserType:
                                                                      getUserType,
                                                                  getDate:
                                                                      getDate,
                                                                  getTime:
                                                                      getTime,
                                                                  getPostTitle:
                                                                      getPostTitle,
                                                                  getPostContent:
                                                                      getPostContent,
                                                                  getPostViews:
                                                                      getPostViews,
                                                                  getPostComments:
                                                                      getPostComments,
                                                                  getPostLikes:
                                                                      getPostLikes,
                                                                )));
                                                  },
                                                  child: Row(
                                                    children: [
                                                      //Comments Icon
                                                      Icon(
                                                        Icons.comment_outlined,
                                                        color: Colors.grey,
                                                      ),
                                                      //Comments Count
                                                      Container(
                                                        margin: EdgeInsets.only(
                                                            left: 10),
                                                        child: Text(
                                                            userData[
                                                                "Comments"],
                                                            style: GoogleFonts
                                                                .roboto(
                                                                    color: Colors
                                                                        .grey,
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .w400,
                                                                    fontSize:
                                                                        14)),
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                                //Likes
                                                InkWell(
                                                  onTap: () {
                                                    getUserID =
                                                        userData["UserID"];
                                                    getUserName =
                                                        userData["UserName"];
                                                    getFullName =
                                                        userData["FullName"];
                                                    getUserType =
                                                        userData["User Type"];
                                                    getDate = userData["Date"];
                                                    getTime = userData["Time"];
                                                    getPostTitle =
                                                        userData["Post Title"];
                                                    getPostContent = userData[
                                                        "Post Content"];
                                                    getPostViews =
                                                        userData["Views"];
                                                    getPostComments =
                                                        userData["Comments"];
                                                    getPostLikes =
                                                        userData["Likes"];
                                                    getNotification = userData[
                                                        "Notifications"];

                                                    if (getUserType ==
                                                        'I am a Donor') {
                                                      setState(() {
                                                        int x = int.parse(
                                                                getPostLikes!) +
                                                            1;
                                                        getPostLikes =
                                                            x.toString();
                                                        likesAddDonor();
                                                        likesAddAll();
                                                        likesAddSpecificDonor();
                                                        likesAddSpecificAll();
                                                        likesAddSpecificProfileDonor();
                                                        likesAddSpecificProfileAll();

                                                        //Notification count
                                                        int y = int.parse(
                                                                getNotification!) +
                                                            1;
                                                        countNotification =
                                                            y.toString();
                                                        notificationAddDonor();
                                                        notificationAddAll();
                                                        notificationAddSpecificDonor();
                                                        notificationAddSpecificAll();
                                                      });
                                                    } else if (getUserType ==
                                                        'I am a Patient') {
                                                      setState(() {
                                                        int x = int.parse(
                                                                getPostLikes!) +
                                                            1;
                                                        getPostLikes =
                                                            x.toString();
                                                        likesAddPatient();
                                                        likesAddAll();
                                                        likesAddSpecificPatient();
                                                        likesAddSpecificAll();
                                                        likesAddSpecificProfilePatient();
                                                        likesAddSpecificProfileAll();

                                                        //Notification count
                                                        int y = int.parse(
                                                                getNotification!) +
                                                            1;
                                                        countNotification =
                                                            y.toString();
                                                        notificationAddPatient();
                                                        notificationAddAll();
                                                        notificationAddSpecificPatient();
                                                        notificationAddSpecificAll();
                                                      });
                                                    } else {
                                                      setState(() {
                                                        int x = int.parse(
                                                                getPostLikes!) +
                                                            1;
                                                        getPostLikes =
                                                            x.toString();
                                                        likesAddAdmin();
                                                        likesAddAll();
                                                      });
                                                    }
                                                  },
                                                  child: Row(
                                                    children: [
                                                      //Likes Icon
                                                      Icon(
                                                        Icons.thumb_up_outlined,
                                                        color: Colors.grey,
                                                      ),
                                                      //Likes Count
                                                      Container(
                                                        margin: EdgeInsets.only(
                                                            left: 10),
                                                        child: Text(
                                                            userData["Likes"],
                                                            style: GoogleFonts
                                                                .roboto(
                                                                    color: Colors
                                                                        .grey,
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .w400,
                                                                    fontSize:
                                                                        14)),
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                              ],
                                            ),
                                          )
                                        ],
                                      ),
                                    ),
                                  ),
                                ],
                              );
                            },
                          );
                        } else {
                          return Text('Users Data Not Available');
                        }
                      },
                    ),
                  ),
                  // Checks if Current user id matches with his post and if user is null
                  FutureBuilder(
                    future: getUserData(),
                    builder: (context, snapshot) {
                      if (snapshot.connectionState != ConnectionState.done)
                        return Text("");
                      if (donorUserID != null) {
                        currentUserID = donorUserID;

                        return Container(
                            height: 20,
                            width: 240,
                            child: Text(
                              '',
                              style: GoogleFonts.roboto(
                                  color: Colors.black87,
                                  fontSize: 15,
                                  fontWeight: FontWeight.w500),
                            ));
                      }
                      if (patientUserID != null) {
                        currentUserID = patientUserID;

                        return Container(
                            height: 20,
                            width: 240,
                            child: Text(
                              '',
                              style: GoogleFonts.roboto(
                                  color: Colors.black87,
                                  fontSize: 15,
                                  fontWeight: FontWeight.w500),
                            ));
                      } else {
                        WidgetsBinding.instance!.addPostFrameCallback((_) {
                          autoLogout();
                          showSnackBar(context);
                        });
                      }
                      return Text(
                        "",
                        style: GoogleFonts.roboto(
                            color: Colors.black54, fontSize: 14),
                      );
                    },
                  ),

                  //Header
                  Row(
                    children: [
                      Container(
                        margin: EdgeInsets.only(top: 30, left: 20),
                        child: Row(
                          children: [
                            Icon(
                              Icons.post_add_outlined,
                              size: 24,
                              color: Colors.pink,
                            ),
                            Text(
                              '\tClick to add post',
                              style: GoogleFonts.roboto(
                                  color: Colors.black87, fontSize: 18),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ],
        ));
  }

  //For Auto Log out if user account is archived

  void autoLogout() async {
    await authClass.logout();
    Navigator.pushAndRemoveUntil(
        context,
        MaterialPageRoute(builder: (builder) => SignInPage()),
        (route) => false);
  }

  void showSnackBar(BuildContext context) {
    ScaffoldMessenger.of(context)
        .showSnackBar(SnackBar(content: Text("Logged Out")));
  }

  //Get User Information
  getUserData() async {
    final donor = FirebaseAuth.instance.currentUser;
    if (donor != null) {
      await FirebaseFirestore.instance
          .collection('Users')
          .doc("Donor")
          .collection("Lists")
          .doc(donor.uid)
          .get()
          .then((value) {
        donorUserID = value.data()!['UserID'];
        donorCountPosts = value.data()!['Posts'];
        donorCountNotifications = value.data()!['Notifications'];
      }).catchError((e) {
        print(e);
      });
    }

    final patient = FirebaseAuth.instance.currentUser;
    if (patient != null) {
      await FirebaseFirestore.instance
          .collection('Users')
          .doc("Patient")
          .collection("Lists")
          .doc(patient.uid)
          .get()
          .then((value) {
        patientUserID = value.data()!['UserID'];
        patientCountPosts = value.data()!['Posts'];
        patientCountNotifications = value.data()!['Notifications'];
      }).catchError((e) {
        print(e);
      });
    }
  }

  //Display All Posts
  getPostsData() async {
    final firestore = FirebaseFirestore.instance;
    QuerySnapshot user = await firestore
        .collection('Posts')
        .doc('All')
        .collection('All Posts')
        .get();
    return user.docs;
  }

  //Delete
  deleteDonorPost() async {
    String? uid = getTime;
    await FirebaseFirestore.instance
        .collection("Posts")
        .doc('Donor')
        .collection('All Posts')
        .doc(uid)
        .delete();
  }

  deletePatientPost() async {
    String? uid = getTime;
    await FirebaseFirestore.instance
        .collection("Posts")
        .doc('Patient')
        .collection('All Posts')
        .doc(uid)
        .delete();
  }

  deleteAlltPost() async {
    String? uid = getTime;
    await FirebaseFirestore.instance
        .collection("Posts")
        .doc('All')
        .collection('All Posts')
        .doc(uid)
        .delete();
  }

  deleteSpecificDonorPost() async {
    String? uid = getUserID;
    String? did = getTime;
    await FirebaseFirestore.instance
        .collection("Users")
        .doc('Donor')
        .collection('Lists')
        .doc(uid)
        .collection('Posts')
        .doc(did)
        .delete();
  }

  deleteSpecificPatientPost() async {
    String? uid = getUserID;
    String? did = getTime;
    await FirebaseFirestore.instance
        .collection("Users")
        .doc('Patient')
        .collection('Lists')
        .doc(uid)
        .collection('Posts')
        .doc(did)
        .delete();
  }

  deleteSpecificAllPost() async {
    String? uid = getUserID;
    String? did = getTime;
    await FirebaseFirestore.instance
        .collection("Users")
        .doc('All')
        .collection('Lists')
        .doc(uid)
        .collection('Posts')
        .doc(did)
        .delete();
  }

  Widget buildCurrentUserSheet() => Container(
        height: 130,
        child: ListView(
          children: [
            //Edit
            ListTile(
              title: Row(
                children: [
                  Text(
                    'Edit Post',
                    style: GoogleFonts.roboto(
                        color: Colors.black87,
                        fontSize: 20,
                        fontWeight: FontWeight.w400),
                  ),
                  Container(
                    margin: EdgeInsets.only(left: 10),
                    child: Icon(
                      Icons.reply_outlined,
                      color: Colors.red[100],
                    ),
                  ),
                ],
              ),
              onTap: () {
                Navigator.of(context).pop();
                Navigator.of(context).push(MaterialPageRoute(
                    builder: (context) => EditPosts(
                        getUserName: getUserName,
                        getFullName: getFullName,
                        getUserID: getUserID,
                        getUserType: getUserType,
                        getDate: getDate,
                        getTime: getTime,
                        getPostTitle: getPostTitle,
                        getPostContent: getPostContent)));
              },
            ),
            //Delete
            ListTile(
              title: Row(
                children: [
                  Text(
                    'Delete Post',
                    style: GoogleFonts.roboto(
                        color: Colors.black87,
                        fontSize: 20,
                        fontWeight: FontWeight.w400),
                  ),
                  Container(
                    margin: EdgeInsets.only(left: 10),
                    child: Icon(
                      Icons.delete_outline,
                      color: Colors.red[100],
                    ),
                  ),
                ],
              ),
              onTap: () {
                setState(() {
                  if (getUserType == 'I am a Donor') {
                    //Post Count Minus

                    int x = int.parse(donorCountPosts!) - 1;
                    countPost = x.toString();
                    postMinusDonor();
                    postMinusAll();

                    //Delete
                    deleteDonorPost();
                    deleteAlltPost();
                    deleteSpecificDonorPost();
                    deleteSpecificAllPost();
                    Navigator.of(context).pop();
                    showDeleteToast();
                  } else {
                    //Post Count Minus

                    int x = int.parse(patientCountPosts!) - 1;
                    countPost = x.toString();
                    postMinusPatient();
                    postMinusAll();

                    //Delete
                    deletePatientPost();
                    deleteAlltPost();
                    deleteSpecificPatientPost();
                    deleteSpecificAllPost();
                    Navigator.of(context).pop();
                    showDeleteToast();
                  }
                });
              },
            ),
          ],
        ),
      );
  Widget buildNotCurrentUserSheet() => Container(
        height: 70,
        child: ListView(
          children: [
            //Delete
            ListTile(
              title: Row(
                children: [
                  Text(
                    'No Actions Available',
                    style: GoogleFonts.roboto(
                        color: Colors.black87,
                        fontSize: 20,
                        fontWeight: FontWeight.w400),
                  ),
                ],
              ),
              onTap: () {},
            ),
          ],
        ),
      );

  //Show Delete Success
  void showDeleteToast() =>
      toast.showToast(child: buildDeleteToast(), gravity: ToastGravity.CENTER);

  Widget buildDeleteToast() => Container(
        padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
        decoration: BoxDecoration(
          color: Colors.green[300],
          borderRadius: BorderRadius.circular(5),
        ),
        child: Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            Text(
              "Deleted",
              style: TextStyle(color: Colors.black87, fontSize: 16),
            ),
          ],
        ),
      );

  //Post Utilities

  //Views Count
  viewsAddDonor() async {
    String? did = getTime;
    await FirebaseFirestore.instance
        .collection("Posts")
        .doc('Donor')
        .collection('All Posts')
        .doc(did)
        .update({
      'Views': getPostViews,
    });
  }

  viewsAddPatient() async {
    String? did = getTime;
    await FirebaseFirestore.instance
        .collection("Posts")
        .doc('Patient')
        .collection('All Posts')
        .doc(did)
        .update({
      'Views': getPostViews,
    });
  }

  viewsAddAdmin() async {
    String? did = getTime;
    await FirebaseFirestore.instance
        .collection("Posts")
        .doc('Admin')
        .collection('All Posts')
        .doc(did)
        .update({
      'Views': getPostViews,
    });
  }

  viewsAddAll() async {
    String? did = getTime;
    await FirebaseFirestore.instance
        .collection("Posts")
        .doc('All')
        .collection('All Posts')
        .doc(did)
        .update({
      'Views': getPostViews,
    });
  }

  viewsAddSpecificDonor() async {
    String? uid = getUserID;
    String? did = getTime;
    await FirebaseFirestore.instance
        .collection("Users")
        .doc('Donor')
        .collection('Lists')
        .doc(uid)
        .collection('Posts')
        .doc(did)
        .update({
      'Views': getPostViews,
    });
  }

  viewsAddSpecificPatient() async {
    String? uid = getUserID;
    String? did = getTime;
    await FirebaseFirestore.instance
        .collection("Users")
        .doc('Patient')
        .collection('Lists')
        .doc(uid)
        .collection('Posts')
        .doc(did)
        .update({
      'Views': getPostViews,
    });
  }

  viewsAddSpecificAll() async {
    String? uid = getUserID;
    String? did = getTime;
    await FirebaseFirestore.instance
        .collection("Users")
        .doc('All')
        .collection('Lists')
        .doc(uid)
        .collection('Posts')
        .doc(did)
        .update({
      'Views': getPostViews,
    });
  }

  //Likes Counter
  likesAddDonor() async {
    String? did = getTime;
    await FirebaseFirestore.instance
        .collection("Posts")
        .doc('Donor')
        .collection('All Posts')
        .doc(did)
        .update({
      'Likes': getPostLikes,
    });
  }

  likesAddPatient() async {
    String? did = getTime;
    await FirebaseFirestore.instance
        .collection("Posts")
        .doc('Patient')
        .collection('All Posts')
        .doc(did)
        .update({
      'Likes': getPostLikes,
    });
  }

  likesAddAdmin() async {
    String? did = getTime;
    await FirebaseFirestore.instance
        .collection("Posts")
        .doc('Admin')
        .collection('All Posts')
        .doc(did)
        .update({
      'Likes': getPostLikes,
    });
  }

  likesAddAll() async {
    String? did = getTime;
    await FirebaseFirestore.instance
        .collection("Posts")
        .doc('All')
        .collection('All Posts')
        .doc(did)
        .update({
      'Likes': getPostLikes,
    });
  }

  likesAddSpecificDonor() async {
    String? uid = getUserID;
    String? did = getTime;
    await FirebaseFirestore.instance
        .collection("Users")
        .doc('Donor')
        .collection('Lists')
        .doc(uid)
        .collection('Posts')
        .doc(did)
        .update({
      'Likes': getPostLikes,
    });
  }

  likesAddSpecificPatient() async {
    String? uid = getUserID;
    String? did = getTime;
    await FirebaseFirestore.instance
        .collection("Users")
        .doc('Patient')
        .collection('Lists')
        .doc(uid)
        .collection('Posts')
        .doc(did)
        .update({
      'Likes': getPostLikes,
    });
  }

  likesAddSpecificAll() async {
    String? uid = getUserID;
    String? did = getTime;
    await FirebaseFirestore.instance
        .collection("Users")
        .doc('All')
        .collection('Lists')
        .doc(uid)
        .collection('Posts')
        .doc(did)
        .update({
      'Likes': getPostLikes,
    });
  }

  //Likes added to User Profile
  likesAddSpecificProfileDonor() async {
    String? uid = getUserID;

    await FirebaseFirestore.instance
        .collection("Users")
        .doc('Donor')
        .collection('Lists')
        .doc(uid)
        .update({
      'Likes': getPostLikes,
    });
  }

  likesAddSpecificProfilePatient() async {
    String? uid = getUserID;

    await FirebaseFirestore.instance
        .collection("Users")
        .doc('Patient')
        .collection('Lists')
        .doc(uid)
        .update({
      'Likes': getPostLikes,
    });
  }

  likesAddSpecificProfileAll() async {
    String? uid = getUserID;

    await FirebaseFirestore.instance
        .collection("Users")
        .doc('All')
        .collection('Lists')
        .doc(uid)
        .update({
      'Likes': getPostLikes,
    });
  }

  //Count Posts
  postMinusDonor() async {
    User? user = FirebaseAuth.instance.currentUser;
    await FirebaseFirestore.instance
        .collection("Users")
        .doc('Donor')
        .collection('Lists')
        .doc(user!.uid)
        .update({
      'Posts': countPost,
    });
  }

  postMinusPatient() async {
    User? user = FirebaseAuth.instance.currentUser;
    await FirebaseFirestore.instance
        .collection("Users")
        .doc('Patient')
        .collection('Lists')
        .doc(user!.uid)
        .update({
      'Posts': countPost,
    });
  }

  postMinusAll() async {
    User? user = FirebaseAuth.instance.currentUser;
    await FirebaseFirestore.instance
        .collection("Users")
        .doc('All')
        .collection('Lists')
        .doc(user!.uid)
        .update({
      'Posts': countPost,
    });
  }

  //Notification Count
  notificationAddDonor() async {
    String? did = getTime;
    await FirebaseFirestore.instance
        .collection("Posts")
        .doc('Donor')
        .collection('All Posts')
        .doc(did)
        .update({
      'Notifications': countNotification,
    });
  }

  notificationAddPatient() async {
    String? did = getTime;
    await FirebaseFirestore.instance
        .collection("Posts")
        .doc('Patient')
        .collection('All Posts')
        .doc(did)
        .update({
      'Notifications': countNotification,
    });
  }

  notificationAddAll() async {
    String? did = getTime;
    await FirebaseFirestore.instance
        .collection("Posts")
        .doc('All')
        .collection('All Posts')
        .doc(did)
        .update({
      'Notifications': countNotification,
    });
  }

  notificationAddSpecificDonor() async {
    String? uid = getUserID;
    await FirebaseFirestore.instance
        .collection("Users")
        .doc('Donor')
        .collection('Lists')
        .doc(uid)
        .update({
      'Notifications': countNotification,
    });
  }

  notificationAddSpecificPatient() async {
    String? uid = getUserID;
    await FirebaseFirestore.instance
        .collection("Users")
        .doc('Patient')
        .collection('Lists')
        .doc(uid)
        .update({
      'Notifications': countNotification,
    });
  }

  notificationAddSpecificAll() async {
    String? uid = getUserID;
    await FirebaseFirestore.instance
        .collection("Users")
        .doc('All')
        .collection('Lists')
        .doc(uid)
        .update({
      'Notifications': countNotification,
    });
  }
}
