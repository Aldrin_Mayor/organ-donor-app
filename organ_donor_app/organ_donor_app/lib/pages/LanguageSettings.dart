import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:organ_donor_app/pages/ProfileSettings.dart';

class LanguageSettings extends StatefulWidget {
  LanguageSettings({Key? key}) : super(key: key);

  @override
  _LanguageSettingsState createState() => _LanguageSettingsState();
}

class _LanguageSettingsState extends State<LanguageSettings> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xffe5eef8),
      body: Stack(
        children: [
          Container(
            margin: EdgeInsets.only(top: 40, left: 20, right: 20),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                InkWell(
                  onTap: () {
                    Navigator.pushAndRemoveUntil(
                        context,
                        MaterialPageRoute(
                            builder: (builder) => ProfileSettings()),
                        (route) => false);
                  },
                  child: Icon(Icons.arrow_back_ios_outlined, size: 25.0),
                ),
                Container(
                  margin: EdgeInsets.only(left: 30),
                  child: Text(
                    "Language Settings",
                    style: GoogleFonts.roboto(
                        color: Colors.black87,
                        fontWeight: FontWeight.bold,
                        fontSize: 18),
                  ),
                ),
                Text(
                  'Complete',
                  style: GoogleFonts.roboto(color: Colors.red, fontSize: 16),
                ),
              ],
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 70),
            child: ListView(
              children: [
                //Filipino
                ListTile(
                  trailing: Icon(
                    Icons.arrow_forward_ios_outlined,
                    color: Colors.grey,
                  ),
                  title: Text(
                    'Filipino',
                    style:
                        GoogleFonts.roboto(color: Colors.black87, fontSize: 15),
                  ),
                  onTap: () {},
                ),

                //English
                ListTile(
                  trailing: Icon(
                    Icons.arrow_forward_ios_outlined,
                    color: Colors.grey,
                  ),
                  title: Text(
                    'English',
                    style:
                        GoogleFonts.roboto(color: Colors.black87, fontSize: 15),
                  ),
                  onTap: () {},
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
