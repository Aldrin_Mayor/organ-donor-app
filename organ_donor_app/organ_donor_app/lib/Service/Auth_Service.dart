import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:organ_donor_app/pages/CompleteRegistration.dart';
import 'package:organ_donor_app/pages/Dashboard.dart';
import 'package:organ_donor_app/pages/Verification.dart';
import 'package:organ_donor_app/pages/VerificationLogin.dart';

class AuthClass {
  final FirebaseAuth _auth = FirebaseAuth.instance;
  final storage = new FlutterSecureStorage();

  Future<void> verifyPhoneNumber(
      String phoneNumber, BuildContext context, Function setData) async {
    PhoneVerificationCompleted verificationCompleted =
        (PhoneAuthCredential phoneAuthCredential) async {
      showSnackBar(context, "Verification Completed");
    };
    PhoneVerificationFailed verificationFailed =
        (FirebaseAuthException exception) {
      showSnackBar(context, exception.toString());
    };
    PhoneCodeSent codeSent =
        (String verificationID, [int? forceResendingToken]) {
      showSnackBar(context, "Verification Code sent on the Mobile Number");
      setData(verificationID);
    };
    PhoneCodeAutoRetrievalTimeout codeAutoRetrievalTimeout =
        (String verificationID) {
      showSnackBar(context, "Timeout");
    };
    try {
      await _auth.verifyPhoneNumber(
          phoneNumber: phoneNumber,
          verificationCompleted: verificationCompleted,
          verificationFailed: verificationFailed,
          codeSent: codeSent,
          codeAutoRetrievalTimeout: codeAutoRetrievalTimeout);
    } catch (e) {
      showSnackBar(context, e.toString());
    }
  }

  Future<void> signInwithPhoneNumber(String verificationId, String smsCode,
      BuildContext context, String mobileNumber) async {
    //Check if Number Already Registered
    try {
      AuthCredential credential = PhoneAuthProvider.credential(
          verificationId: verificationId, smsCode: smsCode);

      UserCredential userCredential =
          await _auth.signInWithCredential(credential);
      storeTokenAndData(userCredential);
      Navigator.pushAndRemoveUntil(
          context,
          MaterialPageRoute(
              builder: (builder) => CompleteRegistration(
                    mobileNumber: mobileNumber,
                  )),
          (route) => false);

      showSnackBar(
          context, "Please Complete these Step to Verify Your Identity");
    } catch (e) {
      showSnackBar(context, e.toString());
    }
  }

  Future<void> checkRegisterPhoneNumber(
      String mobileNumber, BuildContext context) async {
    // For Registered Donor
    final CollectionReference checkIfDonorVerified = FirebaseFirestore.instance
        .collection("Users")
        .doc("Donor")
        .collection("Lists");

    QuerySnapshot _queryDonor = await checkIfDonorVerified
        .where('Mobile Number', isEqualTo: mobileNumber)
        .get();

    // For Archived Donor
    final CollectionReference checkIfArchivedDonorVerified = FirebaseFirestore
        .instance
        .collection("Archived")
        .doc("Users")
        .collection("Registered")
        .doc("Donor")
        .collection("Lists");

    QuerySnapshot _queryArchivedDonor = await checkIfArchivedDonorVerified
        .where('Mobile Number', isEqualTo: mobileNumber)
        .get();

    // For Registered Patient
    final CollectionReference checkIfPatientVerified = FirebaseFirestore
        .instance
        .collection("Users")
        .doc("Patient")
        .collection("Lists");

    QuerySnapshot _queryPatient = await checkIfPatientVerified
        .where('Mobile Number', isEqualTo: mobileNumber)
        .get();

    // For Archived Patient
    final CollectionReference checkIfArchivedPatientVerified = FirebaseFirestore
        .instance
        .collection("Archived")
        .doc("Users")
        .collection("Registered")
        .doc("Donor")
        .collection("Lists");

    QuerySnapshot _queryArchivedPatient = await checkIfArchivedPatientVerified
        .where('Mobile Number', isEqualTo: mobileNumber)
        .get();

    if ((_queryDonor.docs.length > 0) ||
        (_queryPatient.docs.length > 0) ||
        (_queryArchivedDonor.docs.length > 0) ||
        (_queryArchivedPatient.docs.length > 0)) {
      showSnackBar(context, "This Number is already used. Please Try Again!");
    } else {
      Navigator.of(context).push(MaterialPageRoute(
          builder: (context) => Verification(mobileNumber: mobileNumber)));
    }
  }

  Future<void> logInwithPhoneNumber(
      String verificationId, String smsCode, BuildContext context) async {
    try {
      AuthCredential credential = PhoneAuthProvider.credential(
          verificationId: verificationId, smsCode: smsCode);

      UserCredential userCredential =
          await _auth.signInWithCredential(credential);
      storeTokenAndData(userCredential);
      Navigator.pushAndRemoveUntil(
          context,
          MaterialPageRoute(builder: (builder) => Dashboard()),
          (route) => false);

      showSnackBar(context, "Logged In Successfull!");
    } catch (e) {
      showSnackBar(context, e.toString());
    }
  }

  Future<void> checkLoginPhoneNumber(
      String mobileNumber, BuildContext context) async {
    // For Registered onor
    final CollectionReference checkIfDonorVerified = FirebaseFirestore.instance
        .collection("Users")
        .doc("Donor")
        .collection("Lists");

    QuerySnapshot _queryDonor = await checkIfDonorVerified
        .where('Mobile Number', isEqualTo: mobileNumber)
        .get();

    // For Archived Donor
    final CollectionReference checkIfArchivedDonorVerified = FirebaseFirestore
        .instance
        .collection("Archived")
        .doc("Users")
        .collection("Registered")
        .doc("Donor")
        .collection("Lists");

    QuerySnapshot _queryArchivedDonor = await checkIfArchivedDonorVerified
        .where('Mobile Number', isEqualTo: mobileNumber)
        .get();

    //For Registered Patient
    final CollectionReference checkIfPatientVerified = FirebaseFirestore
        .instance
        .collection("Users")
        .doc("Patient")
        .collection("Lists");

    QuerySnapshot _queryPatient = await checkIfPatientVerified
        .where('Mobile Number', isEqualTo: mobileNumber)
        .get();

    //For Archived Patient
    final CollectionReference checkIfArchivedPatientVerified = FirebaseFirestore
        .instance
        .collection("Archived")
        .doc("Users")
        .collection("Registered")
        .doc("Patient")
        .collection("Lists");

    QuerySnapshot _queryArchivedPatient = await checkIfArchivedPatientVerified
        .where('Mobile Number', isEqualTo: mobileNumber)
        .get();

    if ((_queryDonor.docs.length > 0) ||
        (_queryPatient.docs.length > 0) ||
        (_queryArchivedDonor.docs.length > 0) ||
        (_queryArchivedPatient.docs.length > 0)) {
      Navigator.of(context).push(MaterialPageRoute(
          builder: (context) => VerificationLogin(mobileNumber: mobileNumber)));
    } else {
      showSnackBar(
          context, "Sorry, We Cannot Find this Number. Please Try Again!");
    }
  }

  void showSnackBar(BuildContext context, String text) {
    final snackBar = SnackBar(content: Text(text));
    ScaffoldMessenger.of(context).showSnackBar(snackBar);
  }

  //Make User From Logging In
  Future<void> storeTokenAndData(UserCredential userCredential) async {
    await storage.write(
        key: "token", value: userCredential.credential!.token.toString());
    await storage.write(
        key: "userCredential", value: userCredential.toString());
  }

  Future<String?> getToken() async {
    return await storage.read(key: "token");
  }

  Future<void> logout() async {
    try {
      await _auth.signOut();
      await storage.delete(key: "token");
    } catch (e) {}
  }
}
